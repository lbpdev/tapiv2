<div class="col-md-12">
    <div class="row">
        <div class="margin-t-80 col-md-12 text-center" id="footer">
            <div class="row"><a class="btn btn-link pull-left padding-l-0" href="#" data-toggle="modal" data-target="#termsModal">Terms of Use</a></div>
            <div class="row">
                <a class="navbar-brand" href="#">
                    <img src="{{ asset('public/images/LB-Logo.png') }}" id="lb-logo" height="50">
                </a>
                <a class="navbar-brand" href="#">
                    <img src="{{ asset('public/images/Tapi-Logo.png') }}" id="lb-logo" height="50">
                </a>
            </div>
            </div>
            <div class="row text-center">
                Copyright © Leading Brands 2017
            </div>
        </div>
    </div>
</div>


</div>

@include('pages.customers.terms')

</div>