<div class="row">
    {!! Form::open(['route'=>'settings.info.store','files'=>true,'accept'=>'image/*','onsubmit'=>'return validatePassword()']) !!}

    @if(Session::has('message'))
        <div class="col-md-12">
            <div class="alert alert-success">
                {{ Session::get('message') }}
            </div>
        </div>
    @endif

    <div class="col-md-6">
        Company Name
        <input type="text" class="form-control" name="name" placeholder="Please enter the name of your company"  required="required" value="{{ isset($options['name']) ? $options['name'] : '' }}">

        <div class="row margin-t-10">
            <div class="col-md-6">
                Location
                <div class="margin-b-7">
                    <select class="form-control selectpicker" id="country" name="country"><option value="" selected="selected">Choose Country</option></select>
                </div>
            </div>
            <div class="col-md-6">
                &nbsp;
                <div class="margin-b-7">
                    <select class="form-control selectpicker" id="city" name="city"><option value="" selected="selected">Choose City</option></select>
                </div>
            </div>
        </div>
        <div class="row margin-t-10">
            <div class="col-md-6">
                Time Zone
                <?php

                foreach ($timezones as $key => $value) {
                    $tz[$value] = $key;
                }
                ?>
                <div class="margin-b-7">
                    {!! Form::select('timezone',$tz, isset($options['timezone']) ? $options['timezone'] : 0,['id'=>'timezone','class'=>'form-control selectpicker']) !!}
                </div>
            </div>
            <div class="col-md-6">
                Number of Employees

                <?php
                $employeeSelection = [
                        '1-10' => '1-10 Employees',
                        '11-30' => '11-30 Employees',
                        '31-50' => '31-50 Employees',
                        '51-100' => '51-100 Employees',
                        '100+' => 'More than 100'
                ]
                ?>

                <div class="margin-b-7">
                    {!! Form::select('employees',$employeeSelection, isset($options['employees']) ? $options['employees'] : 0, ['id'=>'employees','class'=>'form-control selectpicker']) !!}
                </div>
            </div>
        </div>
        <div class="row margin-t-10">
            <div class="col-md-6">
                Working Days

                <?php
                $daysOfWeek = [
                        'Mon' => 'Mon',
                        'Tue' => 'Tue',
                        'Wed' => 'Wed',
                        'Thu' => 'Thu',
                        'Fri' => 'Fri',
                        'Sat' => 'Sat',
                        'Sun' => 'Sun',
                ]
                ?>

                {{--<div class="row">--}}
                {{--<div class="col-md-6">--}}
                {{--<div class="small margin-b-7">--}}
                {{--{!! Form::select('workdays_from',$daysOfWeek, isset($options['workdays_from']) ? $options['workdays_from'] : 7,['id'=>'workdays_from','class'=>' selectpicker']) !!}--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-6">--}}
                {{--<div class="small margin-b-7">--}}
                {{--{!! Form::select('workdays_to',$daysOfWeek, isset($options['workdays_to']) ? $options['workdays_to'] : 4,['id'=>'workdays_to','class'=>' selectpicker']) !!}--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div> --}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="margin-b-7">
                            {!! Form::select('workdays[]',$daysOfWeek, isset($options['workdays']) ? $options['workdays'] : 7,['id'=>'workdays','class'=>' selectpicker','multiple']) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                Working Hours

                <?php
                for($x=1;$x<25;$x++)
                    $workHours[$x.':00'] = $x.':00';

                ?>

                <div class="row">
                    <div class="col-md-6">
                        <div class="small margin-b-7">
                            {!! Form::select('workhours',$workHours,'8',['id'=>'workhours_from','class'=>'form-control selectpicker']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 relative" style="width: 330px;">
        Company Logo  <span class="pull-right font-12 margin-t-5">200px by 300px</span> <br>
        <input type="file" name="logo" id="logo-input" onchange="onFileSelected(event)">
        <img src="{{ asset(isset($options['logo']) ? 'public/uploads/logo/'.$options['logo'] : 'public/images/logo-placeholder.png') }}" id="company-logo" width="300">
        <br>
        <input type="submit" class="link-bt link-color pull-right" value="Update">

    </div>

    {!! Form::close() !!}
</div>

@section('info-js')

    <script>

        @if(isset($options['country']))
          $('#country').val("{{ $options['country'] }}");
        $('#country').trigger("change");
        @endif

        @if(isset($options['city']))
          setTimeout(function(){
            $('#city').val("{{ $options['city'] }}");
        },300);
        @endif


        // Variable to store your files
        var files;

        // Add events
        $('input[type=file]').on('change', prepareUpload);

        // Grab the files and set them to our variable
        function prepareUpload(event)
        {
            files = event.target.files;
        }

        // Catch the form submit and upload the files
        function uploadFiles(event)
        {


            event.stopPropagation(); // Stop stuff happening
            event.preventDefault(); // Totally stop stuff happening

            var fileUpload = new FormData();
            $.each(files, function(key, value)
            {
                data.append(key, value);
            });

            $.ajax({
                url: 'submit.php?files',
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                success: function(data, textStatus, jqXHR)
                {
                    if(typeof data.error === 'undefined')
                    {
                        // Success so call function to process the form
                        submitForm(event, data);
                    }
                    else
                    {
                        // Handle errors here
                    }
                },
                error: function(jqXHR, textStatus, errorThrown)
                {
                    // Handle errors here
                    // STOP LOADING SPINNER
                }
            });
        }


        function onFileSelected(event) {
            var selectedFile = event.target.files[0];
            var reader = new FileReader();

            var imgtag = document.getElementById("company-logo");
            imgtag.title = selectedFile.name;

            reader.onload = function(event) {
                imgtag.src = event.target.result;
            };

            reader.readAsDataURL(selectedFile);
        }
    </script>
@endsection