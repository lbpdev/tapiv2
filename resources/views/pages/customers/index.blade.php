<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Online team scheduling and project management and planning software - Register</title>
    <meta name="description" content="TapiApp is a customizable online team scheduling, project management and planning software.">
    <meta name="keywords" content="TapiApp,scheduling,production,project,management,spreadsheets,software,Excel,app">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">

    <!-- Dublin Core -->
    <meta name="DC.Title" content="Tapi App - Official Website">
    <meta name="DC.Creator" content="Leading Brands JLT">
    <meta name="DC.Description" content="TapiApp is a customizable online team scheduling, project management and planning software.">
    <meta name="DC.Language" content="English">

    <!-- Open Graph data -->
    <meta property="og:title" content="Tapi App - Official Website" />
    <meta property="og:type" content="web page" />
    <meta property="og:url" content="http://www.tapiapp.com/" />
    <meta property="og:description" content="TapiApp is a customizable online team scheduling, project management and planning software." />
    <meta property="og:site_name" content="http://www.tapiapp.com/" />

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('public/fonts/stylesheet.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/bootstrap-overrides.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/global.css') }}" rel="stylesheet">

    <link href="{{ asset('public/css/login.css') }}" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('public/plugins/bootstrap-select-1.10.0/css/bootstrap-select.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/settings.css') }}">
    <style>
        .dropdown-menu.inner {
            max-height: 250px !important;
        }

        .bootstrap-select > .dropdown-toggle {
            background-color: #ffffff !important;
        }

        .panel {
            font-weight: normal;
        }

        .dropdown-menu a {
            width: 100%;
            padding: 2px 5px;
        }

        .dropdown-menu a .text {
            font-size: 12px;
            color: #000 !important;
        }
        .bootstrap-select.btn-group .dropdown-toggle .filter-option {
            height: 24px !important;
        }
        input, select, textarea, .bootstrap-select, .form-control {
            margin-top: 3px;
        }
        #registerBt {
            display: inline-block;
            color: #fff;
            float: none;
        }

        .glyphicon-ok:before {
            color: #000;
        }

        .error {
            color: #d84040;
            font-weight: 300;
        }

        form {
            line-height: 18px;
        }

        input, select, textarea, .bootstrap-select, .form-control {
            margin-bottom: 6px;
        }
        
        #termsField {
            color: #fff;
            font-family: helvetica;
            font-size: 13px;
            padding-top: 0;
            display: inline-block;
            font-weight: 300;
            cursor: pointer;
            float: left;
            margin: 2px 0 0 7px;
        }

        form {
            color: #fff;
            font-weight: 300;
        }
        #registerBt {
            border: 1px solid #fff;
            padding: 3px 20px;
            height: auto;
            color: #fff !important;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-radius: 0;
            background-color: transparent;
            font-size: 15px;
            font-weight: 300;
        }
    </style>
</head>
<body id="app-layout">
<div class="container">
    <div class="row">
        <div class="col-md-12 text-left">
            <a class="navbar-brand" href="{{ url('/') }}">
            </a>
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="" style="width: 500px;display: block;margin: 0 auto;">
            <div class="panel panel-default" id="register-panel">
                <div class="panel-heading text-center padding-b-0">
                    <img src="{{ asset('public/images/Tapi-Logo.png') }}" height="120">
                </div>
                <div class="panel-body padding-t-0">
                    {{--@if (count($errors) > 0)--}}
                    {{--<div class="alert alert-danger">--}}
                    {{--<strong>Whoops!</strong> There were some problems with your input.<br><br>--}}
                    {{--<ul>--}}
                    {{--@foreach ($errors->all() as $error)--}}
                    {{--<li>{{ $error }}</li>--}}
                    {{--@endforeach--}}
                    {{--</ul>--}}
                    {{--</div>--}}
                    {{--@endif--}}

                    <div class="row">
                        {!! Form::open(['route'=>'company.register']) !!}

                        @if(Session::has('message'))
                            <div class="col-md-12">
                                <div class="alert alert-success">
                                    {{ Session::get('message') }}
                                </div>
                            </div>
                        @endif

                        <div class="col-md-12">
                            Company Name
                            <div class="name error"></div>
                            <input type="text" class="form-control" name="option[name]" placeholder="Please enter the name of your company"  required="required" value="{{ isset($options['name']) ? $options['name'] : '' }}">

                            <div class="row">
                                <div class="col-md-6">
                                    Location
                                    <div class="">
                                        <select class="form-control selectpicker" id="country" name="option[country]"><option value="" selected="selected">Choose Country</option></select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    &nbsp;
                                    <div class="">
                                        <select class="form-control selectpicker" id="city" name="option[city]"><option value="" selected="selected">Choose City</option></select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    Time Zone
                                    <?php

                                    foreach ($timezones as $key => $value) {
                                        $tz[$value] = $key;
                                    }
                                    ?>
                                    <div class="">
                                        {!! Form::select('option[timezone]',$tz, isset($options['timezone']) ? $options['timezone'] : 'Europe/London',['id'=>'timezone','class'=>'form-control selectpicker']) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    Number of Employees

                                    <?php
                                    $employeeSelection = [
                                            '0' => 'Free Trial (1-5 Employees)',
                                            '2' => '6-15 Employees ($10/month)',
                                            '3' => '16-30 Employees ($27/month)',
                                            '4' => '31-50 Employees ($48/month)'
                                    ]
                                    ?>

                                    <div class="">
                                        {!! Form::select('subscription',$employeeSelection, isset($_GET['plan']) ? $_GET['plan']  : 0, ['id'=>'employees','class'=>'form-control selectpicker']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    Working Days

                                    <?php
                                    $daysOfWeek = [
                                            'Mon' => 'Mon',
                                            'Tue' => 'Tue',
                                            'Wed' => 'Wed',
                                            'Thu' => 'Thu',
                                            'Fri' => 'Fri',
                                            'Sat' => 'Sat',
                                            'Sun' => 'Sun'
                                    ]
                                    ?>

                                    {{--<div class="row">--}}
                                    {{--<div class="col-md-6">--}}
                                    {{--<div class="small margin-b-7">--}}
                                    {{--{!! Form::select('workdays_from',$daysOfWeek, isset($options['workdays_from']) ? $options['workdays_from'] : 7,['id'=>'workdays_from','class'=>' selectpicker']) !!}--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-6">--}}
                                    {{--<div class="small margin-b-7">--}}
                                    {{--{!! Form::select('workdays_to',$daysOfWeek, isset($options['workdays_to']) ? $options['workdays_to'] : 4,['id'=>'workdays_to','class'=>' selectpicker']) !!}--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                    {{--</div> --}}
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="">
                                                {!! Form::select('workdays[]',$daysOfWeek, ['Mon','Tue','Wed','Thu','Fri'] ,['id'=>'workdays','class'=>' selectpicker','multiple']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    Working Hours


                                    <?php
                                    for($x=1;$x<25;$x++)
                                        $workHours[$x] = $x;
                                    ?>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="small">
                                                {!! Form::select('workhours',$workHours,'8',['id'=>'workhours_from','class'=>'form-control selectpicker']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 relative">
                            <div class="row">
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="col-md-12">
                                            User Information
                                            <input type="text" class="form-control" name="user[name]" placeholder="Full Name" value="" required="required">
                                            {{--<input type="text" class="form-control" name="user[username]" placeholder="Username" value="" required="required">--}}
                                            <div class="email error"></div>
                                            <input type="email" class="form-control" name="user[email]" placeholder="Email Address (This will be your username)" value="" required="required">
                                            <div class="password error"></div>
                                            <input type="password" class="form-control" name="user[password]" placeholder="Password" value="" required="required">
                                            <input type="password" class="form-control" name="user[password_confirm]" placeholder="Confirm Password" value="" required="required">
                                             <input class="pull-left" type="checkbox" id="termsRadio"> <span href="#" id="termsField">I have read and accept the terms of use. </span>
                                            <a id="termsError" style="display:none;color: #d84040;padding-top:5px;">Please make sure you have read the terms of use and ticked the checkbox above.</a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 text-center">
                            <br>
                            {!! Form::submit('Register',['id'=>'registerBt','class'=>'link-bt']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>

                    <div class="form-group">
                        <div class="col-md-12 margin-t-30">
                            <div class="row text-center">
                                <a class="btn btn-link padding-l-0 padding-r-10" href="#" data-toggle="modal" data-target="#termsModal">Terms of Use</a><br>
                                <img src="{{ asset('public/images/LB-Logo.png') }}" id="lb-logo" height="40">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('pages.customers.terms')

        <!-- JavaScripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>



<script src="{{ asset('public/js/country_city.js') }}"></script>
<script src="{{ asset('public/plugins/bootstrap-select-1.10.0/js/bootstrap-select.min.js') }}"></script>
<script>

//    function validatePassword(){
//
//        password = $($('form').find('input[type=password]')[0]).val();
//        cpassword = $($('form').find('input[type=password]')[1]).val();
//
//        if(password==cpassword)
//            return true;
//
//        $('.password-error').html('Passwords do not match.');
//        return false;
//    };


    $('form').on('submit',function(e){

        $('.email.error').html('');
        $('.name.error').html('');
        $('.password.error').html('');

        if(!$('#termsRadio').is(":checked")){
            $('#termsError').show();

            e.preventDefault();
            e.stopPropagation();
        } else {
            $('#termsField').css({ 'background-color' : 'transparent'});
            $('#termsError').hide();
        }


        if(!$('form').hasClass('valid')){
            e.preventDefault();
            e.stopPropagation();
        }

        password = $($('form').find('input[type=password]')[0]).val();
        cpassword = $($('form').find('input[type=password]')[1]).val();
        email = $('form').find('input[type=email]').val();
        name = $('form').find('input[name="option[name]"]').val();

        if(password==cpassword){
            $('.password.error').html('');

            if(!$('form').hasClass('valid')){
                $.ajax({
                    type: "GET",
                    url: '{{ route('api.users.validateRegisterCompany') }}?email='+email+'&name='+name+'&password='+password,
                    success: function(response){

                        if(response.status==500) {

                            if(response.data.code=="email-exists")
                                $('.email.error').html(response.data.message);

                            if(response.data.code=="name-exists")
                                $('.name.error').html(response.data.message);

                            if(response.data.code=="password-too-short")
                                $('.password.error').html(response.data.message);

                            return false;

                        } else {
                            $('form').addClass('valid');
                            $('form').trigger('submit');
                        }
                    },
                    done : function (){
                        processFlag = 1;
                    }
                });
            }
        } else {
            $('.password.error').html('Passwords do not match.');
        }

        return true;
    });

    {{--function validate(){--}}

        {{--password = $($('form').find('input[type=password]')[0]).val();--}}
        {{--cpassword = $($('form').find('input[type=password]')[1]).val();--}}
        {{--email = $('form').find('input[type=email]').val();--}}

        {{--if(password==cpassword){--}}
            {{--$.ajax({--}}
                {{--type: "POST",--}}
                {{--url: '{{ route('api.users.validateEmail') }}/'+email,--}}
                {{--data: data,--}}
                {{--success: function(response){--}}
                    {{--console.log(response);--}}

                    {{--if(response.status==200) {--}}
                        {{--Alert('email already registered');--}}
                        {{--return false;--}}
                    {{--} else {--}}
                        {{--return true;--}}
                    {{--}--}}
                {{--},--}}
                {{--done : function (){--}}
                    {{--processFlag = 1;--}}
                {{--}--}}
            {{--});--}}
        {{--}--}}

        {{--$('.password-error').html('Passwords do not match.');--}}
        {{--return false;--}}
    {{--};--}}

    @if(isset($options['country']))
        $('#country').val("{{ $options['country'] }}");
    $('#country').trigger("change");
    @endif

    @if(isset($options['city']))
      setTimeout(function(){
        $('#city').val("{{ $options['city'] }}");
    },300);
    @endif

    $('#termsBt').on('click', function(){
        $('#termsModal').modal('show');
    });

    $('#termsField').on('click', function(e){
        $('#termsRadio').trigger('click');
    });

</script>

{{-- <script src="{{ elixir('js/app.js') }}"></script> --}}

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-1910012-65', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>
