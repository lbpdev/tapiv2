@extends('layouts.app')

@section('page-title','Super Admin: Week View')

@section('style')
    <link rel="stylesheet" href="{{ asset('public/css/calendar.css') }}">
    <link rel="stylesheet" href="{{ asset('public/plugins/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('public/plugins/bootstrap-select-1.10.0/css/bootstrap-select.css') }}">
    <link href='{{ asset('public/plugins/jquery-ui/jquery-ui.min.css') }}' rel='stylesheet' />
    <link href='{{ asset('public/plugins/inline-datepicker/daterangepicker.css') }}' rel='stylesheet' />
    <link href='{{ asset('public/css/calendar.user.week.css') }}' rel='stylesheet' />
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 calendar-header padding-t-10">
                <div class="row">
                    <div id="week-sidebar-left" class="week-sidebar col-md-2 padding-r-0">
                        @include('pages.admin.calendar.week.sidebar-left')
                    </div>
                    <div class="col-md-10 relative top-header" style="overflow: auto;">
                        @include('pages.admin.calendar.week.calendar')
                    </div>
                </div>
            </div>
        </div>
        <a href="{{ url('logout') }}" id="bottom-nav">
            Logout
        </a>
    </div>

    {{--@include('pages.user.calendar.week.modals.create-task')--}}
    @if($currentUser->id == Auth::user()->id)
        @include('pages.user.calendar.week.modals.comment')
    @else
        @include('pages.admin.calendar.week.modals.comment')
    @endif

    @include('pages.admin.calendar.week.modals.view-task')
@endsection

@section('js')
    <script src="{{ asset('public/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('public/js/bootstrap-datetime-picker.js') }}"></script>
    <script src="{{ asset('public/js/progressbar.min.js') }}"></script>
    <script src="{{ asset('public/plugins/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script src="{{ asset('public/plugins/bootstrap-select-1.10.0/js/bootstrap-select.min.js') }}"></script>
    <script src='{{ asset('public/plugins/inline-datepicker/jquery.daterangepicker.js') }}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.bundle.min.js" type="text/javascript"></script>
    <script>

        var workHours = parseInt('{{ $workHours }}');
        // Assuming we have an empty <div id="container"></div> in
        // HTML
        // progressbar.js@1.0.0 version is used
        // Docs: http://progressbarjs.readthedocs.org/en/1.0.0/

        var workDays = JSON.parse("{!! $workDaysName !!}");
        var droppedItem,droppedUI;

        $(".day-tasks").mCustomScrollbar({
            autoHideScrollbar: true
        });

        $("#left-sidebar-scroll").mCustomScrollbar({
            autoHideScrollbar: true
        });

        $("#right-sidebar-scroll").mCustomScrollbar({
            autoHideScrollbar: true
        });



        $('.collapse-bt').on('click', function(e) {

        });

        $('#currentDay').datepicker({
            dateFormat : 'dd-mm-yy',
            beforeShowDay: function(date) {
                if(workDays){
                    var day = date.getDay();

                    return [($.inArray( day, workDays ) >= 0 ? true : false), ''];
                }
            }
        }).on('change',function(){
            url = "{!! route('user.calendar.week'). ( isset($_GET['user']) ? "?user=".$_GET['user'] : "" ) . ( isset($_GET['department']) ? "&department=".$_GET['department'] : "" ) !!}";
            window.location = url + '&day='+$(this).val();
        });

        setTimeout(function(){
            $( "#department_id").trigger('change');
        },400);


//        function attachUTaskCollapseEvent() {
//            $('.calendar .collapse-icon').unbind( "click" );
//
//            $('.calendar .collapse-icon').on('click', function(){
//                parent = $(this).closest('.ui-state-default');
//
//                if(parent.hasClass('closed'))
//                    $(this).closest('.ui-state-default').removeClass('closed');
//                else
//                    $(this).closest('.ui-state-default').addClass('closed');
//
//                if($(this).hasClass('collapsed'))
//                    $(this).removeClass('collapsed');
//                else
//                    $(this).addClass('collapsed');
//            });
//        }
//        attachUTaskCollapseEvent();

        {{--$('#createTaskBt').on('click',function (e) {--}}
            {{--e.preventDefault();--}}
            {{--$('#addTaskForm').attr('action','{{ route('api.user.tasks.store') }}');--}}
            {{--$('#addTaskForm')[0].reset();--}}
            {{--$('#date').val(moment().format('MM/DD/YYYY'));--}}

            {{--$('#department_id').trigger('change');--}}

            {{--$('#create-task-modal').modal('show');--}}
            {{--$('#createTask').val('Create');--}}
            {{--$('#key_id').selectpicker('refresh');--}}
            {{--$('#createTask').hide();--}}
        {{--});--}}

    </script>


    <script>
        setInterval(function(){
            location.reload();
        },600000);
    </script>

    @yield('calendar-js')
    @yield('create-comment-js')
    @yield('create-task-js')


    @if(isset($_GET['user']))
        @if(Auth::user()->id == $_GET['user'])
            <script>
                attachCompleteIconListener();
            </script>
        @endif
    @endif

@endsection

