<!-- Modal -->
<div id="view-comment-modal" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 300px;">

        <!-- Modal content-->
        <div class="modal-content">
            {!! Form::open(['id'=>'addCommentForm','route'=>'api.comments.store']) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Comment</h4>
            </div>
            <div class="modal-body clearfix padding-b-0">
                <div class="col-md-12">
                    <div class="row">
                        <input type="hidden" name="date" id="commentDate">
                        <textarea class="" name="comment" id="commentBox" rows="5"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer clearfix padding-t-0">
                <div class="col-md-12">
                    <div class="row">
                        {!! Form::submit('Add',['class'=>'btn btn-default padding-r-0']) !!}
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>


@section('create-comment-js')
    <script>

        /** SEND CREATE REQUEST **/

        $('#addCommentForm').on('submit',function(e){
            var form = $(this);
            processFlag = 1;

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $('#addCommentForm input[type=submit]').attr('disabled','disabled');

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){

                        if(response.status==200) {
                            console.log(response.data);

                            button = $('.view-comment-bt[data-date='+$('#commentDate').val()+']');

                            if(response.data.comment)
                                $(button).addClass('hasComments');
                            else
                                $(button).removeClass('hasComments');

                            $('#view-comment-modal').modal('hide');
                        }

                        doAlert(response);
                    },
                    complete : function (){
                        processFlag = 1;

                        $('#addCommentForm input[type=submit]').removeAttr('disabled');
                    }
                });
            }
        });

    </script>
@endsection
