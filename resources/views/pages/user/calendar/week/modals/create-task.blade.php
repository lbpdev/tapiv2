
<!-- Modal -->
<div id="create-task-modal" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 500px;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Create Extra Task</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['id'=>'addTaskForm','route'=>'api.user.tasks.store']) !!}
                {!! Form::hidden('project_id',null,['id'=>'project_id']) !!}
                {!! Form::hidden('task_id',null,['id'=>'create_task_id']) !!}
                {!! Form::hidden('is_extra',1) !!}
                <div class="col-md-12">

                    <div class="col-md-6">
                        <div class="row">
                            <?php
                            $departmentsList['general'] = 'General';
                            ?>
                            Department:
                            {!! Form::select('department_id',$departmentsList,$currentDepartment ? $currentDepartment->id : null,['id'=>'department_id','class'=>'medium selectpicker','required']) !!}
                        </div>
                    </div>

                    <div class="col-md-6 padding-r-0">
                        Key:
                        {!! Form::select('key_id',[],null,['id'=>'key_id','class'=>'medium selectpicker','required']) !!}
                    </div>

                    Project:
                    {!! Form::input('text','project[selection]',null,['id'=>'projectSelection','class'=>'form-control','required']) !!}

                    <div class="col-md-6">
                        <div class="row">
                            Client
                            {!! Form::input('text','project[client]',null,['readonly','id'=>'projectClient','class'=>'form-control','required']) !!}
                        </div>
                    </div>

                    <div class="col-md-6 padding-r-0">
                        Brand Name
                        {!! Form::input('text','project[brand]',null,['readonly','id'=>'projectBrand','class'=>'form-control','required']) !!}
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            Deadline
                            {!! Form::input('text','project[deadline]',null,['readonly','id'=>'projectDeadline','class'=>'form-control','required']) !!}
                        </div>
                    </div>

                    <div class='col-md-6 padding-r-0'>
                        Task Date
                        <?php
                        $workDays = [];
                        foreach ($currentWeek as $day){
                            $d = \Carbon\Carbon::parse($day['day'])->format('m/d/Y');
                            $workDays[$d] = $d;
                        }
                        ?>

                        {!! Form::select('date',$workDays,\Carbon\Carbon::now()->setTimezone(Auth::user()->companyTimezone)->format('m/d/Y'),['id'=>'date','class'=>'medium selectpicker','required']) !!}
                    </div>


                    <div class="col-md-12 alert alert-danger margin-t-5" id="date-error">
                        Date input exceeds project deadline.
                    </div>

                    <div class="col-md-12">
                        <div class="row">
                            Note ( optional )
                            {!! Form::input('text','note',null,['class'=>'form-control','maxlength'=>'10','id'=>'taskNotes']) !!}
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="row">
                            Hours:
                            {!! Form::input('number','hours',1,['id'=>'taskHours','class'=>'form-control','required','min'=>"0", 'max'=>"40"]) !!}
                        </div>
                    </div>
                    <div class="col-md-6 padding-r-0">
                        Minutes:
                        {!! Form::input('number','minutes',00,['id'=>'taskMins','class'=>'form-control','required','min'=>"0", 'max'=>"59"]) !!}
                    </div>

                    <div class="col-md-6 padding-r-0 padding-t-10">
                        <div class="row">
                            Urgent :
                            {!! Form::input('checkbox','is_urgent',null,['id'=>'is_urgent']) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Create',['class'=>'btn btn-default','id'=>"createTask"]) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>
@section('create-task-js')
    <script>

        /** SEND CREATE REQUEST **/

        $('#addTaskForm').on('submit',function(e){
            var form = $(this);
            processFlag = 1;

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){

                        if(response.status==200) {
                            console.log(response.data);
                            ul = $('#'+response.data.column);

                            ul.prepend(response.data.item);

                            attachCompleteIconListener();
                            attachUTaskCollapseEvent();

                            updateColumnCompleteProgress(ul);
                            updateColumnBookedProgress(ul);

//                            addDeleteUserTaskEvent();
//                            addUserTaskEditEvent();

                            $('#create-task-modal').modal('hide');
                        }

                        doAlert(response);
                    },
                    done : function (){
                        processFlag = 1;
                    }
                });
            }
        });

        var availableProjects = JSON.parse('{!! $projectList !!}');

        $( "#projectSelection" ).autocomplete({
            source: availableProjects,
            select: function(event,ui){
                $("#project_id").val(ui.item.value);
                $("#projectSelection").val(ui.item.label);
                $('#projectClient').val(ui.item.client);
                $('#projectBrand').val(ui.item.brand);
                $('#projectDeadline').val(ui.item.deadline);
                checkDates();
                return false;
            }
        });

        $( "#department_id").on('change', function(){
            $.ajax({
                type: "GET",
                url: '{{ route('api.departments.getkeys') }}?id='+$(this).val(),
                success: function(response){
                    $('#key_id').html('');
                    $.each(response, function(index,value){
                        $('#key_id').append('<option value="'+value.id+'">'+value.name+'</option>');

                    });
                },
                complete : function(){
                }
            });
        });

        $('#date').datepicker({
            dateFormat : 'mm/dd/yy',
            onSelect : function(){
                checkDates()
            }
        });

        $('#projectDeadline').on('change',function(){ checkDates(); });


        //date

        function checkDates(){
            console.log('Checking Dates');
            assignedDate = $('#date').val();
            deadline = $('#projectDeadline').val();

            if(assignedDate<=deadline){
                $('#createTask').show();
                $('#date-error').hide();
            }
            else{
                $('#createTask').hide();
                $('#date-error').show();
            }
        }

    </script>
@endsection