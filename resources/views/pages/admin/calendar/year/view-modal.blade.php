 <!-- Modal -->
<div id="viewModal" class="modal fade" data-keyboard="false" role="dialog">
<div class="vertical-alignment-helper">
  <div class="modal-dialog vertical-align-center" style="width: 900px">

    <!-- Modal content-->
    <div class="modal-content text-center">
      <div class="modal-header padding-l-10" style="border-bottom: 1px solid rgba(0,0,0,0.4);">
        <h4 class="modal-title text-left">Edit Project</h4>
      </div>
      <div class="modal-body clearfix padding-0">
        <div class="col-md-12 calendar-header year">
            <div class="col-md-12 text-left details">
                <div class="row">
                    <div class="col-md-3 info"><b>Client:</b> <span id="view-stage-client">GSK</span></div>
                    <div class="col-md-3 info"><b>Project:</b> <span id="view-stage-project">The Patient</span></div>
                    <div class="col-md-3 info"><b>Version:</b> <span id="view-stage-version">7</span></div>
                    <div class="col-md-3 info">&nbsp;</div>
                    <div class="col-md-3 info"><b>Brand Name:</b> <span id="view-stage-brand">n/a</span></div>
                    <div class="col-md-3 info"><b>Format:</b> <span id="view-stage-format">Journal</span></div>
                    <div class="col-md-3 info"><b>Language:</b> <span id="view-stage-language">English</span></div>
                    <div class="col-md-3 info"></div>
                </div>
            </div>

            <div class="col-md-2 padding-0">
                <div class="list-top">
                    Stages
                </div>

                <ul id="stage-list-modal" class="clearfix">
                    <li><a href="#" data-project-id="1" class="viewModalLink"></a></li>
                </ul>
            </div>
            <div class="col-md-10 relative top-header padding-0 dragscroll" style="overflow: auto">

                <div class="scroller clearfix ">
                    @include('pages.admin.calendar.year.date-modal-header')

                    <div class="clearfix" id="stage-modal-bars">
                          <div class="gridster pull-left">
                              <ul>
                                  <li data-row="1" data-col="10" data-sizex="25" data-sizey="1"></li>
                              </ul>
                          </div>
                    </div>

                </div>
            </div>
        </div>

      </div>
      <div class="modal-footer padding-t-0">

          {!! Form::open(['route'=>'api.projects.stages.update','id'=>'stage-modal-form']) !!}

          <button class="btn btn-default pull-right padding-r-0" data-dismiss="modal">Cancel</button>
          <button id="deleteRoleTrigger" class="createbt btn btn-default pull-right border-right-gray">Save</button>
          {!! Form::close() !!}

      </div>

    </div>

  </div>
</div>
</div>