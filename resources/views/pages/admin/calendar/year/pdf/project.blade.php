<link rel="stylesheet" href="{{ asset('public/plugins/jasny-bootstrap/css/jasny-bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('public/css/calendar.css') }}">
<link rel="stylesheet" href="{{ asset('public/css/global.css') }}">
<link rel="stylesheet" href="{{ asset('public/css/bootstrap.no-print.min.css') }}">

<style>
    #viewModal .dates .month {
        font-size: 14px;
    }
    .date-top {
        width: auto;
        border-top: 1px solid rgba(0,0,0,0.2);
        -webkit-print-color-adjust: exact;
    }
    .dates {
        -webkit-print-color-adjust: exact;
        background-color: transparent;
    }
    .days {
        margin-bottom: 1px;
    }
    .stages li {
        border:0;
        -webkit-print-color-adjust: exact;
        opacity: 1;
    }
    .stages {
        border-right: 1px solid rgba(0,0,0,0.2);
        overflow: hidden;
        background-image: none;
    }
    #viewModal .date-top {
        height: 43px;
    }
    #project-list {
        border-right: 1px solid rgba(0,0,0,0.2);
    }
    .modal-header h4 {
        font-size: 16px;
        color: #BF1C73;
        border-bottom: 0;
        font-weight: 300;
    }
    .modal-header {
        padding: 9px;
    }
    #viewModal .dates .month {
        font-size: 11px;
    }
</style>

<style type="text/css" media="print">
    @page { size: landscape; }
    .page
    {
        filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
    }
    .page { width: @container-desktop !important; }
</style>

<div class="page" style="width:1474px;max-width:1474px;">
    <div style="width:1443px;max-width:1443px;">
        <div id="viewModal">
        <div class="modal-header padding-l-10" style="border-bottom: 1px solid rgba(0,0,0,0.4);">
            <h4 class="modal-title text-left">{{ $project->name }}</h4>
        </div>
        <div class="col-md-12 text-left details">
            <div class="row">
                <div class="col-md-3 info"><b>Client:</b> {{ $project->client ? $project->client->name : 'N/A' }}</div>
                <div class="col-md-3 info"><b>Project:</b> {{ $project->name }}</div>
                <div class="col-md-3 info"><b>Version:</b> {{ $project->version }}</div>
                <div class="col-md-3 info">&nbsp;</div>
                <div class="col-md-3 info"><b>Brand Name:</b> {{ $project->brand ? $project->brand->name : 'N/A' }}</div>
                <div class="col-md-3 info"><b>Format:</b> {{ $project->format ? $project->format->name : 'N/A' }}</div>
                <div class="col-md-3 info"><b>Language:</b> {{ $project->languagesString }}</div>
                <div class="col-md-3 info"></div>
            </div>
        </div>
        </div>
    </div>

    @foreach($weeksPerMonth as $key=>$year)
        <?php $veryFirst = 0; ?>
        @if(isset($projectsInFormatHtml[$key]))
            <div class="col-md-12">
                <div class="row">
                    <h5 class="text-left padding-l-10 margin-b-0">{{ $key }}</h5>
                    <div class="padding-r-0 pull-left" style="width:230px;max-width:230px;">
                        <div class="plan-project" style="">
                            <div class="text-center" style="font-size: 12px;padding-top:10px;background-color: #ececec; height: 43px; border: 1px solid rgba(0,0,0,0.2);">
                                Stages
                            </div>
                            <ul id="project-list">
                            @if($project->stages)
                                @foreach($project->stages as $stage)
                                    <li style="border-left: 1px solid rgba(0,0,0,0.2);" class="text-center "><a href="#" class="viewProjectLink">{{ $stage->stage->name }}</a></li>
                                @endforeach
                            @else
                                Please add a project stage at the settings.
                            @endif
                            </ul>
                        </div>
                    </div>

                    <div class="padding-0 pull-left" style="width:1220px;max-width:1220px;" id="viewModal">
                        <div class="date-top relative">
                            <ul class="dates pull-left">
                                    @foreach($year as $week)

                                        <li class="clearfix">
                                            <div class="month  {{ \Carbon\Carbon::now()->setTimezone(Auth::user()->companyTimezone)->format('F')==$week['name'] ? 'activeMonth' : '' }}">{{ $week['name'] }}</div>
                                            {{--<div class="week">Week</div>--}}
                                            <ul class="days clearfix">

                                                @for($x=1;$x<=$week['weeks'];$x++)
                                                    @if($veryFirst<1)
                                                        <?php $veryFirst++; ?>
                                                        <li style="background-image: none">{{ $x }}</li>
                                                    @else
                                                        <li>{{ $x }}</li>
                                                    @endif
                                                @endfor

                                            </ul>
                                        </li>

                                    @endforeach


                            </ul>

                            <div id="projectStages">
                                {!! $projectsInFormatHtml[$key] !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endforeach
</div>