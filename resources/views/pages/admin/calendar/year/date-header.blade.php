<div class="date-top relative">
<ul class="dates pull-left">
    <?php
        $activeWeek = false;
        $activeMonth = false;
        $divider = ( $firstWorkDay == "Mon" ? 7 : 6 );
    ?>
    @foreach($weeksPerMonth as $yearIndex=>$year)
        @foreach($year as $week)

        <li class="" style="width: {{$week['weeks']*50}}px">
            {{--<div style="width: {{$week['weeks']*50}}px" class="month {{ \Carbon\Carbon::now()->setTimezone(Auth::user()->companyTimezone)->format('F')==$week['name'] ? 'activeMonth' : '' }}">{{ $week['name'] }}</div>--}}
            <div style="width: {{$week['weeks']*50}}px" class="month">{{ $week['name'] }}</div>
            {{--<div class="week">Week</div>--}}

            <?php
                if((\Carbon\Carbon::now()->setTimezone(Auth::user()->companyTimezone)->format('F')==$week['name']) && (\Carbon\Carbon::now()->setTimezone(Auth::user()->companyTimezone)->format('Y')==$yearIndex)){
                    $activeMonth = true;
                    $currentDay = \Carbon\Carbon::now()->setTimezone(Auth::user()->companyTimezone)->format('d');
                } else {
                    $activeMonth = false;
                }
            ?>

            <ul class="days clearfix" style="width: {{$week['weeks']*50}}px">
            @for($x=1;$x<=$week['weeks'];$x++)
                @if($activeMonth)
                    @if(!$activeWeek)
                        <?php
                            if(intval($currentDay) / $divider < 1)
                                $activeWeek = 1;
                            else
                                $activeWeek = floor(intval($currentDay) / $divider) + 1;

                            $activeWeek = $activeWeek > 5 ? 5 : $activeWeek;

                            if($activeWeek>$week['weeks'])
                                $activeWeek = 4;
                        ?>
                    @endif
                @endif
                <li {{ $activeMonth && $activeWeek == $x ? 'class=currentWeek' : '' }}>{{ $x }} wk</li>
            @endfor

            </ul>
        </li>

        @endforeach
    @endforeach

</ul>
</div>