@extends('............layouts.app')

@section('page-title',Auth::user()->name.': Year View')

@section('style')
    <link rel="stylesheet" href="{{ asset('public/css/calendar.css') }}">
    <link rel="stylesheet" href="{{ asset('public/plugins/gridster/gridster.css') }}">
    <link rel="Stylesheet" type="text/css" href="{{ asset('public/plugins/smooth-scroll/css/smoothDivScroll.css') }}" />
    <link href='{{ asset('public/plugins/jquery-ui/jquery-ui.min.css') }}' rel='stylesheet' />
    <link href='{{ asset('public/plugins/jquery-daterangepicker/jquery.comiseo.daterangepicker.css') }}' rel='stylesheet' />
    <link rel="stylesheet" type="text/css" href="{{ asset('public/plugins/bgrins-spectrum/spectrum.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/plugins/bootstrap-select-1.10.0/css/bootstrap-select.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/calendar.admin.year.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/plugins/jquery-confirm/jquery-confirm.css') }}">
    <style>
        #viewModal .top-header {
            border-top: 1px solid rgba(0,0,0,0.4);
            padding-bottom: 2px !important;
        }
        #viewModal .calendar-header {
            padding: 0px;
            border-left: 0;
            border-right: 0;
        }
        .navmenu-fixed-right {left: auto !important;}
        .navbar-toggle {left: auto !important;}
        body.canvas-sliding, body.canvas-slid {left: auto !important;}

        .sp-replacer {
            height: 22px;
            width: 48px;
            top: -29px;
            border: 0;
            opacity: 0;
            position: absolute;
            right:0;
        }


        #viewModal .scroller::-webkit-scrollbar {
            -webkit-appearance: none;
            width: 3px;
            height: 8px;
        }

        #viewModal .scroller::-webkit-scrollbar-thumb {
            border-radius: 4px;
            background-color: #009D9C;
            -webkit-box-shadow: 0 0 1px rgba(255, 255, 255, .5);
        }

        #addProjectForm .to {
            height: 30px;
            line-height: 27px;
            box-sizing: border-box;
            text-align: center;
        }

        .stageSchedules .to {
            text-align: center;
        }
    </style>
@endsection

@section('content')
    @include('pages.admin.calendar.year.modals.add-project')

    <div class="container">
        <div class="row  relative">
            <div id="globalAlert" class="alert"></div>
            <div class="col-md-12 calendar-header year padding-b-20">
                <div class="row">
                    <div class="col-md-2 padding-r-0">
                        <div class="list-top">

                            <?php
                                $filters = [
                                    'format'  => 'Format',
                                    'client'  => 'Client',
                                    'project' => 'Project',
                                    'archive'  => 'Archived',
                                ];
                            ?>
                            {!! Form::select('project_sort',$filters, isset($_GET['filter']) ? $_GET['filter'] : 'format',['id'=>'project_sort','class'=>'selectpicker margin-b-4']) !!}

                            {!! Form::open(['route'=>'calendar.year.search']) !!}
                            {!! Form::input('text','keyword',isset($_GET['keyword']) ? $_GET['keyword'] : null ,['placeholder'=>'Search','class'=>'pull-right','id'=>'search-top']) !!}
                            {!! Form::close() !!}
                        </div>
                        <ul id="project-list" class="clearfix">

                            <?php
                                $itemCount = 0;
                            ?>

                            @foreach($projectsInFormat as $format)

                                @if(count($format->projects))
                                    <li class="category">
                                        <a href="#">{{ $format->name }}</a>
                                    </li>
                                    @foreach($format->projects as $project)
                                        <?php $itemCount++; ?>
                                        <li><a href="#" data-project-id="{{ $project->id }}" class="viewProjectLink">{{ $project->name }}</a></li>
                                    @endforeach
                                @else
                                    @if(isset($_GET['filter']))
                                        @if($_GET['filter']=='project'||$_GET['filter']=='archive')
                                            <?php $itemCount++; ?>
                                            <li><a href="#" data-project-id="{{ $format->id }}" class="viewProjectLink">{{ $format->name }}</a></li>
                                        @endif
                                    @elseif(isset($_GET['keyword']))
                                        <?php $itemCount++; ?>
                                        <li><a href="#" data-project-id="{{ $format->id }}" class="viewProjectLink">{{ $format->name }}</a></li>
                                    @endif
                                @endif
                            @endforeach

                            @while($itemCount<18)
                                <li class="fillers">&nbsp;</li>
                                <?php $itemCount++ ?>
                            @endwhile
                        </ul>
                    </div>
                    <div class="pull-left relative top-header padding-l-0" id="embeddedYear" style="overflow: auto;width: 73.3%;">
                        <div class="spinner white"></div>

                        <h2 class="year relative">
                            <?php
                                $currentYear = isset($_GET['year']) ? $_GET['year'] : \Carbon\Carbon::now()->setTimezone(Auth::user()->companyTimezone)->subYear()->format('Y');
                            ?>

                            <a href="{{ route('calendar.year') }}?year={{ intval($currentYear)-1 }}" class="year-nav" id="prevYear"></a>
                                <span id="currentYear">{{ $currentYear }}</span>
                            <a href="{{ route('calendar.year') }}?year={{ intval($currentYear)+1 }}" class="year-nav" id="nextYear"></a>
                        </h2>


                        <div class="scroller clearfix dragscroll">
                            @include('pages.admin.calendar.year.date-header')
                            <div id="projectStages">
                                {!! $projectsInFormatHtml !!}
                            </div>
                        </div>
                    </div>

                    <div class="pull-left" style="width: 10%; padding-left: 12px;">
                        <div id="bottom-nav">
                            <a href="#" id="addProject">Add Project</a>

                            @if( isset($_GET['year']) ? ($_GET['year'] == \Carbon\Carbon::now()->setTimezone(Auth::user()->companyTimezone)->format('Y') ? true : false ) : true )
                                <a href="#" id="scrollToday">Today</a>
                            @else
                                <a href="{{ route('calendar.year') }}">Today</a>
                            @endif

                            <a target="_blank" href="#" id="printBt">Print</a>
                        </div>

                        <div id="stage-legend">
                            @foreach($stagesList as $stage)
                                <span class="a" href="#"><span style="background-color: {{ $stage->color }};"></span><p class="name" style="width:90px">{{ $stage->name }}</p></span><br>
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>

            <div id="deleteModal" class="modal fade delete" data-backdrop="static" data-keyboard="false" role="dialog">
                <div class="vertical-alignment-helper">
                    <div class="modal-dialog vertical-align-center" style="width: 300px">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title text-center">Delete Project</h4>
                            </div>
                            <div class="modal-body text-center">
                                Are you sure you want to delete this?<br> <span class="yes">Please type "yes" to confirm.</span>
                                {!! Form::open(['route'=>'api.projects.delete','onSubmit'=>'return(false)']) !!}
                                <input type="hidden" name="id" value="0">
                                <input id="deleteConfirmInput" class="text-center margin-b-0 margin-t-5">
                                {!! Form::close() !!}
                            </div>
                            <div class="modal-footer  text-center-important padding-t-0">
                                <button id="deleteTrigger" class="createbt btn btn-default">Confirm</button>
                                <button class="btn btn-default" id="deleteCancel" data-dismiss="modal">Cancel</button>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('pages.admin.calendar.year.view-modal')
    @include('pages.admin.calendar.year.modals.print')
@endsection

@section('js')
    <script src="{{ asset('public/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
	<!-- Latest version (3.1.4) of jQuery Mouse Wheel by Brandon Aaron
	     You will find it here: https://github.com/brandonaaron/jquery-mousewheel -->
	{{--<script src="{{ asset('public/plugins/smooth-scroll/js/jquery.mousewheel.min.js') }}" type="text/javascript"></script>--}}

	<!-- jQuery Kinectic (1.8.2) used for touch scrolling -->
	<!-- https://github.com/davetayls/jquery.kinetic/ -->
	{{--<script src="{{ asset('public/plugins/smooth-scroll/js/jquery.kinetic.min.js') }}" type="text/javascript"></script>--}}

	<!-- Smooth Div Scroll 1.3 minified-->
	{{--<script src="{{ asset('public/plugins/smooth-scroll/js/jquery.smoothdivscroll-1.3-min.js') }}" type="text/javascript"></script>--}}
	<script src="{{ asset('public/plugins/dragscroll/jquery.dragscroll.js') }}" type="text/javascript"></script>
	<script src="{{ asset('public/js/jquery.scrollTo.min.js') }}" type="text/javascript"></script>

    {{--<script src='{{ asset('public/plugins/jquery-daterangepicker/jquery.comiseo.daterangepicker.js') }}'></script>--}}

    <script src="{{ asset('public/plugins/bgrins-spectrum/spectrum.js') }}"></script>

    <script src="{{ asset('public/plugins/bootstrap-select-1.10.0/js/bootstrap-select.min.js') }}"></script>

    <script src="{{ asset('public/plugins/jquery-confirm/jquery-confirm.min.js') }}"></script>
    <script src="{{ asset('public/js/numbersOnly.js') }}"></script>

    {{--<script src="{{ asset('public/js/velocity.js') }}"></script>--}}
    <script src="{{ asset('public/plugins/gridster/gridster.js') }}"></script>
    <script src="{{ asset('public/plugins/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script>
      var dragHistory = 0;
      var resizeHistory = 0;
      var currentDragItem = 0;
      var colors = getColors();

      function getColors(){

          $.ajax({
              type: "GET",
              url: '{{ route('api.colors.get') }}',
              success: function(response){
                  colors = JSON.parse(response);

                  $("#clientColor").spectrum({
                      showPaletteOnly: true,
                      showPalette:true,
                      hideAfterPaletteSelect:true,
                      color: colors[0],
                      palette: [
                          colors
                      ],
                      change: function(color) {
                          $('#clientColor').val(color.toHexString()).attr('value',color.toHexString()); // #ff0000
                          $('#clientCircleColor').css('background-color',color.toHexString()); // #ff0000
                      }
                  });
              }
          });
      }

      var changesMade = 0;
      var selectedYear = '{{ isset($_GET['year']) ? $_GET['year'] : \Carbon\Carbon::now()->setTimezone(Auth::user()->companyTimezone)->format('Y') }}';

      $('.datepicker').datepicker();

      /**
       *
       * ADD PROJECT
       *
       */

      $('#addProject').on('click', function () {
          $('#projectModalTitle').text('Add Project');
          $('#addProjectForm')[0].reset();
          $('#view_project_id').val('');
          $('#addProjectModal').modal('show');
          $('body').css('padding-right', '17px');

          randomHex = colors[0];
          $('#clientColor').val(randomHex).attr('value', randomHex).trigger('change');
          $('#pLanguage').val(3).trigger('change');
          $('.sp-preview-inner').css('background-color', randomHex);

          $('#project-details-title').text('Add Project');
          $('#delete-bt').hide();
          $('#complete-bt').hide();

          $('#addProjectForm').attr('action', '{{ route('api.projects.store') }}');
          $('#generatePDFLink').hide();

          $('.stageSchedules .daterangepicker').attr('data-start', '');
          $('.stageSchedules .daterangepicker').attr('data-end', '');

          attachProjectDurationEvent();
          attachDatePickerListeners(false, false);

          updateDatepickerRanges(null,null);

          setTimeout(function(){
              $('#addProjectModal .col-md-4').height($('#addProjectModal .modal-body').height());
          },1000);
      });
      /**
       *
       * ADD PROJECT
       *
       */

      $('#printBt').on('click', function (e) {
          e.preventDefault();
          $('#printModal').modal('show');
      });

      /**
       *
       * VIEW STAGES MODAL
       *
       */


      $('.viewStagesLink').on('click',function() {
            $('#spinner').fadeIn(500);
            var projectId = $(this).attr('data-project-id');

          $('#overlay-modal').show();
          $('#overlay-modal #spinner').show();

          projectDates = [];

            $.ajax({
                type: "GET",
                url: '{{ route('api.projects.get') }}?id='+projectId+'&year='+selectedYear,
                success: function(response){
                    projectDates = response.project.range_dates;

                    $('#stage-list-modal').html('');
//                    $('#stage-modal-bars .gridster ul').html('');
                    $('#stage-modal-bars').html('');

                    $('#view-stage-client').html(response.project.client.name);
                    $('#view-stage-brand').html(response.project.brand.name);
                    $('#view-stage-project').html(response.project.name);
                    $('#view-stage-format').html(response.project.format.name);
                    $('#view-stage-version').html(response.project.version);
                    $('#view-stage-language').html(response.project.language);

                    $('#overlay-modal .modal-title').text(response.project.name);

                    filler = '<div class="filler" style="width:'+((response.project.range_start-1)*20)+'px"></div>';

                    var index = 0;
                    $.each(response.stages, function(e,i){
                        $('#stage-list-modal').append('<li><a href="#" data-project-id="1" class="viewModalLink">'+i.name+'</a></li>');

                        width = ((i.project_stage.end_index)-(i.project_stage.start_index-1));

                        bar = '<div class="gridster pull-left">'+filler+'<ul data-range="'+response.project.range+'">'+
                              '<li title="Drag and drop or resize to change date values." style="background-color: '+i.color+'"'+
                               'data-row="1" data-col="'+i.project_stage.start_index+'"'+
                               'data-stage-id="'+i.project_stage.id+'"'+
                               'data-sizex="'+width+'" data-sizey="1">';

                        if(i.hours>0)
                            bar += parseInt(i.hours/60) + ' hrs';

                        bar += '</li></ul></div>';

//                      bar = '<li style="background-color: '+i.color+'" data-row="'+index+'" data-col="10" data-sizex="25" data-sizey="1"></li>';
//                      $('#stage-modal-bars .gridster ul').append(bar);

                        startInputId = '<input type="hidden" name="stage['+index+'][stage_id]" value="'+i.project_stage.id+'">';
                        startInputStart = '<input type="hidden" id="stage-'+i.project_stage.id+'-start" name="stage['+index+'][start_at]">';
                        startInputEnd = '<input type="hidden" id="stage-'+i.project_stage.id+'-end" name="stage['+index+'][end_at]">';

                        $('#stage-modal-form').append(startInputId);
                        $('#stage-modal-form').append(startInputStart);
                        $('#stage-modal-form').append(startInputEnd);
                        $('#stage-modal-bars').append(bar);
                        index++;
                    });
                },
                complete : function(){
                $(function(){ //DOM Ready

                    for(x=0;x<$("#stage-modal-bars .gridster ul").length;x++){
                        range = parseInt($("#stage-modal-bars .gridster ul").eq(x).attr('data-range'));

                        $("#stage-modal-bars .gridster ul").eq(x).gridster({
                          widget_base_dimensions: [20, 20],
                          min_cols: range,
                          max_cols: range,
                          max_row: 0,
                          autogrow_cols: false,
                          autogrow_rows: false,
                          widget_margins: [0, 0],
                          helper: 'clone',
                          resize: {
                            stop: function(e,i,w){
                                stageId = $(w[0]).attr('data-stage-id');

                                start = projectDates[w[0].dataset.col - 1];
                                end = parseInt(w[0].dataset.col - 1) + parseInt(w[0].dataset.sizex-1)
                                end = projectDates[end];

                                $.confirm({
                                    title: 'Are you sure?',
                                    content: 'Do you want to change the date from '+ start + ' to ' + end + '?',
                                    confirmButton: 'Confirm',
                                    cancelButton: 'Cancel',
                                    confirm: function(){
                                        $('#stage-'+stageId+'-start').val(w[0].dataset.col);
                                        $('#stage-'+stageId+'-end').val(w[0].dataset.sizex);
                                    },
                                    cancel: function(){
                                        currentDragItem.dataset.sizex = resizeHistory;
                                        $('#stage-'+stageId+'-start').val(resizeHistory);
                                    }
                                });

                            },
                              start: function(e,i){
                                  dragHistory = i.$helper[0].parentNode.dataset.col;
                                  resizeHistory = i.$helper[0].parentNode.dataset.sizex;
                                  currentDragItem  = i.$helper[0].parentNode;
                              },
                            enabled: true,
                            axes: ['x']
                          },
                          draggable: {
                            stop: function(e,i){

                                stageId = $(e.target).attr('data-stage-id');

                                start = projectDates[i.$helper[0].dataset.col - 1];

                                end = parseInt(i.$helper[0].dataset.col - 1) + parseInt(i.$helper[0].dataset.sizex-1);
                                end = projectDates[end];

                                $.confirm({
                                    title: 'Are you sure?',
                                    content: 'Do you want to change the date from '+ start + ' to ' + end + '?',
                                    confirmButton: 'Confirm',
                                    cancelButton: 'Cancel',
                                    confirm: function(){
                                        $('#stage-'+stageId+'-start').val(i.$helper[0].dataset.col);
                                        $('#stage-'+stageId+'-end').val(i.$helper[0].dataset.sizex);
                                    },
                                    cancel: function(){
                                        currentDragItem.dataset.col = dragHistory;
                                        $('#stage-'+stageId+'-start').val(dragHistory);
                                    }
                                });


                            },
                            start: function(e,i){
                                dragHistory = i.$helper[0].dataset.col;
                                resizeHistory = i.$helper[0].dataset.sizex;
                                currentDragItem  = i.$helper[0];
                            }
                          }
                        });
                    }

                });

                    setTimeout(function(){
                        $('#spinner').hide();
                        $('#overlay-modal').fadeOut(300);
                        $('#overlay-modal #spinner').fadeOut(300);

                        $('#viewModal').modal('show');

                    },300);

                }
            });


            scrollWidth = 5;

            $.each($('#viewModal .dates li'),function(){
                scrollWidth += $(this).width();
            });

            $('#viewModal .scrollableArea').attr('style','width:'+scrollWidth+'px !important');
            $('#viewModal .dates').attr('style','width:'+scrollWidth+'px !important');
            $('#viewModal .date-top').attr('style','width:'+scrollWidth+'px !important');
            $('#stage-modal-bars').attr('style','width:'+scrollWidth+'px !important');

      });

      /**
       *
       * VIEW EDIT / PROJECT
       *
       */

      function attachViewProjectListener(){

          $('.viewProjectLink').unbind('click');
          $('.viewProjectLink').on('click',function(e) {
              e.preventDefault();

              $('#overlay-modal').show();
              $('#spinner').show();

              var projectId = $(this).attr('data-project-id');

              $('#addProjectModal input[type=text]').val('');

              $.ajax({
                  type: "GET",
                  url: '{{ route('api.projects.getDetails') }}?id='+projectId,
                  success: function(response){

                      $('#project-details-title').text(response['project[name]']);
                      $('#projectModalTitle').text(response['project[name]']);

                      $.each(response,function(key,value){
                          $('#addProjectForm input[name="'+key+'"]').val(value);
                      });

                      $.each(response,function(key,value){
                          $('#addProjectForm select[name="'+key+'"]').val(value);
                      });

                      $('#addProjectForm select[class="selectpicker"]').selectpicker('refresh');

                      $('#pLanguage').val(response.language_ids.length ? response.language_ids : 3).trigger('change');

                      $('#complete-bt').attr('data-project-id',response['project_id']);
                      $('#delete-bt').attr('data-project-id',response['project_id']);

                      if(response['project[completed_at]'])
                          $('#complete-bt').text('Unarchive');
                      else
                          $('#complete-bt').text('Archive');

                      $('#clientColor').trigger('change');

                      $('#addProjectForm').attr('action','{{ route('api.projects.update') }}');
                      $('#addProjectModal').modal('show');

                      $('#delete-bt').show();
                      $('#complete-bt').show();

                      /***
                       *
                       *  DISPLAY STAGES
                       *
                       */

                      project_start = moment(response['project[start_at]'].date).format('MM/DD/YYYY');
                      project_end = moment(response['project[end_at]'].date).format('MM/DD/YYYY');

//                      $('#start_at').val(start_at);
//                      $('#end_at').val(end_at);
//                      $('#projectDuration').val(start_at+'-'+end_at);


//                      $('#projectDuration').daterangepicker('setRange',{
//                          start: new Date(start_at),
//                          end: new Date(end_at)
//                      });

                      if(response['stages'].stages){
                          $.each(response['stages'].stages, function(key,value){
                              if(value){
                                  start_at = moment(value.start_at).format('MM/DD/Y');
                                  end_at = moment(value.end_at).format('MM/DD/Y');

                                  $('input[name="stage['+value.stage_id+'][start_at]"]').val(start_at);
                                  $('input[name="stage['+value.stage_id+'][end_at]"]').val(end_at);
                                  $('input[name="stage['+value.stage_id+'][time]"]').val(value.time);

                                  $('input[name=stageDuration-'+value.stage_id+']').attr('data-start',start_at).attr('data-end',end_at);
                                  $('input[name=stageDuration-'+value.stage_id+']').val(start_at+' - '+end_at);

                              }
                          });
                      }

                      updateDatepickerRanges(project_start,project_end);

                      $('#generatePDFLink').attr('href','{{ route('api.projects.pdf') }}?id='+response['project_id']);

                      $('#overlay-modal').hide();
                      $('#spinner').hide();
                  },
                  complete : function(){
                      $('#overlay-modal').hide();
                      $('#spinner').hide();
                  }
              });


              $('#addProjectForm .stageSchedules .comiseo-daterangepicker-triggerbutton').on('click',function(){
                  picker = $(this).closest('div').find('.daterangepicker');
                  start = $(picker).attr('data-start');
                  end =  $(picker).attr('data-end');

                  if(start && end){
//                      $(picker).daterangepicker('setRange',{
//                          start: new Date(start),
//                          end: new Date(end)
//                      });
                  }
              });

              $('#viewModal .scrollableArea').attr('style','width:'+367*20+'px !important');
          });

          $('#clientColor').on('change',function(){
              $('.sp-preview-inner').css('background-color',$(this).val());
              $('#clientCircleColor').css('background-color',$(this).val());
          });
      }

      attachViewProjectListener();

      var gridster;


      $(function(){
          <?php
            $currentYear = isset($_GET['year']) ? $_GET['year'] : \Carbon\Carbon::now()->setTimezone(Auth::user()->companyTimezone)->subYear()->format('Y') ;
          ?>
          {{--attachDatePickerListeners("12/25/{{ $currentYear }}","01/01/{{ $currentYear }}");--}}

            attachProjectDurationEvent();
            attachProjectStagesDurationEvent();

          $('.date-top').css('width',$('#embeddedYear .dates .days li').length * 50 + 1 +'px');
          $('#projectStages').css('width',$('#embeddedYear .dates .days li').length * 50 + 1 +'px');

          setTimeout(function(){
              $('#embeddedYear .scroller').scrollTo($('#embeddedYear .scroller .currentWeek'),500);
          },500);

          var availableClients = getAvailableClients();

          function getAvailableClients(){

              $.ajax({
                  type: "GET",
                  url: '{{ route('api.clients.getJson') }}',
                  success: function(response){
                      availableClients = JSON.parse(response);
                      refreshClientAC(availableClients);
                  }
              });
          }

          function refreshClientAC(availableClients) {
              $( "#clients" ).autocomplete({
                  source: availableClients,
                  select: function(event,ui){
                      $("#clients").val(ui.item.label);
                      $('#clientColor').val(ui.item.value).attr('value',ui.item.value).trigger('change');
                      $('.sp-preview-inner').css('background-color',ui.item.value);
                      $(this).closest('.side-form').find('.color-circle').first().css('background-color',ui.item.value);
                      return false;
                  },focus: function (event, ui) {
                      this.value = ui.item.label;
                      // or $('#autocomplete-input').val(ui.item.label);

                      // Prevent the default focus behavior.
                      event.preventDefault();
                      // or return false;
                  }
              });
          }


      });


      function attachDatePickerListeners(start,end){
//          $('.daterangepicker').each(function(){
//              $(this).daterangepicker({
//                  initialText : 'Please select date range...',
//                  datepickerOptions : {
//                      numberOfMonths : 2,
//                      maxDate : start ? moment(start).format('M/D/Y') : null,
//                      minDate : end ? moment(end).format('M/D/Y') : null
//                  },
//                  presetRanges: [],
//                  presets: { dateRange: "Date Range" },
//              }).on('change', function(e){
//                  dateRange = (JSON.parse(e.currentTarget.value));
//                  $(this).closest('.side-form').find('.hidden .project-start').val(moment(dateRange.start).format('MM/DD/Y'));
//                  $(this).closest('.side-form').find('.hidden .project-end').val(moment(dateRange.end).format('MM/DD/Y'));
//              })
//          });
      }


      function addStageFillers(){
          toAdd = $('.fillers').length;

          for(x=0;x<toAdd;x++)
            $('#projectStages').append('<ul class="stages"></ul>');
      }
      addStageFillers();

    $('#project_sort').on('change',function(){
        window.location = '{{ route('calendar.year') }}?filter='+$(this).val();
    });

    $(window).load(function(){
        $('.spinner').delay(300).hide();

        $('.scrollWrapper').css('cursor','url({{ asset('public/images/placeholders/drag-cursor.png') }}),auto');
        $('.viewStagesLink').css('cursor','url({{ asset('public/images/placeholders/cursor.png') }}),auto');

        $($('#embeddedYear .dates .days li').last()).css('border',0);
//        $('#addProjectModal .col-md-4').height($('#addProjectModal .modal-body').height());
    });

      $('#viewModal').on('shown.bs.modal', function(){
          var activeMonth = $('#viewModal .activeMonth');

          if(activeMonth){
              activeMonthIndex = $("#viewModal .scroller").find(".month").index( activeMonth );

              setTimeout(function(){
                  $("#viewModal .dragscroll").scrollTo($($('#viewModal .dragscroll .activeMonth')[0]),500);
              },100);
          }
      });

        $('#scrollToday').on('click',function(){
            $('#embeddedYear .scroller').scrollTo($('#embeddedYear .scroller .currentWeek'),500);
        });


        $('#embeddedYear .scroller').on( "scroll", function(){
            currentScroll = $(this).find('.date-top').offset();

            if(currentScroll.left <= (-2050) && currentScroll.left>= (-5100))
                $('#currentYear').text(parseInt(selectedYear));
            else if(currentScroll.left <= (-5100))
                $('#currentYear').text(parseInt(selectedYear)+1);
            else
                $('#currentYear').text(parseInt(selectedYear)-1);
        });

      function refreshFormats(){

          $('#project_format').each(function(){
              $.ajax({
                  type: "GET",
                  url: '{{ route('api.formats.getForSelect') }}',
                  success: function(response){

                      $('#project_format').find('option').remove();

                      for(var x=0;x<response.length;x++){
                          el = '<option value="'+response[x]['id']+'">'+response[x]['name']+'</option>';

                          $('#project_format').append(el);
                      }

                      $('#project_format').append('<option value="0">-- Add a new format --</option>');
                      $('#project_format').selectpicker('refresh').trigger('change');

                  }
              });
          });
      }
      refreshFormats();

      $('#project_format').on('change', function(){
          if($(this).val()==0){
              $(this).closest('.bootstrap-select').hide();
              $('#new_format_name').show();
              $('.new_format .close').show();
          }
          else {
              $(this).closest('.bootstrap-select').show();
              $('#new_format_name').hide();
              $('.new_format .close').hide();

          }
      });

      $('.new_format .close').on('click', function(){
          $('#new_format_name').val('').hide();
          $('.new_format .close').hide();
          $(this).closest('.gray').find('.bootstrap-select').show();
          $('#project_format').val($("#project_format option:first").val());
          $('#project_format').selectpicker('refresh');
      });

      function attachProjectDurationEvent(){

          {{--$('#projectDuration').daterangepicker({--}}
              {{--initialText : 'Please select date range...',--}}
              {{--datepickerOptions : {--}}
                  {{--numberOfMonths : 2,--}}
                  {{--maxDate : "12/31/{{ (int)$currentYear + 2 }}",--}}
                  {{--minDate : "01/01/{{ $currentYear }}",--}}
              {{--},--}}
              {{--presetRanges: [],--}}
              {{--presets: { dateRange: "Date Range" }--}}
          {{--}).on('change', function(e){--}}
              {{--dateRange = JSON.parse(e.currentTarget.value);--}}
              {{--target = $(this).closest('div');--}}
              {{--$(target).find('.hidden .project-start').val(moment(dateRange.start).format('MM/DD/Y'));--}}
              {{--$(target).find('.hidden .project-end').val(moment(dateRange.end).format('MM/DD/Y'));--}}

              {{--range = moment(dateRange.start).format('MM/DD/Y') + ' - ' +moment(dateRange.end).format('MM/DD/Y');--}}

              {{--setTimeout(function(){--}}
                  {{--$(target).find('.project-start').first().val(range);--}}
              {{--},100);--}}

              {{--attachDatePickerListeners(moment(dateRange.end).format('MM/DD/Y'),moment(dateRange.start).format('MM/DD/Y'));--}}
          {{--});--}}
      }

      function attachProjectStagesDurationEvent(){
//          $('.stageSchedules .daterangepicker').daterangepicker({
//              initialText : 'Please select date range...',
//              datepickerOptions : {
//                  numberOfMonths : 2,
//              },
//              presetRanges: [],
//              presets: { dateRange: "Date Range" }
//          }).on('change', function(e){
//              dateRange = (JSON.parse(e.currentTarget.value));
//              $(this).closest('div').find('.hidden .project-start').val(moment(dateRange.start).format('MM/DD/Y'));
//              $(this).closest('div').find('.hidden .project-end').val(moment(dateRange.end).format('MM/DD/Y'));
//              $(this).closest('div').find('.stage-range').val(moment(dateRange.start).format('MM/DD/Y') + ' - ' +moment(dateRange.end).format('MM/DD/Y'));
//          });
      }

        function updateDatepickerRanges(min,max){

            if(!min)
                min = moment().format('MM/DD/YYYY');
            if(!max)
                max = moment().add(1, 'month').format('MM/DD/YYYY');

            $('#start_at').datepicker('destroy');
            $('#start_at').datepicker({
                setDate : min,
                onSelect : function(e){

                    startDate = moment($('#start_at').val());
                    endDate = moment($('#end_at').val());

                    if(endDate < startDate)
                        endDate = moment($('#start_at').val()).add(1,'month');


                    $('#end_at').datepicker('destroy');
                    $('#end_at').datepicker({
                        minDate : $('#start_at').val(),
                        setDate : endDate.format('MM/DD/YYYY'),
                        onSelect : function(e){
                            updateStagePickers();
                        }
                    }).val(endDate.format('MM/DD/YYYY'));

                    updateStagePickers();
                }
            }).val(min);

            $('#end_at').datepicker('destroy');
            $('#end_at').datepicker({
                minDate : min,
                setDate : max,
                onSelect : function(e){
                    updateStagePickers();
                }
            }).val(max);

            updateStagePickers();
        }

        updateDatepickerRanges(null,null);

        function updateStagePickers(){

            startDate = $('#start_at').val();
            endDate = $('#end_at').val();

            $('.stage-start').each(function(e){
                $(this).datepicker('destroy');

                currentDate = moment($(this).val());

                if(currentDate < startDate)
                    currentDate = moment($('#start_at').val());

                $(this).datepicker({
                    minDate : startDate,
                    maxDate : endDate,
                    setDate : currentDate.format('MM/DD/YYYY'),
                });
            });

            $('.stage-end').each(function(e){
                $(this).datepicker('destroy');

                currentDate = moment($(this).val());

                if(currentDate < startDate)
                    currentDate = moment($('#start_at').val());

                $(this).datepicker({
                    minDate : startDate,
                    maxDate : endDate
                });
            });
        }
      updateStagePickers()
    </script>

    @yield('add-project-js')
    @yield('plan-project-js')
@endsection