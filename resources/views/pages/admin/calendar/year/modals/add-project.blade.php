<!-- Modal -->
<div id="addProjectModal" class="modal fade" data-keyboard="false" role="dialog">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center" style="width: 900px">

            <!-- Modal content-->
            <div class="modal-content">

                {!! Form::open(['id'=>'addProjectForm','route'=>'api.projects.store']) !!}
                {!! Form::hidden('project_id',null,['id'=>'view_project_id']) !!}

                <div class="modal-header border-bottom-gray">
                    <h4 class="modal-title text-left"><span id="projectModalTitle">Add Project</span> <a href="{{ route('api.projects.pdf') }}" target="_blank" id="generatePDFLink"><img src="{{ asset('public/images/icons/Save-Icon.png') }}" class="pull-right" height="24" style="margin-top: -3px;"></a></h4>
                </div>
                <div class="modal-body clearfix">
                    <div class="row">
                        <div class="col-md-4 calendar-header year border-right-gray" style="width: 30%;">
                            <p class="col-head">Project Details</p>
                            <div class="side-form">

                                <p>Client  <span class="pull-right">&nbsp;Color</span><span class="color-circle pull-right" id="clientCircleColor" style="background-color: #3355cc"></span></p>
                                <div class="relative">
                                    {!! Form::input('text','client[name]',null,['id'=>'clients','class'=>'form-control','required']) !!}
                                    <input id="clientColor" class="form-control" name='client[color]' value='#3355cc'/>
                                </div>

                                <p>Brand Name</p>
                                {!! Form::input('text','brand',null,['class'=>'form-control','required']) !!}

                                <p>Project Name</p>
                                {!! Form::input('text','project[name]',null,['class'=>'form-control','required']) !!}

                                <p>Type</p>
                                <div class="gray margin-b-7">
                                    {!! Form::select('project[format_id]', [] , null,['id'=>'project_format','class'=>'selectpicker','required']) !!}

                                    <div class="relative new_format">
                                        {!! Form::input('text','format_name',null,['placeholder'=>'New Format Name','id'=>'new_format_name','class'=> ( count($formats) ? 'temp-hide' : '' ) .' form-control','maxlength'=>20]) !!}
                                        @if(count($formats))
                                            <span class="close"></span>
                                        @endif
                                    </div>
                                </div>

                                <p>Version / Issue No.</p>
                                {!! Form::input('text','project[version]',1,['class'=>'form-control','required']) !!}

                                <p>Language</p>
                                <div class="gray margin-b-7">
                                    {!! Form::select('language_ids[]',$languages, [3] ,['id'=>'pLanguage','class'=>'selectpicker','multiple']) !!}
                                </div>

                                <div>
                                    <p>Project Duration:</p>
                                    <div class="pull-left padding-0 " style="width: 45%;" >
{{--                                        {!! Form::input('text','projectDuration ', null ,['placeholder'>'Please select date range...','id'=>'projectDuration','required','class' => 'project-start form-control show','style'=>'display: block !important;']) !!}--}}

                                        <div class="">
                                            {!! Form::input('text','start_at', isset($conference) ? $conference->start_at : \Carbon\Carbon::now()->setTimezone(Auth::user()->companyTimezone)->format('m/d/Y') , ['id'=>'start_at','require','placeholder'=>'Session Start','required','class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                    <div class="pull-left to" style="width: 10%;" > to </div>
                                    <div class="pull-left" style="width: 45%;" >
                                        <div class="">
                                            {!! Form::input('text','end_at', isset($conference) ? $conference->end_at : \Carbon\Carbon::now()->setTimezone(Auth::user()->companyTimezone)->addDays(7)->format('m/d/Y') , ['id'=>'end_at','placeholder'=>'Session End','required','class' => 'form-control']) !!}
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                        <div class="col-md-4 border-right-gray stageSchedules" style="width: 35%;min-height:421px;">
                            <p class="col-head">Internal Services</p>
                            @if($stagesEx)
                                @foreach($stagesIn as $stage)
                                    {!! Form::hidden('stage['.$stage->id.'][stage_id]', $stage->id) !!}
                                    <div class="side-form clearfix" style="">
                                        <p><span class="color-circle" style="background-color: {{ $stage->color }}"></span> {{ $stage->name }}</p>
                                        <div style="width: 45%;" class="pull-left padding-l-0 padding-r-0">
{{--                                            {!! Form::input('text','stageDuration-'.$stage->id, null , ['id'=>'stage['.$stage->id.'][start_at]','placeholder'=>'Please select date range...','class' => 'daterangepicker stage-range show form-control']) !!}--}}
                                            <div class="">
                                                {!! Form::input('text','stage['.$stage->id.'][start_at]', null , ['placeholder'=>'Start','class' => 'stage-start show form-control']) !!}
                                            </div>
                                        </div>
                                        <div  style="width: 10%;" class="pull-left padding-0 to"> to </div>
                                        <div style="width: 45%;" class="pull-left padding-l-0 padding-r-0">
                                            <div class="">
                                                {!! Form::input('text','stage['.$stage->id.'][end_at]', null , ['placeholder'=>'End','class' => 'stage-end form-control']) !!}
                                            </div>
                                        </div>
                                        @if($stage->hasTime)
                                            <div class="col-md-12">
                                                <div class="row assignHour text-right">
                                                    <p>Billable hours</p>
                                                    {!! Form::input('number','stage['.$stage->id.'][time]', null , ['id'=>'stage['.$stage->id.'][time]','min'=>0,'placeholder'=>'','class' => 'pull-right numbersOnly-3 hours form-control','maxlength'=>3,'max'=>999]) !!}
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                @endforeach
                            @else
                                Please add a project stage at the settings.
                            @endif

                        </div>
                        <div class="col-md-4 stageSchedules" style="width: 35%;">
                            <p class="col-head">External Services</p>
                            @if(count($stagesEx))
                                @foreach($stagesEx as $stage)
                                    {!! Form::hidden('stage['.$stage->id.'][stage_id]', $stage->id) !!}
                                    <div class="side-form clearfix" style="">
                                        <p><span class="color-circle" style="background-color: {{ $stage->color }}"></span> {{ $stage->name }}</p>
                                        <div style="width: 45%;" class="pull-left padding-l-0 padding-r-0">
                                            {{--                                            {!! Form::input('text','stageDuration-'.$stage->id, null , ['id'=>'stage['.$stage->id.'][start_at]','placeholder'=>'Please select date range...','class' => 'daterangepicker stage-range show form-control']) !!}--}}
                                            <div class="">
                                                {!! Form::input('text','stage['.$stage->id.'][start_at]', null , ['placeholder'=>'Start','class' => 'stage-start show form-control']) !!}
                                            </div>
                                        </div>
                                        <div  style="width: 10%;" class="pull-left padding-0 to"> to </div>
                                        <div style="width: 45%;" class="pull-left padding-l-0 padding-r-0">
                                            <div class="">
                                                {!! Form::input('text','stage['.$stage->id.'][end_at]', null , ['placeholder'=>'End','class' => 'stage-end form-control']) !!}
                                            </div>
                                        </div>
                                        @if($stage->hasTime)
                                            <div class="col-md-12">
                                                <div class="row assignHour text-right">
                                                    <p>Billable hours</p>
                                                    {!! Form::input('number','stage['.$stage->id.'][time]', null , ['id'=>'stage['.$stage->id.'][time]','min'=>0,'placeholder'=>'','class' => 'pull-right numbersOnly-3 hours form-control','maxlength'=>3,'max'=>999]) !!}
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="pull-left width-full border-top-gray padding-t-10">

                        <button class="btn btn-default pull-left padding-l-0 border-right-gray" data-project-id="" id="complete-bt">Archive</button>
                        <button class="btn btn-default pull-left" data-project-id="" id="delete-bt"> Delete</button>

                        <button class="btn btn-default pull-right padding-r-0" data-dismiss="modal">Cancel</button>
                        <button id="deleteRoleTrigger" class="createbt btn btn-default pull-right border-right-gray">Save</button>
                    </div>
                </div>

                {!! Form::close() !!}
            </div>

        </div>
    </div>
</div>


{{--<div class="add-project navmenu-default navmenu-fixed-right offcanvas-md offcanvas-lg" style="">--}}
{{--<h3 id="project-details-title">Add Project</h3>--}}
    {{--{!! Form::open(['id'=>'addProjectForm','route'=>'api.projects.store']) !!}--}}
        {{--{!! Form::hidden('project_id',null,['id'=>'view_project_id']) !!}--}}
        {{--<div class="side-form">--}}
            {{--Client--}}
            {{--<span class="pull-right" style="font-size: 10px">Color</span>--}}
            {{--<div class="form-group relative clientData">--}}
            {{--{!! Form::input('text','client[name]',null,['id'=>'clients','class'=>'form-control','required']) !!}--}}
            {{--<input id="clientColor" class="form-control" name='client[color]' value='#3355cc'/>--}}
            {{--</div>--}}

            {{--Brand Name--}}
            {{--{!! Form::input('text','brand[name]',null,['class'=>'form-control','required']) !!}--}}
            {{--Project Name--}}
            {{--{!! Form::input('text','project[name]',null,['class'=>'form-control','required']) !!}--}}
            {{--Format--}}
            {{--<div class="gray margin-b-7">--}}
                {{--{!! Form::select('project[format_id]', $formats ? $formats : [] , null,['class'=>'selectpicker','required']) !!}--}}
            {{--</div>--}}
            {{--Version / Issue No.--}}
            {{--{!! Form::input('text','project[version]',1,['class'=>'form-control','required']) !!}--}}

            {{--Language--}}
            {{--<div class="gray margin-b-7">--}}
                {{--{!! Form::select('language_ids[]',$languages, [3] ,['id'=>'pLanguage','class'=>'selectpicker','multiple']) !!}--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--{!! Form::submit('Plan',['class'=>'t-pink link-bt link-color margin-b-0 pull-right dir-arrow right-pink','id'=>"showPlan"]) !!}--}}
    {{--{!! Form::close() !!}--}}

    {{--<button class="t-pink link-bt link-color margin-b-0 pull-left padding-l-0" data-project-id="" id="complete-bt">Move to Archive</button>--}}
    {{--<button class="t-pink link-bt link-color margin-b-0 pull-left padding-l-0" data-project-id="" id="delete-bt"> | Delete</button>--}}
{{--</div>--}}

@section('add-project-js')
    <script>

        /** SEND CREATE REQUEST **/

    $('#addProjectForm').on('submit',function(e){
        var ref = $(this).find("[required]");

        $(ref).each(function(){
            if ( $(this).val() == '' )
            {
                alert("Please fill up the form.");

                $(this).focus();

                e.preventDefault();
                return false;
            }
        });

        if($('#project_format').val() == 0){
            str = $('#new_format_name').val();
            if(!str.replace(/\s+/g, '')){
                alert("Please enter a valid format name.");
                return false;
            }
        }

        var dateFrom = $('#start_at').val();
        var dateTo = $('#end_at').val();
        var outOfRange = false;

//        $('.stageSchedules .hidden input').each(function(event,data){
//            dateCheck = $(this).val();
//
//            if(dateCheck){
//                console.log(dateFrom+' >= '+dateCheck+' <= '+dateTo);
//                d1 = dateFrom.split("/");
//                d2 = dateTo.split("/");
//                c = dateCheck.split("/");
//
//                from = new Date(d1[2], parseInt(d1[1])-1, d1[0]);  // -1 because months are from 0 to 11
//                to   = new Date(d2[2], parseInt(d2[1])-1, d2[0]);
//                check = new Date(c[2], parseInt(c[1])-1, c[0]);
//
//                if(!(check >= from && check <= to))
//                    outOfRange = true;
//            }
//        });

//        if(outOfRange){
//            alert('Some stages are out of range.');
//            return false;
//        }

        return true;
    });

    {{--$('#addProjectForm').on('submit',function(e){--}}

        {{--e.preventDefault();--}}
            {{--e.stopPropagation();--}}

        {{--var form = $(this);--}}
        {{--url = form.attr('action');--}}

        {{--if($('#view_project_id').val())--}}
            {{--url = '{{ route('api.projects.update') }}';--}}

        {{--processFlag = 1;--}}


        {{--data = form.serialize();--}}

        {{--if(processFlag) {--}}
            {{--processFlag = 0;--}}

            {{--$.ajax({--}}
              {{--type: "POST",--}}
              {{--url: url,--}}
              {{--data: data,--}}
              {{--success: function(response){--}}
                {{--changesMade = 0;--}}

                {{--console.log(response);--}}
                {{--if(response.status==200) {--}}
                    {{--$(".add-project").offcanvas('hide');--}}
                    {{--$(".plan-project").offcanvas('show');--}}

{{--//                    refreshProjects(response.data.items);--}}

                    {{--$('#project_id').val(response.data.project.id);--}}
                    {{--$('#project_name').text(response.data.project.name);--}}

                    {{--start_at = moment(response.data.project.start_at).format('MM/DD/Y');--}}
                    {{--end_at = moment(response.data.project.end_at).format('MM/DD/Y');--}}

                    {{--$('#start_at').val(start_at);--}}
                    {{--$('#end_at').val(end_at);--}}

                    {{--attachDatePickerListeners(end_at,start_at);--}}

                    {{--if(response.data.project.stages){--}}
                        {{--$.each(response.data.project.stages, function(key,value){--}}
                            {{--start_at = moment(value.start_at).format('MM/DD/Y');--}}
                            {{--end_at = moment(value.end_at).format('MM/DD/Y');--}}

                            {{--$('input[name="stage['+value.stage_id+'][start_at]"]').val(start_at);--}}
                            {{--$('input[name="stage['+value.stage_id+'][end_at]"]').val(end_at);--}}
                            {{--$('input[name="stage['+value.stage_id+'][time]"]').val(value.time);--}}
                        {{--});--}}
                    {{--}--}}

                    {{--$('#generatePDFLink').attr('href','{{ route('api.projects.pdf') }}?id='+response.data.project.id);--}}

                    {{--$('#overlay-modal').unbind('click');--}}
                    {{--$('#overlay-modal').on('click',function(){--}}

                        {{--if(changesMade){--}}
                            {{--$.confirm({--}}
                                {{--confirmButton: 'Yes',--}}
                                {{--cancelButton: 'Cancel',--}}
                                {{--title: 'Confirm Action',--}}
                                {{--content: 'Are you sure you want to exit without saving your changes?',--}}
                                {{--confirm: function(){--}}

                                    {{--jQuery('.navmenu-fixed-right.in').offcanvas('hide');--}}
                                    {{--$('#overlay-modal').fadeOut(10);--}}
                                    {{--$('#spinner').hide();--}}
                                {{--},--}}
                                {{--cancel: function(){--}}

                                {{--}--}}
                            {{--});--}}
                        {{--} else {--}}

                            {{--jQuery('.navmenu-fixed-right.in').offcanvas('hide');--}}
                            {{--$('#overlay-modal').fadeOut(10);--}}
                            {{--$('#spinner').hide();--}}
                        {{--}--}}
                    {{--});--}}

                    {{--$('.plan-project .ui-button-text').on('click',function(){--}}
                        {{--changesMade = 1;--}}
                    {{--});--}}

                    {{--$('.plan-project .numbersOnly').on('change',function(){--}}
                        {{--changesMade = 1;--}}
                    {{--});--}}

                {{--}--}}

                {{--doAlert(response);--}}

              {{--},--}}
              {{--done : function (){--}}


                {{--processFlag = 1;--}}
              {{--}--}}
            {{--});--}}
        {{--}--}}
    {{--});--}}

    function refreshProjects(data){
        var projectList = "";
        var projectStages = "";
        $.each(data, function(key,format){

            if(format.projects.length){

                projectList += '<li class="category">'+
                                '<a href="#">'+format.name+'</a>'+
                              '</li>';

                projectStages +=  '<ul class="stages blank"></ul>';

                $.each(format.projects, function(key,project){
                    projectList += '<li><a href="#" data-project-id="'+project.id+'" class="viewProjectLink">'+project.name+'</a></li>';
                });
            }
        });
        $('#project-list').html('').append(projectList);
        attachViewProjectListener();
    }

//    $('#pLanguage').multiselect({
//        buttonText: function(options, select) {
//            console.log(select[0].length);
//            if (options.length === 0) {
//                return 'None selected';
//            }
//            if (options.length === select[0].length) {
//                return 'All selected ('+select[0].length+')';
//            }
//            else if (options.length >= 4) {
//                return options.length + ' selected';
//            }
//            else {
//                var labels = [];
//                console.log(options);
//                options.each(function() {
//                    labels.push($(this).val());
//                });
//                return labels.join(', ') + '';
//            }
//        }
//    });


    $('#complete-bt').on('click',function(e) {

        e.preventDefault();
        var projectId = $(this).attr('data-project-id');

        $.ajax({
            type: "GET",
            url: '{{ route('api.projects.complete') }}?id='+projectId,
            success: function(response){
                console.log(response);
                $('a[href=#'+projectId+']').remove();
                $('a[data-project-id='+projectId+']').closest('li').remove();
                jQuery('.add-project').offcanvas('hide');
                $('#overlay-modal').fadeOut(400);
                location.reload();
            },
            complete : function(){
            }
        });

        $('#viewModal .scrollableArea').attr('style','width:'+367*20+'px !important');

        e.stopPropagation();
    });


    $('#delete-bt').on('click',function(e) {
        e.preventDefault();
        var projectId = $(this).attr('data-project-id');

        $('#deleteModal').modal('show');
        $('#addProjectModal').modal('hide');
        $('#deleteModal input[name=id]').val(projectId);
    });

    $('#deleteTrigger').on('click',function() {
        if($('#deleteConfirmInput').val()=="yes"){
            deleteProject();
        } else {
            $('#deleteModal .yes').css('color','red');
        }
    });

    $('#deleteCancel').on('click',function() {
        $('#deleteModal').modal('hide');
        $('#addProjectModal').modal('show');
    });


    function deleteProject(){
        url = "{{ route('api.projects.delete') }}";
        data   = $('#deleteModal form').serialize();
        deleteProcessFlag = 1;

        if(deleteProcessFlag) {
            deleteProcessFlag = 0;

            $.ajax({
                type: "POST",
                url: '{{ route('api.projects.delete') }}',
                data: data,
                success: function(response){
                    window.location.reload();

                },
                done : function (){
                    deleteProcessFlag = 1;
                }
            });
        }
    }

        $('#planProjectForm').on('submit',function(e){
            var form = $(this);
            processFlag = 1;


            if($('#view_project_id').val()){
                url = '{{ route('api.projects.stages-calendar.update') }}';
                $('#planProjectForm').attr('action',url);
            } else {
                url = '{{ route('api.projects.stages.store') }}';
                $('#planProjectForm').attr('action',url);
            }

//
//            e.preventDefault();
//                e.stopPropagation();
//
//            data = form.serialize();
//            url = form.attr('action');
//
//            if(processFlag) {
//                processFlag = 0;
//
//                $.ajax({
//                  type: "POST",
//                  url: url,
//                  data: data,
//                  success: function(response){
//
//                    console.log(response);
//                    if(response.status==200) {
//                        $(".add-project").offcanvas('hide');
//                        $(".plan-project").offcanvas('show');
//                        refreshProjects(response.data);
//                    }
//
//                    doAlert(response);
//                  },
//                  done : function (){
//                    processFlag = 1;
//                  }
//                });
//            }
        });

        $('#hidePlan').on('click',function(){
            $(".plan-project").offcanvas('hide');
            $(".add-project").offcanvas('show');
        });


        $('#generateExcelLink').on('click',function(){
            $.ajax({
                type: "GET",
                url: '{{ route('api.projects.excel') }}',
                success: function(response){

//                    if(response.status==200) {
//                        $(".add-project").offcanvas('hide');
//                        $(".plan-project").offcanvas('show');
//                        refreshProjects(response.data);
//                    }
//
//                    doAlert(response);
                },
                done : function (){
                    processFlag = 1;
                }
            });

        });
    </script>
@endsection