<!-- Modal -->
<div id="printModal" class="modal fade" data-keyboard="false" role="dialog">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center" style="width: 200px">
            <!-- Modal content-->
            <div class="modal-content">
                {!! Form::open(['route'=>'calendar.year.print']) !!}
                {{--{!! Form::open(['url'=>'http://tapiapp.com/app/calendar/year-print']) !!}--}}
                <div class="modal-header border-bottom-gray">
                    <h4 class="modal-title text-left"><span id="projectModalTitle">Print Options</span></h4>
                </div>
                <div class="modal-body clearfix">
                    <div class="row">
                        <div class="col-md-12 calendar-header year">
                            <div class="side-form">
                                <?php
                                    $filters = [
                                        'project' => 'Project Name',
                                        'client' => 'Client',
                                        'format' => 'Format',
                                        'archive' => 'Archived',
                                    ]
                                ?>
                                <p>Filter</p>
                                {!! Form::select('filter',$filters, 'project' ,['id'=>'pLanguage','class'=>'selectpicker']) !!}

                                <?php
                                $year = [];
                                    for($x=2015;$x<2100;$x++)
                                        $years[$x] = $x;
                                ?>
                                <p>Year</p>
                                {!! Form::select('year',$years, isset($_GET['year']) ? $_GET['year'] : \Carbon\Carbon::now()->setTimezone(Auth::user()->company->timezone)->format('Y'),['id'=>'pLanguage','class'=>'selectpicker']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="pull-left width-full border-top-gray padding-t-10">
                        <button class="btn btn-default pull-right padding-r-0" data-dismiss="modal">Cancel</button>
                        <button id="printTrigger" class="createbt btn btn-default pull-right border-right-gray">Print</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>


{{--<div class="add-project navmenu-default navmenu-fixed-right offcanvas-md offcanvas-lg" style="">--}}
{{--<h3 id="project-details-title">Add Project</h3>--}}
{{--{!! Form::open(['id'=>'addProjectForm','route'=>'api.projects.store']) !!}--}}
{{--{!! Form::hidden('project_id',null,['id'=>'view_project_id']) !!}--}}
{{--<div class="side-form">--}}
{{--Client--}}
{{--<span class="pull-right" style="font-size: 10px">Color</span>--}}
{{--<div class="form-group relative clientData">--}}
{{--{!! Form::input('text','client[name]',null,['id'=>'clients','class'=>'form-control','required']) !!}--}}
{{--<input id="clientColor" class="form-control" name='client[color]' value='#3355cc'/>--}}
{{--</div>--}}

{{--Brand Name--}}
{{--{!! Form::input('text','brand[name]',null,['class'=>'form-control','required']) !!}--}}
{{--Project Name--}}
{{--{!! Form::input('text','project[name]',null,['class'=>'form-control','required']) !!}--}}
{{--Format--}}
{{--<div class="gray margin-b-7">--}}
{{--{!! Form::select('project[format_id]', $formats ? $formats : [] , null,['class'=>'selectpicker','required']) !!}--}}
{{--</div>--}}
{{--Version / Issue No.--}}
{{--{!! Form::input('text','project[version]',1,['class'=>'form-control','required']) !!}--}}

{{--Language--}}
{{--<div class="gray margin-b-7">--}}
{{--{!! Form::select('language_ids[]',$languages, [3] ,['id'=>'pLanguage','class'=>'selectpicker','multiple']) !!}--}}
{{--</div>--}}
{{--</div>--}}
{{--{!! Form::submit('Plan',['class'=>'t-pink link-bt link-color margin-b-0 pull-right dir-arrow right-pink','id'=>"showPlan"]) !!}--}}
{{--{!! Form::close() !!}--}}

{{--<button class="t-pink link-bt link-color margin-b-0 pull-left padding-l-0" data-project-id="" id="complete-bt">Move to Archive</button>--}}
{{--<button class="t-pink link-bt link-color margin-b-0 pull-left padding-l-0" data-project-id="" id="delete-bt"> | Delete</button>--}}
{{--</div>--}}

@section('add-project-js')
    <script>

        /** SEND CREATE REQUEST **/

        $('#printModal').on('submit',function(e){
            var ref = $(this).find("[required]");

            $(ref).each(function(){
                if ( $(this).val() == '' )
                {
                    alert("Please fill up the form.");

                    $(this).focus();

                    e.preventDefault();
                    return false;
                }
            });

            if($('#project_format').val() == 0){
                str = $('#new_format_name').val();
                if(!str.replace(/\s+/g, '')){
                    alert("Please enter a valid format name.");
                    return false;
                }
            }

            var dateFrom = $('#start_at').val();
            var dateTo = $('#end_at').val();
            var outOfRange = false;

//        $('.stageSchedules .hidden input').each(function(event,data){
//            dateCheck = $(this).val();
//
//            if(dateCheck){
//                console.log(dateFrom+' >= '+dateCheck+' <= '+dateTo);
//                d1 = dateFrom.split("/");
//                d2 = dateTo.split("/");
//                c = dateCheck.split("/");
//
//                from = new Date(d1[2], parseInt(d1[1])-1, d1[0]);  // -1 because months are from 0 to 11
//                to   = new Date(d2[2], parseInt(d2[1])-1, d2[0]);
//                check = new Date(c[2], parseInt(c[1])-1, c[0]);
//
//                if(!(check >= from && check <= to))
//                    outOfRange = true;
//            }
//        });

//        if(outOfRange){
//            alert('Some stages are out of range.');
//            return false;
//        }

            return true;
        });

        {{--$('#addProjectForm').on('submit',function(e){--}}

        {{--e.preventDefault();--}}
        {{--e.stopPropagation();--}}

        {{--var form = $(this);--}}
        {{--url = form.attr('action');--}}

        {{--if($('#view_project_id').val())--}}
        {{--url = '{{ route('api.projects.update') }}';--}}

        {{--processFlag = 1;--}}


        {{--data = form.serialize();--}}

        {{--if(processFlag) {--}}
        {{--processFlag = 0;--}}

        {{--$.ajax({--}}
        {{--type: "POST",--}}
        {{--url: url,--}}
        {{--data: data,--}}
        {{--success: function(response){--}}
        {{--changesMade = 0;--}}

        {{--console.log(response);--}}
        {{--if(response.status==200) {--}}
        {{--$(".add-project").offcanvas('hide');--}}
        {{--$(".plan-project").offcanvas('show');--}}

        {{--//                    refreshProjects(response.data.items);--}}

        {{--$('#project_id').val(response.data.project.id);--}}
        {{--$('#project_name').text(response.data.project.name);--}}

        {{--start_at = moment(response.data.project.start_at).format('MM/DD/Y');--}}
        {{--end_at = moment(response.data.project.end_at).format('MM/DD/Y');--}}

        {{--$('#start_at').val(start_at);--}}
        {{--$('#end_at').val(end_at);--}}

        {{--attachDatePickerListeners(end_at,start_at);--}}

        {{--if(response.data.project.stages){--}}
        {{--$.each(response.data.project.stages, function(key,value){--}}
        {{--start_at = moment(value.start_at).format('MM/DD/Y');--}}
        {{--end_at = moment(value.end_at).format('MM/DD/Y');--}}

        {{--$('input[name="stage['+value.stage_id+'][start_at]"]').val(start_at);--}}
        {{--$('input[name="stage['+value.stage_id+'][end_at]"]').val(end_at);--}}
        {{--$('input[name="stage['+value.stage_id+'][time]"]').val(value.time);--}}
        {{--});--}}
        {{--}--}}

        {{--$('#generatePDFLink').attr('href','{{ route('api.projects.pdf') }}?id='+response.data.project.id);--}}

        {{--$('#overlay-modal').unbind('click');--}}
        {{--$('#overlay-modal').on('click',function(){--}}

        {{--if(changesMade){--}}
        {{--$.confirm({--}}
        {{--confirmButton: 'Yes',--}}
        {{--cancelButton: 'Cancel',--}}
        {{--title: 'Confirm Action',--}}
        {{--content: 'Are you sure you want to exit without saving your changes?',--}}
        {{--confirm: function(){--}}

        {{--jQuery('.navmenu-fixed-right.in').offcanvas('hide');--}}
        {{--$('#overlay-modal').fadeOut(10);--}}
        {{--$('#spinner').hide();--}}
        {{--},--}}
        {{--cancel: function(){--}}

        {{--}--}}
        {{--});--}}
        {{--} else {--}}

        {{--jQuery('.navmenu-fixed-right.in').offcanvas('hide');--}}
        {{--$('#overlay-modal').fadeOut(10);--}}
        {{--$('#spinner').hide();--}}
        {{--}--}}
        {{--});--}}

        {{--$('.plan-project .ui-button-text').on('click',function(){--}}
        {{--changesMade = 1;--}}
        {{--});--}}

        {{--$('.plan-project .numbersOnly').on('change',function(){--}}
        {{--changesMade = 1;--}}
        {{--});--}}

        {{--}--}}

        {{--doAlert(response);--}}

        {{--},--}}
        {{--done : function (){--}}


        {{--processFlag = 1;--}}
        {{--}--}}
        {{--});--}}
        {{--}--}}
        {{--});--}}

        function refreshProjects(data){
            var projectList = "";
            var projectStages = "";
            $.each(data, function(key,format){

                if(format.projects.length){

                    projectList += '<li class="category">'+
                            '<a href="#">'+format.name+'</a>'+
                            '</li>';

                    projectStages +=  '<ul class="stages blank"></ul>';

                    $.each(format.projects, function(key,project){
                        projectList += '<li><a href="#" data-project-id="'+project.id+'" class="viewProjectLink">'+project.name+'</a></li>';
                    });
                }
            });
            $('#project-list').html('').append(projectList);
            attachViewProjectListener();
        }

        //    $('#pLanguage').multiselect({
        //        buttonText: function(options, select) {
        //            console.log(select[0].length);
        //            if (options.length === 0) {
        //                return 'None selected';
        //            }
        //            if (options.length === select[0].length) {
        //                return 'All selected ('+select[0].length+')';
        //            }
        //            else if (options.length >= 4) {
        //                return options.length + ' selected';
        //            }
        //            else {
        //                var labels = [];
        //                console.log(options);
        //                options.each(function() {
        //                    labels.push($(this).val());
        //                });
        //                return labels.join(', ') + '';
        //            }
        //        }
        //    });


        $('#complete-bt').on('click',function() {
            var projectId = $(this).attr('data-project-id');

            $.ajax({
                type: "GET",
                url: '{{ route('api.projects.complete') }}?id='+projectId,
                success: function(response){
                    $('a[href=#'+projectId+']').remove();
                    $('a[data-project-id='+projectId+']').closest('li').remove();
                    jQuery('.add-project').offcanvas('hide');
                    $('#overlay-modal').fadeOut(400);
                },
                complete : function(){
                }
            });

            $('#viewModal .scrollableArea').attr('style','width:'+367*20+'px !important');
        });


        $('#delete-bt').on('click',function(e) {
            e.preventDefault();
            var projectId = $(this).attr('data-project-id');

            $('#deleteModal').modal('show');
            $('#addProjectModal').modal('hide');
            $('#deleteModal input[name=id]').val(projectId);
        });

        $('#deleteTrigger').on('click',function() {
            if($('#deleteConfirmInput').val()=="yes"){
                deleteProject();
            } else {
                $('#deleteModal .yes').css('color','red');
            }
        });

        $('#deleteCancel').on('click',function() {
            $('#deleteModal').modal('hide');
            $('#addProjectModal').modal('show');
        });


        function deleteProject(){
            url = "{{ route('api.projects.delete') }}";
            data   = $('#deleteModal form').serialize();
            deleteProcessFlag = 1;

            if(deleteProcessFlag) {
                deleteProcessFlag = 0;

                $.ajax({
                    type: "POST",
                    url: '{{ route('api.projects.delete') }}',
                    data: data,
                    success: function(response){
                        window.location.reload();

                    },
                    done : function (){
                        deleteProcessFlag = 1;
                    }
                });
            }
        }

        $('#planProjectForm').on('submit',function(e){
            var form = $(this);
            processFlag = 1;


            if($('#view_project_id').val()){
                url = '{{ route('api.projects.stages-calendar.update') }}';
                $('#planProjectForm').attr('action',url);
            } else {
                url = '{{ route('api.projects.stages.store') }}';
                $('#planProjectForm').attr('action',url);
            }

//
//            e.preventDefault();
//                e.stopPropagation();
//
//            data = form.serialize();
//            url = form.attr('action');
//
//            if(processFlag) {
//                processFlag = 0;
//
//                $.ajax({
//                  type: "POST",
//                  url: url,
//                  data: data,
//                  success: function(response){
//
//                    console.log(response);
//                    if(response.status==200) {
//                        $(".add-project").offcanvas('hide');
//                        $(".plan-project").offcanvas('show');
//                        refreshProjects(response.data);
//                    }
//
//                    doAlert(response);
//                  },
//                  done : function (){
//                    processFlag = 1;
//                  }
//                });
//            }
        });

        $('#hidePlan').on('click',function(){
            $(".plan-project").offcanvas('hide');
            $(".add-project").offcanvas('show');
        });


        $('#generateExcelLink').on('click',function(){
            $.ajax({
                type: "GET",
                url: '{{ route('api.projects.excel') }}',
                success: function(response){

//                    if(response.status==200) {
//                        $(".add-project").offcanvas('hide');
//                        $(".plan-project").offcanvas('show');
//                        refreshProjects(response.data);
//                    }
//
//                    doAlert(response);
                },
                done : function (){
                    processFlag = 1;
                }
            });

        });
    </script>
@endsection