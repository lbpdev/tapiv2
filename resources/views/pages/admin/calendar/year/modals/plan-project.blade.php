<div class="plan-project navmenu-default navmenu-fixed-right offcanvas-md offcanvas-lg" style="">

<a href="#" class="link-color dir-arrow left-teal" id="hidePlan">Back</a>

<h3 class="project-title">Plan Project <a href="{{ route('api.projects.pdf') }}" id="generatePDFLink"><img src="{{ asset('public/images/icons/Save-Icon.png') }}" class="pull-right" height="27" style="margin-top: -5px;"></a></h3>
<h3 id="project_name">Project Name</h3>
    {!! Form::open(['id'=>'planProjectForm','route'=>'api.projects.stages.store']) !!}
        {!! Form::hidden('project_id', null , ['id'=>'project_id']) !!}
        <div class="side-form small clearfix margin-b-10">
             {{--{!! Form::input('text','date',null ,['id'=>'sessionDate','required' ,'id' => 'daterange' ,'placeholder'=>'Session date','class' => 'form-control']) !!}<br>--}}
             <div class="col-md-6 padding-0 ">
             Project Duration:
                {!! Form::input('text','start_at', isset($conference) ? $conference->start_at : \Carbon\Carbon::now()->setTimezone(Auth::user()->companyTimezone)->format('m/d/Y') , ['id'=>'start_at','require','placeholder'=>'Session Start','required','class' => 'project-start show form-control']) !!}
             </div>
             <div class="col-md-6 padding-r-0">
             &nbsp;
                {!! Form::input('text','end_at', isset($conference) ? $conference->end_at : \Carbon\Carbon::now()->setTimezone(Auth::user()->companyTimezone)->addDays(7)->format('m/d/Y') , ['readonly','id'=>'end_at','placeholder'=>'Session End','required','class' => 'project-end form-control']) !!}
             </div>
        </div>
        Internal Project Stages:
        @if($stagesEx)
            @foreach($stagesIn as $stage)
                {!! Form::hidden('stage['.$stage->id.'][stage_id]', $stage->id) !!}
                <div class="side-form small clearfix" style="background-color: {{ $stage->color }}">
                    <div class="col-md-6 padding-0">
                    {{ $stage->name }}
                       {!! Form::input('text','stage['.$stage->id.'][start_at]', null , ['id'=>'stage['.$stage->id.'][start_at]','placeholder'=>'Start','class' => 'daterangepicker project-start show form-control']) !!}
                    </div>
                    <div class="col-md-6 padding-r-0">
                        &nbsp;
                       {!! Form::input('text','stage['.$stage->id.'][end_at]', null , ['readonly','id'=>'stage['.$stage->id.'][end_at]','placeholder'=>'End','class' => 'project-end form-control']) !!}
                    </div>
                    @if($stage->hasTime)
                        <div class="col-md-6 padding-l-10 padding-t-5 text-right padding-r-0">
                            Allocate Hours
                        </div>
                        <div class="col-md-4 padding-r-10">
                            {!! Form::input('number','stage['.$stage->id.'][time]', null , ['id'=>'stage['.$stage->id.'][start_at]','min'=>0,'placeholder'=>'','class' => 'numbersOnly form-control','maxlength'=>3,'max'=>999]) !!}
                        </div>
                    @endif
                </div>
            @endforeach
        @else
            Please add a project stage at the settings.
        @endif

        @if(count($stagesEx))
        <hr>
        External Project Stages:
            @foreach($stagesEx as $stage)
            {!! Form::hidden('stage['.$stage->id.'][stage_id]', $stage->id) !!}
                <div class="side-form small clearfix" style="background-color: {{ $stage->color }}">
                    <div class="col-md-6 padding-0">
                    {{ $stage->name }}
                       {!! Form::input('text','stage['.$stage->id.'][start_at]', null , ['id'=>'stage['.$stage->id.'][start_at]','placeholder'=>'Start','class' => 'daterangepicker project-start show form-control','max'=>999]) !!}
                    </div>
                    <div class="col-md-6  padding-r-0">
                        &nbsp;
                       {!! Form::input('text','stage['.$stage->id.'][end_at]', null , ['readonly','id'=>'stage['.$stage->id.'][end_at]','placeholder'=>'End','class' => 'project-end form-control']) !!}
                    </div>
                    @if($stage->hasTime)
                        <div class="col-md-6 padding-0 padding-r-10">
                            {!! Form::input('number','stage['.$stage->id.'][time]', null , ['id'=>'stage['.$stage->id.'][start_at]','min'=>0,'placeholder'=>'Allocate Time','class' => 'numbersOnly form-control','maxlength'=>3]) !!}
                        </div>
                        <div class="col-md-6 padding-l-10 padding-t-10">
                            Allocate Time
                        </div>
                    @endif
                </div>
            @endforeach
        @endif

        <div class="row text-center">
            {!! Form::submit('Save',['class'=>'link-color link-bt t-pink margin-b-20 text-center full-width all-caps']) !!}
        </div>
    {!! Form::close() !!}
</div>

@section('plan-project-js')
    <script>
        /** SEND CREATE REQUEST **/



    </script>
@endsection