<link rel="stylesheet" media="all"  href="http://tapiapp.com/app/public/plugins/jasny-bootstrap/css/jasny-bootstrap.min.css">
<link rel="stylesheet" media="all"  href="http://tapiapp.com/app/public/css/calendar.css">
<link rel="stylesheet" media="all"  href="http://tapiapp.com/app/public/css/global.css">
<link rel="stylesheet" media="all"  href="http://tapiapp.com/app/public/css/bootstrap.no-print.min.css">

<style>
    img {
        position: relative;
        z-index: 99999;
    }
    .dates .days li {
        width: 20px !important;
    }
    .project-list li a {
        padding-left: 7px;
    }
    .stages {
        width: 300%;
        /*margin-left: -1200px;*/
        background-image: url('http://tapiapp.com/app/public/images/calendar-line-20x100.png') !important;
        background-position: left center;
    }

    #print-bt {
        float: left;
        margin: 1px 0;
        color: #fff;
        background-color: #bf1c73;
        float: left;
        width: auto;
        border: 1px solid #bf1c73;
        padding: 1px 20px;
        border-radius: 15px;
        margin-top: 10px;
        font-size: 20px;
        text-transform: uppercase;
    }

    .stages li {
        border: 0 !important;
        opacity: .8 !important;
        z-index: 99999 !important;
    }

    #stage-legend .a {
        width: auto;
        float: left;
    }

    #stage-legend .a p {
        width: auto !important;
        margin-right: 15px;
    }
    .logo-placeholder {
        display: block;
        height: 82px;
        background-color: #ccc;
        padding: 30px 38px;
        color: #989898;
        margin-top: 5px;
        margin-left: 0;
        width: 150px;
        font-size: 14px;
        text-align: center;
    }


    .project-list {
        border-right: 1px solid #999999;
        margin-bottom: 0;
    }

    .project-list {
        float: left;
        width: 100%;
        margin-top: -1px;
    }


    #stage-list-modal {
        border-right: 0;
        margin-bottom: 0;
    }

    #stage-modal-bars {
        overflow: hidden;
        border-bottom: 1px solid rgba(0,0,0,0.2);
        background-color: #ccc;
    }

    .project-list li, #stage-list-modal li {
        width: 100%;
        display: inline-block;
        float: left;
        border-top: 1px solid #999999;
        height: 20px;
        overflow: hidden;
        padding-left: 0;
    }

    .project-list li {
        border-top: 1px solid rgba(0,0,0,0.2);
    }


    .project-list li:first-child {
        border-top: 0;
    }

    .project-list li:last-child {
        border-bottom: 1px solid rgba(0,0,0,0.2) !important;
    }

    #stage-list-modal li {
        width: 100%;
        display: inline-block;
        float: left;
        border-right: 1px solid #999999;
        border-bottom: 0;
        height: 20px;
        font-size: 12px;
        line-height: 20px;
        padding-left: 10px;
        text-align: left;
        border-bottom: 1px solid rgba(0,0,0,0.2);
        border-top: 0;
    }

    .project-list li:last-child {
        padding-bottom: 0;
        border-top: 1px solid rgba(0,0,0,0.2);
        border-bottom: 0;
    }

    .project-list li a, #stage-list-modal li a {
        color: #4c4c4c;
        width: 100%;
        font-size: 12px;
        line-height: 20px;
        height: 20px;
        display: block;
    }

    .project-list li a:hover, #stage-list-modal a:hover {
        text-decoration: none;
    }

    .project-list .category {
        display: block;
        padding-bottom: 0;
        padding-top: 19px;
        height: 40px;
        text-transform: uppercase;
        color: #cf2982;
        padding-left: 0;
    }

    .project-list .category a {
        color: #cf2982;
        line-height: 26px;
    }

    .project-list .stages li {
        margin: 10px 0;
        height: 20px;
        position: absolute;
        opacity: .7;
    }

    #projectStages li , .projectStages li {
        list-style: none;
        float:left;
        width: 100%;
    }

    .projectStages li.viewStagesLink {
        height: 20px;
    }

    #projectStages ul , .projectStages ul {
        list-style: none;
        float:left;
    }

    .projectStages {
        overflow: hidden;
    }

</style>

{{--<style type="text/css" media="print">--}}
    {{--@page { size: landscape; }--}}
    {{--.page--}}
    {{--{--}}
        {{--filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=3);--}}
    {{--}--}}
    {{--.page { width: @container-desktop !important; }--}}

    {{--* {--}}
        {{---webkit-print-color-adjust: exact;--}}
        {{--opacity: 1;--}}
    {{--}--}}
    {{--.no-print {--}}
        {{--display: none;--}}
    {{--}--}}
{{--</style>--}}

<div class="page" style="width:1474px;max-width:1474px;" id="mainReport">
    <div style="width:1443px;max-width:1443px;">
        <div class="col-md-12 calendar-header year padding-b-20" id="calendar">

            <div class="row padding-t-10">

                <div class="col-md-6 text-left paddring-r-0">

                    @if(Request::path()!="settings")
                        <a class="" href="#">
                    @endif
                            @if($company->logo)
                                <img src="http://tapiapp.com/app/public/uploads/logo/{{ $company->logo }}" id="lb-logo" height="82" class="pull-left">
                            @else
                                <div id="lb-logo" class="logo-placeholder">Your Logo</div>
                            @endif

                    @if(Request::path()!="settings")
                        </a>
                    @endif
                </div>
                <div class="col-md-6 text-right">
                    <a class="padding-b-0 padding-r-0" href="#">
                        <img class="pull-right" src="http://tapiapp.com/app/public/images/Tapi-Logo.png" id="lb-logo" height="96">
                    </a>
                </div>
            </div>

            <div class="row" id="mama">

                <?php
                    $currentYear = isset($_POST['year']) ? $_POST['year'] : \Carbon\Carbon::now()->setTimezone(Auth::user()->companyTimezone)->format('Y');
                ?>

                <div class="col-md-12 margin-b-10 margin-t-10 text-center">{{ Auth::user()->company->name }} {{ $currentYear }}</div>
                    <div class="padding-r-0 pull-left" style="width: 240px">
                    <div id="projectsHeading" class="text-center" style="font-size: 14px;padding-top: 20px;background-color: #ececec;height: 60px;border: 1px solid rgba(0,0,0,0.2);">
                        Projects
                    </div>
                    <ul id="project-list" class="clearfix project-list">
                        {!! $projectsInFormat !!}
                    </ul>
                </div>
                <div class="pull-left relative top-header padding-l-0" id="embeddedYear" style="overflow: hidden;width: 1180px">
                    <div id="dateHeading">
                    <h2 class="year relative margin-t-0">

                        <span id="currentYear">{{ $currentYear }}</span>
                    </h2>
                        <div class="date-top relative">
                            <ul class="dates pull-left">
                                <?php
                                $activeWeek = false;
                                $activeMonth = false;
                                $divider = ( $firstWorkDay == "Mon" ? 7 : 6 );
                                ?>
                                @foreach($weeksPerMonth as $yearIndex=>$year)
                                    @foreach($year as $week)

                                        <li class="" style="width: {{$week['weeks']*20}}px">
                                            {{--<div style="width: {{$week['weeks']*50}}px" class="month {{ \Carbon\Carbon::now()->setTimezone(Auth::user()->companyTimezone)->format('F')==$week['name'] ? 'activeMonth' : '' }}">{{ $week['name'] }}</div>--}}
                                            <div style="width: {{$week['weeks']*20}}px" class="month">{{ $week['name'] }}</div>
                                            {{--<div class="week">Week</div>--}}

                                            <?php
                                            if((\Carbon\Carbon::now()->setTimezone(Auth::user()->companyTimezone)->format('F')==$week['name']) && (\Carbon\Carbon::now()->setTimezone(Auth::user()->companyTimezone)->format('Y')==$yearIndex)){
                                                $activeMonth = true;
                                                $currentDay = \Carbon\Carbon::now()->setTimezone(Auth::user()->companyTimezone)->format('d');
                                            } else {
                                                $activeMonth = false;
                                            }
                                            ?>

                                            <ul class="days clearfix" style="width: {{$week['weeks']*20}}px">
                                                @for($x=1;$x<=$week['weeks'];$x++)
                                                    @if($activeMonth)
                                                        @if(!$activeWeek)
                                                            <?php
                                                            if(intval($currentDay) / $divider < 1)
                                                                $activeWeek = 1;
                                                            else
                                                                $activeWeek = round(intval($currentDay) / $divider) + 1;
                                                            ?>
                                                        @endif
                                                    @endif
                                                    <li {{ $activeMonth && $activeWeek == $x ? 'class=currentWeek' : '' }}>{{ $x }}</li>
                                                @endfor

                                            </ul>
                                        </li>

                                    @endforeach
                                @endforeach

                            </ul>
                        </div>
                    </div>
                        <div id="projectStages">
                            {!! $projectsInFormatHtml !!}
                        </div>
                    </div>
                </div>
                <!--- ROW ---->

            <div id="stage-legend" class="col-md-12 stage-legendC" >
                @foreach($stagesList as $stage)
                    <span class="a" href="#"><span style="background-color: {{ $stage->color }};"></span><p class="name" style="width:90px">{{ $stage->name }}</p></span>
                @endforeach
            </div>
            </div>
        </div>
        <div class="col-md-12 tex" >
            http://tapiapp.com
        </div>
    </div>
</div>

{{--<button id="print-bt" onclick="window.print()" class="no-print">Print</button>--}}
        <!-- JavaScripts -->
<script src="http://tapiapp.com/app/public/js/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>
<script src="https://html2canvas.hertzen.com/build/html2canvas.js"></script>
<script>
    $(function(){
        $('.date-top').css('width',$('#embeddedYear .dates .days li').length * 20 + 1 +'px');
        $('#projectStages').css('width',$('#embeddedYear .dates .days li').length * 20 + 1 +'px');
    });
//
//    function addStageFillers(){
//        toAdd = $('.fillers').length;
//
//        for(x=0;x<toAdd;x++)
//            $('#projectStages').append('<ul class="stages"></ul>');
//    }
//    addStageFillers();

    var rows = 0;
    var totalHeight = 0;
    var tableIndex = 0;

    $('#project-list li').each(function(){
        totalHeight += $(this).outerHeight();

        if((totalHeight / 750) > 1){
            tableIndex++;
            totalHeight = 0;

            $($('.calendar-header')[0]).append('<div class="row" id="table-'+tableIndex+'" style="margin-top:250px"></div>');

            legend = $('#stage-legend').clone();
            legend.appendTo('.calendar-header');
            $($('#table-'+tableIndex)[0]).append('<div class="padding-r-0 pull-left" style="width: 240px;"><ul class="project-list" id="project-list-'+tableIndex+'" class="clearfix"></ul>');
            $($('#table-'+tableIndex)[0]).append('<div class="pull-left relative top-header padding-l-0" id="embeddedYear-'+tableIndex+'" style="overflow: hidden;width: 1180px"></div><div class="projectStages" id="projectStages-'+tableIndex+'" style="width: 1181px;float:left"></div>');

            $('#projectsHeading').clone().appendTo('#project-list-'+tableIndex);
            $('#dateHeading').clone().appendTo('#embeddedYear-'+tableIndex);
        }
    });



    totalHeight= 0;
    tableIndex = 0;
    loopIndex = 0;

    $('#project-list li').each(function(){
        totalHeight += $(this).outerHeight();

        if(totalHeight >= 750){
            tableIndex++;
            totalHeight = 0;
            if($(this).hasClass('category'))
                $(this).css('height','42px !important');
        }

        if(tableIndex > 0){
            $(this).appendTo('#project-list-'+tableIndex);
        }
        loopIndex++;
    });

    totalHeight= 0;
    tableIndex = 0;
    $('#projectStages .row-item').each(function(){
        totalHeight += $(this).outerHeight();

        if(totalHeight >= 750){
            tableIndex++;
            totalHeight = 0;
        }

        if(tableIndex > 0){
            $(this).appendTo('#projectStages-'+tableIndex);
        }
    });

//
//    function createCanvas(){
//
//        html2canvas($('#calendar'), {
//            onrendered: function(canvas) {
//                canvas.id = 'Do';
//
//                document.getElementById('mainReport').appendChild(canvas);
//
//                html2canvas($("#Do"), {
//                    onrendered: function(canvas) {
//
//                        setpixelated(canvas.getContext('2d'));
//                        var imgData = canvas.toDataURL(
//                                'image/png');
//
//                        var doc = new jsPDF('landscape','mm');
//                        doc.addImage(imgData, 'PNG', 1, 1);
//                        doc.save('Report.pdf');
////                        window.open(doc, '_blank', 'fullscreen=yes');
//
////                                window.open(doc);
//
//                        $("#Do").remove();
//                    },
//                });
//            },
//            background : '#fff',
//            width: 1440,
//            height:3000
//        });
//    }

//    function setpixelated(context){
//        context['imageSmoothingEnabled'] = true;       /* standard */
//        context['mozImageSmoothingEnabled'] = true;    /* Firefox */
//        context['oImageSmoothingEnabled'] = true;      /* Opera */
//        context['webkitImageSmoothingEnabled'] = true; /* Safari */
//        context['msImageSmoothingEnabled'] = true;     /* IE */
//    }
</script>
