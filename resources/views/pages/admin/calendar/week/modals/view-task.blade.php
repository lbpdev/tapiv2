
<!-- Modal -->
<div id="view-user-task-modal" data-keyboard="false" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 400px;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Project Information <span id="viewTaskUrgency"></span></h4>
            </div>
            <div class="modal-body detail-sheet">
                <div class="clearfix">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="form-group">
                                <div class="label">Client:</div><div class="project-client">GSK</div>
                            </div>
                            <div class="form-group">
                                <div class="label">Brand:</div><div class="project-brand">n/a</div>
                            </div>
                            <div class="form-group">
                                <div class="label">Project:</div><div class="project-name">The Patient</div>
                            </div>
                            <div class="form-group">
                                <div class="label">Format:</div><div class="project-format">Journal</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="form-group">
                                <div class="label">Version:</div><div class="project-version">7</div>
                            </div>
                            <div class="form-group">
                                <div class="label">Language:</div><div class="project-language">English</div>
                            </div>
                            <div class="form-group">
                                <div class="label">Project timeline:</div>
                            </div>
                            <div class="form-group">
                                <div class="label"></div><div class="project-duration">17/10/2016 - 27/11/2016</div>
                            </div>
                        </div>
                    </div>
                    <hr class="margin-b-10">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="form-group">
                                    <div class="label">Task:</div><div class="project-type">Article edit</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="form-group">
                                    <div class="label">Time:</div><div class="project-time">1 hour 15 minutes</div>
                                </div>
                            </div>
                        </div>
                    <hr class="margin-b-10">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="form-group">
                                    <div class="label">Notes:</div>
                                    <div class="project-notes">Istempero mo volupta vendus et omnis et verchilit ad que natur? Totatem ditatet omni ute vernam aborum natenis ut exeria dolor sunt laccum accupta esequo mi, aut aciatur sundaeribus, exceria</div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
