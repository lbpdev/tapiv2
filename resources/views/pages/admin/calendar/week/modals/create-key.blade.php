
<!-- Modal -->
<div id="create-key-modal" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 300px;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Key</h4>
            </div>
            {!! Form::open(['id'=>'addKeyFrom','route'=>'api.keys.store']) !!}
            <div class="modal-body">
                <div class="col-md-12">

                    Department
                    {!! Form::select('department_id',$departmentsList,$currentDepartment ? $currentDepartment->id : null,['id'=>'department_id','class'=>'margin-b-3 medium selectpicker','required']) !!}

                    Name
                    {!! Form::input('text','name',null,['id'=>'clients','class'=>'form-control','required','maxlength'=>'14']) !!}

                    Code
                    {!! Form::input('text','code',null,['class'=>'form-control','required','maxlength'=>'3']) !!}
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Create',['class'=>'btn btn-default','id'=>"showPlan"]) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>

            {!! Form::close() !!}
        </div>

    </div>
</div>


@section('create-key-js')
    <script>
        /** SEND CREATE REQUEST **/

        $('#addKeyFrom').on('submit',function(e){

            var form = $(this);
            processFlag = 1;

            e.preventDefault();
                e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                  type: "POST",
                  url: url,
                  data: data,
                  success: function(response){

                    if(response.status==200) {

                        if(response.data.length){

                            $('#key'+response.data[0].department_id).html('');

                            $.each(response.data, function (key,value) {
                                option = "<a href=#><b>"+value.code+"</b> "+value.name+"</a><br>";
                                $('#key'+value.department_id).append(option);
                            });
                        }
                        $( "#department_id").trigger('change');
                        $('#key_id').selectpicker('refresh');
                        $('#create-key-modal').modal('hide');

                    }

                    doAlert(response);
                  },
                  error: function(response){
                      doAlert({'message':'Error'});
                  },
                  done : function (){
                    processFlag = 1;
                  }
                });
            }
        });


    </script>
@endsection
