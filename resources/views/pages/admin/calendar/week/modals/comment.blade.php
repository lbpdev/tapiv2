
<!-- Modal -->
<div id="view-comment-modal" class="modal fade" role="dialog">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog  vertical-align-center" style="width: 300px;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">View Comment</h4>
                </div>
                <div class="modal-body clearfix">
                    <div class="col-md-12">
                        <div class="row" id="commentBox" style="line-height:15px;">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
