<!-- Modal -->
<div id="create-task-modal" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 400px;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header padding-b-0 border-bottom-gray margin-b-5">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Tasks</h4>
            </div>
            <div class="modal-body clearfix padding-t-0 padding-b-0">
                <div class="row">
                    {!! Form::open(['id'=>'addTaskForm','route'=>'api.tasks.store']) !!}
                    {!! Form::hidden('project_id',null,['id'=>'project_id']) !!}
                    {!! Form::hidden('task_id',null,['id'=>'create_task_id']) !!}

                    <div class='col-md-6'>
                        Client  <p class="pull-right margin-b-0 client-color"><span class="client-color-circle pull-right" id="clientCircleColor" style="background-color: #3355cc"></span><span class="pull-right">&nbsp;Colour</span></p>
                        <div class="relative">
                            {!! Form::input('text','project[client]',null,['placeholder'=>'Client','id'=>'projectClient','readonly','class'=>'form-control','required']) !!}
                            <input id="clientColor" class="hidden form-control" name='client[color]' value='#3355cc'/>
                        </div>

                    </div>

                    <div class="col-md-6">
                        Version / Issue No.
                        {!! Form::input('text','project[version]',null,['placeholder'=>'Version','readonly','id'=>'projectVersion','class'=>'form-control','required']) !!}
                    </div>

                    <div class='col-md-6'>
                        Brand Name
                        {!! Form::input('text','project[brand][name]',null,['placeholder'=>'Brand Name','id'=>'projectBrand','readonly','class'=>'form-control','required']) !!}
                    </div>

                    <div class="col-md-6">
                        Language
                        {!! Form::input('text','project[language]',null,['placeholder'=>'Language','readonly','id'=>'projectLanguage','readonly','class'=>'form-control','required']) !!}
                    </div>

                    <div class="col-md-6">
                        Format
                        {!! Form::input('text','project[format]',null,['readonly','id'=>'projectFormat','readonly','class'=>'form-control','required']) !!}
                    </div>

                    <div class="col-md-6">
                        Project Duration
                        {!! Form::input('text','project[duration]',null,['readonly','id'=>'projectDuration','readonly','class'=>'form-control','required']) !!}
                        {!! Form::hidden('deadline',null,['readonly','id'=>'deadline','readonly','class'=>'form-control','required']) !!}
                        {!! Form::hidden('date',$_GET['day'],['readonly','id'=>'deadline','readonly','class'=>'form-control','required']) !!}
                    </div>

                    <div class='col-md-6'>
                        Project Name
                        {!! Form::select('projectSelect',[],null ,['placeholder'=>'Select a project','id'=>'projectSelect','class'=>'medium selectpicker']) !!}
                        {{--{!! Form::input('text','project[selection]',null,['placeholder'=>'Enter project name here','id'=>'projectSelection','class'=>'form-control']) !!}--}}
                    </div>

                    <div class="col-md-3">
                        Urgent
                        <?php
                            $urgency = [
                                    1 => 'Yes',
                                    0 => 'No'
                            ]
                        ?>

                        {!! Form::select('is_urgent',$urgency,0,['id'=>'projectUrgent','class'=>'selectpicker caretize tiny','required']) !!}

                    </div>

                    <div class="col-md-12 margin-t-5" id="date-error">
                        <div class=" alert alert-danger">
                            Date input exceeds project deadline.
                        </div>
                    </div>

                    <div class="col-md-12 margin-b-10">
                        <hr>
                    </div>
                    {{--<div class="col-md-6">--}}
                    {{--Task Type:--}}
                    <?php
                    //                        $task_types = [
                    //                                0 => 'Existing Project',
                    //                                1 => 'Extra Task'
                    //                        ]
                    ?>

                    {{--                        {!! Form::select('is_extra',$task_types,0,['placeholer'=>'Task Type','id'=>'is_extra','class'=>'selectpicker','required']) !!}--}}
                    {{--</div>--}}

                    <div class="col-md-6">
                        Task type
                        {!! Form::select('key_id',[],null,['placeholder'=>'Key','id'=>'key_id','class'=>'medium selectpicker']) !!}

                        <div class="new_key relative">
                            {!! Form::input('text','new_key_name',null,['placeholder'=>'New Type','id'=>'key_name','class'=>'temp-hide form-control','maxlength'=>20]) !!}
                            <span class="close temp-hide"></span>
                        </div>
                        @if(Auth::user()->role->slug == 'admin')
                            {{--<span class="title" data-toggle="modal" style="float:right;font-size:11px;cursor:pointer" data-target="#create-key-modal">Add a Key</span>--}}
                        @endif
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-4">
                            <div class="row">
                                Hours:
                                {!! Form::input('number','hours',1,['id'=>'taskHours','class'=>'numbersOnly form-control','required','min'=>"0", 'max'=>"999",'maxlength'=>'2']) !!}
                            </div>
                        </div>
                        <div class="col-md-8 padding-r-0">
                            Minutes:
                            {!! Form::input('number','minutes',00,['id'=>'taskMins','class'=>'numbersOnly form-control small','required','min'=>"0", 'max'=>"59",'maxlength'=>'2']) !!}
                        </div>
                    </div>

                    <?php
                    $departmentsList['general'] = 'General';
                    ?>
                    {!! Form::hidden('department_id',$currentDepartment ? $currentDepartment->id : null,['id'=>'department_id','class'=>'medium selectpicker','required']) !!}

                    <div class="col-md-12 margin-b-4">
                        <hr>
                    </div>

                    <div class="col-md-12">
                        Notes
                        {!! Form::textarea('note',null,['placeholder'=>'(Optional) ','class'=>'form-control','rows'=>'5','maxlength'=>'130','id'=>'taskNotes']) !!}
                    </div>

                    <div class="col-md-12 margin-b-4">
                        <hr>
                    </div>

                </div>
            </div>
            <div class="modal-footer padding-t-4">
                {!! Form::submit('Save',['class'=>'btn btn-default border-right','id'=>"createTask"]) !!}
                <button type="button" class="btn btn-default padding-r-0" data-dismiss="modal">Cancel</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@section('create-task-js')
    <script>

        /** SEND CREATE REQUEST **/

        $('#addTaskForm').on('submit',function(e){

            submitBt = $(this).closest('.modal').find('input[type=submit]');
            $(submitBt).attr('disabled','disabled');

            var form = $(this);
            processFlag = 1;

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if($('#key_id').val() == 0 || $('#key_id').val() == null){
                str = $('#key_name').val();
                if(!str.replace(/\s+/g, '')){
                    alert("Please enter a task type name.");
                    $(submitBt).removeAttr('disabled');
                    return false;
                }
            }

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){

                        if(response.status==200) {

                            if(response.data) {

                                if(response.data.id){

                                    urgency = assets+'/images/icons/Star-Icon-Gray.png';

                                    updateTaskTime(response.data.id);

                                    if(response.data.is_urgent>0)
                                        urgency = assets+'/images/icons/Star-Icon-Gray-Important.png';

                                    $(".calendar li[data-task-id="+response.data.id+"]").find('.urgent_icon').attr('src',urgency);
                                    $(".calendar li[data-task-id="+response.data.id+"]").find('.user-task-icon .title').html(response.data.key.name);

                                    if(response.data.merged){
                                        dup_ids = response.data.merged;

                                        for (var i = 0, len = dup_ids.length; i < len; i++) {
                                            $('#task-'+dup_ids[i]).remove();
                                        }
                                    }
                                }
                                else{
                                    if(currentDepartment){
                                        $('#tasksList').prepend(response.data);
                                        attachTaskEditListener();
                                        attachTaskDeleteListener();
                                    }
                                }


                                $('#key_id').closest('.bootstrap-select').show();
                                $('#key_name').val('').hide();
                                $( "#department_id").trigger('change');

                                setTimeout(function(){ sortTasks(); resizeSideBars(); },300);
                                $('#create-task-modal').modal('hide');
                            }
                        }

                        doAlert(response);

                        $(submitBt).removeAttr('disabled');
                    },
                    done : function (){
                        processFlag = 1;
                    }
                });
            }
        });

        var availableProjects = getAvailableProjects();

        function getAvailableProjects(){

            $.ajax({
                type: "GET",
                url: '{{ route('api.projects.getJson') }}',
                success: function(response){
                    availableProjects = JSON.parse(response);
                    updateProjectList();
                }
            });
        }

        function updateProjectList(){
            $.each(availableProjects, function(key,row){
                el = '<option value="'+key+'">'+row['label']+'</option>';
                $('#projectSelect').append(el);
            });
            $('#projectSelect').selectpicker('refresh');
        }

        $( "#projectSelect" ).on('change',function(){
            index = $(this).val();

            if(availableProjects[index]){
                $("#project_id").val(availableProjects[index].value);
                $('#projectClient').val(availableProjects[index].client);
                $('#projectBrand').val(availableProjects[index].brand);
                $('#projectVersion').val(availableProjects[index].version);
                $('#projectLanguage').val(availableProjects[index].language);
                $('#projectFormat').val(availableProjects[index].format);
                $('#projectDuration').val(availableProjects[index].duration);
                $('#deadline').val(availableProjects[index].deadline);
                checkDates();

                $( "#clients" ).val(availableProjects[index].color);
                $('#clientColor').val(availableProjects[index].color).attr('value',availableProjects[index].color).trigger('change');
                $('.sp-preview-inner').css('background-color',availableProjects[index].color);
                $(this).closest('.side-form').find('.color-circle').first().css('background-color',availableProjects[index].color);
            } else {
                $('#addTaskForm')[0].reset();
                $("#project_id").val('');
            }
        });

        //$( "#projectSelect" ).autocomplete({
//            source: availableProjects,
//            select: function(event,ui){
//                $("#project_id").val(ui.item.value);
//                $("#projectSelection").val(ui.item.label);
//                $('#projectClient').val(ui.item.client);
//                $('#projectBrand').val(ui.item.brand);
//                $('#projectVersion').val(ui.item.version);
//                $('#projectLanguage').val(ui.item.language);
//                $('#projectFormat').val(ui.item.format);
//                $('#projectDuration').val(ui.item.duration);
//                $('#deadline').val(ui.item.deadline);
//                checkDates();
//
//                $( "#clients" ).val(ui.item.color);
//                $('#clientColor').val(ui.item.color).attr('value',ui.item.color).trigger('change');
//                $('.sp-preview-inner').css('background-color',ui.item.color);
//                $(this).closest('.side-form').find('.color-circle').first().css('background-color',ui.item.color);
//
//                return false;
//            },focus: function (event, ui) {
//                this.value = ui.item.label;
//                // or $('#autocomplete-input').val(ui.item.label);
//
//                // Prevent the default focus behavior.
//                event.preventDefault();
//                // or return false;
//            }
//        });

        $('#projectSelection').on('change', function(){

        });

        $( "#department_id").on('change', function(){

            $.ajax({
                type: "GET",
                url: '{{ route('api.departments.getkeys') }}?id='+$(this).val(),
                success: function(response){
                    $('#key_id').html('');

                    $.each(response, function(index,value){
                        $('#key_id').append('<option value="'+value.id+'">'+value.name+'</option>');
                    });

                    if(response.length < 1){

                        $('#key_id').closest('.bootstrap-select').hide();
                        $('#key_name').show();

                    } else {

                        $('#key_id').append('<option value="0">-Add Task Type-</option>');
                        $('#key_id').selectpicker('refresh').trigger('change');

                    }

                },
                complete : function(){
                }
            });

        });

        $('#key_id').on('change',function(){
            if($(this).val()==0){
                $(this).closest('.bootstrap-select').hide();
                $('#key_name').show().focus();
            }
        });

        $('#date').datepicker({
            dateFormat : 'mm/dd/yy',
            onSelect : function(){
                checkDates()
            },
            beforeShowDay: function(date) {
                if(workDays){
                    var day = date.getDay();

                    return [($.inArray( day, workDays ) >= 0 ? true : false), ''];
                }
            }
        });

        $('#projectDeadline').on('change',function(){ checkDates(); });


        //date

        function checkDates(){

            assignedDate = moment('{{ \Carbon\Carbon::parse($_GET['day'])->setTimezone(Auth::user()->companyTimezone) }}');
            deadline = moment($('#deadline').val());

            if(assignedDate<=deadline){
                $('#createTask').show();
                $('#date-error').hide();
            }
            else{
                $('#createTask').hide();
                $('#date-error').show();
            }
        }

    </script>
@endsection