
<!-- Modal -->
<div id="assign-task-modal" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 300px;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header margin-b-5">
                <h4 class="modal-title">Book Task</h4>
            </div>
            <div class="modal-body clearfix padding-b-0 padding-t-0">
                {!! Form::open(['id'=>'assignTaskForm','route'=>'api.tasks.assign']) !!}
                {!! Form::hidden('task_id',null,['id'=>'task_id']) !!}
                {!! Form::hidden('user_task_id',null,['id'=>'user_task_id']) !!}
                {!! Form::hidden('moved_user_task_id',null,['id'=>'moved_user_task_id']) !!}
                {!! Form::hidden('user_id',null,['id'=>'user_id']) !!}
                {!! Form::hidden('date_assigned',null,['id'=>'task_assign_date']) !!}
                {!! Form::hidden('order',null,['id'=>'task_assign_order']) !!}
                {!! Form::hidden('',null,['id'=>'dropId']) !!}

                <div class="gray clearfix">
                    <div class="col-md-12">
                        <div class='col-md-12 padding-r-0 padding-l-0 '>
                            Project:
                            {!! Form::input('text','project[selection]',null,['readonly','placeholder'=>'Project Name','id'=>'assign-project','class'=>'project form-control','required']) !!}
                        </div>

                        <div class="col-md-6">
                            <div class="row">
                                {!! Form::input('text','project[client]',null,['readonly','placeholder'=>'Client','id'=>'assign-client','readonly','class'=>'client form-control','required']) !!}
                            </div>
                        </div>

                        <div class="col-md-6 padding-r-0">
                            {!! Form::input('text','project[brand]',null,['readonly','placeholder'=>'Brand','readonly','id'=>'assign-brand','class'=>'brand form-control','required']) !!}
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                {!! Form::input('text','project[deadline]',null,['readonly','placeholder'=>'Deadline','id'=>'assign-deadline','readonly','class'=>'deadline form-control','required']) !!}
                            </div>
                        </div>

                        <hr style="border-color: #999;">

                        <div class="col-md-12">
                            <div class="row">
                                Task:
                                {!! Form::input('text','',null,['readonly','placeholer'=>'Task Type','id'=>'assign-task-type','class'=>'is_extra form-control','required']) !!}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="row">
                                {!! Form::input('text','department_id', null,['readonly','id'=>'assign-department','class'=>'department form-control','required']) !!}
                            </div>
                        </div>

                        <div class="col-md-6 padding-r-0">
                            {!! Form::input('text','key',null,['readonly','placeholder'=>'Key','id'=>'assign-key','class'=>'key form-control','required']) !!}
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                Remaining time: <span class="assign-hours" style="font-weight: normal;"></span><span class="assign-mins" style="font-weight: normal;"></span>
                                {!! Form::hidden('hours',1,['readonly','class'=>'assign-hours form-control','required','min'=>"0", 'max'=>"40"]) !!}
                                {!! Form::hidden('minutes',00,['readonly','class'=>'assign-mins form-control','required','min'=>"0", 'max'=>"59"]) !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="row">
                            &nbsp; <br>
                            Assign time:
                            </div>
                        </div>

                        <div class="col-md-4">
                        Hours:
                        {!! Form::input('number','hours',1,['id'=>'hours','class'=>'numbersOnly form-control','required','max'=>'24' ]) !!}
                        </div>

                        <div class="col-md-4">
                        Minutes:
                        {!! Form::input('number','minutes',00,['id'=>'minutes','class'=>'numbersOnly form-control','required','max'=>'59']) !!}
                        </div>
                    </div>
                    <div class="row">
                        Note: <br>
                        {!! Form::textarea('note',null,['placeholder'=>'(Optional) ','class'=>'form-control','rows'=>'4','id'=>'assign-notes']) !!}

                        {{--Assign to whole week:--}}
                        {{--{!! Form::checkbox('whole_week',null,['placeholder'=>'Notes (Optional) ','class'=>'form-control']) !!}--}}
                        <hr>
                    </div>
                    {{--<div class="row text-left">--}}
                        {{--Urgent:--}}
                        {{--<img src="{{ asset('public/images/icons/task-icons/Important.png') }}" height="22" class="urgentImage">--}}
                        {{--{!! Form::input('checkbox','is_urgent',null,['id'=>'is_urgent','class'=>'urgentRadio']) !!}--}}
                        {{--&nbsp;--}}
                        {{--<hr class="margin-b-0" style="border-color: #999;">--}}
                    {{--</div>--}}
                </div>
            </div>
            <div class="modal-footer padding-t-4">
                {!! Form::hidden('department_id',$currentDepartment ? $currentDepartment->id : null) !!}
                {!! Form::submit('Assign',['class'=>'btn btn-default border-right','id'=>"showPlan"]) !!}
                <button type="button" class="btn btn-default padding-r-0" data-dismiss="modal" id="cancel-assign-task">Cancel</button>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>


@section('assign-task-js')
    <script>
        /** SEND CREATE REQUEST **/

        $('#assignTaskForm').on('submit',function(e){

            submitBt = $(this).closest('.modal').find('input[type=submit]');
            $(submitBt).attr('disabled','disabled');

            var form = $(this);
            processFlag = 1;

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){

                        if(response.status==200) {

                            dID = $('#dropId').val();

                            parent = $('#'+dID).closest('.column');
                            ul = $('#'+dID).closest('.ui-sortable');

                            if(response.data.merged_user_task){
                                $('#'+response.data.merged_user_task.id).replaceWith(response.data.element);
                                $('#'+dID).remove();
                            }
                            else
                                $('#'+dID).replaceWith(response.data.element);

                            attachCompleteIconListener();
                            attachUTaskCollapseEvent();
                            attachViewUserTaskListener();

                            updateColumnCompleteProgress(ul);
                            updateColumnBookedProgress(ul);

                            if($('#moved_user_task_id').val()!=""){
                                updateColumnCompleteProgress($('#'+$('#moved_user_task_id').val()));
                                updateColumnBookedProgress($('#'+$('#moved_user_task_id').val()));
                            }

                            updateTaskTime($('#task_id').val());
                            $('#moved_user_task_id').val('');

                            addDeleteUserTaskEvent();
                            addUserTaskEditEvent();

                            $('#assign-task-modal').modal('hide');
                            $('#create-task-modal').modal('hide');

                            updateTaskSize($('#'+response.data.user_task_id));

                            attachViewUserTaskListener();
                        }

                        doAlert(response);

                        $(submitBt).removeAttr('disabled');

                    },
                    done : function (){
                        processFlag = 1;
                    }
                });
            }
        });

    </script>
@endsection