@section('leave-style')
@endsection

<!-- Modal -->
<div id="leave-modal" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 520px;">

    {!! Form::open(['id'=>'leaveForm','route'=>'api.leaves.store']) !!}
    {!! Form::hidden('user_id', isset($_GET['user']) ? $_GET['user'] : Auth::user()->id ,['id'=>'user_id']) !!}
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <div class="col-md-12">
                    <div class="row">
                        <h4 class="modal-title pull-left margin-t-0">Leave</h4>

                    </div>
                </div>
            </div>
            <div class="modal-body clearfix">
                <div class="col-md-12 margin-b-0">
                    <div class="row">
                        <div class="col-md-12 margin-b-0 leaves-table">
                            <div class="row">
                                <table id="currentLeaves" class="" width="100%">
                                    <tr>
                                        <th width=="30%">From</th>
                                        <th width=="30%">To</th>
                                        <th width="15%">Type</th>
                                        <th width="15%">Duration</th>
                                        <th width="10%"></th>
                                    </tr>
                                    @foreach($currentUser->leaves as $leave)
                                        <tr class="data-row" id="leave-{{ $leave->id }}">
                                            <td>{{ $leave->from->format('d/m/y') }}</td>
                                            <td>{{ $leave->to->format('d/m/y') }}</td>
                                            <td>{{ $leave->type }}</td>
                                            <td>{{ $leave->time }} {{ $leave->time > 0 ? ( $leave->time / 60 ) . ' Hours' : 'Whole day' }}</td>
                                            <td style="text-align: right;">
                                                <a href="#" data-leave-id="{{ $leave->id }}" class="delete-leave delete-icon link-icon margin-t-3"></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                        <input type="hidden" name="start" id="leaveDateStart" required/>
                        <input type="hidden" name="end" id="leaveDateEnd" required />
                        <div id="TxtStrtDate" style="font-style: normal"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 pull-left margin-r-0">
                            <div class="row">
                            <?php
                                for($x=1;$x<$workHours;$x++)
                                    $partial_leave_options[$x] = $x . ( $x > 1 ? ' Hours' : ' Hour' );

                                $partial_leave_options[0] = 'Whole day';
                            ?>
                            {!! Form::select('time',$partial_leave_options, 0,['class'=>'pull-right small slim-select selectpicker']) !!}
                            </div>
                        </div>
                        <div class="col-md-4 pull-right margin-r-0">
                            <div class="row">
                            {!! Form::select('type',['annual'=>'Annual Leave' ,'sick'=>'Sick Leave','maternal'=>'Maternal Leave','holiday'=>'Holiday'],$currentDepartment ? $currentDepartment->id : null,['id'=>'department_id','class'=>'pull-right small slim-select selectpicker','required']) !!}
                            </div>
                        </div>
                    </div>
                </div>


                <div class="modal-footer col-md-12 margin-b-0 padding-l-0 padding-r-0 padding-b-0">
                    <div class="row">
                        {!! Form::submit('Submit',['class'=>'btn btn-default','id'=>"showPlan"]) !!}
                        <button type="button" class="btn btn-default" data-dismiss="modal" id="cancel-assign-task">Close</button>
                    </div>
                </div>
            </div>
        </div>

    {!! Form::close() !!}
    </div>
</div>


@section('leave-js')
    <script>

        $('#sickLeaveLink').on('click',function(e){

            submitBt = $(this).closest('.modal').find('input[type=submit]');
            $(submitBt).attr('disabled','disabled');

            e.preventDefault();

            $.ajax({
                    type: "GET",
                    url: '{{ route('api.leaves.get') }}?id={{ $currentUser->id }}',
                    success : function(response){

                        if(response)
                            loadUserLeaves(response);

                        $(submitBt).removeAttr('disabled');
                    },

                    done : function (){
                        processFlag = 1;
                    }
                });

            $('#leave-modal').modal('show');
        });


        $('#TxtStrtDate').dateRangePicker({
            inline:true,
            stickyMonths: true,
            container: '#TxtStrtDate',
            alwaysOpen:true,
//            beforeShowDay: function(t)
//            {
//                if(workDays){
//                    var day = t.getDay();
//
//                    var valid = $.inArray( day, workDays ) >= 0 ? true : false;
////                    var valid = !(t.getDay() == 0 || t.getDay() == 6);  //disable saturday and sunday
//                    var _class = '';
//                    var _tooltip = valid ? '' : 'weekends are disabled';
//                    return [valid,_class,_tooltip];
//                }
//            }
        }).bind('datepicker-change',function(event,obj)
        {
            /* This event will be triggered when second date is selected */
            $('#leaveDateStart').val(moment(obj.date1).format('YYYY/MM/DD'));
            $('#leaveDateEnd').val(moment(obj.date2).format('YYYY/MM/DD'));
            // obj will be something like this:
            // {
            // 		date1: (Date object of the earlier date),
            // 		date2: (Date object of the later date),
            //	 	value: "2013-06-05 to 2013-06-07"
            // }
        });

        $('#leaveForm').on('submit',function(e){
            var form = $(this);
            processFlag = 1;

            e.preventDefault();
            e.stopPropagation();

            if($('.last-date-selected').length == 0){
                alert('Please select a start and end date');
                return false;
            }

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){

                        if(response.status==200) {

                            if(response.data.dates.length > 0){
                                console.log(response.data);
                                $.each(response.data.dates,function(e,i){

                                    column = $('#'+i).closest('.column');
                                    if(response.data.time[e]<1){
                                        $('#'+i).addClass('past').addClass('leave');
                                        $('#'+i).closest('.day-tasks').addClass('past');
                                        column.addClass('past');
                                        $('#'+i).removeClass('droppable');

                                        type = response.data.type[e];
                                        switch(type) {
                                            case 'annual':
                                                type = 'Annual Leave';
                                                break;
                                            case 'maternal':
                                                type = 'Maternal Leave';
                                                break;
                                            case 'sick':
                                                type = 'Sick Leave';
                                                break;
                                            case 'holiday':
                                                type = 'Holiday';
                                                break;
                                            default:
                                                type = 'Annual Leave';
                                        }

                                        $('#'+i).closest('.column').find('.push').text(type);

                                        column.find('.push-all').removeClass('hidden');
                                        column.find('.push-all').addClass('leave');

                                        if(column.find('li.ui-state-default').length>0)
                                            column.find('.form').removeClass('temp-hide');
                                        else
                                            column.find('.form').addClass('temp-hide');

                                        column.find('.partial-leave').addClass('hidden').attr('data-hours',0);


                                    } else {
                                        column.find('.partial-leave').removeClass('hidden').text('Leave '+response.data.time[e]/60+'hr').attr('data-hours',response.data.time[e]/60);
                                    }
                                });
                            }

                            loadUserLeaves(JSON.parse(response.data.leaves));

                            refreshCharts();

                            $('#leave-modal').modal('hide');
                        }

                        doAlert(response);
                    },
                    done : function (){
                        processFlag = 1;
                    }
                });
            }
        });


        function addDeleteUserLeaveEvent(){

            $('.delete-leave').unbind('click');
            $('.delete-leave').on('click',function(e){
                e.preventDefault();

                deleteUserLeave($(this).attr('data-leave-id'));
            });
        }
        addDeleteUserLeaveEvent();


        function loadUserLeaves(data){
            $('#currentLeaves .data-row').remove();

            $.each(data,function(e,i){
                tr = $("<tr>", {id: "leave-"+i.id,class:'data-row'});
                tr.append('<td>'+moment(i.from).format('MM-DD-Y')+'</td>');
                tr.append('<td>'+moment(i.to).format('MM-DD-Y')+'</td>');
                tr.append('<td>'+i.type+'</td>');
                tr.append('<td>'+ ( i.time > 0 ? ( i.time / 60 ) + ' Hours' : 'Whole day' ) +'</td>');
                tr.append('<td style="text-align: right;"><a href="#" data-leave-id="'+i.id+'" class="delete-leave delete-icon link-icon margin-t-3"></a></td>');
                $('#currentLeaves').append(tr);
            });
            addDeleteUserLeaveEvent();
        }

        function deleteUserLeave(leaveId){

            $.ajax({
                type: "GET",
                url: '{{ route('api.leaves.delete') }}?id='+leaveId,
                success : function(response){
                    if(response){

                        $('#leave-'+leaveId).remove();

                        if(response.data.open.length > 0){

                            $.each(response.data.open,function(e,i){
                                targetEl = $('#'+i);

                                $(targetEl).removeClass('leave');

                                if(!$(targetEl).hasClass('past'))
                                    $(targetEl).addClass('droppable');

                                $(targetEl).closest('.column').find('.push-all').removeClass('leave');
                                $(targetEl).closest('.active-day').find('.past').removeClass('past');
                                $(targetEl).closest('.active-day').removeClass('past');
                                $(targetEl).closest('.active-day .droptrue').addClass('droppable');

                                index = $(targetEl).closest('.column').find('.chart-pie').attr('data-index');
                                value = $(targetEl).closest('.column').find('.chart-pie').attr('data-percent');

                                $(targetEl).closest('.column').find('.partial-leave').addClass('hidden').attr('data-hours',0);

                                updatePie(index,value);
                            });

                            $.each(response.data.closed,function(e,i){
                                targetEl = $('#'+i.date);

                                if(i.type=="leave")
                                    $(targetEl).closest('.column').find('.push h2').text('On Leave');
                                else
                                    $(targetEl).closest('.column').find('.push h2').text('Holiday');
                            });

                        }

                        attachDropSortListener();
                    }
                },
                done : function (){
                    processFlag = 1;
                }
            });
        }


    </script>
@endsection