
<!-- Modal -->
<div id="edit-user-task-modal" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 300px;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Task</h4>
            </div>
            <div class="modal-body clearfix padding-b-0">
                {!! Form::open(['id'=>'editUserTaskForm','route'=>'api.utasks.update']) !!}
                {!! Form::hidden('id',null,['id'=>'id']) !!}

                <div class="gray clearfix">
                    <div class="col-md-12">
                        <div class='col-md-12 padding-r-0 padding-l-0 '>
                            Project:
                            {!! Form::input('text','project[selection]',null,['readonly','placeholder'=>'Project Name','class'=>'project form-control','required']) !!}
                        </div>

                        <div class="col-md-6">
                            <div class="row">
                                {!! Form::input('text','project[client]',null,['readonly','placeholder'=>'Client','readonly','class'=>'client form-control','required']) !!}
                            </div>
                        </div>

                        <div class="col-md-6 padding-r-0">
                            {!! Form::input('text','project[brand]',null,['readonly','placeholder'=>'Brand','readonly','class'=>'brand form-control','required']) !!}
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                {!! Form::input('text','project[deadline]',null,['readonly','placeholder'=>'Deadline','readonly','class'=>'deadline form-control','required']) !!}
                            </div>
                        </div>

                        <hr style="border-color: #999;">

                        <div class="col-md-12">
                            <div class="row">
                                Task:
                                {!! Form::input('text','',null,['readonly','placeholer'=>'Task Type','class'=>'is_extra form-control','required']) !!}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="row">
                                {!! Form::input('text','department', null,['readonly','class'=>'department form-control','required']) !!}
                                {!! Form::hidden('department_id',$currentDepartment ? $currentDepartment->id : null) !!}
                            </div>
                        </div>

                        <div class="col-md-6 padding-r-0">
                            {!! Form::input('text','key',null,['readonly','placeholder'=>'Key','class'=>'key form-control','required']) !!}
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                Remaining time: <span class="assign-hours" style="font-weight: normal;"></span><span class="assign-mins" style="font-weight: normal;"></span>
                                {!! Form::hidden('hours',1,['readonly','id'=>'assign-hours','class'=>'form-control','required','min'=>"0", 'max'=>"40"]) !!}
                                {!! Form::hidden('minutes',00,['readonly','id'=>'assign-mins','class'=>'form-control','required','min'=>"0", 'max'=>"59"]) !!}
                            </div>
                        </div>
                    </div>
                </div>


                    <div class="col-md-12 margin-t-10 margin-b-0">
                        <div class="row">
                            <div class="row">
                                <div class="col-md-4 padding-r-0">
                                    &nbsp; <br>
                                    Assign time:
                                </div>

                                <div class="col-md-4">
                                    Hours:
                                    {!! Form::input('number','hours',1,['id'=>'hours','class'=>'numbersOnly form-control','required','min'=>"0",'max'=>"24",'maxlength'=>'2']) !!}
                                </div>

                                <div class="col-md-4">
                                    Minutes:
                                    {!! Form::input('number','minutes',00,['id'=>'minutes','class'=>'numbersOnly form-control','required','min'=>"0",'max'=>"59",'maxlength'=>'2']) !!}
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="row">
                                    Notes:<br>
                                    {!! Form::textarea('note',null,['placeholder'=>'(Optional) ','class'=>'notes form-control','maxlength'=>'120','rows'=>'4']) !!}
                                    <hr>
                                </div>
                            </div>
                        </div>
                        {{--<div class="col-md-12">--}}
                            {{--<div class="row">--}}
                                {{--Urgent :--}}
                                {{--<img src="{{ asset('public/images/icons/task-icons/Important.png') }}" height="22" class="urgentImage">--}}
                                {{--{!! Form::input('checkbox','is_urgent',null,['id'=>'is_urgent','class'=>'urgentRadio']) !!}--}}
                                {{--&nbsp;--}}
                                {{--<hr class="margin-b-0" style="border-color: #999;">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
            <div class="modal-footer padding-t-4">
                {!! Form::submit('Update',['class'=>'btn btn-default border-right']) !!}
                <button type="button" class="btn btn-default padding-r-0" data-dismiss="modal" id="cancel-assign-task">Cancel</button>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>


@section('edit-task-js')
    <script>
        /** SEND CREATE REQUEST **/

        $('#editUserTaskForm').on('submit',function(e){

            submitBt = $(this).closest('.modal').find('input[type=submit]');
            $(submitBt).attr('disabled','disabled');

            var form = $(this);
            processFlag = 1;

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

//            if(parseInt($(form).find('#hours').val())+parseInt($(form).find('#minutes').val()) < 1){
//                alert('Please assign time');
//                return false;
//            }

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){

                        if(response.status==200) {
                            time = ( response.data.time / 60 ) >= 1 ? parseInt( response.data.time / 60 ) + "hr " : '';

                            if(( response.data.time) % 60)
                                time +=  ( response.data.time) % 60 + "min";

                            $('#'+response.data.id).find('.time').html(time);
                            $('#'+response.data.id).find('.user-task-icon').attr('data-time',response.data.time);
                            $('#'+response.data.id).find('.notes span').html(response.data.note);

                            if(response.data.task.is_urgent)
                                $('#'+response.data.id).find('.urgent_icon ').attr('src', '{{ asset('public/images/icons/Star-Icon-Gray-Important.png') }}');
                            else
                                $('#'+response.data.id).find('.urgent_icon ').attr('src','{{ asset('public/images/icons/Star-Icon-Gray.png') }}');

                            updateTaskTime(response.data.task.id);
                            updateColumnCompleteProgress($('#'+response.data.id));
                            updateColumnBookedProgress($('#'+response.data.id));

                            updateTaskSize($('#'+response.data.id));

                            $('#edit-user-task-modal').modal('hide');
                        }

                        doAlert(response);

                        $(submitBt).removeAttr('disabled');

                    },
                    done : function (){
                        processFlag = 1;
                    }
                });
            }
        });


    </script>
@endsection