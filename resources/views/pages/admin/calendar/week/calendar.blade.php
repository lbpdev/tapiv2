<div class="top clearfix">
    <div class="col-md-5">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="small margin-b-12">
                        {!! Form::input('text','currentDay',$_GET['day'],['id'=>'currentDay','class'=>'selectpicker']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-7">
        <div class="row text-right">
            {{--@if(Auth::user()->role->slug!='admin')--}}
                {{--@if(isset($_GET['user']))--}}
                    {{--@if($_GET['user']==Auth::user()->id)--}}
                        {{--<span class="title plus-icon margin-t-5" id="createTaskBt">Add an Extra Task</span>--}}
                    {{--@endif--}}
                {{--@endif--}}
            {{--@endif--}}
            @if($_GET['day']!=\Carbon\Carbon::now()->setTimezone(Auth::user()->companyTimezone)->format('d-m-Y'))
                <a class="pull-right link-text" href="{{ route('calendar.week') }}?user={{$currentUser->id}}&department={{$currentUser->department->name}}">Today</a>
            @endif
        </div>
    </div>
</div>

<?php $colHeight = 450; ?>
<div class="calendar">
    <div id="week-calendar">
        <div  class="scroller width-full clearfix">
            <div class="scrollable" style="{{ 'width: '.(7*180).'px' }};">

                @foreach($currentWeek as $index=>$week)
                <?php
                    $booked = 0;
                    $rowDay = \Carbon\Carbon::parse($week['day'])->setTimezone(Auth::user()->companyTimezone);
                    $currentDay = \Carbon\Carbon::now()->setTimezone(Auth::user()->companyTimezone);
                ?>

                <div data-date="{{ $week['day'] }}" class="pull-left column {{ !in_array( $rowDay->format('D'),Auth::user()->company->workDays) ? 'past' : ( \Carbon\Carbon::parse($week['day'])->diffInDays($currentDay,false) > 0 ? '' : 'active-day') }} {{ $week['on_leave'] && !isset($week['on_leave_time']) ? 'past leave' : '' }} {{ $rowDay->diffInDays($currentDay,false) > 0 ? 'past' : '' }} " style="width:180px" >

                    <div class="inactive-layer push-all {{ ( $week['on_leave'] && !isset($week['on_leave_time']) ) ? 'leave' : 'temp-hide' }}">
                        <div class="push">
                            @if(isset($week['is_holiday']))
                                <h2>{{ $week['is_holiday'] ? 'Holiday' : '' }}</h2>
                            @elseif(isset($week['leave_type']))
                                <?php
                                    switch($week['leave_type']){
                                        case 'annual':
                                            echo 'Annual Leave'; break;
                                        case 'sick':
                                            echo 'Sick Leave'; break;
                                        case 'maternal':
                                            echo 'Maternal Leave'; break;
                                        case 'holiday':
                                            echo 'Holiday'; break;
                                    }

                                ?>
                            @endif
                            <br>
                            <div class="form {{ count($week['tasks']) ? '' : 'temp-hide' }}">
                                <h3>Would you like to remove all booked tasks?</h3>
                                <button class="push">Yes. Remove all</button>
                                {{--<button class="cancel">No. <br>Do it manually.</button>--}}
                            </div>
                        </div>
                    </div>

                    <span class="weekDay">{!! $rowDay->format('d-m-y')==$currentDay->format('d-m-y') ? '<span id="todayCircle"></span>Today' : $rowDay->format('l') !!} <br><span class="weekDayNum"> {{ \Carbon\Carbon::parse($week['day'])->format('jS F Y') }}</span></span>
                    <div class="pull-left day-tasks clearfix {{ $week['on_leave'] && !isset($week['on_leave_time']) ? 'past ' : '' }} {{ $rowDay->diffInDays($currentDay,false) > 0 ? 'past' : '' }} " {{ $index+1 == count($currentWeek) ? '' : '' }}>

                        @if(!Auth::user()->isEmployee)
                            <ul id="{{ strtotime($week['day']) }}" class="droptrue {{ $week['on_leave'] && !isset($week['on_leave_time']) ? 'past ' : '' }} {{ $rowDay->diffInDays($currentDay,false) > 0 ? 'past' : ( $week['on_leave'] && !isset($week['on_leave_time']) || !in_array( $rowDay->format('D'),Auth::user()->company->workDays) ? '' : 'droppable' ) }} " data-date="{{ $week['day'] }}" data-user="{{ $currentUser->id }}">
                        @else
                            <ul id="{{ strtotime($week['day']) }}" class="droptrue {{ $week['on_leave'] && !isset($week['on_leave_time']) ? 'past ' : '' }} {{ $rowDay->diffInDays($currentDay,false) > 0 ? 'past' : '' }} " data-date="{{ $week['day'] }}" data-user="{{ $currentUser->id }}">
                        @endif
                            <?php
                                $countComplete = 0;
                            ?>
                            @foreach($week['tasks'] as $userTask)

                                <?php
                                    $rowColor = $userTask->task->project->client->color;
                                    $booked += $userTask->time;
                                    $taskHeight = floor(( ( $userTask->time / 60 ) / ( $workHours ) ) * $colHeight);
                                    $taskHeight = $taskHeight < 90 ? 91 : $taskHeight;
                                ?>
                                <li id="{{ $userTask->id }}" class="closed ui-state-default clearfix ui-sortable-handle" data-user-task-id="{{ $userTask->id }}" data-task-id="{{ $userTask->task->id }}" style="height:{{$taskHeight}}px;background-color:#fff !important;">

                                    <div class="color-bg" style="height:{{$taskHeight}}px;background-color: {{ $rowColor }} !important;"></div>
                                    <div class="">
                                        @if($userTask->date_completed)
                                            <a data-color="{{ $rowColor }}" data-id="{{ $userTask->id }}" data-time="{{ $userTask->time }}" class="user-task-icon complete" href="#">
                                                {{--@if($rowDay->diffInDays($currentDay,false) > 0)--}}
                                                    {{--<div class="icon" style="border:0; background-color: #ccc;"></div>--}}
                                                {{--@else--}}
                                                    <div class="icon" style="border:0;background-color: {{ $rowColor }};"></div>
                                                {{--@endif--}}
                                                    <div class="title">{{ $userTask->task->project->client ? $userTask->task->project->client->name: 'N/A' }}</div>
                                            </a>
                                            <?php $countComplete++ ?>
                                        @else
                                            <a data-color="{{ $rowColor }}" data-id="{{ $userTask->id }}" data-time="{{ $userTask->time }}" class="user-task-icon"  href="#">
                                                <div class="icon" style="border-color: {{ $rowColor }} !important;">
                                                </div>
                                                <div class="title">{{ $userTask->task->project->client ? $userTask->task->project->client->name: 'N/A' }}</div>
                                            </a>
                                        @endif
                                        <div class="details">
                                            <span class="title edit-utask">{{ $userTask->task->project->name }}</span><br>
                                            <div class="title">{{ $userTask->task->key ? $userTask->task->key->name : 'N/A' }}</div>
                                        </div>
                                    </div>
                                    <div class="details notes">
                                        <span class="" >{{ $userTask->note }}</span>
                                    </div>
                                    <div class="details" style="border-top: 1px solid {{ $rowColor }};">
                                        <span class="time">
                                            {{ $userTask['hours']  ? $userTask['hours'].'hr' : '' }}
                                            {{ $userTask['minutes']  ? $userTask['minutes'].'min' : '' }}
                                        </span>

                                        <div class="details actions">
                                        <span class="icons pull-right text-right" style="border: 0; padding:0">

                                            <img src="{{ asset('public/images/icons/Star-Icon-Gray'. ( $userTask->task->is_urgent ? '-Important' : '' ).'.png' ) }}" height="12" title="{{ $userTask->task->is_urgent ? 'Urgent' : 'Not urgent' }}" class="urgent_icon">
                                            <a href="#" class="edit edit-utask" data-utask-id="{{ $userTask->id }}"><img src="{{ asset('public/images/icons/Edit-Icon-Gray.png') }}" height="12"></a>
                                            {{--<a target="_blank" href="https://podio.com/login?force_locale=en_US"><img src="{{ asset('public/images/icons/Edit-Icon-Gray.png') }}" height="12" title="Link to Podio"></a>--}}
                                            <a data-utask-id="{{ $userTask->id }}" class="delete-utask" href="#"><img src="{{ asset('public/images/icons/Delete-Icon-X.png') }}" height="12" title="Remove"></a>
                                        </span>
                                        </div>

                                    </div>
                                </li>
                            @endforeach

                        </ul>
                        <div id="" class="partial-leave text-center {{ $week['on_leave'] && isset($week['on_leave_time']) ? '' : 'hidden' }}" data-hours="{{ $week['on_leave'] && isset($week['on_leave_time']) ? $week['on_leave_time'] / 60 : 0 }}">
                            @if($week['on_leave'] && isset($week['on_leave_time']))
                                Leave {{ intval($week['on_leave_time']) / 60 }}hr
                            @endif
                        </div>
                        <div class="loader">Please wait</div>
                    </div>

                    <div class="padding-10 bott">
                        <div class="divider"></div>
                        <div class="pull-left pie day-stats" {{ $index+1 == count($currentWeek) ? '' : '' }}>

                            <?php
                                $bookedPercent = ( $booked / 60 ) / ( $currentDepartment->work_hours ? $currentDepartment->work_hours : $workHours);
                            ?>

                            <span class="label">Time <br> Booked</span>
                            <div class="cover"></div>
                            <canvas class="chart-pie progressHours" data-index="{{ $index }}" data-hours="{{ $booked }}" data-percent="{{ $bookedPercent }}"></canvas>
                            <span class="text"><span class="val"></span>hr</span>
                        </div>
                        <div class="pull-left day-stats" {{ $index+1 == count($currentWeek) ? '' : '' }}>
                            <span class="label">Tasks <br>Done</span>

                            <?php
                                $countTasks = count($week['tasks']);
                            ?>

                            <div class="holder">
                                <div class="cover">
                                    <div class="vertBar chart-bar progress" data-index="{{ $index }}" data-percent="{{ $countTasks ? $countComplete / $countTasks : 0 }}">
                                        <div class="bar " style="height: {{ $countTasks ? $countComplete / $countTasks : 0 }}"></div>
                                    </div>
                                </div>
                            </div>
                            <span class="text"><span class="val"></span>%</span>
                        </div>
                        <div class="col-md-12 comments">
                            <a href="#" class="view-comment-bt {{ count($week['comments']) ? 'hasComments' : '' }}" data-date="{{ $rowDay->format('Y-m-d') }}" data-user_id="{{ $_GET['user'] }}">
                                {{ Auth::user()->isAdmin ? 'View' : '' }} Comment
                            </a>
                        </div>
                    </div>
                </div>
        @endforeach
        </div>
            {{--<div class="scrollerpad"></div>--}}

        </div>
    </div>
</div>

@section('calendar-js')
    <script>
        var bookedProgress = $('.progressHours');
        var completeProgresses = $('.progress');
        var bookedBars = [];
        var completeBars = [];

//        $.each(completeProgresses, function(event,element){
//            updateCompleteBars(element);
//        });
//
//        $.each(bookedProgress, function(event,element){
//            updateBookedBars(element);
//        });

        function addPushAllListener(){

            $('.push-all button.push').unbind('click');
            $('.push-all button.push').on('click', function(){
                utasks = $(this).closest('.column').find('li.ui-state-default');

                $.each(utasks, function(e,i){
                    setTimeout(function(){
                        userTaskId = $(i).attr('data-user-task-id');

                        if(userTaskId)
                            deleteUserTask(userTaskId);
                    },300);
                });

                hidePushAllModal(this);
            });

            $('.push-all button.cancel').unbind('click');
            $('.push-all button.cancel').on('click', function(){
                hidePushAllModal(this);
            });
        }

        function hidePushAllModal(element) {
            $(element).closest('.form').hide();
        }

        $('.view-comment-bt').on('click',function(e){
            e.preventDefault();

            showLoader();

            date = $(this).attr('data-date');
            user_id = $(this).attr('data-user_id');

            $.ajax({
                type: "GET",
                url: '{{ route('api.comments.get') }}?user_id='+user_id+'&date='+date,
                success : function(response){

                    if(response){
                        $('#commentBox').html('');
                        $('#commentDate').val('');

                        for(x=0;x<response.length;x++)
                            $('#commentBox').text(response[x].comment);

                        $('#commentDate').val(date);
                        $('#view-comment-modal').modal('show');
                    }
                },

                done : function(){
                    processFlag = 1;
                },
                complete : function(){
                    hideLoader();
                }
            });

        });

        $(document).ready(function () {
            addPushAllListener();
        });
    </script>


    <script>

        function updatePie(index,value){

            taskHours = value * 100;

            pie = document.getElementsByClassName("chart-pie")[index];
            isPast = $(".chart-pie").eq(index).closest('.column.past').length;
            hours = $(".chart-pie").eq(index).closest('.column').length;
            leave = $(".chart-pie").eq(index).closest('.column').find('.partial-leave').attr('data-hours');
            leave = ( parseInt(leave) / workHours ) * 100;

            value = ( parseInt(taskHours) + leave ) / 100;
            text = Math.round(value*workHours);

            if(value>1)
                value = 1;

            var config = {
                type: 'pie',
                data: {
                    datasets: [{
                        data: [value,(1-value)],
                        backgroundColor: [
                            isPast ? '#777' : 'rgb(0, 157, 156)', isPast ? '#e7e7e7' : '#fff',
                        ],
                        borderWidth: 0,
                    }],
                },
                options: {
                    responsive: true,
                    tooltips : [{
                        enabled: false
                    }],
                    hover : [{
                        onHover: null
                    }],
                    cutoutPercentage: -1
                }
            };

            if(pie){
                var ctx = pie.getContext("2d");
                window.myPie = new Chart(ctx, config);

                $(pie).closest('.day-stats').find('.text .val').html(text);
            }
        }

        function refreshCharts(){
            $('.chart-pie').each(function(){
                index = $('.chart-pie').index($(this));
                value = $(this).attr('data-percent');
                updatePie(index,value)
            });

            $('.chart-bar').each(function(){
                index = $('.chart-bar').index($(this));
                value = $(this).attr('data-percent');
                updateBar(index,value)
            });
        }

        function updateBar(index,value){
            mHeight = $('.vertBar').first().height();
            $('.chart-bar').eq(index).find('.bar').animate({'height':(mHeight*value)},1000);

            $(".chart-bar").eq(index).closest('.day-stats').find('.text .val').html(parseInt(value*100));
        }

        window.onload = function() {

            $('.chart-pie').each(function(){
                index = $('.chart-pie').index($(this));
                value = $(this).attr('data-percent');
                updatePie(index,value)
            });

            $('.chart-bar').each(function(){
                index = $('.chart-bar').index($(this));
                value = $(this).attr('data-percent');
                updateBar(index,value)
            });

        };


        function attachCompleteIconListener(){
            $('.user-task-icon .icon').unbind('click');
            $('.user-task-icon .icon').on('click', function(e){
                userTask = $(this).closest('.user-task-icon');
                e.preventDefault();

                color = $(userTask).attr('data-color');
                id = $(userTask).attr('data-id');
                el = $(userTask);

                if($(userTask).hasClass('complete')){
                    $(el).attr('class','user-task-icon').find('.icon').attr('style','').css({
                        "border" : '1px solid '+color,
                        "background-color" : 'transparent',
                    });
                    url = "{{ route('api.utasks.incomplete') }}?id="+id;
                }
                else{
                    $(el).attr('class','user-task-icon complete').find('.icon').css({
                        'background-color' : color,
                        "border" : '0',
                        "color" : '#fff !important'
                    });
                    url = "{{ route('api.utasks.complete') }}?id="+id;
                }

                $.ajax({
                    type: "GET",
                    url: url,
                    success: function(response){
//                        color = '#ccc';
                        if(response==1){
//                            if($(el).closest('.day-tasks').hasClass('past'))
                            $(el).attr('class','user-task-icon complete').find('.icon').css({
                                'background-color' : color,
                                "border" : '0',
                                "color" : '#fff !important'
                            });
                        }
                        else if(response==0){
                            $(el).attr('class','user-task-icon').find('.icon').attr('style','').css({
                                "border" : '1px solid '+color,
                                "background-color" : 'transparent',
                            });
                        }

                        updateColumnCompleteProgress(el);
                        //bookedBars[progIndex - 1].animate(booked / 60);
                    },
                    done : function (){
                        processFlag = 1;
                    }
                });

            });
        }


        @if(Auth::user()->id == $_GET['user'])
            attachCompleteIconListener();
        @elseif(Auth::user()->scheduleDepartment)
            @if($currentUser->department->id == Auth::user()->scheduleDepartment->id)
                attachCompleteIconListener();
            @endif
        @elseif(Auth::user()->role->id==1)
            attachCompleteIconListener();
        @elseif(Auth::user()->role->id==2 && !Auth::user()->scheduleDepartment)
            attachCompleteIconListener();
        @endif


        function attachViewUserTaskListener(){
            $('.user-task-icon .title').on('click', function(e){
                userTask = $(this).closest('.user-task-icon');
                e.preventDefault();

                id = $(userTask).attr('data-id');

                url = "{{ route('api.utasks.view') }}?id="+id;

                $.ajax({
                    type: "GET",
                    url: url,
                    success: function(response){
                        modal = $('#view-user-task-modal');

                        $(modal).find('.project-client').html(response.task.project.client ? response.task.project.client.name : 'N/A');
                        $(modal).find('.project-brand').html(response.task.project.brand ? response.task.project.brand.name : 'N/A');
                        $(modal).find('.project-version').html(response.task.project.version);
                        $(modal).find('.project-name').html(response.task.project.name);
                        $(modal).find('.project-language').html(response.language);
                        $(modal).find('.project-format').html(response.task.project.format ? response.task.project.format.name : 'N/A');

                        $(modal).find('.project-duration').html(response.duration);
                        $(modal).find('.project-type').html(response.task.key ? response.task.key.name : 'N/A');

                        if(response.task.is_urgent)
                            $('#viewTaskUrgency').addClass('active');
                        else
                            $('#viewTaskUrgency').removeClass('active');

                        hours = parseInt(response.time / 60);
                        minutes = response.time % 60;

                        $(modal).find('.project-time').html(( hours>0 ? hours+'hours ' : '' )+( minutes > 0 ? minutes+' minutes' : ''));
                        $(modal).find('.project-notes').html(response.note);
                        $(modal).modal('show');
                    },
                    done : function (){
                        processFlag = 1;
                    }
                });

            });
        }
        attachViewUserTaskListener();



        function updateColumnCompleteProgress(item){
            stats = $(item).closest('.day-tasks').find('.user-task-icon');

            completed = 0;
            booked = 0;
            percent = 0;

            if(stats.length){
                $.each(stats, function(e,i){

                    booked += parseInt($(this).attr('data-time'));

                    if($(this).attr('class') == 'user-task-icon complete')
                        completed += parseInt($(this).attr('data-time'));

                });
                percent = completed / booked;
            }

            progIndex = $(item).closest('.column').find('.progress').attr('data-index');
            $(item).closest('.column').find('.progress').attr('data-percent',percent);

            updateBar(progIndex,percent);
        }

        function updateColumnBookedProgress(item){
            stats = $(item).closest('.day-tasks').find('.user-task-icon');
            completed = 0;
            booked = 0;

            $.each(stats, function(e,i){
                booked += parseInt($(this).attr('data-time'));
            });

            progIndex = $(item).closest('.column').find('.progressHours').attr('data-index');
            $(item).closest('.column').find('.progressHours').attr('data-percent',(booked / 60) / workHours);
            $(item).closest('.column').find('.progressHours').attr('data-hours',booked);

            updatePie(progIndex,(booked / 60) / workHours);
        }


        function updateTaskSizes(){
            colHeight = ($('.day-tasks').first().height());

            $('.day-tasks').each(function(e,i){

                firstTask = $(i).find('li').first();

                if(firstTask.height() >= 430)
                    if($(i).find('li').length > 1 )
                        firstTask.height(firstTask.height()-20);
                    else
                        firstTask.height(colHeight);

            });
        }

        updateTaskSizes();

    </script>

@endsection