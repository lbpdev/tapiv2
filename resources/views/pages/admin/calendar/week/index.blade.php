@extends('layouts.app')

{{--@section('page-title','Week Tasks: '. isset($currentUser) ? $currentUser->name : '')--}}

@section('style')
    <link rel="stylesheet" href="{{ asset('public/css/calendar.css') }}">
    <link rel="stylesheet" href="{{ asset('public/plugins/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('public/plugins/bootstrap-select-1.10.0/css/bootstrap-select.css') }}">
    <link href='{{ asset('public/plugins/jquery-ui/jquery-ui.min.css') }}' rel='stylesheet' />
    <link href='{{ asset('public/plugins/inline-datepicker/daterangepicker.css') }}' rel='stylesheet' />
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap-override-week.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/calendar.admin.week.css') }}">
    <style>

        .modal-holder .modal {
            text-align: center;
            padding: 0!important;
        }

        .modal-holder .modal:before {
            content: '';
            display: inline-block;
            height: 100%;
            vertical-align: middle;
            margin-right: -4px; /* Adjusts for spacing */
        }

        .modal-holder .modal-dialog {
            display: inline-block;
            text-align: left;
            vertical-align: middle;
        }

        #week-calendar .scroller::-webkit-scrollbar {
            -webkit-appearance: none;
            width: 3px;
            height: 8px;
        }

        #week-calendar .scroller::-webkit-scrollbar-thumb {
            border-radius: 4px;
            background-color: #009D9C;
            -webkit-box-shadow: 0 0 1px rgba(255, 255, 255, .5);
        }
        #globalAlert {
            /*top: 700px;*/
        }


        .ui-datepicker-current-day {
            background-color: #ccc !important;
        }

        .ui-datepicker-current-day a {
            color : #000 !important;
        }

        .ui-datepicker-today {
            background-color: #ce229a !important;
        }

        .ui-datepicker-today a {
            color : #fff !important;
        }

        .scroller {
            position: relative;
        }
    </style>
@endsection

@section('content')

    <div class="container">
        <div class="row relative">
            <div class="col-md-12 calendar-header padding-t-0 margin-t-12">
                {{--<div class="alert alert-danger text-center">No active employees.</div>--}}
                <div class="row">
                    @if(isset($currentUser))
                        <div id="week-sidebar-left pull-left" class="week-sidebar padding-r-0" style="width: 182px;float:left;">
                            @include('pages.admin.calendar.week.sidebar-left')
                        </div>

                        @if(Auth::user()->scheduleDepartment)
                            @if($currentUser->department->id != Auth::user()->scheduleDepartment->id)
                                <div class="col-md-10 relative top-header" style="overflow: auto;">
                                    @include('pages.admin.calendar.week.calendar')
                                </div>
                            @else
                                <div class="relative top-header pull-left" style="margin: 0 10px 0 7px;width: 810px;float:left;">
                                    @include('pages.admin.calendar.week.calendar')
                                </div>
                                <div class="relative top-header week-sidebar right  pull-right" style="width: 182px;float:right;">
                                    @include('pages.admin.calendar.week.sidebar-right')
                                </div>
                            @endif
                        @else
                            <div class="relative top-header pull-left" style="margin: 0 10px 0 7px;width: 810px;float:left;">
                                @include('pages.admin.calendar.week.calendar')
                            </div>
                            <div class="relative top-header week-sidebar right  pull-right" style="width: 182px;float:right;">
                                @include('pages.admin.calendar.week.sidebar-right')
                            </div>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="modal-holder">
        <!-- MODALS -->
        @if(isset($currentUser))
            @include('pages.admin.calendar.week.modals.assign-task')
            @include('pages.admin.calendar.week.modals.create-task')
            @include('pages.admin.calendar.week.modals.create-key')
            @include('pages.admin.calendar.week.modals.edit-user-task')
            @include('pages.admin.calendar.week.modals.leave')

            @if($currentUser->id == Auth::user()->id)
                @include('pages.user.calendar.week.modals.comment')
            @else
                @include('pages.admin.calendar.week.modals.comment')
            @endif

            @include('pages.admin.calendar.week.modals.view-task')
        @endif
    </div>
@endsection

@section('js')
    <script src="{{ asset('public/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('public/js/bootstrap-datetime-picker.js') }}"></script>
    <script src="{{ asset('public/js/progressbar.min.js') }}"></script>
    <script src="{{ asset('public/plugins/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script src="{{ asset('public/plugins/bootstrap-select-1.10.0/js/bootstrap-select.min.js') }}"></script>
    <script src='{{ asset('public/plugins/inline-datepicker/jquery.daterangepicker.js') }}'></script>
    <script src='{{ asset('public/plugins/TinySort-master/tinysort.js') }}'></script>
    <script src='{{ asset('public/plugins/TinySort-master/jquery.tinysort.js') }}'></script>
    <script src='{{ asset('public/plugins/TinySort-master/tinysort.charorder.js') }}'></script>
    <script src='{{ asset('public/js/jquery.confirm.min.js') }}'></script>
    <script src='{{ asset('public/js/numbersOnly.js') }}'></script>
    <script src="{{ asset('public/plugins/dragscroll/jquery.dragscroll.js') }}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.bundle.min.js" type="text/javascript"></script>


    @if($currentUser->id == Auth::user()->id)
        @yield('create-comment-js')
    @endif

    @if(isset($currentUser))
        <script>
        // Assuming we have an empty <div id="container"></div> in
        // HTML
        // progressbar.js@1.0.0 version is used
        // Docs: http://progressbarjs.readthedocs.org/en/1.0.0/

        var workDays = workDaysName();

        function workDaysName(){

            $.ajax({
                type: "GET",
                url: '{{ route('api.options.workdays.get') }}',
                success: function(response){
                    workDays = JSON.parse(response);
                }
            });
        }

        var workHours = parseInt('{{ $currentDepartment->work_hours ? $currentDepartment->work_hours : $workHours }}');
        var currentDepartment = "{{ $_GET['department'] }}";
        var assets = "{{ asset('public') }}";
        var maxHours = "{{ $currentUser->maxHours }}";

        var droppedItem,droppedUI;

        function sortTasks(){
            type = $('#task_filter_type').val();
            order = $('#task_filter_order').val();

            tinysort('#tasksList>li',{order:order,data:type});
        }

        sortTasks();

        $("#filters select").on('change', function(){
            sortTasks();
        });

        $('.collapse-bt').on('click', function(e) {

        });


        function attachDropSortListener() {

            $( "ul.droppable" ).sortable({
                connectWith: "ul.droppable",
                tolerance: 'pointer',
                appendTo: '#week-calendar .scroller',
                items: "> li",
                cursor: 'pointer', //change the cursor when dragging
                over: function(e, ui) {
                    $('.ui-sortable').removeClass('hover-drag');
                    $(e.target).addClass('hover-drag');
                },
                start: function(e, ui) {
                    $( "ul.droppable" ).sortable( "refreshPositions" );
                    $('.ui-sortable').removeClass('hover-drag');
                    $('#week-calendar .scroller').addClass('noscroll');
                },
                change: function(e, ui) {
                },
                stop: function(e, ui) {
                    $('#moved_user_task_id').val($(e.target).attr('id'));
                    $('.ui-sortable').removeClass('hover-drag');
                    $('#week-calendar .scroller').removeClass('noscroll');

                    pastColumn = $(ui.item[0]).closest('ul').attr('id');
                    newColumn = e.target.id;

                    order = $(ui.item[0]).closest('ul').find( "li.ui-state-default" ).index( ui.item[0] );

                    user_task_id = $(ui.item[0]).attr('id');

                    // SAME COLUMN + CHANGED ORDER
                    if(pastColumn==newColumn){
                        $(e.target).closest('.column').find('.loader').show();

                        $.ajax({
                            type: "GET",
                            url: '{{ route('api.utasks.updateOrder') }}?id='+user_task_id+'&order='+(order+1),
                            success : function(response){

                                if(response){

                                }
                            },
                            complete : function(){
                                $(e.target).closest('.column').find('.loader').hide();
                            }
                        });
                    }
                },
                receive: function (e, ui) {

                    showLoader();

                    droppedItem = $(ui.item[0]);
                    droppedUI = ui;
                    droppedId = Math.random().toString(36).substring(7);
                    taskId = droppedItem.attr('data-task-id');
                    order = droppedItem.closest('ul').find( "li.ui-state-default" ).index( droppedItem );

                    $('#assignTaskForm').find('#user_id').val($(e.target).attr('data-user'));
                    $('#assignTaskForm').find('#task_id').val(taskId);
                    $('#assignTaskForm').find('#user_task_id').val(droppedItem.attr('data-user-task-id'));
                    $('#assignTaskForm').find('#task_assign_date').val($(e.target).attr('data-date'));
                    $('#assignTaskForm').find('#dropId').val(droppedId);
                    $('#assignTaskForm').find('#task_assign_order').val(order + 1);

                    droppedItem.attr('style','background-color:#fff !important;');

                    $('#cancel-assign-task').attr('data-task-id',droppedItem.attr('data-task-id'));

                    droppedItem.attr('id',droppedId);

                    utaskId = droppedItem.attr('data-user-task-id');

                    if(utaskId){
                        // GET USER TASK DETAILS
                        $.ajax({
                            type: "GET",
                            url: '{{ route('api.utasks.get') }}?id='+utaskId,
                            success : function(response){

                                if(response){

                                    $.each(response, function(e,i){
                                        $('#assignTaskForm #'+e).val(i);
                                    });

                                    if(response.hours >=1){
                                        if(response.hours > maxHours)
                                            $('#assignTaskForm #hours').val(maxHours);
                                        else
                                            $('#assignTaskForm #hours').val(response.hours >= 1 ? parseInt(response.hours) : 0);
                                    }

                                    $('#assignTaskForm #minutes').prop('checked', response.hours >= 1 ? response.hours : 0);

                                    if(response.is_urgent){
                                        $('#assignTaskForm #is_urgent').prop('checked', true);
                                        $('#assignTaskForm #is_urgent').attr('checked', true);
                                    }

                                    showTaskDetails(response.task);
                                    $('#assign-notes').val(response.note);
                                }
                            },

                            done : function (){
                                processFlag = 1;
                            },
                            complete : function(){
                                hideLoader();
                                $('#assign-task-modal').modal('show');
                            }
                        });
                    } else {
                        // NEW USER TASK - GET TASK DETAILS
                        $.ajax({
                            type: "GET",
                            url: '{{ route('api.tasks.get') }}?id='+taskId,
                            success : function(response){

                                if(response){

                                    $.each(response, function(e,i){
                                        $('#assignTaskForm #'+e).val(i);
                                    });

                                    if(parseInt(response.time/60) >=1){
                                        if(parseInt(response.time/60) > maxHours)
                                            $('#assignTaskForm #hours').val(maxHours);
                                        else
                                            $('#assignTaskForm #hours').val(parseInt(response.time/60));
                                    }

//                                    $('#assignTaskForm #hours').val(response.time/60 >= 1 ? parseInt(response.time/60) : 0);
                                    $('#assignTaskForm #minutes').val(response.time%60);

                                    $('#assign-hours').text((response.time/60 >= 1 ? parseInt(response.time/60) : 0 ) + 'hrs ');
                                    $('#assign-mins').text(response.time%60+ 'mins');

                                    if(response.is_urgent){
                                        $('#assignTaskForm #is_urgent').prop('checked', true);
                                        $('#assignTaskForm #is_urgent').attr('checked', true);
                                    }

                                    $('#assign-notes').val(response.note);
                                    $('#assignTaskForm .assign-hours').text((response.time/60 >= 1 ? parseInt(response.time/60) : 0 ) + 'hrs ');
                                    $('#assignTaskForm .assign-mins').text(response.time%60+ 'mins');

                                    showTaskDetails(response);

                                }
                            },

                            done : function (){
                                processFlag = 1;
                            },
                            complete : function(){
                                hideLoader();
                                $('#assign-task-modal').modal('show');
                            }
                        });
                    }

                }
            });

            $( "ul.droppable" ).on('sortupdate',function(){
            });
            $( "ul.droppable" ).trigger('sortupdate');


        }

        attachDropSortListener();

        function showTaskDetails(response){
            $('#assign-project').val(response.project.name);
            $('#assign-client').val(response.project.client ? response.project.client.name : 'N/A');
            $('#assign-brand').val(response.project.brand ? response.project.brand.name : 'N/A');
            $('#assign-deadline').val(moment(response.project.end_at).format('DD-MM-YYYY'));
            $('#assign-department').val(response.department ? response.department.name : 'General');
            $("#assign-task-type").val(response.is_extra ? 'Extra Task' : 'Existing Project');
            $('#assign-key').val(response.key.name);
            $('#assign-hours').val(response.time >= 1 ? parseInt(response.time/60) : 0);
            $('#assign-mins').val(response.time%60);
        }
        /**
         * CANCEL TASK ASSIGN
         */

        $('#cancel-assign-task').on('click',function(){
            $(droppedItem).remove();

            if($('#user_task_id').val()){
                $(droppedUI.sender[0]).append(droppedItem);
                addUserTaskEditEvent();
                addDeleteUserTaskEvent();
            }
        });


        $( "#workdays_from").on('change', function(){
            url = "{!! route('calendar.week'). ( isset($_GET['user']) ? "?user=".$_GET['user'] : "" ) . ( isset($_GET['department']) ? "&department=".$_GET['department'] : "" ) !!}";
            window.location = url + '&week='+$(this).val()+'-'+$('#switch_year').val();
        });

        setTimeout(function(){
            $( "#department_id").trigger('change');
        },400);


        function attachUTaskCollapseEvent() {
            $('.calendar .collapse-icon').unbind( "click" );

            $('.calendar .collapse-icon').on('click', function(){
                parent = $(this).closest('.ui-state-default');

                if(parent.hasClass('closed'))
                    $(this).closest('.ui-state-default').removeClass('closed');
                else
                    $(this).closest('.ui-state-default').addClass('closed');

                if($(this).hasClass('collapsed'))
                    $(this).removeClass('collapsed');
                else
                    $(this).addClass('collapsed');
            });
        }
        attachUTaskCollapseEvent();


        function addDeleteUserTaskEvent(){

            $('.delete-utask').unbind('click');
            $('.delete-utask').confirm({
                text: "Are you sure you want to remove this task from user? Time assigned will be returned.",
                confirm: function(e) {
                    deleteUserTask($(e[0]).attr('data-utask-id'));
                },
                cancel: function() {
                    // nothing to do
                }
            });

        }
        addDeleteUserTaskEvent();

        function addUserTaskEditEvent(){

            $('.edit-utask').unbind('click');
            $('.edit-utask').on('click',function(e){
                e.preventDefault();

                showLoader();

                utaskId = $(this).closest('.ui-state-default').attr('data-user-task-id');

                $.ajax({
                    type: "GET",
                    url: '{{ route('api.utasks.get') }}?id='+utaskId,
                    success : function(response){
                        if(response){

                            $.each(response, function(e,i){
                                $('#editUserTaskForm #'+e).val(i);
                            });

                            $('#editUserTaskForm #hours').val(response.hours >= 1 ? parseInt(response.hours) : 0);
                            $('#editUserTaskForm #minutes').prop('checked', response.hours >= 1 ? response.hours : 0);
                            $('#editUserTaskForm .assign-hours').text((response.task.time/60 >= 1 ? parseInt(response.task.time/60) : 0 ) + 'hrs ');
                            $('#editUserTaskForm .assign-mins').text(response.task.time%60+ 'mins');

                            if(response.is_urgent){
                                $('#editUserTaskForm #is_urgent').prop('checked', true);
                                $('#editUserTaskForm #is_urgent').attr('checked', true);
                            } else {
                                $('#editUserTaskForm #is_urgent').prop('checked', false);
                                $('#editUserTaskForm #is_urgent').attr('checked', false);
                            }

                            toggleUrgent($('#editUserTaskForm #is_urgent'));

                            $('#editUserTaskForm .project').val(response.task.project.name);
                            $('#editUserTaskForm .client').val(response.task.project.client.name);
                            $('#editUserTaskForm .brand').val(response.task.project.brand.name);
                            $('#editUserTaskForm .deadline').val(moment(response.task.project.end_at).format('DD-MM-YYYY'));
                            $('#editUserTaskForm .department').val(response.task.department.name);
                            $("#editUserTaskForm .is_extra").val(response.task.is_extra ? 'Extra Task' : 'Existing Project');
                            $('#editUserTaskForm .key').val(response.task.key.name);
                            $('#editUserTaskForm .notes').val(response.note);
                            $('#editUserTaskForm .r-hours').val(parseInt(parseInt(response.task.time) / 60));
                            $('#editUserTaskForm .r-minutes').val(parseInt(response.task.time)%60 >= 1 ? parseInt(response.task.time)%60 : 0);

                        }
                    },

                    done : function (){
                        processFlag = 1;
                    },
                    complete : function(){
                        hideLoader();
                        $('#edit-user-task-modal').modal('show');
                    }
                });

            });
        }
        addUserTaskEditEvent();

        function deleteUserTask(utaskId){

            $.ajax({
                type: "GET",
                url: '{{ route('api.utasks.delete') }}?id='+utaskId,
                success : function(response){
                    if(response){

                        toDelete = $('#'+utaskId).closest('ul');

                        $('#'+utaskId).remove();

                        updateTaskTime(response);

                        updateColumnCompleteProgress(toDelete);
                        updateColumnBookedProgress(toDelete);
                        updateTaskSizes();

                        return true;
                    }
                },
                done : function (){
                    processFlag = 1;
                }
            });
        }

        function updateTaskTime(task_id){

            $.ajax({
                type: "GET",
                url: '{{ route('api.tasks.getItem') }}?id='+task_id,
                success : function(response){
                    if(response){
                        if($('#task-'+task_id).length)
                            $('#task-'+task_id).replaceWith(response);
                        else
                            $('#tasksList').append(response);
                    } else {
                        $('#task-'+task_id).remove();
                    }
                    attachTaskEditListener();
                    attachTaskDeleteListener();
                },
                done : function (){
                    processFlag = 1;
                }
            });
        }

        $(window).load(function(){

            resizeSideBars();

            $(".day-tasks").mCustomScrollbar({
                autoHideScrollbar: true
            });

            $(".scrollers").mCustomScrollbar({
                autoHideScrollbar: true
            });

            $("#left-sidebar-scroll").mCustomScrollbar({
                autoHideScrollbar: true,
            });

            $("#right-sidebar-scroll").mCustomScrollbar({
                autoHideScrollbar: true
            });
        });


        function resizeSideBars(){
            var windowH = $(window).height()-$('.navbar').height()-10;

            var rMenu = $('.week-sidebar.right .menu').height();

            if($('#tasksList li').length>6){
                if(windowH>600){
                    $('.calendar-header').height(windowH);

                    $('.week-sidebar.right').height(windowH);
                    $('.week-sidebar.right .col-md-12').height(windowH);
                    $('#right-sidebar-scroll').height(windowH-190);
                } else {
                    $('.calendar-header').height(windowH);

                    $('.week-sidebar.right').height(windowH);
                    $('.week-sidebar.right .col-md-12').height(windowH);
                    $('#right-sidebar-scroll').height(windowH-170);
                }
            } else {
                $('#right-sidebar-scroll').css('height','auto');
            }

            $('#left-sidebar-scroll').height(windowH-30);

        }

        $(window).resize(function(){
            setTimeout(function(){
                resizeSideBars();
            },500);
        });

        $('#currentDay').datepicker({
            dateFormat : 'dd-mm-yy',
            beforeShow: function(date) {
            },
            beforeShowDay: function(date) {

                if(workDays){
                    var day = date.getDay();

                    return [($.inArray( day, workDays ) >= 0 ? true : false), ''];
                }

            }
        }).on('change',function(){
            url = "{!! route('calendar.week'). ( isset($_GET['user']) ? "?user=".$_GET['user'] : "" ) . ( isset($_GET['department']) ? "&department=".$_GET['department'] : "" ) !!}";
            window.location = url + '&day='+$(this).val();
        }).on('click',function(){
            setTimeout(function(){$('.ui-state-highlight').removeClass('ui-state-highlight ui-state-active');},10);
        });


        $('.urgentRadio').on('click',function(){
            toggleUrgent(this);
        }).on('change',function(){
            toggleUrgent(this);
        });

        $('#key-select').on('change',function(){
            $('#key-list .collapse').hide();
            $('#key'+$(this).val()).show();
        });

        $('#key-select').trigger('change');

        function toggleUrgent(el){

            if($(el).is(":checked"))
                $(el).parent().find('.urgentImage').attr('src','{{ asset('public/images/icons/task-icons/Important-active.png') }}');
            else
                $(el).parent().find('.urgentImage').attr('src','{{ asset('public/images/icons/task-icons/Important.png') }}');
        }

        $('#clientColor').on('change',function(){
            $('.sp-preview-inner').css('background-color',$(this).val());
            $('#clientCircleColor').css('background-color',$(this).val());
        });

        $('#create-task-modal').on('shown.bs.modal', function(){
//            $('#key_id').closest('.bootstrap-select').show();
//            $('#key_name').empty().hide();
        });

        $('#key_id').on('change', function(){
            if($(this).val()==0){
                $(this).closest('.bootstrap-select').hide();
                $('#key_name').show();

                if($('#key_id option').length>1)
                    $('.new_key .close').show();
                else
                    $('.new_key .close').hide();
            }
            else {
                $(this).closest('.bootstrap-select').show();
                $('#key_name').hide();
                $('.new_key .close').hide();
            }
        });

        $('.new_key .close').on('click', function(){
            $('#key_name').val('').hide();
            $('.new_key .close').hide();
            $(this).closest('.col-md-6').find('.bootstrap-select').show();
            $('#key_id').val($("#key_id option:first").val());
            $('#key_id').selectpicker('refresh');
        });


        function updateTaskSize(element){
            time = $(element).find('.user-task-icon').attr('data-time');
            colHeight = ($(element).closest('.day-tasks').height());

            taskHeight = Math.floor(( ( time / 60 ) / ( workHours ) ) * colHeight);
            taskHeight = taskHeight < 90 ? 91 : taskHeight;

            setTimeout(function(){
                $(element).find('.color-bg').height(taskHeight);
                $(element).height(taskHeight-20);
            },500);

            updateTaskSizes();
        }
    </script>
    @endif

    @yield('calendar-js')
    @yield('create-key-js')
    @yield('create-task-js')
    @yield('assign-task-js')
    @yield('sidebar-right-js')
    @yield('edit-task-js')
    @yield('leave-js')
@endsection


