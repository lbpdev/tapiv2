<div id="left-sidebar-scroll" class="sidebar">
    <div class="collapsible" id="employees">
    @foreach($departments as $department)
        <button data-parent="#employees" class="" >{{ $department->name }}</button>
        <div id="dept{{ $department->id }}" class="users clearfix">
            @foreach($department->users as $user)
                {{-- Jeffrey Update --}}
                {{-- @if($user->is_active && $user->role->slug=='employee') --}}
                @if($user->is_active)
                    @if(!Auth::user()->isEmployee)
                        <a href="{{ route('calendar.week').'?user='.$user->id.'&department='.$department->name }}" class="{{ $currentUser->id == $user->id ? 'active' : '' }}">
                            <!-- Jeffrey Update 2 -->
                            @if(Auth::user()->id == $user->id)
                                Me
                            @else
                                {{ $user->name }}
                            @endif
                        </a>
                    @else
                        <a href="{{ route('user.calendar.week').'?user='.$user->id.'&department='.$department->name }}" class="{{ $currentUser->id == $user->id ? 'active' : '' }}">
                            @if(Auth::user()->id == $user->id)
                                Me
                            @else
                                {{ $user->name }}
                            @endif
                        </a>
                    @endif
                @endif
            @endforeach
        </div>
    @endforeach
    </div>
    <div class="col-md-12 break">
        <div class="row">
            <div id="stage-legend" class="margin-top-0">
                @foreach($clients as $client)
                    <span class="a" href="#"><span style="background-color: {{ $client->color }};"></span><p>{{ $client->name }}</p></span><br>
                @endforeach
            </div>
        </div>
    </div>
    {{--<div class="col-md-12">--}}
        {{--<div class="row" id="sidebar-keys">--}}
            {{--<h3 class="ul">--}}
                {{--Key--}}
                {{--@if(Auth::user()->role->slug == 'admin')--}}
                    {{--<span class="title" data-toggle="modal" style="float:right;font-size:11px;cursor:pointer" data-target="#create-key-modal">Add a Key</span>--}}
                {{--@endif--}}
            {{--</h3>--}}
            {{--<select class="selectpicker small" id="key-select">--}}
                {{--@foreach($departments as $department)--}}
                    {{--<option value="{{ $department->id }}">{{ $department->name }}</option>--}}
                {{--@endforeach--}}
            {{--</select>--}}
            {{--<div class="collapsible list margin-t-5" id="key-list">--}}
                {{--@foreach($departments as $department)--}}
                    {{--<div id="key{{ $department->id }}" class="collapse">--}}
                        {{--@foreach($department->keys as $key)--}}
                            {{--<a href="#"><b>{{ $key->code }}</b> {{ $key->name }}</a>--}}
                        {{--@endforeach--}}
                    {{--</div>--}}
                {{--@endforeach--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
</div>