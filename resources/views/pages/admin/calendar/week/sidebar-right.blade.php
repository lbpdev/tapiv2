<div class="col-md-12">
    <div class="row scrollCover">
        <h3 class="ul" style="margin-top: 37px;margin-bottom: 5px;">Project Tasks
        </h3>

        <!-- <div id="filters">
            <?php
//            $filterType = [
//                    'deadline' => 'Deadline',
//                    'name' => 'Name',
//            ]
            ?>
{{--            {!! Form::select('filter',$filterType,'az',['id'=>'task_filter_type','class'=>'small selectpicker','required']) !!}--}}
            <?php
//            $filterOrder = [
//                    'asc' => 'Ascending',
//                    'desc' => 'Descending',
//            ]
            ?>
{{--            {!! Form::select('filter',$filterOrder,'desc',['id'=>'task_filter_order','class'=>'small selectpicker','required']) !!}--}}
        </div> -->
    </div>
    <div class="row" id="right-sidebar-scroll" style="width: 169px">

        <ul id="tasksList" class="dropfalse">
            @forelse($currentTasks as $index=>$task)
                <li class="ui-state-default clearfix" id="task-{{$task['id']}}" data-name="{{ $task['project']['name'] }}"  data-deadline="{{ $task['project']['end_at'] }}" data-color="{{ $task['project']['client']['color'] }}" data-task-id="{{ $task['id'] }}" >
                    <div class="color-bg" style="background-color: {{ $task['project']['client']['color'] }} !important;"></div>
                    <div class="details">
                        <span class="key">{{ $task['project']['client']['name'] }}</span>
                        <span class="title">{{ $task['project']['name'] }}</span>
                        <span class="title">{{ $task['key']['name'] }}</span>
                        {{--@if($task['note'])--}}
                            {{--<span class="notes">{{ $task['note'] }}</span>--}}
                        {{--@endif--}}
                    </div>
                    <div class="action" style="border-top: 1px solid {{ $task['project']['client']['color'] }};">
                        <span class="time">{{ $task['hours'] }}hr {{ $task['minutes'] }}mins</span>
                        <a data-task-id="{{ $task['id'] }}" class="delete" href="#"><img src="{{ asset('public/images/icons/Delete-Icon-X.png') }}" height="12" title="Remove"></a>
                        <a href="#" class="edit" data-task-id="{{ $task['id'] }}"><img src="{{ asset('public/images/icons/Edit-Icon-Gray.png') }}" height="12"></a>
                        <img src="{{ asset('public/images/icons/Star-Icon-Gray') . ($task['is_urgent'] ? '-Important' : '') .'.png' }}" height="12">
                    </div>
                </li>

                {{--<li class="closed ui-state-default clearfix ui-sortable-handle" data-color="{{ $task['project']['client']['color'] }}" data-user-task-id="" data-task-id="{{ $task['id'] }}" style="background-color:{{ $task['project']['client']['color'] }} !important;">--}}
                    {{--<div class="">--}}
                        {{--<a data-color="{{ $task['project']['client']['color'] }}" data-id="" data-time="" class="user-task-icon"  href="#">--}}
                            {{--<div class="icon" style="border-color: {{ $task['project']['client']['color'] }};color: {{ $task['project']['client']['color'] }}">--}}
                                {{--{{ $task['key']['code'] }}--}}
                            {{--</div>--}}
                        {{--</a>--}}
                        {{--<div class="details" style="color: {{ $task['project']['client']['color'] }}">--}}
                            {{--<span class="title">{{ $task['project']['name'] }}</span>--}}
                            {{--<span class="collapse-icon"></span>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="">--}}
                        {{--<div class="">--}}
                            {{--<img class="time-icon" src="{{ asset('public/images/icons/task-icons/Clock-Icon.png') }}" height="28">--}}
                        {{--</div>--}}
                        {{--<div class="details" style="color: {{ $task['project']['client']['color'] }}">--}}
                            {{--<span class="time">--}}
                                {{--{{ $task['hours']  ? $task['hours'].'hr' : '' }}--}}
                                {{--{{ $task['minutes']  ? $task['minutes'].'min' : '' }}--}}
                            {{--</span>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="details notes">--}}
                        {{--<span class="">{{ $task['note'] }}</span>--}}
                    {{--</div>--}}
                    {{--<div class="details actions">--}}
                                {{--<span class="icons pull-right" style="border: 0; padding:0">--}}
                                    {{--<img src="{{ asset('public/images/icons/task-icons/Important.png') }}" height="22">--}}
                                    {{--<a target="_blank" href="https://podio.com/login?force_locale=en_US"><img src="{{ asset('public/images/icons/task-icons/Podio-Icon.png') }}" height="22"></a>--}}
                                    {{--<a href="#"><img src="{{ asset('public/images/icons/task-icons/Forward-Icon.png') }}" height="22"></a>--}}
                                    {{--<img src="{{ asset('public/images/icons/task-icons/Delete-Icon.png') }}" height="22">--}}
                                {{--</span>--}}
                    {{--</div>--}}
                {{--</li>--}}
            @empty
            @endforelse
          {{--<li class="ui-state-default clearfix" style="background-color: #3CBAFF !important;">--}}
              {{--<div class="icon">?</div>--}}
              {{--<div class="details">--}}
                {{--<span class="title">ADIB Newsletter</span>--}}
                {{--<span class="time">1hr 30mins</span>--}}
              {{--</div>--}}
              {{--<div class="action">--}}
                {{--<img src="{{ asset('public/images/icons/Edit-White-Icon.png') }}">--}}
              {{--</div>--}}
          {{--</li>--}}
        </ul>
    </div>

    <div class="row menu">
        <div class="col-md-12 text-right width-full padding-l-0">
            <span id="createTaskBt"><a class="pull-left" href="#"><h3>Add Task</h3></a></span>

            @if(!Auth::user()->isEmployee)
                <a href="#" id="sickLeaveLink"><h3>Add Leave</h3></a>
            @endif
        </div>
    </div>
    {{--<div class="row scrollCover">--}}
        {{--<div id="add-task-bt">--}}
            {{--<div class="icon">+</div>--}}
            {{--<div class="details">--}}
                {{--<!-- Trigger the modal with a button -->--}}
                {{--<span class="title" id="createTaskBt">Add a Task</span>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
</div>



@section('sidebar-right-js')
    <script>
        /** SEND CREATE REQUEST **/


        $( "#tasksList" ).sortable({
            connectWith: "ul.droppable",
            items: "> li",
            over: function(e, ui) {
                $('.ui-sortable').removeClass('hover-drag');
                $(e.target).addClass('hover-drag');
            },
            stop: function(e, ui) {

                if($(ui.item).closest('#tasksList').length>0)
                    $(ui.item[0]).remove();

                attachTaskEditListener();
                attachTaskDeleteListener();

                $('.ui-sortable').removeClass('hover-drag');
            },
            helper: function (e, li) {
                if($(e.currentTarget).attr('id')=="tasksList"){
                    this.copyHelper = li.clone().insertAfter(li);

                    $(this).data('copied', false);

                    return li.clone();
                }
            },
            receive: function (e, ui) {
                ui.sender.data('copied', true);
            }
        });

        function attachTaskEditListener() {

            $('#tasksList a.edit').on('click',function(e){

                showLoader();

                e.preventDefault();

                taskId = $(this).attr('data-task-id');

                $.ajax({
                    type: "GET",
                    url: "{{ route('api.tasks.get') }}?id="+taskId,
                    success: function(response){

                        if(response) {
                            $('#projectSelection').val(response.project.name);
                            $('#projectClient').val(response.project.client.name);
                            $('#projectBrand').val(response.project.brand.name);
                            $('#projectVersion').val(response.project.version);
                            $('#projectFormat').val(response.formatName);
                            $('#key_id').val(response.key.id);
                            $('#projectSelect').val(response.project_id).trigger('change');
                            $('#projectDuration').val(moment(response.project.end_at).format('DD-MM-YYYY'));
                            $('#projectUrgent').val(response.is_urgent).selectpicker('refresh');
                            $('#date').val(moment(response.date).format('DD-MM-YYYY'));
                            $('#taskNotes').val(response.note);
                            $('#taskHours').val(parseInt(response.time/60));
                            $('#taskMins').val(response.time%60);
                            $('#project_id').val(response.project.id);

                            $('#projectLanguage').val(response.langs);

                            $('#is_extra').val(response.is_extra).trigger('change');

//                            if(response.department_id)
//                                $('#department_id').val(response.department_id);
//                            else
//                                $('#department_id').val('general');
//                            $('#department_id').trigger('change');

                            setTimeout(function () {
                                $('#key_id').val(response.key_id).trigger('change');
                            },300);

                            $('#create_task_id').val(response.id);

                            $('#addTaskForm').attr('action','{{ route('api.tasks.update') }}');
                            $('#createTask').val('Update');

                            resizeSideBars();
                        }

                    },
                    done : function (){
                        processFlag = 1;
                    },
                    complete : function(){

                        hideLoader();
                        $('#create-task-modal').modal('show');
                        $('#createTask').show();
                    }
                });

            });
        }

        attachTaskEditListener();

        function attachTaskDeleteListener() {

            $('#tasksList a.delete').unbind('click');

            $('#tasksList a.delete').confirm({
                text: "Are you sure you want to remove this task?",
                confirm: function(e) {
                    taskId = $($(this)[0].button[0]).attr('data-task-id');

                    $.ajax({
                        type: "GET",
                        url: "{{ route('api.tasks.delete') }}?id="+taskId,
                        success: function(response){
                            if(response) {
                                $("#task-" + response.data.id).remove();
                            }

                            resizeSideBars();
                        },
                        done : function (){
                            processFlag = 1;
                        }
                    });
                },
                cancel: function() {
                    // nothing to do
                }
            });
        }

        attachTaskDeleteListener();

        $('#createTaskBt').on('click',function (e) {
            e.preventDefault();
            $('#addTaskForm').attr('action','{{ route('api.tasks.store') }}');
            $('#addTaskForm')[0].reset();
            $('#date').val(moment().format('MM/DD/YYYY'));

            $('#department_id').trigger('change');
            $('#project_id').val("");

            $('#create-task-modal').modal('show');
            $('#createTask').val('Save');
            $('#key_id').selectpicker('refresh');
            $( "#department_id").trigger('change');
            $( "#projectSelection").trigger('focus');
            $( "#projectSelect" ).selectpicker('refresh');
            $( "#date-error" ).hide();

            $( "#projectSelect" ).val(0).trigger('change');
        });

    </script>
@endsection