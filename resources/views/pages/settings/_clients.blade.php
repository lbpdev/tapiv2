@section('department-style')
@endsection

<hr>
<div class="row">
    <div class="row">
        <div class="col-md-6">
            {!! Form::open(['id'=>'clientsForm','route'=>'api.clients.store']) !!}

            <div class="col-md-12">
                Clients <span class="info-icon"  data-toggle="tooltip" title="If you do not have a client simply enter your own company name here and you will be listed as the client. "></span>

                <div class="form-group relative">
                    @if(isset($company))
                        <input type="hidden" name="company_id" value="{{ $company->id }}">
                    @endif

                    <input type="text" class="form-control pull-left" style="width: 190px" name="name" placeholder="Add Client" value="" required="required" maxlength="30">
                    <input id="clientColor" class="form-control" name='color' value='#3355cc'/>
                    {!! Form::submit('Add',['class'=>'t-pink link-bt link-color margin-t-0 margin-b-10 pull-left divider-left']) !!}
                </div>
            </div>

        </div>

        @include('pages.settings._clients-list')

    </div>
</div>


<!-- Modal -->
@include('pages.settings._clients-edit')

        <!-- Modal -->
@include('pages.settings._clients-delete')

@section('clients-js')
    <script>
        $("#clientColor").spectrum({
            showPaletteOnly: true,
            showPalette:true,
            hideAfterPaletteSelect:true,
            color: colors[0],
            palette: [
                colors
            ],
            change: function(color) {
                $('#clientColor').val(color.toHexString()).attr('value',color.toHexString()); // #ff0000
            }
        }).val(colors[0]).attr('value',colors[0]);

        $("#clientEditColor").spectrum({
            showPaletteOnly: true,
            showPalette:true,
            hideAfterPaletteSelect:true,
            color: colors[0],
            palette: [
                colors
            ],
            change: function(color) {
                $('#clientEditColor').val(color.toHexString()).attr('value',color.toHexString()); // #ff0000
            }
        }).val(colors[0]).attr('value',colors[0]);

    </script>
    <script>

        var appUrl = '{{ url('/') }}';

        /** SEND CREATE REQUEST **/

        $('#clientsForm').on('submit', function(e){

            submitBt = $(this).find('input[type=submit]');
            $(submitBt).attr('disabled','disabled');

            var form = $(this);
            processFlag = 1;

            e.preventDefault();
                e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                  type: "POST",
                  url: url,
                  data: data,
                  success: function(response){

                    if(response.status==200) {
                        refreshClientsList(response.data);
                        $(form).find('input[type=text]').val('');
                    }

                    doAlert(response);

                    $(submitBt).removeAttr('disabled');
                  },
                  done : function (){
                    processFlag = 1;
                  }
                });
            }
        });

        /** SEND UPDATE REQUEST **/

        $('#clientEditForm').on('submit', function(e){
            var form = $(this);
            processFlag = 1;

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){
                        if(response.status==200) {
                            refreshClientsList(response.data);
                            $(form).trigger('reset');
                            $(form).closest('.modal').modal('hide');
                        }

                        doAlert(response);
                    },
                    done : function (){
                        processFlag = 1;
                    }
                });
            }
        });


        /** SEND DELETE REQUEST **/

        function attachDeleteClientEvent() {
            $('.delete-client').unbind('click');
            $('.delete-client').on('click', function (e) {
                e.preventDefault();

                $('#deleteClient').val($(this).attr('data-clientid'));

                clientId = $(this).attr('data-clientid');

                if (clientId > 0){

                    $.ajax({
                        type: "GET",
                        url: '{{ route('api.clients.get') }}?id='+clientId,
                        success: function(response){
                            if(response.status==200){

                                tasksCount = response.data.tasksCount;
                                taskS = tasksCount.length == 1 ? '' : 's' ;
                                projectsCount = response.data.projectsCount;
                                projectS = projectsCount.length == 1 ? '' : 's' ;

                                if(tasksCount || projectsCount){
                                    $('#deleteClientModal .deleteMessage .counts').html('You currently have <b>'+tasksCount+' task'+taskS+'</b> and <b>'+projectsCount+' project'+projectS+'</b> connected to this client.');
                                    $('#deleteClientModal .deleteMessage').show();
                                }
                                else
                                    $('#deleteClientModal .deleteMessage').hide();

                                $('#deleteClientModal').modal('show');

                            } else {
                                doAlert(response);
                            }

                        },
                        done : function (){
                            deleteProcessFlag = 1;
                        }
                    });

                }
                else
                    alert('Please select a client to delete.');

            });
        }


        function attachEditClientEvent(){
            $('.edit-client').unbind('click');

            $('.edit-client').on('click', function(e){

                e.preventDefault();

                clientId = $(this).attr('data-clientid');

                if(clientId>0){

                    $.ajax({
                        type: "GET",
                        url: '{{ route('api.clients.get') }}?id='+clientId,
                        success: function(response){

                            if(response.status==200){
                                $('#editClientModal').modal('show');

                                $.each(response.data.client, function(i,e){
                                    $('#editClientModal input[name='+i+']').val(e);
                                });

                                $('#clientEditColor').attr('value', response.data.client.color); // #ff0000
                                $('#editClientModal .sp-preview-inner').css('background-color', response.data.client.color); // #ff0000

                            } else {
                                doAlert(response);
                            }

                        },
                        done : function (){
                            deleteProcessFlag = 1;
                        }
                    });


                }
            });
        }

        $('#deleteClientTrigger').on('click', function(){
            if($('#deleteClientConfirmInput').val()=="yes"){
                deleteClient(); 
                $('#deleteClientModal').modal('hide'); 
            } else {
                alert('Please follow the instructions');
            }
        });


        /*********************************** FUNCTIONS **************************/

        function refreshClientsList(data){
            $('#clientsList').html('');

            for(var x=0;x<data.length;x++){

                el =
                    '<li>'+
                        '<div class="color-dot padding-l-0">'+
                            '<span style="background-color: '+data[x]['color']+';"></span>'+
                            '<a href="#">'+data[x]['name']+'</a>'+
                        '</div>'+
                        '<div class="margin-l-10 pull-left divider-left">'+
                            '<a href="#" data-clientid="'+data[x]['id']+'" class="edit-client link-text padding-r-0">Edit</a>'+
                        '</div>'+
                        '<div class="margin-l-10 pull-left divider-left">'+
                            '<a href="#" data-clientid="'+data[x]['id']+'" class="delete-client delete-icon link-icon"></a>'+
                        '</div>'+
                    '</li>';

                $('#clientsList').append(el);
            }

            attachDeleteClientEvent();
            attachEditClientEvent();
        }


        function deleteClient(){

            url = "{{ route('api.clients.delete') }}";
            departmentId = $('#deleteClient').val();
            deleteProcessFlag = 1;    

            if(deleteProcessFlag) {
                deleteProcessFlag = 0;

                if(departmentId){
                    $.ajax({
                        type: "GET",
                        url: url+'?client_id='+departmentId,
                        success: function(response){

                            if(response.status==200){
                                $('#deleteClient').attr('data-clientid','0');

                                refreshClientsList(response.data.clients);
                                attachDeleteClientEvent();
                                attachEditClientEvent();
                            }
                            
                            doAlert(response);
                        },
                        done : function (){
                            deleteProcessFlag = 1;
                        }
                    });
                } else {
                    alert('Please select a client to delete.');
                } 
            } 
        }

        attachDeleteClientEvent();
        attachEditClientEvent();
    </script>
@endsection