<div id="editDeptModal" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center" style="width: 300px">

            <!-- Modal content-->
            <div class="modal-content clearfix">
                {!! Form::open(['id'=>'deptEditForm','route'=>'api.departments.update','class'=>'pull-left width-full']) !!}
                @if(isset($company))
                    <input type="hidden" name="company_id" value="{{ $company->id }}">
                @endif

                <div class="modal-header clearfix">
                    <h4 class="modal-title">Edit Department</h4>
                </div>
                <div class="modal-body clearfix width-full">

                    <input type="hidden" name="id" value="0">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="color-label">
                                <p class="pull-left">Name</p>
                                <p class="pull-right"><input id="deptEditColor" class="form-control" name='color' value='#3355cc'/></p>
                            </div>
                            <div class="form-group relative pull-left width-full">

                                <input type="text" class="form-control margin-b-5 text-left" name="name" placeholder="Name" value="" required="required" maxlength="30">
                                <div class="col-md-12">
                                    <div class="row work-hours">
                                        <?php
                                        for($x=1;$x<25;$x++)
                                            $workHours[$x] = $x;
                                        ?>

                                        <div class="pull-right">
                                            <p>Work Hours</p>
                                            {!! Form::select('work_hours',$workHours, 8,['id'=>'work_hours','readonly','class'=>'hours form-control selectpicker','style'=>'width:40px']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                    <div class="modal-footer clearfix pull-left width-full">
                        {!! Form::submit('Save',['class'=>'createbt btn btn-default margin-b-0']) !!}
                        <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>
</div>