
<!-- Modal -->
<div id="deleteDepartmentModal" class="modal fade delete" data-backdrop="static" data-keyboard="false" role="dialog">

    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center" style="width: 250px">

            <!-- Modal content-->
            <div class="modal-content text-center">
                <div class="modal-header">
                    <h4 class="modal-title">Delete department</h4>
                </div>
                <div class="modal-body">
                    Are you sure you want to delete this?
                      <span class="deleteMessage">
                          <span class="counts">You currently have XX users and XX task types connected to this department.</span>
                          <br>By deleting this department, all items mentioned above will also be deleted.
                          <br><b>This cannot be undone.</b>
                      </span>
                    <br>
                    Please type "yes" to confirm.<br>
                    <input type="hidden" id="deleteDepartment">
                    <input id="deleteDepartmentConfirmInput" class="text-center">
                </div>
                <div class="modal-footer text-center-important">
                    <button id="deleteDepartmentTrigger" class="createbt btn btn-default confirmBt">Confirm</button>
                    <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>

            </div>

        </div>
    </div>
</div>