<!-- Modal -->
<div id="deleteFormatModal" class="modal fade delete" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center" style="width: 250px">

            <!-- Modal content-->
            <div class="modal-content text-center">
                <div class="modal-header">
                    <h4 class="modal-title">Delete Deliverable</h4>
                </div>
                <div class="modal-body">
                    Are you sure you want to delete this? <br>
                      <span class="deleteMessage">
                          <span class="counts">You currently have XX projects and XX tasks connected to this deliverable</span>.
                          <br>By deleting this deliverable, all items mentioned above will also be deleted.
                          <br><b>This cannot be undone.</b>
                      </span>
                    <br>
                    Please type "yes" to confirm.<br>
                    {!! Form::open(['route'=>'api.formats.delete']) !!}
                        <input type="hidden" id="deleteFormat" name="id">
                        <input id="deleteFormatConfirmInput" class="text-center">
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer text-center-important">
                    <button id="deleteFormatTrigger" class="createbt btn btn-default confirmBt">Confirm</button>
                    <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>

            </div>

        </div>
    </div>
</div>