<div id="deleteKeyModal" class="modal fade delete" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="vertical-alignment-helper">
      <div class="modal-dialog vertical-align-center" style="width: 300px">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title text-center">Delete Task Type</h4>
          </div>
          <div class="modal-body text-center">
              Are you sure you want to delete this Task Type.
              <br>
              <span class="deleteMessage">
                  <span class="counts">You currently have XX tasks and XX projects connected to this Task Type</span>.
                  <br>By deleting this Task Type, all items mentioned above will also be deleted.
                  <br><b>This cannot be undone.</b>
              </span>
              <br>Please type "yes" to confirm.
            {!! Form::open(['route'=>'api.keys.delete']) !!}
                <input type="hidden" name="id" value="0">
                <input id="deleteKeyConfirmInput" class="text-center">
            {!! Form::close() !!}
          </div>
          <div class="modal-footer  text-center-important">
                <button id="deleteKeyTrigger" class="createbt btn btn-default confirmBt">Confirm</button>
                <button class="btn btn-default" data-dismiss="modal">Cancel</button>
          </div>

        </div>

      </div>
  </div>
</div>
