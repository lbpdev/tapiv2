<div class="col-md-8">
    <div class="alert row-alert" style="display: none"></div>
    <div class="col-md-12">
        <p class="margin-b-5" style="font-weight: bold">Internal</p>
        <ul id="stageListInternal" class="sortable">
            <?php $internalCount = 0; ?>
            @foreach($stages as $stage)
                @if($stage->internal)


                    <li class="ui-state-default" data-stage-id="{{ $stage->id }}">
                        <span class="glyphicon glyphicon-option-vertical drag-handle" aria-hidden="true"></span>

                        <div class="color-dot padding-l-0">
                            <span style="background-color: {{ $stage->color }}"></span>
                            <a href="#">{{ $stage->name }}</a>
                        </div>
                        <div class="pull-left divider-left">
                            <a href="#" data-stageid="{{ $stage->id }}" class="edit-stage link-text padding-r-0">Edit</a>
                        </div>
                        <div class="pull-left divider-left margin-l-10">
                            <a href="#" data-stageid="{{ $stage->id }}" class="delete-stage delete-icon link-icon"></a>
                        </div>


                        {{--@if(count($stage->department))--}}
                        {{--<div class="margin-l-10 margin-t-10 pull-left">--}}
                        {{--Linked to <b>{{ $stage->department->name }}</b>--}}
                        {{--<span class="ui-icon ui-icon-arrowthick-2-n-s pull-right margin-t-3"></span>--}}
                        {{--</div>--}}
                        {{--@endif--}}
                    </li>
                    <?php $internalCount++ ?>
                @endif
            @endforeach

            @if(!$internalCount)
                <li style="margin-left: 20px;">None</li>
            @endif
        </ul>

        <p class="pull-left margin-t-10 margin-b-5" style="width: 100%;font-weight: bold">External</p>
        <ul id="stageListExternal"  class="sortable">
            <?php $externalCount = 0; ?>
            @foreach($stages as $stage)
                @if(!$stage->internal)
                    <li class="ui-state-default" data-stage-id="{{ $stage->id }}">
                        <span class="glyphicon glyphicon-option-vertical drag-handle" aria-hidden="true"></span>

                        <div class="color-dot padding-l-0">
                            <span style="background-color: {{ $stage->color }}"></span>
                            <a href="#">{{ $stage->name }}</a>
                        </div>
                        <div class="pull-left divider-left">
                            <a href="#" data-stageid="{{ $stage->id }}" class="edit-stage link-text padding-r-0">Edit</a>
                        </div>
                        <div class="pull-left divider-left margin-l-10">
                            <a href="#" data-stageid="{{ $stage->id }}" class="delete-stage delete-icon link-icon"></a>
                        </div>

                        {{--@if(count($stage->department))--}}
                        {{--<div class="margin-l-10 margin-t-8 pull-left">--}}
                        {{--Linked to <b>{{ $stage->department->name }}</b>--}}
                        {{--</div>--}}
                        {{--@endif--}}
                    </li>
                    <?php $externalCount++ ?>
                @endif
            @endforeach
            @if(!$externalCount)
                <li style="margin-left: 20px;">None</li>
            @endif
        </ul>
    </div>
</div>