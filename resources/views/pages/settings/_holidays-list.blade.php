<div class="col-md-12">
    <div class="alert row-alert" style="display: none"></div>
    <div class="col-md-12">
        <ul id="formatList">
            @if(count($formats))
                @foreach($formats as $format)
                    <li>
                        <div class="pull-left t24">
                            <p class=margin-b-0>{{ $format->name }}</p>
                        </div>
                        <div class="margin-l-10 pull-left divider-left">
                            <a href="#" data-formatid="{{ $format->id }}" class="edit-format link-text padding-r-0">Edit</a>
                        </div>
                        <div class="margin-l-10 pull-left divider-left">
                            <a href="#" data-formatid="{{ $format->id }}" class="delete-format delete-icon link-icon"></a>
                        </div>
                    </li>
                @endforeach
            @endif
        </ul>
    </div>
</div>