@extends('layouts.app')

@section('page-title','Settings')

@section('style')
    <link rel="stylesheet" href="{{ asset('public/css/settings.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/plugins/bgrins-spectrum/spectrum.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/plugins/bootstrap-select-1.10.0/css/bootstrap-select.css') }}">
    <link href='{{ asset('public/plugins/jquery-daterangepicker/jquery.comiseo.daterangepicker.css') }}' rel='stylesheet' />
    <link href='{{ asset('public/css/jquery.Jcrop.min.css') }}' rel='stylesheet' />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

    @yield('department-style')
    @yield('stages-style')
    @yield('notifications-style')

    <style>
        .bootstrap-select .btn-default {
            height: 24px;
        }
        body {
            line-height: 14px;
        }
    </style>

    <link rel="stylesheet" href="{{ asset('public/plugins/jquery-ui/jquery-ui.css') }}">
    <style>
        #sortable { list-style-type: none; margin: 0; padding: 0; width: 100%; float: left;}
        /*.ui-state-highlight { height: 40px !important; background-color: transparent !important; width: 290px !important; }*/
    </style>
@endsection


@section('content')

    <div class="container margin-t-20 relative">
        <div class="col-md-12 clearfix">
            <div id="main-alert" class="row alert alert-danger {{ !count($formats) && !count($departments) ? '' : 'temp-hide' }}">
                Please complete all the items marked in red below to get your Tapi account up and running. If these items are not completed, you will not be able to start scheduling any tasks.
            </div>
        </div>

    @include('pages.settings._info')
    @include('pages.settings.notification')
    @include('pages.settings._holidays')
    {{--@include('pages.settings._roles')--}}
    @include('pages.settings._department')
    @include('pages.settings._invite')
    @include('pages.settings._employees')
    @include('pages.settings._stages')
    @include('pages.settings._formats')
    @include('pages.settings._clients')
    @include('pages.settings._keys')

    @include('pages.footer')

@endsection

@section('js')
    <script src="{{ asset('public/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('public/js/country_city.js') }}"></script>
    <script src="{{ asset('public/plugins/bgrins-spectrum/spectrum.js') }}"></script>
    <script src="{{ asset('public/plugins/bootstrap-select-1.10.0/js/bootstrap-select.min.js') }}"></script>
    <script src='{{ asset('public/plugins/jquery-daterangepicker/jquery.comiseo.daterangepicker.js') }}'></script>
    <script src='{{ asset('public/js/jquery.Jcrop.min.js') }}'></script>
    <script>

        $('.daterangepicker').daterangepicker({
            datepickerOptions : {
                numberOfMonths : 2,
                maxDate: null,
                minDate: null
            },
            presetRanges: [],
            presets: { dateRange: "Date Range" }
        }).on('change', function(e){
            dateRange = (JSON.parse(e.currentTarget.value));
            $(this).closest('div').find('.hidden .project-start').val(moment(dateRange.start).format('MM/DD/Y'));
            $(this).closest('div').find('.hidden .project-end').val(moment(dateRange.end).format('MM/DD/Y'));
            $(this).closest('div').find('.project-start').first().val(moment(dateRange.start).format('MM/DD/Y') + ' - ' +moment(dateRange.end).format('MM/DD/Y'));

            attachDatePickerListeners(moment(dateRange.end).format('MM/DD/Y'),moment(dateRange.start).format('MM/DD/Y'));
        });

        var colors = JSON.parse('{!! $colors !!}');
        var requirements = 0;
        var workHours = {{ isset($options['workhours']) ? ( $options['workhours'] ? $options['workhours'] : false ) : 8  }};

        function checkRequirements(){

            if( $('#departmentsList li').length && $('#departments .employee.active').length && $('#formatList li').length){
                $('#main-alert').hide();
                $('#sched-link').removeClass('temp-hide').show();
                $('#report-link').removeClass('temp-hide').show();
            } else {
                $('#main-alert').show();
                $('#sched-link').addClass('temp-hide').hide();
                $('#report-link').addClass('temp-hide').hide();
            }
        }

        checkRequirements();

        $('#termsBt').on('click', function(){
            $('#termsModal').modal('show');
        });

        $('#employees').on('click', function(){
            $('#plansModal').modal('show');
        });

//        fixAlert();
    </script>

    @yield('info-js')
    @yield('roles-js')
    @yield('invite-js')
    @yield('department-js')
    @yield('employees-js')
    @yield('stages-js')
    @yield('formats-js')
    @yield('clients-js')
    @yield('keys-js')
    @yield('holidays-js')
    @yield('notifications-js')

    <script>
        $(window).load(function(){
            $('.modal.delete input').keypress(function(e) {

                if(e.which == 13) {
                    e.preventDefault();
                    e.stopPropagation();

                    $(this).closest('.modal-dialog').find('.confirmBt').trigger('click');
                }

            });

            $('#edit_user_scheduler_department').selectpicker('refresh');
            $('#scheduler_department').selectpicker('refresh');
            $('#reportsInterval').trigger('change');
        });

        $('.modal.delete').on('shown.bs.modal',function(){
            $(this).find('input.text-center').val('');
        });

    </script>

@endsection
