<div class="col-md-12">
    <div class="alert row-alert" style="display: none"></div>
    <div class="col-md-12">
        <ul id="keyList">
            @if(count($keys))
                @foreach($keys as $key)
                    <li>
                        <div class="pull-left t24">
                            <p class=margin-b-0>{{ $key->name }} ({{ $key->department->name }})</p>
                        </div>
                        <div class="margin-l-10 pull-left divider-left">
                            <a href="#" data-keyid="{{ $key->id }}" class="edit-key link-text padding-r-0">Edit</a>
                        </div>
                        <div class="margin-l-10 pull-left divider-left">
                            <a href="#" data-keyid="{{ $key->id }}" class="delete-key delete-icon link-icon"></a>
                        </div>
                    </li>
                @endforeach
            @endif
        </ul>
    </div>
</div>