<!-- Modal -->
<div id="deleteHolidayModal" class="modal fade delete" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center" style="width: 250px">

            <!-- Modal content-->
            <div class="modal-content text-center">
                <div class="modal-header">
                    <h4 class="modal-title">Delete Holiday</h4>
                </div>
                <div class="modal-body">
                    Are you sure you want to delete this? Please type "yes" to confirm.<br>
                    {!! Form::open(['route'=>'api.formats.delete']) !!}
                        <input type="hidden" id="deleteHoliday" name="id">
                        <input id="deleteHolidayConfirmInput" class="text-center">
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer text-center-important">
                    <button id="deleteHolidayTrigger" class="createbt btn btn-default confirmBt">Confirm</button>
                    <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>

            </div>

        </div>
    </div>
</div>