<div class="col-md-12">
    <div class="alert row-alert" style="display: none"></div>
    <div class="col-md-12">
        <ul id="clientsList" class="list-with-actions">
            @if(count($clients))
                @foreach($clients as $client)
                    <li>
                        <div class="color-dot padding-l-0">
                            <span style="background-color: {{ $client->color }};"></span>
                            <a href="#">{{ $client->name }}</a>
                        </div>
                        <div class="margin-l-10 pull-left divider-left">
                            <a href="#" data-clientid="{{ $client->id }}" class="edit-client link-text padding-r-0">Edit</a>
                        </div>
                        <div class="margin-l-10 pull-left divider-left">
                            <a href="#" data-clientid="{{ $client->id }}" class="delete-client delete-icon link-icon"></a>
                        </div>
                    </li>
                @endforeach
            @endif
        </ul>
    </div>

    <div class="col-md-3 text-left margin-t-5">
        {!! Form::close() !!}
    </div>
</div>