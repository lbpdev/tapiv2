@section('stages-style')

@endsection

<hr>
<div class="row">
    <div class="row">
        <div class="col-md-8">
            {!! Form::open(['id'=>'keysForm','route'=>'api.keys.store']) !!}

            @if(isset($company))
                <input type="hidden" name="company_id" value="{{ $company->id }}">
            @endif

            <div class="col-md-3">
                Task Types <span class="info-icon"  data-toggle="tooltip" title="For example, your graphic design department may have tasks such as logo creation, pitch work, layout, image editing, creating print files, etc."></span>
                <div class="form-group relative">
                    <input type="text" class="form-control margin-b-5" name="name" placeholder="Add Task Type" value="" required="required" maxlength="30">
                </div>
            </div>
            <div class="col-md-2 padding-r-0 padding-l-0">
                Department
                <div class="small">
                    {!! Form::select('department_id',$departmentList, null ,['id'=>'key_department_select','class'=>' selectpicker']) !!}
                </div>
            </div>
            <div class="col-md-2">
                &nbsp;
                <div class="small" style="">
                    {!! Form::submit('Add',['class'=>'link-bt t-pink link-color margin-b-0 divider-left']) !!}
                </div>
            </div>

            <div class="col-md-12 margin-b-10">
                {!! Form::close() !!}
            </div>
        </div>

        @include('pages.settings._keys-list')

    </div>
</div>


    <!-- Modal -->
    @include('pages.settings._keys-delete-modal')

    <!-- Modal -->
    @include('pages.settings._keys-edit-modal')

@section('keys-js')
    <script>

        /** SEND CREATE REQUEST **/

        $('#keysForm').on('submit', function(e){
            submitBt = $(this).find('input[type=submit]');
            $(submitBt).attr('disabled','disabled');

            var form = $(this);
            processFlag = 1;

            e.preventDefault();
                e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                  type: "POST",
                  url: url,
                  data: data,
                  success: function(response){

                    if(response.status==200) {
                        refreshKeyList(response.data);
                        $(form).find('input[type=text]').val('');
                    }

                    doAlert(response);

                    $(submitBt).removeAttr('disabled');
                  },
                  done : function (){
                    processFlag = 1;
                  }
                });
            }
        });

        /** SEND UPDATE REQUEST **/

        $('#keysEditForm').on('submit', function(e){

            var form = $(this);
            processFlag = 1;

            e.preventDefault();
                e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                  type: "POST",
                  url: url,
                  data: data,
                  success: function(response){

                    if(response.status==200) {
                        refreshKeyList(response.data);
                        $(form).trigger('reset');
                        $(form).closest('.modal').modal('hide');
                    }

                    doAlert(response);
                  },
                  done : function (){
                    processFlag = 1;
                  }
                });
            }
        });


        /** SEND DELETE REQUEST **/


        $('#deleteKeyTrigger').on('click', function(){
            if($('#deleteKeyConfirmInput').val()=="yes"){
                deleteKey();
                $('#deleteKeyModal').modal('hide');
            } else {
                alert('Please follow the instructions');
            }
        });


        /*********************************** FUNCTIONS **************************/

        function refreshKeyList(data){
            $('#keyList').html('');

            for(var x=0;x<data.length;x++){
                el =
                        '<li>'+
                        '<div class="pull-left t24">'+
                        '<p class=margin-b-0>'+data[x]['name']+' ('+data[x]['department']['name'] +')</p>'+
                        '</div>'+
                        '<div class="margin-l-10 pull-left divider-left">'+
                        '<a href="#" data-keyid="'+data[x]['id']+'" class="edit-key link-text padding-r-0">Edit</a>'+
                        '</div>'+
                        '<div class="margin-l-10 pull-left divider-left">'+
                        '<a href="#" data-keyid="'+data[x]['id']+'" class="delete-key delete-icon link-icon"></a>'+
                        '</div>'+
                        '</li>';

                $('#keyList').append(el);

            }

            attachDeleteKeyEvent();
            attachEditKeyEvent();
        }


        function deleteKey(){

            url = "{{ route('api.keys.delete') }}";
            data   = $('#deleteKeyModal form').serialize();
            deleteProcessFlag = 1;

            if(deleteProcessFlag) {
                deleteProcessFlag = 0;

                $.ajax({
                    type: "POST",
                    url: '{{ route('api.keys.delete') }}',
                    data: data,
                    success: function(response){


                        if(response.status==200){
                            refreshKeyList(response.data);
                        }

                        doAlert(response);
                    },
                    done : function (){
                        deleteProcessFlag = 1;
                    }
                });
            }
        }

        function attachDeleteKeyEvent(){
            $('.delete-key').on('click', function(e){

                e.preventDefault();

                keyId = $(this).attr('data-keyid');

                $.ajax({
                    type: "GET",
                    url: '{{ route('api.keys.get') }}?id='+keyId,
                    success: function(response){
                        if(response.status==200){

                            tasksCount = response.data.tasksCount;
                            taskS = tasksCount.length == 1 ? '' : 's' ;
                            projectsCount = response.data.projectsCount;
                            projectS = projectsCount.length == 1 ? '' : 's' ;

                            if(tasksCount || projectsCount){
                                $('#deleteKeyModal .deleteMessage .counts').html('You currently have <b>'+tasksCount+' task'+taskS+'</b> and <b>'+projectsCount+' project'+projectS+'</b> connected to this task type.');
                                $('#deleteKeyModal .deleteMessage').show();
                            }
                            else
                                $('#deleteKeyModal .deleteMessage').hide();

                            if(keyId>0){
                                $('#deleteKeyModal').modal('show');
                                $('#deleteKeyModal input[name=id]').val(keyId);
                            }

                        } else {
                            doAlert(response);
                        }

                    },
                    done : function (){
                        deleteProcessFlag = 1;
                    }
                });
            });
        }

        function attachEditKeyEvent(){
            $('.edit-key').on('click', function(e){

                submitBt = $(this).find('input[type=submit]');
                $(submitBt).attr('disabled','disabled');

                e.preventDefault();

                keyId = $(this).attr('data-keyid');

                if(keyId>0){

                    $.ajax({
                        type: "GET",
                        url: '{{ route('api.keys.get') }}?id='+keyId,
                        success: function(response){

                            if(response.status==200){
                                $('#editKeyModal').modal('show');

                                $.each(response.data.key, function(i,e){
                                    $('#editKeyModal input[name='+i+']').val(e);
                                });

                                $('#edit_key_department_select').val(response.data.key.department_id).selectpicker('refresh'); // #ff0000

                                $('#editKeyModal .sp-preview-inner').css('background-color', response.data.key.color); // #ff0000

                            } else {
                                doAlert(response);
                            }

                        },
                        done : function (){
                            deleteProcessFlag = 1;
                        }
                    });


                }
            });
        }

        attachDeleteKeyEvent();
        attachEditKeyEvent();

        $('#key_department_select').selectpicker('refresh').trigger('change');

    </script>
@endsection