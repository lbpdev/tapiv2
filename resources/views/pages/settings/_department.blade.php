@section('department-style')
@endsection

<hr>
<div class="row">
    <div class="row">
        <div class="col-md-6">
            {!! Form::open(['id'=>'departmentsForm','route'=>'api.departments.store']) !!}

            @if(isset($company))
                <input type="hidden" name="company_id" value="{{ $company->id }}">
            @endif

            <div class="col-md-8 padding-r-0 margin-b-5">
                Departments <span class="info-icon"  data-toggle="tooltip" title="Customise departments for your company to group members of your team. "></span>
                <div class="form-group relative">
                    <input type="text" class="form-control pull-left" style="width: 190px" name="name" placeholder="Add Department" value="" required="required" maxlength="30">
                    <input id="deptColor" class="form-control" name='color' value='{{ count($colors) ? $colors[0] : '#ffdb1a'  }}'/>
                    {!! Form::submit('Add',['class'=>'t-pink link-bt link-color margin-t-0 margin-b-10 pull-left divider-left']) !!}
                </div>
            </div>
            <div class="col-md-1 padding-r-0">

                &nbsp;
            </div>

            <div class="col-md-8">
                <div class="alert alert-danger {{ !count($departments) ? '' : 'temp-hide' }}">Please add at least one department (for example, editorial, design, coding, creative, etc.)</div>
            </div>
        </div>

        @include('pages.settings._department-list')

    </div>
</div>


<!-- Modal -->
@include('pages.settings._departments-edit-modal')

        <!-- Modal -->
@include('pages.settings._departments-delete-modal')

@section('department-js')
    <script>

        $('.info-icon').tooltip();

        $("#deptColor").spectrum({
            showPaletteOnly: true,
            showPalette:true,
            hideAfterPaletteSelect:true,
            color: colors[0],
            palette: [
                colors
            ],
            change: function(color) {
                $('#deptColor').val(color.toHexString()); // #ff0000
                $('#deptColor').attr('value',color.toHexString()); // #ff0000
            }
        }).val(colors[0]).attr('value',colors[0]);

        $("#deptEditColor").spectrum({
            showPaletteOnly: true,
            showPalette:true,
            hideAfterPaletteSelect:true,
            color: colors[0],
            palette: [
                colors
            ],
            change: function(color) {
                $('#deptEditColor').val(color.toHexString()); // #ff0000
                $('#deptEditColor').attr('value',color.toHexString()); // #ff0000
            }
        });
    </script>
    <script>

        var appUrl = '{{ url('/') }}';

        /** SEND CREATE REQUEST **/

        $('#departmentsForm').on('submit', function(e){
            submitBt = $(this).find('input[type=submit]');
            $(submitBt).attr('disabled','disabled');

            var form = $(this);
            processFlag = 1;

            e.preventDefault();
                e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                  type: "POST",
                  url: url,
                  data: data,
                  success: function(response){

                    if(response.status==200) {
                        refreshDepartmentsList(response.data);
                        refreshDepartmentSelect(response.data);
                        $(form).find('input[type=text]').val('');
                    }

                    doAlert(response);
                  },
                  done : function (){
                    processFlag = 1;
                  },
                  complete : function(){
                      $(submitBt).removeAttr('disabled');
                  }
                });
            }
        });

        /** SEND UPDATE REQUEST **/

        $('#deptEditForm').on('submit', function(e){

            submitBt = $(this).find('input[type=submit]')
            $(submitBt).attr('disabled','disabled');

            var form = $(this);
            processFlag = 1;

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){

                        if(response.status==200) {
                            $('#department-row-name-'+response.data.new_department.id).text(response.data.new_department.name);
                            refreshDepartmentsList(response.data);
                            refreshDepartmentSelect(response.data);
                            $(form).trigger('reset');
                            $(form).closest('.modal').modal('hide');
                        }

                        doAlert(response);

                        $(submitBt).removeAttr('disabled');
                    },
                    done : function (){
                        processFlag = 1;
                    }
                });
            }
        });


        /** SEND DELETE REQUEST **/

        function attachDeleteStageEvent() {
            $('.delete-dept').on('click', function (e) {
                e.preventDefault();

                $('#deleteDepartment').val($(this).attr('data-deptid'));

                departmentId = $(this).attr('data-deptid');

                if (departmentId > 0)
                    $('#deleteDepartmentModal').modal('show');
                else
                    alert('Please select a department to delete.');

            });
        }


        function attachEditStageEvent(){
            $('.edit-dept').on('click', function(e){
                e.preventDefault();

                stageId = $(this).attr('data-deptid');
                hours = $(this).find('.hours').val();

                if(stageId>0){

                    $.ajax({
                        type: "GET",
                        url: '{{ route('api.departments.get') }}?id='+stageId,
                        success: function(response){


                            if(response.status==200){
                                $('#editDeptModal').modal('show');

                                $.each(response.data, function(i,e){
                                    $('#editDeptModal input[name='+i+']').val(e);
                                });

                                $('#deptEditColor').attr('value', response.data.color); // #ff0000
                                $('#work_hours').val(response.data.work_hours ? response.data.work_hours : hours).trigger('change');
                                $('#editDeptModal .sp-preview-inner').css('background-color', response.data.color); // #ff0000

                            } else {
                                doAlert(response);
                            }

                        },
                        done : function (){
                            deleteProcessFlag = 1;
                        }
                    });


                }
            });
        }

        $('#deleteDepartmentTrigger').on('click', function(){
            if($('#deleteDepartmentConfirmInput').val()=="yes"){
                deleteDepartment(); 
                $('#deleteDepartmentModal').modal('hide'); 
            } else {
                alert('Please follow the instructions');
            }
        });


        /*********************************** FUNCTIONS **************************/

        function refreshDepartmentsList(data){
            $('#departmentsList').html('');

            if(data.departments.length){

            var depts = data.departments;

            for(var x=0;x<depts.length;x++){
                workH = depts[x]["work_hours"] ? depts[x]["work_hours"] : workHours;

                el = '<li>'+
                           '<div data-deptid="'+depts[x]['id']+'" class="edit-dept pull-left">'+
                                    '<input type="text" name="dept_id[]" class="form-control" readonly="true" value="'+depts[x]['name']+'" required="required">'+
                                    '<div class="pull-left">'+
                                    '<div class="tiny">'+
                                    '<div class="cover"></div>'+
                                    '<input type="text" name="workhours" value="'+ workH +'" id="workhours_from" readonly="readonly" class="hours form-control selectpicker" style="width:40px">'+
                                    'Work Hours'+
                            '</div>'+
                            '</div>'+
                            '<div class="margin-l-10 pull-left">'+
                                    '<div class="color-dot divider-left">'+
                                        '<span style="background-color: '+depts[x]['color']+';"></span>'+
                                        '<a href="#">Color</a>'+
                                    '</div>'+
                                    '</div>'+
                                    '<div class="pull-left divider-left padding-r-5">'+
                                    '<a href="#" data-deptid="'+depts[x]['id']+'" class="link-text">Edit</a>'+
                                    '</div>'+
                                    '</div>'+
                                    '<div class="pull-left divider-left">'+
                                    '<a href="#" data-deptid="'+depts[x]['id']+'" class="delete-dept delete-icon link-icon"></a>'+
                            '</div>'+
                        '</li>';

                $('#departmentsList').append(el);
            }

            if(data.new_department){
                if($('#department-row-'+data.new_department['id']).length<1){
                    element = '<div class="row" id="department-row-'+data.new_department['id']+'">'+
                                '<div class="col-md-12 col-department">'+
                                    '<p class="name" id="department-row-name-'+data.new_department['id']+'">'+data.new_department['name']+'</p>'+
                                    '<div class="col-user" id="employeesList-'+data.new_department['id']+'">'+
                                        '<div class="col-md-12 padding-5-10 padding-l-0">'+
                                            '<div class="pull-left">'+
                                                '<img class="role-icon" src="{{ asset('public/images/icons/user-icons/User.png') }}" height="18" id="role_icon">'+
                                            '</div>'+
                                            '<div class="pull-left">'+
                                                '<p>N/A</p>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>';
                    $('#departments').append(element);
                }
            }

            attachDeleteDeptEvent();
            attachEditDeptEvent();

            $('#departmentsList').closest('.row').find('.alert').hide();
            $('#inviteBt').show();
            $('#inviteBtHidden').hide();

            } else {
                $('#departmentsList').closest('.row').find('.alert').removeClass('temp-hide').show();

                checkRequirements();
            }
        }


        function refreshDepartmentSelect(data){
            $('.selectpicker.department_select').find('option').remove();
            $('#stage_department_select').find('option').remove();
            $('#edit_stage_department_select').find('option').remove();
            $('#key_department_select').find('option').remove();
            $('#edit_user_scheduler_department').find('option').remove();
            $('#scheduler_department').find('option').remove();

            $('#stage_department_select').append('<option value="0">None</option>');
            $('#edit_stage_department_select').append('<option value="0">None</option>');

            for(var x=0;x<data.departments.length;x++){
                el = '<option value="'+data.departments[x]['id']+'">'+data.departments[x]['name']+'</option>';

                $('#edit_user_scheduler_department').append(el);
                $('#scheduler_department').append(el);
                $('.selectpicker.department_select').append(el);
                $('#stage_department_select').append(el);
                $('#edit_stage_department_select').append(el);
                $('#key_department_select').append(el);
            }

            $('#edit_user_scheduler_department').prepend('<option value="0">All departments</option>');
            $('#scheduler_department').prepend('<option value="0">All departments</option>');

            $('.selectpicker').selectpicker('refresh');
            $('#stage_department_select').selectpicker('refresh').trigger('change');
        }

        $('#edit_user_scheduler_department').prepend('<option value="0">All departments</option>');
        $('#edit_user_scheduler_department').val(0).trigger('change');

        $('#scheduler_department').prepend('<option value="0">All departments</option>');
        $('#scheduler_department').val(0).trigger('change');

        $('#stage_department_select').prepend('<option value="0">None</option>');
        $('#stage_department_select').val(0);
        $('#edit_stage_department_select').prepend('<option value="0">None</option>');
        $('#edit_stage_department_select').val(0);

        function refreshEmployeeDepartment(department_id){
            if(department_id)
            $('#department-row-'+department_id).remove();
        }



        function attachDeleteDeptEvent(){
            $('.delete-dept').on('click', function(e){

                e.preventDefault();

                stageId = $(this).attr('data-deptid');

                if(stageId>0){

                    $.ajax({
                        type: "GET",
                        url: '{{ route('api.departments.get') }}?id='+stageId,
                        success: function(response){
                            if(response.status==200){

                                keysCount = response.data.typeCounts;
                                taskS = keysCount == 1 ? '' : 's';
                                usersCount = response.data.usersCount;
                                projectS = usersCount == 1 ? '' : 's' ;

                                if(keysCount || usersCount){
                                    $('#deleteDepartmentModal .deleteMessage .counts').html('You currently have <b>'+usersCount+' user'+projectS+'</b> and <b>'+keysCount+' task type'+taskS+'</b> connected to this department.');
                                    $('#deleteDepartmentModal .deleteMessage').show();
                                }
                                else
                                    $('#deleteDepartmentModal .deleteMessage').hide();

                                $('#deleteDepartmentModal').modal('show');
                                $('#deleteFormat').val(stageId);

                            } else {
                                doAlert(response);
                            }

                        },
                        done : function (){
                            deleteProcessFlag = 1;
                        }
                    });

                    $('#deleteDepartment').val(stageId);
                }
            });
        }

        function attachEditDeptEvent(){
            $('.edit-dept').on('click', function(e){

                e.preventDefault();

                stageId = $(this).attr('data-deptid');

                if(stageId>0){

                    $.ajax({
                        type: "GET",
                        url: '{{ route('api.departments.get') }}?id='+stageId,
                        success: function(response){


                            if(response.status==200){
                                $('#editDeptModal').modal('show');

                                $.each(response.data.department, function(i,e){
                                    $('#editDeptModal input[name='+i+']').val(e);
                                });

                                $('#deptEditColor').attr('value', response.data.department.color);
                                $('#work_hours').val(response.data.department.work_hours).trigger('change');
                                $('#editDeptModal .sp-preview-inner').css('background-color', response.data.department.color); // #ff0000

                            } else {
                                doAlert(response);
                            }

                        },
                        done : function (){
                            deleteProcessFlag = 1;
                        }
                    });


                }
            });
        }


        function deleteDepartment(){

            url = "{{ route('api.departments.delete') }}";
            departmentId = $('#deleteDepartment').val();
            deleteProcessFlag = 1;    

            if(deleteProcessFlag) {
                deleteProcessFlag = 0;

                if(departmentId){
                    $.ajax({
                        type: "GET",
                        url: url+'?department_id='+departmentId,
                        success: function(response){


                            if(response.status==200){
                                $('#deleteDepartment').attr('data-roleid','0');

                                attachDeleteDeptEvent();
                                attachEditDeptEvent();
                                refreshDepartmentsList(response.data);
                                refreshDepartmentSelect(response.data);
                                refreshEmployeeDepartment(response.data.deleted);
                                refreshEmployeeList(response.data.empData);
                            }
                            

                            doAlert(response);
                        },
                        done : function (){
                            deleteProcessFlag = 1;
                        }
                    });
                } else {
                    alert('Please select a department to delete.');
                } 
            } 
        }

        attachDeleteDeptEvent();
        attachEditDeptEvent();

    </script>
@endsection