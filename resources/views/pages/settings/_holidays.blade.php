@section('holidays-style')
@endsection

<hr>
<div class="row">
    <div class="row">
        <div class="col-md-8">
            {!! Form::open(['id'=>'holidaysForm','route'=>'api.holidays.store']) !!}

            @if(isset($company))
                <input type="hidden" name="company_id" value="{{ $company->id }}">
            @endif

            <div class="col-md-4">
                Holidays
                <div class="form-group relative">
                    <input type="text" class="form-control margin-b-5" name="name" placeholder="Add Holiday Name" value="" maxlength="30">
                </div>
            </div>
            <div class="col-md-4 padding-l-0 ">
                Dates
                <div class="form-group relative width-full">
                    <input type="text" class="form-control margin-b-5" id="holidate" name="date" placeholder="D-M-Y to D-M-Y" value="" maxlength="30">
                </div>
            </div>
            <div class="col-md-2 padding-r-0 padding-l-0">
                Annual
                <div class="small">
                    <select name="is_annual" class=" selectpicker">
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
                </div>
            </div>

            <div class="col-md-2 padding-r-0">
                &nbsp;
                <div class="small">
                    {!! Form::submit('Add',['class'=>'link-bt t-pink link-color margin-b-0 divider-left']) !!}
                </div>
            </div>

            <div class="col-md-12 margin-b-10">
                {!! Form::close() !!}
            </div>
        </div>

        <div class="col-md-12">
            <div class="alert row-alert" style="display: none"></div>
            <div class="col-md-12">
                <ul id="holidayList">
                    @if(count($holidays))
                        @foreach($holidays as $holiday)
                            <li>
                                <div class="pull-left t24">
                                    <p class=margin-b-0>{{ $holiday->name }} ({{ $holiday->range }}) {{ $holiday->is_annual ? '(Annual)' : '' }}</p>
                                </div>
                                <div class="margin-l-10 pull-left divider-left">
                                    <a href="#" data-holidayid="{{ $holiday->id }}" class="edit-holiday link-text padding-r-0">Edit</a>
                                </div>
                                <div class="margin-l-10 pull-left divider-left">
                                    <a href="#" data-holidayid="{{ $holiday->id }}" class="delete-holiday delete-icon link-icon"></a>
                                </div>
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>

    </div>
</div>


<!-- Modal -->
@include('pages.settings._holidays-delete')

        <!-- Modal -->
@include('pages.settings._holidays-edit')

@section('holidays-js')
    <script>

        $('#holidate').daterangepicker({
            datepickerOptions : {
                numberOfMonths : 2,
                maxDate: null,
                minDate: null
            },
            presetRanges: [],
            presets: { dateRange: "Date Range" }
        }).on('change', function(e){
            dateRange = (JSON.parse(e.currentTarget.value));
            $(this).closest('div').find('.from').val(moment(dateRange.start).format('MM/DD/Y'));
            $(this).closest('div').find('.to').val(moment(dateRange.end).format('MM/DD/Y'));
        });

        /** SEND CREATE REQUEST **/

        $('#holidaysForm').on('submit', function(e){

            submitBt = $(this).find('input[type=submit]');
            $(submitBt).attr('disabled','disabled');

            var form = $(this);
            processFlag = 1;

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){

                        if(response.status==200) {
                            refreshHolidayList(response.data);
                            $(form).find('input[type=text]').val('');
                        }

                        doAlert(response);

                        $(submitBt).removeAttr('disabled');
                    },
                    done : function (){
                        processFlag = 1;
                    }
                });
            }
        });

        /** SEND DELETE REQUEST **/


        $('#deleteHolidayTrigger').on('click', function(){
            if($('#deleteHolidayConfirmInput').val()=="yes"){
                deleteHoliday();
                $('#deleteHolidayModal').modal('hide');
            } else {
                alert('Please follow the instructions');
            }
        });


        /** SEND UPDATE REQUEST **/

        $('#holidayEditForm').on('submit', function(e){
            var form = $(this);
            processFlag = 1;

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){

                        if(response.status==200) {
                            refreshHolidayList(response.data);
                            $(form).trigger('reset');
                            $(form).closest('.modal').modal('hide');
                        }

                        doAlert(response);
                    },
                    done : function (){
                        processFlag = 1;
                    }
                });
            }
        });


        /*********************************** FUNCTIONS **************************/

        function refreshHolidayList(data){
            $('#holidayList').html('');

            if(data.length) {
                for(var x=0;x<data.length;x++){
                    el =
                            '<li>'+
                                '<div class="pull-left t24">'+
                                    '<p class=margin-b-0>'+data[x]['name']+' ('+moment(data[x]['from']).format('MM/DD/Y')+' - '+moment(data[x]['to']).format('MM/DD/Y')+')'+(data[x]['is_annual'] ? ' (Annual)' : '')+'</p>'+
                                '</div>'+
                                '<div class="margin-l-10 pull-left divider-left">'+
                                    '<a href="#" data-holidayid="'+data[x]['id']+'" class="edit-holiday link-text padding-r-0">Edit</a>'+
                                '</div>'+
                                '<div class="margin-l-10 pull-left divider-left">'+
                                    '<a href="#" data-holidayid="'+data[x]['id']+'" class="delete-holiday delete-icon link-icon"></a>'+
                                '</div>'+
                            '</li>';

                    $('#holidayList').append(el);
                }
                attachDeleteHolidayEvent();
                attachEditHolidayEvent();

                $('#holidayList').closest('.row').find('.alert').hide();
            }
            else {
                $('#holidayList').closest('.row').find('.alert').removeClass('temp-hide').show();
            }
        }


        function deleteHoliday(){

            url = "{{ route('api.holidays.delete') }}";
            data   = $('#deleteHolidayModal form').serialize();
            deleteProcessFlag = 1;

            if(deleteProcessFlag) {
                deleteProcessFlag = 0;

                $.ajax({
                    type: "POST",
                    url: '{{ route('api.holidays.delete') }}',
                    data: data,
                    success: function(response){


                        if(response.status==200){
                            refreshHolidayList(response.data);
                        }

                        doAlert(response);
                    },
                    done : function (){
                        deleteProcessFlag = 1;
                    }
                });
            }
        }

        function attachDeleteHolidayEvent(){
            $('.delete-holiday').on('click', function(e){

                e.preventDefault();

                holidayId = $(this).attr('data-holidayid');

                if(holidayId>0){
                    $('#deleteHolidayModal').modal('show');
                    $('#deleteHoliday').val(holidayId);
                }
            });
        }

        function attachEditHolidayEvent(){
            $('.edit-holiday').on('click', function(e){

                e.preventDefault();

                stageId = $(this).attr('data-holidayid');

                if(stageId>0){

                    $.ajax({
                        type: "GET",
                        url: '{{ route('api.holidays.get') }}?id='+stageId,
                        success: function(response){


                            if(response.status==200){
                                $('#editHolidayModal').modal('show');

                                $.each(response.data, function(i,e){
                                    $('#editHolidayModal input[name='+i+']').val(e);
                                });

                                $('#edit-holidate').daterangepicker({
                                    datepickerOptions : {
                                    numberOfMonths : 2,
                                    maxDate: null,
                                    minDate: null,
                                },
                                presetRanges: [],
                                presets: { dateRange: "Date Range" }
                                });

                                $('#holidayEditForm select[name="is_annual"]').val(response.data.is_annual).trigger('change');

                                setTimeout(function(){
                                    $('#edit-holidate').daterangepicker("setRange", {
                                        start: new Date(response.data.from),
                                        end: new Date(response.data.to)
                                    });

                                },300);

                            } else {
                                doAlert(response);
                            }

                        },
                        done : function (){
                            deleteProcessFlag = 1;
                        }
                    });


                }
            });
        }

        attachDeleteHolidayEvent();
        attachEditHolidayEvent();
    </script>
@endsection