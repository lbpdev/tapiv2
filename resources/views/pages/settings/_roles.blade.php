<hr>
<div class="row">
    <div class="row">
        <div class="col-md-6">
            {!! Form::open(['id'=>'rolesForm','route'=>'api.roles.store']) !!}

            @if(isset($company))
                <input type="hidden" name="company_id" value="{{ $company->id }}">
            @endif

            <div class="col-md-6">
                Roles
                <input type="text" class="form-control" name="name" placeholder="Add a Role" value="" required="required">
            </div>
            <div class="col-md-4 padding-r-0">
                &nbsp;
                <div class="small margin-b-7">
                    {!! Form::select('icon',$userIcons, null ,['id'=>'icon_select','class'=>' selectpicker']) !!}
                </div>
            </div>
            <div class="col-md-2 padding-t-30 padding-l-0">
                &nbsp;
                <img src="" height="22" id="role_icon">
            </div>

        </div>

        <div class="col-md-12">
            <div class="alert row-alert" style="display: none"></div>

                <div class="col-md-12">
                    <ul id="rolesList">
                    @if(count($roles))
                        @foreach($roles as $role)
                            <li>
                                <img src="{{ asset('public/images/icons/user-icons').'/'.$role->icon }}" height="22" id="role_icon"> 
                                {!! Form::input('radio','role_id[]',$role->id,['id'=>'radio'.$role->id]) !!}
                                <label for="radio{{$role->id}}"><span></span>
                                {{ $role->name }}
                                </label>
                            </li>
                        @endforeach
                    @endif
                    </ul>
                </div>


            <div class="col-md-3 text-left margin-t-5">
                {!! Form::submit('Create',['class'=>'link-bt link-color margin-b-0']) !!}
                {!! Form::close() !!}
            </div>
            <div class="col-md-3 text-right margin-t-5">
                <a href="#" id="deleteRole" data-roleid="0">Delete</a>
            </div>
        </div>
    </div>
</div>


    <!-- Modal -->
    <div id="deleteModal" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
      <div class="modal-dialog" style="width: 300px">

        <!-- Modal content-->
        <div class="modal-content text-center">
          <div class="modal-header">
            <h4 class="modal-title">Delete Role</h4>
          </div>
          <div class="modal-body">
            Are you sure you want to delete this? Please type "yes" to confirm.
            {!! Form::open(['route'=>'api.roles.delete']) !!}
                <input type="hidden" name="id" value="0">
                <input id="deleteConfirmInput">
            {!! Form::close() !!}
          </div>
          <div class="modal-footer">
                <button id="deleteRoleTrigger" class="createbt btn btn-default pull-left">Confirm</button>
                <button class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
          </div>

        </div>

      </div>
    </div>

@section('roles-js')
    <script>

        refreshRadioEvent();

        var appUrl = '{{ url('/') }}';

        $('#icon_select').on('change', function(){
            $('#role_icon').attr('src','{{ url('public/images/icons/user-icons') }}/'+$(this).val());
        }).trigger('change');

        /** SEND CREATE REQUEST **/

        $('#rolesForm').on('submit', function(e){
            var form = $(this);
            processFlag = 1;

            e.preventDefault();
                e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                  type: "POST",
                  url: url,
                  data: data,
                  success: function(response){

                    if(response.status==200) {
                        refreshRolesList(response.data);
                        refreshRolesSelect(response.data)
                        $(form).find('input[type=text]').val('');
                        refreshRadioEvent();
                    }

                    doAlert(response);
                  },
                  done : function (){
                    processFlag = 1;
                  }
                });
            }
        });


        /** SEND DELETE REQUEST **/

        $('#deleteRole').on('click', function(e){
            e.preventDefault();

            roleId = $('#deleteRole').attr('data-roleid');

            if(roleId>0){
                $('#deleteModal').modal('show');
                $('#deleteModal input[name=id]').val(roleId);
            }
            else
                alert('Please select a role to delete.');
        });

        $('#deleteRoleTrigger').on('click', function(){
            if($('#deleteConfirmInput').val()=="yes"){
                deleteRole(); 
                $('#deleteModal').modal('hide'); 
            } else {
                alert('Please follow the instructions');
            }
        });


        /*********************************** FUNCTIONS **************************/

        function refreshRolesList(data){
            $('#rolesList').html('');      

            for(var x=0;x<data.length;x++){
                el = '<li>'+
                        '<img src="'+appUrl+'/public/images/icons/user-icons/'+data[x]['icon']+'" height="30" id="role_icon">'+ 
                        '<input id="radio'+data[x]['id']+'" name="role_id[]" type="radio" value="'+data[x]['id']+'">'+
                        '<label for="radio'+data[x]['id']+'"><span></span>'+
                        ' '+data[x]['name']+
                        '</label>'+
                    '</li>';

                $('#rolesList').append(el);
            }
        }

        function refreshRolesSelect(data){
            $('#role_select').find('option').remove();

            for(var x=0;x<data.length;x++){
                el = '<option value="'+data[x]['id']+'">'+data[x]['name']+'</option>';

                $('#role_select').append(el);
            }

            $('.selectpicker').selectpicker('refresh');
        }

        function refreshRadioEvent(){
            $('#rolesList input[type=radio]').on('click', function(){
                $('#deleteRole').attr('data-roleid', $(this).val());
            });
        }

        function deleteRole(){

            url = "{{ route('api.roles.delete') }}";
            data   = $('#deleteModal form').serialize();
            deleteProcessFlag = 1;

            if(deleteProcessFlag) {
                deleteProcessFlag = 0;

                $.ajax({
                    type: "POST",
                    url: '{{ route('api.roles.delete') }}',
                    data: data,
                    success: function(response){


                        if(response.status==200){
                            $('#deleteRole').attr('data-roleid','0');
                            $('#deleteModal input[name=id]').val(roleId);
                            refreshRadioEvent();
                            refreshRolesList(response.data);
                            refreshRolesSelect(response.data);
                        }


                        doAlert(response);
                    },
                    done : function (){
                        deleteProcessFlag = 1;
                    }
                });

            } 
        }

    </script>
@endsection