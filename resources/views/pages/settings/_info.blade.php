{!! Form::open(['route'=>'settings.info.store','files'=>true,'íd'=>'infoForm','accept'=>'image/*']) !!}

@if(isset($company))
    <input type="hidden" name="company_id" value="{{ $company->id }}">
@endif

@if(isset($_GET['m']))
    <div class="alert alert-success">
        {!! $_GET['m'] == 'thank-you' ? '<p>Your payment was successful. Please note that it may take a few minutes for your subscription to reflect on Tapi.</p><p>Thank you for your business and support.</p>' : '' !!}
    </div>
@endif

<div class="row">
    @if(Session::has('message'))
        <div class="col-md-12">
            <div class="alert alert-success">
                {{ Session::get('message') }}
            </div>
        </div>
    @endif

    <div class="col-md-4">
        Company Name
        <input type="text" class="form-control" name="name" placeholder="Write in Company Name"  required="required" value="{{ isset($options['name']) ? $options['name'] : '' }}">

        <div class="row ">
            <div class="col-md-6">
                Location
                <div class="">
                    <select class="form-control selectpicker" id="country" name="country"><option value="" selected="selected">Choose Country</option></select>
                </div>
            </div>
            <div class="col-md-6">
                &nbsp;
                <div class="">
                    <select class="form-control selectpicker" id="city" name="city"><option value="" selected="selected">Choose City</option></select>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-md-6">
                Time Zone
                <?php
                    foreach ($timezones as $key => $value) {
                        $tz[$value] = $key;
                    }
                ?>
                <div class="">
                    {!! Form::select('timezone',$tz, isset($options['timezone']) ? $options['timezone'] : 0,['id'=>'timezone','class'=>'form-control selectpicker']) !!}
                </div>
            </div>
            <div class="col-md-6">
                Plan

                <?php
                    $employeeSelection = [
                        '0' => 'Free Trial (5 Users)',
                        '1' => 'Plan 1 (5 Users)',
                        '2' => 'Plan 2 (10 Users)',
                        '3' => 'Plan 3 (20 Users)',
                        '4' => 'Plan 4 (35 Users)'
                    ]
                ?>

                <?php
                    if(!$subscription)
                    $subscription = Auth::user()->company->subscription;
                ?>
                <div class="">
                    {!! Form::input('text','plan',$subscription ? $employeeSelection[$subscription->plan_id] : 'Free Trial (1-5 Users)', ['id'=>'employees','readonly','class'=>'form-control']) !!}
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-md-6">
                Working Days

                <?php
                    $daysOfWeek = [
                        'Sun' => 'Sun',
                        'Mon' => 'Mon',
                        'Tue' => 'Tue',
                        'Wed' => 'Wed',
                        'Thu' => 'Thu',
                        'Fri' => 'Fri',
                        'Sat' => 'Sat',
                    ]
                ?>

                {{--<div class="row">--}}
                    {{--<div class="col-md-6">--}}
                        {{--<div class="small margin-b-7">--}}
                            {{--{!! Form::select('workdays_from',$daysOfWeek, isset($options['workdays_from']) ? $options['workdays_from'] : 7,['id'=>'workdays_from','class'=>' selectpicker']) !!}--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-6">--}}
                        {{--<div class="small margin-b-7">--}}
                            {{--{!! Form::select('workdays_to',$daysOfWeek, isset($options['workdays_to']) ? $options['workdays_to'] : 4,['id'=>'workdays_to','class'=>' selectpicker']) !!}--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div> --}}

                <div class="row">
                    <div class="col-md-12">
                        <div class="">
                            {!! Form::select('workdays[]',$daysOfWeek, isset($options['workdays']) ? $options['workdays'] : 7,['id'=>'workdays','class'=>' selectpicker','multiple']) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                Working Hours

                <?php 
                    for($x=1;$x<25;$x++)
                        $workHours[$x] = $x;
                ?>

                <div class="row">
                    <div class="col-md-6">
                        <div class="small" style="width: 45px;">
                            {!! Form::select('workhours',$workHours, isset($options['workhours']) ? $options['workhours'] : '8',['id'=>'workhours_from','class'=>'form-control selectpicker']) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                First working day of the week

                <?php
                    $firstDays['Sun'] = 'Sunday';
                    $firstDays['Mon'] = 'Monday';
                ?>

                <div class="row">
                    <div class="col-md-12">
                        <div class="">
                            {!! Form::select('first-day',$firstDays, isset($options['first-day']) ? $options['first-day'] : 'Sun',['class'=>'form-control selectpicker']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 relative" style="width: 330px;">
        Company Logo  <span class="pull-right font-12">300px by 200px</span> <br>
            <input type="file" name="logo" id="logo-input" onchange="onFileSelected(event)">
            <img src="{{ asset(isset($options['logo']) ? 'public/uploads/logo/'.$options['logo'] : 'public/images/logo-placeholder.png') }}" id="company-logo" width="300">
            <br>

        <div style="display: none">
            <input type="text" id="crop-x" name="crop[x]">
            <input type="text" id="crop-y" name="crop[y]">
            <input type="text" id="crop-w" name="crop[w]">
            <input type="text" id="crop-h" name="crop[h]">
            <input type="text" id="crop-mwidth" name="crop[max_width]">
            <input type="text" id="crop-mheight" name="crop[max_height]">
        </div>
    </div>
    <div class="col-md-12">
        <input type="submit" class="link-bt link-color pull-left t-pink" id="saveInfo" value="Save">
    </div>



</div>


{!! Form::close() !!}


@include('pages.settings.cropper')

@section('info-js')

    <script>
        var workToDefault = '{{ isset($options['workhours_to']) ? $options['workhours_to'] : 10 }}';


        $('#saveCrop').on('click',function(){
            $('#saveInfo').trigger('click');
        });

//        function generateWorkHours(start) {
//            var data = [];
//            var started = false;
//
//            for(x=1;x<25;x++){
//
//                if(started)
//                    data[x] = x+':00';
//
//                if(x == start)
//                    started = true;
//
//            }
//            return data;
//        }
//
//        var workHours = generateWorkHours(9);

//        function updateWorkHoursTo(workHours){
//
//            $('#workhours_to').empty();
//
//            var x = document.getElementById("workhours_to");
//
//            $.each(workHours, function(key,value){
//                var option = document.createElement("option");
//                option.text = value;
//                option.value = key;
//
//                if(value)
//                    x.add(option);
//
//            });
//
//            $('#workhours_to').selectpicker('refresh');
//        }
//
//        updateWorkHoursTo(workHours);
//
//        $('#workhours_from').on('change',function(){
//            workHours = generateWorkHours($(this).val());
//            updateWorkHoursTo(workHours);
//        });


        @if(isset($options['country']))
          $('#country').val("{{ $options['country'] }}");
          $('#country').trigger("change");
        @endif

        @if(isset($options['city']))
          setTimeout(function(){
            $('#city').val("{{ $options['city'] }}");
            $('#city').trigger('change');
          },1000);
        @endif


        // Variable to store your files
        var files;

        // Add events
        $('input[type=file]').on('change', prepareUpload);

        // Grab the files and set them to our variable
        function prepareUpload(event)
        {
          files = event.target.files;
        }

        // Catch the form submit and upload the files
        function uploadFiles(event)
        {

          event.stopPropagation(); // Stop stuff happening
            event.preventDefault(); // Totally stop stuff happening

            var fileUpload = new FormData();
            $.each(files, function(key, value)
            {
                data.append(key, value);
            });

            $.ajax({
                url: 'submit.php?files',
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                success: function(data, textStatus, jqXHR)
                {
                    if(typeof data.error === 'undefined')
                    {
                        // Success so call function to process the form
                        submitForm(event, data);
                    }
                    else
                    {
                        // Handle errors here
                    }
                },
                error: function(jqXHR, textStatus, errorThrown)
                {
                    // Handle errors here
                    // STOP LOADING SPINNER
                }
            });
        }


        function onFileSelected(event) {
          var selectedFile = event.target.files[0];
          var reader = new FileReader();

          var imgtag = document.getElementById("company-logo");
          imgtag.title = selectedFile.name;

          reader.onload = function(event) {
            imgtag.src = event.target.result;
            $('#crop-image').attr('src',event.target.result);
          };

          $('#cropperModal').modal('show');

          reader.readAsDataURL(selectedFile);

          setTimeout(function(){
              $('#crop-image').Jcrop({
                  aspectRatio: 300 / 200,
                  setSelect : [300,200,1,1],
                  onChange: showCoords
              });
          },500);
        }

        function showCoords(c)
        {
            $('#crop-x').val(c.x);
            $('#crop-y').val(c.y);
            $('#crop-w').val(c.w);
            $('#crop-h').val(c.h);
            $('#crop-h').val(c.h);
            $('#crop-mwidth').val($('.jcrop-holder').width());
            $('#crop-mheight').val($('.jcrop-holder').height());
        };

        setTimeout(function(){
            $('#workhours_to').val(workToDefault).trigger('change');
        },1000);
    </script>
@endsection