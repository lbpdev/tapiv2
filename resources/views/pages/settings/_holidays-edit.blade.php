<div id="editHolidayModal" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center" style="width: 300px">

            <!-- Modal content-->
            <div class="modal-content clearfix">
                {!! Form::open(['id'=>'holidayEditForm','route'=>'api.holidays.update','class'=>'pull-left width-full']) !!}
                @if(isset($company))
                    <input type="hidden" name="company_id" value="{{ $company->id }}">
                @endif

                <div class="modal-header clearfix">
                    <h4 class="modal-title">Edit Holiday</h4>
                </div>
                <div class="modal-body clearfix width-full">
                    <input type="hidden" name="id" value="0">
                    <div class="row">
                        <div class="col-md-12">
                            Holidays
                            <div class="form-group relative">
                                <input type="text" class="form-control margin-b-5" name="name" placeholder="Add Holiday Name" value="" maxlength="30">
                            </div>
                        </div>
                        <div class="col-md-12">
                            Dates
                            <div class="form-group relative">
                                <input type="text" class="form-control margin-b-5" id="edit-holidate" name="date" placeholder="D-M-Y to D-M-Y" value="" maxlength="30">
                            </div>
                        </div>
                        <div class="col-md-4 text-center col-md-offset-4">
                            Annual
                            <div class="small">
                                <select name="is_annual" class=" selectpicker">
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer clearfix pull-left width-full">
                    {!! Form::submit('Save',['class'=>'createbt btn btn-default margin-b-0']) !!}
                    <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>

                {!! Form::close() !!}
            </div>

        </div>
    </div>
</div>