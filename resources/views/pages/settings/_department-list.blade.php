<div class="col-md-12">
    <div class="alert row-alert" style="display: none"></div>
    <div class="col-md-6">
        <div class="pull-left" style="width: auto">

            <?php
            for($x=0;$x<24;$x++)
                $workHours[] = $x;
            ?>

            <ul id="departmentsList">
                @if(count($departments))
                    @foreach($departments as $department)
                        <li>
                            <div data-deptid="{{ $department->id }}" class="edit-dept pull-left">
                                <input type="text" name="dept_id[]" class="form-control" readonly="true" value="{{ $department->name }}" required="required">
                                <div class="pull-left">
                                    <div class="tiny">
                                        <div class="cover"></div>
                                        <input type="text" name="workhours" value="{{ $department->work_hours ? $department->work_hours : '8' }}" id="workhours_from" readonly="readonly" class="hours form-control selectpicker" style="width:40px">
                                        Work Hours

                                    </div>
                                </div>
                                <div class="margin-l-10 pull-left">
                                    <div class="color-dot divider-left">
                                        <span style="background-color: {{ $department->color }};"></span>
                                        <a href="#">Color</a>
                                    </div>
                                </div>
                                <div class="pull-left divider-left padding-r-5">
                                    <a href="#" data-deptid="{{ $department->id }}" class="link-text">Edit</a>
                                </div>
                            </div>
                            <div class="pull-left divider-left">
                                <a href="#" data-deptid="{{ $department->id }}" class="delete-dept delete-icon link-icon"></a>
                            </div>
                        </li>
                    @endforeach
                @endif
            </ul>
        </div>
    </div>

    <div class="col-md-3 text-left margin-t-8">
        {!! Form::close() !!}
    </div>
</div>