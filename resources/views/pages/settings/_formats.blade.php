@section('department-style')
@endsection

<hr>
<div class="row">
    <div class="row">
        <div class="col-md-8">
            {!! Form::open(['id'=>'formatsForm','route'=>'api.formats.store']) !!}

            @if(isset($company))
                <input type="hidden" name="company_id" value="{{ $company->id }}">
            @endif

            <div class="col-md-6">
                Deliverables <span class="info-icon"  data-toggle="tooltip" title="Deliverables are products or services that you deliver to your customers. For example, an advertising agency will provide print adverts, campaigns, TV ads, radio ads, etc. "></span>
                <div class="form-group">
                    <input type="text" class="form-control margin-b-5 pull-left" style="width: 190px" name="name" placeholder="Add a Deliverable" value="" required="required" maxlength="30">
                    {!! Form::submit('Add',['class'=>'t-pink link-bt link-color margin-t-0 margin-l-10 margin-b-10 pull-left']) !!}
                </div>
            </div>

            <div class="col-md-12">
                {!! Form::close() !!}
            </div>

            <div class="col-md-6">
                <div class="alert alert-danger {{ !count($formats) ? '' : 'temp-hide' }}">Please create at least one deliverable (for example, brochure, blog post, HTML mailer, report, etc.)</div>
            </div>
        </div>

        @include('pages.settings._formats-list')

    </div>
</div>


<!-- Modal -->
@include('pages.settings._formats-delete')

        <!-- Modal -->
@include('pages.settings._formats-edit')

@section('formats-js')
    <script>

        /** SEND CREATE REQUEST **/

        $('#formatsForm').on('submit', function(e){

            submitBt = $(this).find('input[type=submit]');
            $(submitBt).attr('disabled','disabled');

            var form = $(this);
            processFlag = 1;

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){

                        if(response.status==200) {
                            refreshFormatList(response.data);
                            $(form).find('input[type=text]').val('');
                        }

                        doAlert(response);

                        $(submitBt).removeAttr('disabled');
                    },
                    done : function (){
                        processFlag = 1;
                    }
                });
            }
        });

        /** SEND UPDATE REQUEST **/

        $('#formatsEditForm').on('submit', function(e){
            var form = $(this);
            processFlag = 1;

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){

                        if(response.status==200) {
                            refreshStageList(response.data);
                            $(form).trigger('reset');
                            $(form).closest('.modal').modal('hide');
                        }

                        doAlert(response);
                    },
                    done : function (){
                        processFlag = 1;
                    }
                });
            }
        });


        /** SEND DELETE REQUEST **/


        $('#deleteFormatTrigger').on('click', function(){
            if($('#deleteFormatConfirmInput').val()=="yes"){
                deleteFormat();
                $('#deleteFormatModal').modal('hide');
            } else {
                alert('Please follow the instructions');
            }
        });


        /** SEND UPDATE REQUEST **/

        $('#formatEditForm').on('submit', function(e){
            var form = $(this);
            processFlag = 1;

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){

                        if(response.status==200) {
                            refreshFormatList(response.data);
                            $(form).trigger('reset');
                            $(form).closest('.modal').modal('hide');
                        }

                        doAlert(response);
                    },
                    done : function (){
                        processFlag = 1;
                    }
                });
            }
        });


        /*********************************** FUNCTIONS **************************/

        function refreshFormatList(data){
            $('#formatList').html('');

            if(data.length) {
                for(var x=0;x<data.length;x++){

                    el =
                        '<li>'+
                            '<div class="pull-left t24">'+
                                '<p class=margin-b-0>'+data[x]['name']+'</p>'+
                            '</div>'+
                            '<div class="margin-l-10 pull-left divider-left">'+
                                '<a href="#" data-formatid="'+data[x]['id']+'" class="edit-format link-text padding-r-0">Edit</a>'+
                            '</div>'+
                            '<div class="margin-l-10 pull-left divider-left">'+
                                '<a href="#" data-formatid="'+data[x]['id']+'" class="delete-format delete-icon link-icon"></a>'+
                            '</div>'+
                        '</li>';

                    $('#formatList').append(el);
                }
                attachDeleteFormatEvent();
                attachEditFormatEvent();

                $('#formatList').closest('.row').find('.alert').hide();
                checkRequirements();
            }
            else {
                $('#formatList').closest('.row').find('.alert').removeClass('temp-hide').show();
                checkRequirements();
            }
        }


        function deleteFormat(){

            url = "{{ route('api.formats.delete') }}";
            data   = $('#deleteFormatModal form').serialize();
            deleteProcessFlag = 1;

            if(deleteProcessFlag) {
                deleteProcessFlag = 0;

                $.ajax({
                    type: "POST",
                    url: '{{ route('api.formats.delete') }}',
                    data: data,
                    success: function(response){


                        if(response.status==200){
                            refreshFormatList(response.data);
                        }

                        doAlert(response);
                    },
                    done : function (){
                        deleteProcessFlag = 1;
                    }
                });
            }
        }

        function attachDeleteFormatEvent(){
            $('.delete-format').on('click', function(e){

                e.preventDefault();

                stageId = $(this).attr('data-formatid');

                if(stageId>0){

                    $.ajax({
                        type: "GET",
                        url: '{{ route('api.formats.get') }}?id='+stageId,
                        success: function(response){
                            if(response.status==200){

                                tasksCount = response.data.tasksCount;
                                taskS = tasksCount.length == 1 ? '' : 's' ;
                                projectsCount = response.data.projectsCount;
                                projectS = projectsCount.length == 1 ? '' : 's' ;

                                if(tasksCount || projectsCount){
                                    $('#deleteFormatModal .deleteMessage .counts').html('You currently have <b>'+projectsCount+' project'+projectS+'</b> and <b>'+tasksCount+' task'+taskS+'</b> connected to this deliverable.');
                                    $('#deleteFormatModal .deleteMessage').show();
                                }
                                else
                                    $('#deleteFormatModal .deleteMessage').hide();

                                $('#deleteFormatModal').modal('show');
                                $('#deleteFormat').val(stageId);

                            } else {
                                doAlert(response);
                            }

                        },
                        done : function (){
                            deleteProcessFlag = 1;
                        }
                    });

                }
            });
        }

        function attachEditFormatEvent(){
            $('.edit-format').on('click', function(e){

                e.preventDefault();

                stageId = $(this).attr('data-formatid');

                if(stageId>0){

                    $.ajax({
                        type: "GET",
                        url: '{{ route('api.formats.get') }}?id='+stageId,
                        success: function(response){


                            if(response.status==200){
                                $('#editFormatModal').modal('show');

                                $.each(response.data.format, function(i,e){
                                    $('#editFormatModal input[name='+i+']').val(e);
                                });

                            } else {
                                doAlert(response);
                            }

                        },
                        done : function (){
                            deleteProcessFlag = 1;
                        }
                    });


                }
            });
        }

        attachDeleteFormatEvent();
        attachEditFormatEvent();
    </script>
@endsection