    <hr style="border-top: 1px solid rgba(0,0,0,.1);margin-bottom: 20px;">
    {{--<div class="row">--}}
        {{--<div class="col-md-12">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-4">--}}
                    {{--Departments--}}
                {{--</div>--}}
                {{--<div class="col-md-8 padding-r-0">--}}
                    {{--Member of Staff--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="row"> 
        <div class="col-md-12" id="departments">
            @foreach($departments as $department)
                <div class="row" id="department-row-{{ $department->id }}">
                    <div class="col-md-12 col-department">
                        <p class="name" id="department-row-name-{{ $department->id }}">{{ $department->name }}</p>
                        <div class="col-user" id="employeesList-{{ $department->id }}">
                            @forelse($department->users as $user)
                                <div class="col-md-12 padding-5-10 padding-l-0 {{$user->is_active ? '' : 'inactive'}}" id="user-row-{{$user->id}}">
                                    <div class="pull-left padding-r-5">
                                        <img class="role-icon {{$user->role->slug}} {{ $user->is_active ? 'active' : '' }}" src="{{ asset('public/images/icons/user-icons').'/'.$user->role->icon }}" height="18" id="role_icon">
                                    </div>
                                    <div class="pull-left">
                                        <p>{{ $user->name }}</p>
                                    </div>
                                    <div class="pull-left">
                                        <?php
                                            if($user->lastLogin){
                                                $lastLogin = \Carbon\Carbon::parse($user->lastLogin->created_at);
                                                $lastLogin->setTimezone($user->company->timezone);
                                                $lastLogin = \Carbon\Carbon::parse($lastLogin)->format('d-m-Y H:i');
                                            }
                                        ?>
                                        {{--<a href="#" data-userid="{{ $user->id }}" class="delete-user delete-icon link-icon"></a>--}}
                                        {{--<span class="link-divider"></span>--}}
                                        <a href="#" data-userid="{{ $user->id }}" class="divider-left link-text edit-user">Edit</a>
                                        <a href="#" data-userid="{{ $user->id }}" class="divider-left toggle-user link-text">{{ $user->is_active ? 'Deactivate' : 'Activate' }}</a>
                                        <a href="#" data-userid="{{ $user->id }}" class="divider-left link-text delete-user">Delete</a>
                                        <p class="last-login">{{ $user->lastLogin ? '(Last login:'.$lastLogin.' )' : '' }}</p>
                                    </div>
                                </div>
                            @empty
                                <div class="col-md-12 padding-r-5 padding-l-0 placeholder">
                                    <div class="pull-left padding-r-5">
                                        <img class="role-icon" src="{{ asset('public/images/icons/user-icons/User.png') }}" height="18" id="role_icon">
                                    </div>
                                    <div class="pull-left">
                                        <p>No user assigned to this department yet.</p>
                                    </div>
                                </div>
                            @endforelse
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="col-md-12" id="unassigned_users" {{ count($unassigned_users) ? '' : 'style=display:none'}}>
                <div class="row" id="department-row-0">
                    <div class="col-md-12 col-department">
                        @if($unassigned_users)
                        <p class="name">Unassigned Users</p>
                            <div class="col-user" id="employeesList-0">
                                @forelse($unassigned_users as $user)
                                    <div class="col-md-12 padding-5-10 padding-l-0" id="user-row-{{$user->id}}">
                                        <div class="pull-left padding-r-5">
                                            <img class="role-icon {{$user->role->slug}} {{ $user->is_active ? 'active' : '' }}" src="{{ asset('public/images/icons/user-icons').'/'.$user->role->icon }}" height="18" id="role_icon">
                                        </div>
                                        <div class="pull-left">
                                            <p>{{ $user->name }}</p>
                                        </div>
                                        <div class="pull-left">
                                            <?php
                                                if($user->lastLogin){
                                                    $lastLogin = \Carbon\Carbon::parse($user->lastLogin->created_at);
                                                    $lastLogin->setTimezone($user->company->timezone);
                                                    $lastLogin = \Carbon\Carbon::parse($lastLogin)->format('d-m-Y H:i');
                                                }
                                            ?>
                                            {{--<a href="#" data-userid="{{ $user->id }}" class="delete-user delete-icon link-icon"></a>--}}
                                            {{--<span class="link-divider"></span>--}}
                                            <a href="#" data-userid="{{ $user->id }}" class="divider-left link-text edit-user">Assign</a>
                                            <p class="last-login">{{ $user->lastLogin ? '(Last login:'.$lastLogin.' )' : '' }}</p>
                                        </div>
                                    </div>
                                @empty
                                @endforelse
                            </div>
                        @endif

                    </div>
                </div>
        </div>
    </div>

    @include('pages.settings._employees-edit-modal')
    @include('pages.settings._employees-delete')

@section('employees-js')
    <script>

        $('#edit_user_role_select').on('change',function(){
            if($(this).val()==2)
                $('#scheduler-for').show();
            else
                $('#scheduler-for').hide();
        });

        /** SEND UPDATE REQUEST **/

        $('#userEditForm').on('submit', function(e){
            var form = $(this);
            processFlag = 1;

            e.preventDefault();
                e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                  type: "POST",
                  url: url,
                  data: data,
                  success: function(response){

                    if(response.status==200) {
                        refreshEmployeeList(response.data);
                        $(form).trigger('reset');
                        $(form).closest('.modal').modal('hide');
                    }

                    doAlert(response);
                    checkRequirements();
                  },
                  done : function (){
                    processFlag = 1;
                  }
                });
            }
        });

        /** SEND DELETE REQUEST **/

        $('#deleteUserTrigger').on('click', function(){
            if($('#deleteUserConfirmInput').val()=="yes"){
                deleteUser($(this).attr('data-userid'));
            } else {
                alert('Please follow the instructions');
            }
        });

        /*********************************** FUNCTIONS **************************/

        function deleteUser(user_id){

            url = "{{ route('api.users.delete') }}";
            deleteProcessFlag = 1;

            if(deleteProcessFlag) {
                deleteProcessFlag = 0;

                if(user_id){
                    $.ajax({
                        type: "GET",
                        url: url+'?user_id='+user_id,
                        success: function(response){

                            if(response.status==200){
                                $('#deleteRole').attr('data-roleid','0');
                                $('#deleteUserModal').modal('hide');
                            }
                            
                            doAlert(response);
                            $('#user-row-'+response.data).remove();

                            if($('#departments .employee.active').length){
                                $('#inviteForm').find('.alert').removeClass('temp-hide').hide();
                                checkRequirements();
                            }
                            else{
                                $('#inviteForm').find('.alert').hide();
                                checkRequirements();
                            }

                            refreshUserLimit();
                        },
                        done : function (){
                            deleteProcessFlag = 1;
                        }
                    });
                } else {
                    alert('Please select a department to delete.');
                } 
            } 
        }


        function attachDeleteUserEvent(){
            $('.delete-user').on('click', function(e){
                e.preventDefault();
                $('#deleteUserTrigger').attr('data-userid', $(this).attr('data-userid'));
                $('#deleteUserModal').modal('show');
            });
        }


        function toggleUser(user_id){

            url = "{{ route('api.users.toggleStatus') }}";
            toggleProcessFlag = 1;

            if(toggleProcessFlag) {
                toggleProcessFlag = 0;

                if(user_id){
                    $.ajax({
                        type: "GET",
                        url: url+'?user_id='+user_id,
                        success: function(response){

                            if(response.status==200){

                               if(response.data>0){
                                   $('#user-row-'+user_id).find('.toggle-user').html('Deactivate');
                                   $('#user-row-'+user_id).find('img.role-icon').addClass('active');
                                   $('#user-row-'+user_id).removeClass('inactive');
                               }
                               else{
                                   $('#user-row-'+user_id).find('.toggle-user').html('Activate');
                                   $('#user-row-'+user_id).find('img.role-icon').removeClass('active');
                                   $('#user-row-'+user_id).addClass('inactive');
                               }
                            }

                            doAlert(response);

                            if($('#departments .employee.active').length < 1){
                                $('#inviteForm').find('.alert').show();
                                checkRequirements();
                            } else{
                                $('#inviteForm').find('.alert').hide();
                                checkRequirements();
                            }
                            refreshUserLimit();
                        },
                        done : function (){
                            toggleProcessFlag = 1;
                        }
                    });
                } else {
                }
            }
        }

        function attachToggleUserEvent(){
            $('.toggle-user').unbind('click');
            $('.toggle-user').on('click', function(e){
                e.preventDefault();
                userid = $(this).attr('data-userid');

                if(userid)
                    toggleUser(userid);
            });
        }

        attachToggleUserEvent();

        function attachEditUserEvent(){
            $('.edit-user').on('click', function(e){

                e.preventDefault();

                userId = $(this).attr('data-userid');

                if(userId>0){

                    $.ajax({
                        type: "GET",
                        url: '{{ route('api.users.get') }}?id='+userId,
                        success: function(response){

                            if(response.status==200){
                                $('#editUserModal').modal('show');

                                $.each(response.data, function(i,e){
                                    $('#editUserModal input[name='+i+']').val(e);

                                    if( i=="department" || i=="role" && e.length){
                                        if(e[0])
                                            $('#editUserModal select[name='+i+'_id]').val(e[0]['id']).trigger('change');
                                    }

                                });

                                scheduleDepartment = response.data.schedule_department;

                                if(response.data.role[0].slug=='scheduler'){
                                    if(scheduleDepartment.length){
                                        if(scheduleDepartment[0])
                                            $('#edit_user_scheduler_department').val(scheduleDepartment[0]['pivot'].department_id).trigger('change');
                                    } else {
                                        $('#edit_user_scheduler_department').val(0).trigger('change');
                                    }

                                }


                            } else {
                                doAlert(response);
                            }

                        },
                        done : function (){
                            deleteProcessFlag = 1;
                        }
                    });


                }
            });
        }

        attachEditUserEvent();

        attachDeleteUserEvent();

        function refreshEmployeeList(rows){

            $('#departments').html('');
            $('#unassigned_users').html('');

            if(rows[1].length)
                $('#unassigned_users').show();
            else
                $('#unassigned_users').hide();

            var employeesCount = 0;

            for(var z=0;z<rows.length;z++){

                // z==1 ( No Department )
                for(var x=0;x<rows[z].length;x++){

                    data = rows[z][x];

                    departmentId = z==0 ? data['id'] : 0;
                    departmentName = z==0 ? data['name'] : 'Unassigned Users';

                    el =
                        '<div class="row" id="department-row-'+departmentId+'">'+
                            '<div class="col-md-12 col-department">'+
                                '<p class="name">'+departmentName+'</p>'+
                                    '<div class="col-user" id="employeesList-'+departmentId+'">';

                        if(data['users'].length){
                            for(var y=0;y<data['users'].length;y++){

                                employeeClass = data['users'][y]['role'][0]['slug'] == "employee" ? "employee" : "";
                                activeClass =  data['users'][y]['is_active'] ? 'active' : '';
                                activity =  data['users'][y]['is_active'] ? 'Deactivate' : 'Activate';
                                lastLogin =   data['users'][y]['last_login']!="" ? '(Last login: '+ data['users'][y]['last_login'] +')' : '';

                                el +=
                                    '<div class="col-md-12 padding-5-10 padding-l-0 '+ (data['users'][y]['is_active'] ? '' : 'inactive') +'" id="user-row-'+data['users'][y]['id']+'">'+
                                        '<div class="pull-left padding-r-5">'+
                                            '<img class="role-icon '+ employeeClass +' '+activeClass+'" src="{{ asset('public/images/icons/user-icons/') }}/'+data['users'][y]['role'][0]['icon']+'" height="18" id="role_icon">'+
                                        '</div>'+
                                        '<div class="pull-left">'+
                                            '<p>'+data['users'][y]['name']+'</p>'+
                                        '</div>'+
                                        '<div class="pull-left">'+
                                            '<a href="#" data-userid="'+data['users'][y]['id']+'" class="divider-left link-text edit-user">'+ (z==0 ? 'Edit' : 'Assign') +'</a>';

                                if(z==0)
                                    el +=       '<a href="#" data-userid="'+data['users'][y]['id']+'" class="divider-left toggle-user link-text">'+activity+'</a>';

                                el +=       '<a href="#" data-userid="'+data['users'][y]['id']+'" class="divider-left delete-user link-text">Delete</a>';

                                el +=       '<p class="last-login">'+lastLogin+'</p>'+
                                        '</div>'+
                                    '</div>';
                            }
                        } else {
                            el +=
                                    '<div class="col-md-12 padding-5-10 padding-l-0">'+
                                        '<div class="pull-left">'+
                                            '<img class="role-icon" src="{{ asset('public/images/icons/user-icons/User.png') }}" height="18" id="role_icon">'+
                                        '</div>'+
                                        '<div class="pull-left">'+
                                            '<p>N/A</p>'+
                                        '</div>'+
                                    '</div>';
                        }

                    el +=
                                '</div>'+
                            '</div>'+
                        '</div>';

                    if(z==0)
                        $('#departments').append(el);
                    else
                        $('#unassigned_users').append(el);
                }
            }


            if($('#departments .employee.active').length){
                $('#inviteForm').find('.alert').removeClass('temp-hide').hide();
                checkRequirements();
            }
            else{
                $('#inviteForm').find('.alert').hide();
                checkRequirements();
            }

            attachDeleteUserEvent();
            attachEditUserEvent();
            attachToggleUserEvent();
        }

        if($('#departments .employee.active').length){
            $('#inviteForm').find('.alert').removeClass('temp-hide').hide();
            $('#main-alert').hide();
            $('#bottom-nav').removeClass('temp-hide').show();
        }
        else{
            $('#inviteForm').find('.alert').addClass('temp-hide').show();
            $('#main-alert').show();
            $('#bottom-nav').addClass('temp-hide').hide();
        }
    </script>
@endsection