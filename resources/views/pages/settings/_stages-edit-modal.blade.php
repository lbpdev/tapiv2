<div id="editStageModal" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="vertical-alignment-helper">
      <div class="modal-dialog vertical-align-center" style="width: 300px">

        <!-- Modal content-->
        <div class="modal-content clearfix">
        {!! Form::open(['id'=>'stagesEditForm','route'=>'api.stages.update','class'=>'pull-left width-full']) !!}
            @if(isset($company))
                <input type="hidden" name="company_id" value="{{ $company->id }}">
            @endif

            <div class="modal-header clearfix">
            <h4 class="modal-title">Edit Project Service</h4>
          </div>
          <div class="modal-body clearfix width-full">

            <input type="hidden" name="id" value="0">
            <div class="col-md-12">
                <div class="row">
                    <div class="color-label">
                        <p class="pull-left">Project Service</p>
                        <p class="pull-right"><input id="stageEditColor" class="form-control" name='color' value='#3355cc'/></p>
                    </div>
                    <div class="form-group relative pull-left width-full">
                        <input type="text" class="form-control margin-b-5" name="name" placeholder="Project stage name" value="" required="required" maxlength="30">
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="small margin-b-7">
                        Service Type
                        <select name="internal" class=" selectpicker" id="edit_stage_department_internal">
                            <option value="1">Internal / In-house</option>
                            <option value="0">External / Freelance</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    Link to Department
                    <div class="small margin-b-7">
                        {!! Form::select('department_id',$departmentList, null ,['id'=>'edit_stage_department_select','class'=>' selectpicker']) !!}
                    </div>

                    <?php
                    $hasTimeOptions = [
                            1 => 'Yes',
                            0 => 'No'
                    ]
                    ?>

                    Can allocate time
                    <div class="small margin-b-7">
                        {!! Form::select('hasTime',$hasTimeOptions, null ,['class'=>'selectpicker']) !!}
                    </div>
                </div>

            </div>

          </div>
          <div class="modal-footer clearfix pull-left width-full">
                {!! Form::submit('Save',['class'=>'createbt btn btn-default margin-b-0']) !!}
                <button class="btn btn-default" data-dismiss="modal">Cancel</button>
          </div>

        {!! Form::close() !!}
        </div>

      </div>
  </div>
</div>