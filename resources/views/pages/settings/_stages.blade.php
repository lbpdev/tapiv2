@section('stages-style')

@endsection

<hr>
<div class="row">
    <div class="row">
        <div class="col-md-8">
            {!! Form::open(['id'=>'stagesForm','route'=>'api.stages.store']) !!}

            @if(isset($company))
                <input type="hidden" name="company_id" value="{{ $company->id }}">
            @endif

            <div class="col-md-4">
                Services <span class="info-icon"  data-toggle="tooltip" title="If you are an advertising agency a service could include graphic design, copy writing or media buying. A service must be categorised as internal or external. Internal services are those that will be produced in-house. External services are those provided by third party suppliers. "></span>
                <div class="form-group relative">
                    <input type="text" class="form-control margin-b-5" name="name" placeholder="Add a Service" value="" required="required" maxlength="30">
                </div>
            </div>
            <div class="col-md-2 padding-r-0 padding-l-0">
                &nbsp;
                <div class="small">
                    <select name="internal" class=" selectpicker">
                        <option value="1">Internal / In-house</option>
                        <option value="0">External / Freelance</option>
                    </select>
                </div>
            </div>
            <div class="col-md-2 padding-r-0">
                Link to Department
                <div class="small">
                    {!! Form::select('department_id',$departmentList, null ,['id'=>'stage_department_select','class'=>' selectpicker']) !!}
                </div>
            </div>
            <div class="col-md-2 padding-r-0">
                Can allocate time
                <div class="small">
                    <?php
                        $hasTimeOptions = [
                            1 => 'Yes',
                            0 => 'No'
                        ]
                    ?>
                    {!! Form::select('hasTime',$hasTimeOptions, null ,['class'=>' selectpicker']) !!}
                </div>
            </div>
            <div class="col-md-2 padding-r-0">
                &nbsp;
                <div class="small" style="margin-left: -10px">
                    <input id="stageColor" class="form-control" name='color' value='{{ count($colors) ? $colors[0] : '#ffdb1a'  }}'/>
                    {!! Form::submit('Add',['class'=>'link-bt t-pink link-color margin-b-0 divider-left']) !!}
                </div>
            </div>

            <div class="col-md-12 margin-b-10">
                {!! Form::close() !!}
            </div>
        </div>

        @include('pages.settings._stages-list')

    </div>
</div>


    <!-- Modal -->
    @include('pages.settings._stages-delete-modal')

    <!-- Modal -->
    @include('pages.settings._stages-edit-modal')

@section('stages-js')
    <script src="{{ asset('public/plugins/bgrins-spectrum/spectrum.js') }}"></script>
    <script src="{{ asset('public/plugins/jquery-ui/jquery-ui.js') }}"></script>
    <script>
        $("#stageColor").spectrum({
            showPaletteOnly: true,
            showPalette:true,
            hideAfterPaletteSelect:true,
            color: colors[0],
            palette: [
                colors
            ],
            change: function(color) {
                $('#stageColor').val(color.toHexString()); // #ff0000
                $('#stageColor').attr('value',color.toHexString()); // #ff0000
            }
        }).val(colors[0]).attr('value',colors[0]);

        $("#stageEditColor ").spectrum({
            showPaletteOnly: true,
            showPalette:true,
            hideAfterPaletteSelect:true,
            color: colors[0],
            palette: [
                colors
            ],
            change: function(color) {
                $('#stageEditColor').val(color.toHexString()); // #ff0000
                $('#stageEditColor').attr('value',color.toHexString()); // #ff0000
            }
        });
    </script>
    <script>

        /** SEND CREATE REQUEST **/

        $('#stagesForm').on('submit', function(e){

            submitBt = $(this).find('input[type=submit]');
            $(submitBt).attr('disabled','disabled');

            var form = $(this);
            processFlag = 1;

            e.preventDefault();
                e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                  type: "POST",
                  url: url,
                  data: data,
                  success: function(response){

                    if(response.status==200) {
                        refreshStageList(response.data);
                        $(form).find('input[type=text]').val('');
                    }

                    doAlert(response);


                    $(submitBt).removeAttr('disabled');
                  },
                  done : function (){
                    processFlag = 1;
                  }
                });
            }
        });

        /** SEND UPDATE REQUEST **/

        $('#stagesEditForm').on('submit', function(e){
            var form = $(this);
            processFlag = 1;

            e.preventDefault();
                e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                  type: "POST",
                  url: url,
                  data: data,
                  success: function(response){

                    if(response.status==200) {
                        refreshStageList(response.data);
                        $(form).trigger('reset');
                        $(form).closest('.modal').modal('hide');
                    }

                    doAlert(response);
                  },
                  done : function (){
                    processFlag = 1;
                  }
                });
            }
        });


        /** SEND DELETE REQUEST **/


        $('#deleteStageTrigger').on('click', function(){
            if($('#deleteStageConfirmInput').val()=="yes"){
                deleteStage();
                $('#deleteStageModal').modal('hide');
            } else {
                alert('Please follow the instructions');
            }
        });


        /*********************************** FUNCTIONS **************************/

        function refreshStageList(data){
            $('#stageListInternal').html('');
            $('#stageListExternal').html('');

            for(var x=0;x<data.length;x++){

                el =
                '<li class="ui-state-default" data-stage-id="'+data[x]['id']+'">'+
                    '<span class="glyphicon glyphicon-option-vertical drag-handle" aria-hidden="true"></span>'+
                    '<div class="color-dot padding-l-0">'+
                        '<span style="background-color: '+data[x]['color']+'"></span>'+
                        '<a href="#">'+data[x]['name']+'</a>'+
                    '</div>'+
                    '<div class="pull-left divider-left">'+
                        '<a href="#" data-stageid="'+data[x]['id']+'" class="edit-stage link-text padding-r-0">Edit</a>'+
                    '</div>'+
                    '<div class="pull-left divider-left margin-l-10">'+
                        '<a href="#" data-stageid="'+data[x]['id']+'" class="delete-stage delete-icon link-icon"></a>'+
                    '</div>'+
                '</li>';

                if(data[x]['internal'])
                    $('#stageListInternal').append(el);
                else
                    $('#stageListExternal').append(el);
            }

            attachDeleteStageEvent();
            attachEditStageEvent();
            attachSortableStagesEvent();
        }


        function deleteStage(){

            url = "{{ route('api.stages.delete') }}";
            data   = $('#deleteStageModal form').serialize();
            deleteProcessFlag = 1;

            if(deleteProcessFlag) {
                deleteProcessFlag = 0;

                $.ajax({
                    type: "POST",
                    url: '{{ route('api.stages.delete') }}',
                    data: data,
                    success: function(response){


                        if(response.status==200){
                            refreshStageList(response.data);
                        }

                        doAlert(response);
                    },
                    done : function (){
                        deleteProcessFlag = 1;
                    }
                });
            }
        }

        function attachDeleteStageEvent(){
            $('.delete-stage').on('click', function(e){

                e.preventDefault();

                stageId = $(this).attr('data-stageid');

                $.ajax({
                    type: "GET",
                    url: '{{ route('api.stages.get') }}?id='+stageId,
                    success: function(response){
                        if(response.status==200){

                            tasksCount = response.data.tasksCount;
                            taskS = tasksCount.length == 1 ? '' : 's' ;
                            projectsCount = response.data.projectsCount;
                            projectS = projectsCount.length == 1 ? '' : 's' ;

                            if(tasksCount || projectsCount){
                                $('#deleteStageModal .deleteMessage .counts').html('You currently have <b>'+tasksCount+' task'+taskS+'</b> and <b>'+projectsCount+' project'+projectS+'</b> connected to this service.');
                                $('#deleteStageModal .deleteMessage').show();
                            }
                            else
                                $('#deleteStageModal .deleteMessage').hide();


                            if(stageId>0){
                                $('#deleteStageModal').modal('show');
                                $('#deleteStageModal input[name=id]').val(stageId);
                            }

                        } else {
                            doAlert(response);
                        }

                    },
                    done : function (){
                        deleteProcessFlag = 1;
                    }
                });

            });
        }

        function attachEditStageEvent(){
            $('.edit-stage').on('click', function(e){

                e.preventDefault();

                stageId = $(this).attr('data-stageid');

                if(stageId>0){

                    $.ajax({
                        type: "GET",
                        url: '{{ route('api.stages.get') }}?id='+stageId,
                        success: function(response){

                            if(response.status==200){
                                $('#editStageModal').modal('show');

                                $.each(response.data.stage, function(i,e){
                                    $('#editStageModal input[name='+i+']').val(e);
                                });

                                $('#stageEditColor').attr('value', response.data.stage.color); // #ff0000

                                $('#editStageModal select[name=hasTime]').val(response.data.stage.hasTime ? 1 : 0).trigger('change');

                                $('#edit_stage_department_select').val(response.data.stage.department.length > 0 ? response.data.stage.department[0].id : 0 ).selectpicker('refresh'); // #ff0000
                                $('#edit_stage_department_internal').val(response.data.stage.internal).selectpicker('refresh'); // #ff0000

                                $('#editStageModal .sp-preview-inner').css('background-color', response.data.stage.color); // #ff0000

                            } else {
                                doAlert(response);
                            }

                        },
                        done : function (){
                            deleteProcessFlag = 1;
                        }
                    });


                }
            });
        }

        attachDeleteStageEvent();
        attachEditStageEvent();

        $('#stage_department_select').selectpicker('refresh').trigger('change');

        function attachSortableStagesEvent(){
            $( ".sortable" ).sortable({
                placeholder: "ui-state-highlight",
                stop : function( event, ui ){
                    els = $(this).closest('ul').find('li');
                    order = [];

                    $.each(els,function(event,el){
                        order.push($(el).attr('data-stage-id'));
                    });

                    $.ajax({
                        type: "POST",
                        url: '{{ route('api.stages.order.update') }}',
                        data: { order : order},
                        success: function(response){


                        },
                        done : function (){
                            deleteProcessFlag = 1;
                        }
                    });

                }
            });
            $( ".sortable" ).disableSelection();
        }
        attachSortableStagesEvent();
    </script>
@endsection