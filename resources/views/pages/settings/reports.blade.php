
    {!! Form::open(['route'=>'notifications.reports.store']) !!}
    <div class="pull-left" style="width: 100%;">
        Send me reports
    </div>
    <ul style="padding: 0">
        <li><input name="productivity" {{ isset($options['productivity-report']) ? 'checked' : '' }} type="checkbox" id="sendProductivity"> Productivity</li>
        <li><input name="deadline" {{ isset($options['deadline-report']) ? 'checked' : '' }} type="checkbox" id="sendDeadline"> Project deadlines</li>
        <li><input name="booked" {{ isset($options['time-report']) ? 'checked' : '' }} type="checkbox" id="sendBooked"> Time Booked</li>
        <li><input name="deliverables" {{ isset($options['deliverable-report']) ? 'checked' : '' }} type="checkbox" id="sendDeliverables"> Deliverables</li>
    </ul>
    <?php
    $now = \Carbon\Carbon::now()->setTimezone(Auth::user()->company->timezone)->format('Y-m-d');
    $r['d'] = 'Daily';
    $r['w'] = 'Weekly';
    $r['m'] = 'Monthly';
    $r['y'] = 'Yearly';
    ?>

    Interval
    {!! Form::select('interval',$r, $reports ? $reports->interval : 'w',['class'=>'selectpicker','id'=>'reportsInterval','style'=>'width:100% !important']) !!}

    <div id="week-interval" {{ $reports ? ( $reports->interval == 'w' ? '' : 'style=display:none' ) : 'style=display:none' }}>
        <?php
        $now = \Carbon\Carbon::now()->setTimezone(Auth::user()->company->timezone)->format('Y-m-d');
        $d['Mon'] = 'Monday';
        $d['Tuw'] = 'Tuesday';
        $d['Wed'] = 'Wednesday';
        $d['Thu'] = 'Thursday';
        $d['Fri'] = 'Friday';
        $d['Sat'] = 'Saturday';
        $d['Sub'] = 'Sunday';
        ?>
        Every:
        {!! Form::select('week-day',$d,$reports ? $reports->week_day : null ,['class'=>'selectpicker hasDatepicker','style'=>'width:100% !important']) !!}
    </div>
    <div id="month-interval" {{ $reports ? ( $reports->interval == 'm' ? '' : 'style=display:none' ) : 'style=display:none' }}>
        <?php
        for($x=1;$x<32;$x++)
            $dt[$x] = $x;
        ?>
        Every:
        {!! Form::select('month-day',$dt, $reports ? $reports->day : null ,['class'=>'selectpicker form-control','style'=>'width:100% !important']) !!}
    </div>
    <div id="year-interval" {{ $reports ? ( $reports->interval == 'y' ? '' : 'style=display:none' ) : 'style=display:none' }}>
        <?php
        $m['01'] = 'January';
        $m['02'] = 'February';
        $m['03'] = 'March';
        $m['04'] = 'April';
        $m['05'] = 'May';
        $m['06'] = 'June';
        $m['07'] = 'July';
        $m['08'] = 'August';
        $m['09'] = 'September';
        $m['10'] = 'October';
        $m['11'] = 'November';
        $m['12'] = 'December';
        ?>

        <div class="col-md-12">
            <div class="row">
                Every:
            </div>
        </div>
        <div class="col-md-7">
            <div class="row">
                {!! Form::select('month',$m, $reports ? $reports->month : null ,['class'=>'selectpicker form-control','style'=>'width:100% !important']) !!}
            </div>
        </div>
        <div class="col-md-5 padding-r-0">
            {!! Form::select('day',$dt, $reports ? $reports->day : null ,['class'=>'selectpicker form-control','style'=>'width:100% !important']) !!}
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            Time
            <input type="text" class="timepicker form-control" style="width: 100%" value="{{ $reports ? \Carbon\Carbon::parse($now.' '.$reports->time)->setTimezone(Auth::user()->company->timezone)->format('H:i') : '9:00' }}" name="time" >
        </div>
    </div>
    {!! Form::submit('Save',['class'=>'t-pink link-bt link-color margin-t-0 margin-b-10 pull-left']) !!}
    {!! Form::close() !!}
