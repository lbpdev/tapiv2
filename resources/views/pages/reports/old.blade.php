@extends('layouts.app')

@section('page-title','Reports')


@section('style')
    <link rel="stylesheet" href="{{ asset('public/css/reports.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/plugins/bootstrap-select-1.10.0/css/bootstrap-select.css') }}">
    <style>
        .header {
            color: #fff;
            background-color: #bf1c73;
            border-radius: 15px;
            padding: 3px 8px;
        }
        .section-header {
            color: #000;
            display: inline-block;
            width: 100%;
            text-align: center;
        }
    </style>
@endsection

@section('content')

    @include('pages.header')

    @inject('users','App\Services\UserServices')

    <?php
    $users = $users->getAll();
    ?>

    <div class="col-md-2">
        <ul>
            @foreach($users as $user)
                <li>{{ $user->name }}</li>
            @endforeach
        </ul>
    </div>
    <div class="col-md-10">
        <div class="col-md-12">
            <div class="row text-center">
                <span class="header">Report Dashboard</span>
            </div>
        </div>
        <div class="col-md-6 padding-40 " style="height: 500px">
            <span class="section-header">Productivity</span>
            <?php
            $ranges['today'] = 'Today';
            $ranges['tomorrow'] = 'Tomorrow';
            $ranges['yesterday'] = 'Yesterday';
            $ranges['this-week'] = 'This week';
            $ranges['next-week'] = 'Next Week';
            $ranges['last-week'] = 'Last week';
            $ranges['this-month'] = 'This month';
            $ranges['next-month'] = 'Next month';
            $ranges['last-month'] = 'Last month';
            $ranges['this-quarter'] = 'This quarter';
            $ranges['last-quarter'] = 'Last quarter';
            $ranges['next-quarter'] = 'Next quarter';
            $ranges['this-year'] = 'This year';
            ?>

            {!! Form::select('range',$ranges,'today',['class'=>'selectpicker','id'=>'productivityDateRange']) !!}

            {!! Form::select('range',$userRanges,0,['class'=>'selectpicker','id'=>'productivityUserRange']) !!}

            <canvas id="productivityChart" width="300" height="300"></canvas>
        </div>
        <div class="col-md-6 padding-40" style="height: 500px">

            <?php
            $dRanges['this-quarter'] = 'This quarter';
            $dRanges['last-quarter'] = 'Last quarter';
            $dRanges['next-quarter'] = 'Next quarter';
            $dRanges['this-year'] = 'This year';
            ?>

            {!! Form::select('range',$dRanges,'this-quarter',['class'=>'selectpicker','id'=>'deadlineDateRange']) !!}

            {{--{!! Form::select('range',$userRanges,'today',['class'=>'selectpicker','id'=>'deadlineUserRange']) !!}--}}

            <canvas id="deadlineChart" width="300" height="300"></canvas>
        </div>
        <div class="col-md-6 padding-40" style="height: 500px">

            {!! Form::select('range',$ranges,'today',['class'=>'selectpicker','id'=>'timeDateRange']) !!}
            {!! Form::select('range',$userRanges,0,['class'=>'selectpicker','id'=>'timeUserRange']) !!}

            <canvas id="timeChart" width="300" height="300"></canvas>
        </div>
        <div class="col-md-6 padding-40" style="height: 500px">
            {!! Form::select('range',$ranges,'today',['class'=>'selectpicker','id'=>'deliverableDateRange']) !!}
            {!! Form::select('range',$userRanges,0,['class'=>'selectpicker','id'=>'deliverableUserRange']) !!}
            <canvas id="deliverableChart" width="300" height="300"></canvas>
        </div>
    </div>

    <div class="col-md-6">
        <h4>Today</h4>
        <table class="table">
            <tr>
                <th>Name</th>
                <th>Role</th>
                <th>Logged-in</th>
                <th>Tasks</th>
            </tr>
            @foreach($users as $user)
                <tr>
                    <td><a href="{{ route('reports.users.single',$user->id) }}">{{ $user->name }}</a></td>
                    <td>{{ $user->role->name }}</td>
                    <td>
                        {{ count($user->loggedInToday) ? 'Yes' : 'No' }}
                    </td>
                    <td>{{ count($user->tasksCompletedToday) }}/{{ count($user->tasksToday) }}</td>
                </tr>
            @endforeach
        </table>
    </div>
    <div class="col-md-6">
        <h4>Yesterday</h4>
        <table class="table">
            <tr>
                <th>Name</th>
                <th>Role</th>
                <th>Logged-in</th>
                <th>Tasks</th>
            </tr>
            @foreach($users as $user)
                <?php
                $yesterday = \Carbon\Carbon::now()->setTimezone(Auth::user()->companyTimezone)->subDay()->format('Y-m-d');
                ?>
                <tr>
                    <td><a href="{{ route('reports.users.single',$user->id) }}">{{ $user->name }}</a></td>
                    <td>{{ $user->role->name }}</td>
                    <td>{{ count($user->loggedInOn($yesterday)) ? 'Yes' : 'No' }}</td>
                    <td>{{ count($user->tasksCompletedYesterday) }}/{{ count($user->tasksYesterday) }}</td>
                </tr>
            @endforeach
        </table>
    </div>
    <br>
    <hr>
    <br>
    <div class="col-md-12">
        <h4>Projects</h4>
        <table class="table">
            <tr>
                <th>Name</th>
                <th>Client</th>
                <th>Brand</th>
                <th>Format</th>
                <th>Duration</th>
            </tr>
            @foreach($projects as $project)
                <tr>
                    <td><a href="{{ route('reports.projects.single',$project->id) }}">{{ $project->name }}</a></td>
                    <td>{{ $project->client ? $project->client->name : 'None' }}</td>
                    <td>{{ $project->brand ? $project->brand->name : 'None'  }}</td>
                    <td>{{ $project->format ? $project->format->name : 'None'  }}</td>
                    <td>{{ $project->start_at->format('d-m-Y') . ' - ' . $project->end_at->format('d-m-Y') }}</td>
                </tr>
            @endforeach
        </table>
    </div>
    <br>
    <hr>
    <br>
    <div class="col-md-12">
        <h4>User Ratings</h4>
        <table class="table">
            <tr>
                <th>Name</th>
                <th>Role</th>
                <th>Last Login</th>
                <th>Rating</th>
            </tr>
            @foreach($users as $user)
                <tr>
                    <td><a href="{{ route('reports.users.single',$user->id) }}">{{ $user->name }}</a></td>
                    <td>{{ $user->role->name }}</td>
                    <td>{{ $user->lastLogin ? $user->lastLogin->created_at : 'N/A' }}</td>
                    <td>{{ $user->overallRating }}</td>
                </tr>
            @endforeach
        </table>
    </div>

    @include('pages.footer')

    </div>
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.bundle.min.js" type="text/javascript"></script>
    <script src="{{ asset('public/plugins/bootstrap-select-1.10.0/js/bootstrap-select.min.js') }}"></script>
    <script>

        data = [];
        data['labels'] = ["1", "2", "3", "4", "5", "6","8", "9", "10", "11", "12", "13","1", "2", "3", "4", "5", "6","8", "9", "10", "11", "12", "13"];
        data['values'] = [12, 19, 3, 5, 2, 3,12, 19, 3, 5, 2, 3,12, 19, 3, 5, 2, 3,12, 19, 3, 5, 2, 3];
        data['colors'] = [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)',
            'rgba(255, 123, 132, 1)',
            'rgba(244, 162, 235, 1)',
            'rgba(255, 16, 86, 1)',
            'rgba(144, 192, 122, 1)',
            'rgba(111, 102, 255, 1)',
            'rgba(255, 121, 64, 1)',
            'rgba(153, 162, 255, 1)',
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)',
            'rgba(255, 123, 132, 1)',
            'rgba(244, 162, 235, 1)',
            'rgba(255, 16, 86, 1)',
            'rgba(144, 192, 122, 1)',
            'rgba(111, 102, 255, 1)',
            'rgba(255, 121, 64, 1)',
            'rgba(153, 162, 255, 1)',
        ];


        /*
         ========================================================================
         ============================ END OF PRODUCTIVITY CHART =================
         ========================================================================
         */


        var productivityChart = [];
        function updateProductivityChart(){

            dateRange = $('#productivityDateRange').val();
            user = $('#productivityUserRange').val();

            userRange = 0;
            departmentRange = 0;

            if(user.search('department') == 0)
                departmentRange = user.substring(user.lastIndexOf("[")+1,user.lastIndexOf("]"));
            else if(user.search('user') == 0)
                userRange = user.substring(user.lastIndexOf("[")+1,user.lastIndexOf("]"));

            if(productivityChart.id)
                productivityChart.destroy();

            $.ajax({
                type: "GET",
                url: '{{ route('api.reports.productivity') }}?range='+dateRange+'&department_id='+departmentRange+'&user_id='+userRange,
                success: function(response){
                    if(response){
                        ptx = document.getElementById("productivityChart");
                        productivityChart = new Chart(ptx, {
                            type: 'doughnut',
                            data: {
                                labels: ['100%','+80%','+60%','+40%','-40%'],
                                datasets: [{
                                    data: response,
                                    backgroundColor:data['colors'],
                                }]
                            },
                            options: {
                                legend: {
                                    display: true,
                                    position : 'bottom',
                                    labels: {
                                        boxWidth : 10,
                                        fontColor: 'rgb(255, 99, 132)'
                                    }
                                }
                            }
                        });
                    }
                },
                done : function (){
                    deleteProcessFlag = 1;
                }
            });

        }
        updateProductivityChart();

        $('#productivityDateRange').on('change',function(){
            updateProductivityChart();
        });

        $('#productivityUserRange').on('change',function(){
            updateProductivityChart();
        });

        /*
         ========================================================================
         ============================ END OF PRODUCTIVITY CHART =================
         ========================================================================
         */



        /*
         ========================================================================
         ============================ DEADLINE CHART ============================
         ========================================================================
         */

        var deadlineChart = [];
        function updateDeadlineChart(){

            dateRange = $('#deadlineDateRange').val();
//            user = $('#deadlineUserRange').val();

            userRange = 0;
            departmentRange = 0;

//            if(user.search('department') == 0)
//                departmentRange = user.substring(user.lastIndexOf("[")+1,user.lastIndexOf("]"));
//            else if(user.search('user') == 0)
//                userRange = user.substring(user.lastIndexOf("[")+1,user.lastIndexOf("]"));

            if(deadlineChart.id)
                deadlineChart.destroy();

            $.ajax({
                type: "GET",
                url: '{{ route('api.reports.deadline') }}?range='+dateRange+'&department_id='+departmentRange+'&user_id='+userRange,
                success: function(response){
                    if(response){
                        dtx = document.getElementById("deadlineChart");
                        deadlineChart = new Chart(dtx, {
                            type: 'bar',
                            data: {
                                labels: response.labels,
                                datasets: [{
                                    label: '# of deadlines',
                                    data: response.values,
                                    backgroundColor: data['colors']
                                }]
                            },
                            options: {
                                legend: {
                                    display: false,
                                }
                            }
                        });
                    }
                },
                done : function (){
                    deleteProcessFlag = 1;
                }
            });

        }
        updateDeadlineChart();

        $('#deadlineDateRange').on('change',function(){
            updateDeadlineChart();
        });

        $('#deadlineUserRange').on('change',function(){
            updateDeadlineChart();
        });

        /*
         ========================================================================
         ============================ END OF DEADLINE CHART =====================
         ========================================================================
         */


        /*
         ========================================================================
         ============================== TIME CHART ==============================
         ========================================================================
         */

        var timeChart = [];
        function updateTimeChart(){

            dateRange = $('#timeDateRange').val();
            user = $('#timeUserRange').val();

            userRange = 0;
            departmentRange = 0;

            if(user.search('department') == 0)
                departmentRange = user.substring(user.lastIndexOf("[")+1,user.lastIndexOf("]"));
            else if(user.search('user') == 0)
                userRange = user.substring(user.lastIndexOf("[")+1,user.lastIndexOf("]"));

            if(timeChart.id)
                timeChart.destroy();

            $.ajax({
                type: "GET",
                url: '{{ route('api.reports.booked') }}?range='+dateRange+'&department_id='+departmentRange+'&user_id='+userRange,
                success: function(response){
                    console.log(response);
                    if(response){
                        ttx = document.getElementById("timeChart");
                        timeChart = new Chart(ttx, {
                            type: 'pie',
                            data: {
                                labels: ['Booked','Unbooked'],
                                datasets: [{
                                    label: '# of Votes',
                                    data: response,
                                    backgroundColor:data['colors']
                                }]
                            },
                            options: {
                                legend: {
                                    display: true,
                                    position : 'bottom',
                                    labels: {
                                        boxWidth : 10,
                                        fontColor: 'rgb(255, 99, 132)'
                                    }
                                }
                            }
                        });
                    }
                },
                done : function (){
                    deleteProcessFlag = 1;
                }
            });

        }
        //        updateTimeChart();

        $('#timeDateRange').on('change',function(){
            updateTimeChart();
        });

        $('#timeUserRange').on('change',function(){
            updateTimeChart();
        });

        updateTimeChart();

        /*
         ========================================================================
         ============================ END OF BOOKED CHART =======================
         ========================================================================
         */



        /*
         ========================================================================
         ============================ DELIVERABLES CHART ========================
         ========================================================================
         */

        var deliverableChart = [];
        function updateDeliverableChart(){

            dateRange = $('#deliverableDateRange').val();
            user = $('#deliverableUserRange').val();

            userRange = 0;
            departmentRange = 0;

            if(user.search('department') == 0)
                departmentRange = user.substring(user.lastIndexOf("[")+1,user.lastIndexOf("]"));
            else if(user.search('user') == 0)
                userRange = user.substring(user.lastIndexOf("[")+1,user.lastIndexOf("]"));

            if(deliverableChart.id)
                deliverableChart.destroy();

            $.ajax({
                type: "GET",
                url: '{{ route('api.reports.deliverables') }}?range='+dateRange+'&department_id='+departmentRange+'&user_id='+userRange,
                success: function(response){
                    console.log(response);
                    if(response){
                        var detx = document.getElementById("deliverableChart");
                        deliverableChart = new Chart(detx, {
                            type: 'doughnut',
                            data: {
                                labels: response.labels,
                                datasets: [{
                                    label: '# of devlierables',
                                    data: response.values,
                                    backgroundColor: data['colors'],
                                }]
                            },
                            options: {
                                legend: {
                                    display: true,
                                    position : 'bottom',
                                    labels: {
                                        boxWidth : 10,
                                        fontColor: 'rgb(255, 99, 132)'
                                    }
                                }
                            }
                        });
                    }
                },
                done : function (){
                    deleteProcessFlag = 1;
                }
            });

        }

        $('#deliverableDateRange').on('change',function(){
            updateDeliverableChart();
        });

        $('#deliverableUserRange').on('change',function(){
            updateDeliverableChart();
        });

        updateDeliverableChart();
        /*
         ========================================================================
         ============================ DELIVERABLES CHART ========================
         ========================================================================
         */

    </script>
@endsection
