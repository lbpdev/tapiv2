@extends('layouts.app')

@section('page-title','Reports')


@section('style')
    <link rel="stylesheet" href="{{ asset('public/css/reports.css') }}">
@endsection

@section('content')

    @include('pages.header')

    <h1>{{ $data->name }}</h1>

    <div class="col-md-6">
        <h3>Activity Report</h3>

        <table class="table margin-b-0">
            <tr>
                <th>Activity</th>
                <th>Date</th>
            </tr>
        </table>
        <div class="scroller"  style="max-height: 300px;overflow-y: scroll">
            <table class="table">

            </table>

        </div>
    </div>

    <div class="col-md-6">
        <h3>Productivity </h3>
        <table class="table">
            <tr>
                <th>Task Name</th>
                <th>Notes</th>
                <th>Date Assigned</th>
                <th>Date Completed</th>
                <th>Status</th>
            </tr>
--}}
        </table>
    </div>

    @include('pages.footer')

    </div>
@endsection

@section('js')
@endsection
