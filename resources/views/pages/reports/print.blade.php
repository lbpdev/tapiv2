
    <link rel="stylesheet" type="text/css" href="http://tapiapp.com/app/public/plugins/bootstrap-select-1.10.0/css/bootstrap-select.css">
    <style>
        .header {
            color: #fff;
            background-color: #bf1c73;
            border-radius: 10px;
            padding: 2px 10px;
            font-size: 16px;
            margin-bottom: 10px;
            display: inline-block;
        }
        .section-header {
            color: rgba(0, 0, 0, 0.71);
            display: inline-block;
            width: 100%;
            text-align: center;
            font-size: 16px;
            font-weight: bold;
            margin-bottom: 10px;
        }
        .bootstrap-select {
            width: 144px !important;
            display: inline-block !important;
            margin-right: 10px;
        }

        .legend {
            display: inline-block;
            width: 100%;
            text-align: center;
        }

        .legend li {
            display: inline-block;
            font-size:10px;
            line-height: 10px;
            margin-right: 10px;
        }

        .legend li .color {
            width: 8px;
            height: 8px;
            background-color: #ccc;
            display: inline-block;
            border-radius: 15px;
            margin-right: 4px;
        }

        canvas {
            margin: 10px 0;
        }

        .padding-40 {
            padding: 20px 40px !important;
            background-color: #fff;
        }
        .bootstrap-select.btn-group .dropdown-toggle .caret {
            background-position: center 9px !important;
        }

        /*#Do {*/
        /*width: 720px;*/
        /*}*/

        #mainReport {
            /*overflow-x: hidden;*/
            background-color: transparent;
        }

        @media print {
            .no-print {
                display: none;
            }
        }
    </style>

    <style>
        html,
        body {
            height: 100%;
            min-height: 100%;
            padding: 0 !important;
        }

        body {
            font-family: Helvetica;
            font-weight: 300;
        }

        .fa-btn {
            margin-right: 6px;
        }

        #addProject {
            font-size: 18px;
            margin-top: 5px;
        }

        #subscription-overlay {
            background-color: rgba(0,0,0,0.8);
            height: 100%;
            width: 100%;
            position: fixed;
            top: 22px;
            left: 0;
            z-index:3000;
        }

        #subscription-overlay .message {
            width: 400px;
            height: 100px;
            position: absolute;
            top: 50%;
            left: 50%;
            margin-left: -200px;
            margin-top: -50px;
            background-color: transparent;
            font-size: 24px;
            color: #fff;
            text-align: center;
            line-height: 30px;
        }

        #mobile-overlay {
            display: none;
        }

        @media screen and (max-width: 991px){
            #mobile-overlay {
                display: block;
                background-color: rgba(0,0,0,0.9);
                height: 100%;
                width: 100%;
                position: fixed;
                top: 0;
                left: 0;
                z-index:3000;
            }

            #mobile-overlay .message {
                width: 400px;
                height: 100px;
                position: absolute;
                top: 20%;
                left: 50%;
                margin-left: -200px;
                margin-top: -50px;
                background-color: transparent;
                font-size: 19px;
                color: #fff;
                text-align: center;
                line-height: 30px;
                padding: 30px;
            }
        }

        #plansModal {
            z-index: 30001;
        }
    </style>
</head>

<body id="app-layout">
    <div style="float:left;width: 1063px;text-align: center;">
        @inject('company', 'App\Services\CompanyService')

        {{--@if(Request::path()!="settings")--}}
        {{--@endif--}}
                {{--@if(isset($company_logo))--}}
                    {{--<img style="float:left;" src="{{ $company_logo }}" id="lb-logo" height="82">--}}
                {{--@else--}}
                {{--@elseif($company->getLogo())--}}
                    {{--<img style="float:left;" src="{{ $company->getLogo() }}" id="lb-logo" height="82">--}}
                {{--@else--}}
                    {{--<div style="float:left;" id="lb-logo" class="logo-placeholder">Your Logo</div>--}}
                {{--@endif--}}
        {{--@if(Request::path()!="settings")--}}
        {{--@endif--}}


        <?php
        $ranges['today'] = 'Today';
        $ranges['tomorrow'] = 'Tomorrow';
        $ranges['yesterday'] = 'Yesterday';
        $ranges['this-week'] = 'This week';
        $ranges['next-week'] = 'Next Week';
        $ranges['last-week'] = 'Last week';
        $ranges['this-month'] = 'This month';
        $ranges['next-month'] = 'Next month';
        $ranges['last-month'] = 'Last month';
        $ranges['this-quarter'] = 'This quarter';
        $ranges['last-quarter'] = 'Last quarter';
        $ranges['next-quarter'] = 'Next quarter';
        $ranges['this-year'] = 'This year';
        ?>

        <img style="float:right;" src="http://tapiapp.com/app/public/images/Tapi-Logo.png" id="lb-logo" height="96">
    </div>
    <div class="no-print">
        <div id="mainReport" style="border-bottom: 1px solid #ccc;width: 830px;">
            <div style="width: 1063px;text-align: center;">
                <span class="header">Report {{ \Carbon\Carbon::now()->format('d-m-Y') }}</span>
            </div>
        <div style="width: 1220px;float: left;">

            @if($productivityData)
            <div class="padding-40  text-center" style="text-align:center; float:left; width:450px;height: 570px;border: 1px solid #ccc;">
                <span class="section-header">Productivity</span>

                <p>{{ $ranges[isset($_GET['prodDateRange']) ? $_GET['prodDateRange'] : 'this-quarter']}}
                    @if(isset($_GET['prodDepRange']) || isset($_GET['prodUserRange']))
                        - {{ $userRanges[isset($_GET['prodDepRange']) ? ( $_GET['prodDepRange'] != 0 ? 'department['.$_GET['prodDepRange'].']' : 0 ) : 'user['.$_GET['prodUserRange'].']' ] }}
                    @endif
                </p>

                <canvas id="productivityChart" width="300" height="300"></canvas>
                <div class="legend">
                    <ul>
                        <li><span class="color" style="background-color: #0078b3;"></span>100%</li>
                        <li><span class="color" style="background-color: #ffb943;"></span>+80%</li>
                        <li><span class="color" style="background-color: #da0d81;"></span>+60%</li>
                        <li><span class="color" style="background-color: #2b7c31;"></span>+40%</li>
                        <li><span class="color" style="background-color: #c6c6c6;"></span>-40%</li>
                    </ul>
                </div>
            </div>
            @endif

            @if(isset($deadlineData))
                <div class="padding-40 text-center" style="text-align:center; float:left; width:450px;height: 570px;border-bottom: 1px solid #ccc;border-top: 1px solid #ccc;border-right: 1px solid #ccc;">
                    <span class="section-header">Deadline</span>
                    <?php
                    $dRanges['this-quarter'] = 'This quarter';
                    $dRanges['last-quarter'] = 'Last quarter';
                    $dRanges['next-quarter'] = 'Next quarter';
                    $dRanges['this-year'] = 'This year';
                    ?>

                    <p>{{ $dRanges[isset($_GET['deadDateRange']) ? $_GET['deadDateRange'] : 'this-quarter' ] }}</p>
                    <canvas id="deadlineChart" width="300" height="300"></canvas>
                </div>
            @endif
        </div>

        <div style="width: 1063px;float: left;border-bottom: 1px solid #ccc;border-left: 1px solid #ccc;">

            @if(isset($timeData))
                <div class="padding-40 text-center" style="text-align:center; float:left; width:450px;height: 600px;">

                    <span class="section-header">Time Booked</span>
                    <p>{{ $ranges[isset($_GET['timeDateRange']) ? $_GET['timeDateRange'] : 'today'] }}
                        @if(isset($_GET['timeDepRange']) || isset($_GET['timeUserRange']))
                            - {{ $userRanges[isset($_GET['timeDepRange']) ? ( $_GET['timeDepRange'] != 0 ? 'department['.$_GET['timeDepRange'].']' : 0 ) : 'user['.$_GET['timeUserRange'].']' ]}}
                        @endif
                    </p>
                    <canvas id="timeChart" width="300" height="300"></canvas>
                    <div class="legend">
                        <ul>
                            <li><span class="color" style="background-color: #0078b3;"></span>Booked</li>
                            <li><span class="color" style="background-color: #c6c6c6;"></span>Unbooked</li>
                        </ul>
                    </div>
                </div>
            @endif

            @if(isset($deliverableData))
                <div class="padding-40 text-center" style="text-align:center; float:left; width:450px;min-height: 600px;border-left: 1px solid #ccc;border-right: 1px solid #ccc;">
                    <span class="section-header">Deliverables</span>
                    <p>{{ $ranges[isset($_GET['delDateRange']) ? $_GET['delDateRange'] : 'today'] }}
                        @if(isset($_GET['delDepRange']) || isset($_GET['delUserRange']))
                            - {{ $userRanges[isset($_GET['delDepRange']) ? ( $_GET['delDepRange'] != 0 ? 'department['.$_GET['delDepRange'].']' : 0 ): 'user['.$_GET['delUserRange'].']'] }}</p>
                        @endif
                    <canvas id="deliverableChart" width="400" height="400"></canvas>
                    <div class="legend">
                        <ul>
                            @foreach($deliverables as $index=>$deliverable)
                                <li><span class="color" style="background-color: {{ $colors[$index] }};"></span>{{ $deliverable }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
        </div>

        </div>
    </div>

<div id="to-print"></div>

</div>

        <!-- JavaScripts -->
<script src="http://tapiapp.com/app/public/js/jquery.min.js"></script>
<script src="http://tapiapp.com/app/public/js/moment.js"></script>


<script src="http://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.bundle.min.js" type="text/javascript"></script>
<script src="http://tapiapp.com/app/public/plugins/bootstrap-select-1.10.0/js/bootstrap-select.min.js"></script>
<script>
        var colors = {!! $colors !!};

        @if($productivityData)
            ptx = document.getElementById("productivityChart");
            productivityChart = new Chart(ptx, {
                type: 'doughnut',
                data: {
                    labels: ['100%','+80%','+60%','+40%','-40%'],
                    datasets: [{
                        data: {{$productivityData}},
                        backgroundColor:[
                            '#0078b3',
                            '#ffb943',
                            '#da0d81',
                            '#2b7c31',
                            '#c6c6c6',
                        ],
                    }]
                },
                options: {
                    animation : false,
                    legend: {
                        display: false,
                        position : 'bottom',
                        labels: {
                            boxWidth : 10,
                            fontColor: '#000',
                            radius: 28
                        }
                    }
                }
            });
        @endif

        @if(isset($deadlineData))
            dtx = document.getElementById("deadlineChart");
            deadlineChart = new Chart(dtx, {
                type: 'bar',
                data: {
                    labels: {!! $deadlineLabels !!},
                    datasets: [{
                        label: '# of deadlines',
                        data: {{$deadlineData}},
                        backgroundColor:[
                            '#0078b3',
                            '#ffb943',
                            '#da0d81',
                            '#2b7c31',
                            '#c6c6c6',
                        ],
                    }]
                },
                options: {
                    animation : false,
                    legend: {
                        display: false,
                    }
                }
            });
        @endif

        @if(isset($timeData))
            ttx = document.getElementById("timeChart");
            timeChart = new Chart(ttx, {
                type: 'pie',
                data: {
                    labels: ['Booked','Unbooked'],
                    datasets: [{
                        label: '# of Votes',
                        data: {{$timeData}},
                        backgroundColor:[
                            '#0078b3',
                            '#c6c6c6'
                        ]
                    }]
                },
                options: {
                    animation : false,
                    legend: {
                        display: false,
                        position : 'bottom'
                    }
                }
            });
        @endif

        @if(isset($deliverableData))
            var detx = document.getElementById("deliverableChart");
            deliverableChart = new Chart(detx, {
                type: 'doughnut',
                data: {
                    labels: {!! $deliverableLabels !!},
                    datasets: [{
                        label: '# of devlierables',
                        data: {{ $deliverableData }},
                        backgroundColor:colors,
                    }]
                },
                options: {
                    animation : false,
                    legend: {
                        display: false,
                        position : 'bottom',
                    }
                }
            });
        @endif

</script>
