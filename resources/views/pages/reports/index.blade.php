@extends('layouts.app')

@section('page-title','Reports')

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('public/plugins/bootstrap-select-1.10.0/css/bootstrap-select.css') }}">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <style>
        .header {
            color: #fff;
            background-color: #bf1c73;
            border-radius: 10px;
            padding: 2px 10px;
            font-size: 16px;
            margin-bottom: 10px;
            display: inline-block;
        }
        .section-header {
            color: rgba(0, 0, 0, 0.71);
            display: inline-block;
            width: 100%;
            text-align: center;
            font-size: 16px;
            font-weight: bold;
            margin-bottom: 10px;
        }
        .bootstrap-select,.timepicker {
            width: 144px !important;
            display: inline-block !important;
            margin-right: 10px;
            margin-bottom: 10px;
        }
        #year-interval  {
            width: 144px !important;
        }
        #year-interval .bootstrap-select, #year-interval .timepicker {
            width: 100% !important;
        }

        .legend {
            display: inline-block;
            width: 100%;
            text-align: center;
        }

        .legend li {
            display: inline-block;
            font-size:10px;
            line-height: 10px;
            margin-right: 10px;
        }

        .legend li .color {
            width: 8px;
            height: 8px;
            background-color: #ccc;
            display: inline-block;
            border-radius: 15px;
            margin-right: 4px;
        }

        canvas {
            margin: 10px 0;
        }

        .padding-40 {
            padding: 20px 40px !important;
            background-color: #fff;
        }
        .bootstrap-select.btn-group .dropdown-toggle .caret {
            background-position: center 9px !important;
        }

        /*#Do {*/
            /*width: 720px;*/
        /*}*/

        #mainReport {
            overflow-x: hidden;
            background-color: transparent;
        }

        @media print {
            .no-print {
                display: none;
            }
        }

        .navbar-brand {
            display: inline-block;
            float: none !important;
        }

        .bootstrap-select > .dropdown-toggle {
            padding-right: 15px;
        }
        .bootstrap-select.btn-group .dropdown-toggle .caret {
            margin-left: -15px;
        }

    </style>
@endsection

@section('content')

    <div class="no-print">
        @include('pages.header')

        @inject('users','App\Services\UserServices')

            <?php
                $users = $users->getAll();
            ?>

        <div class="pull-left" style="height: 1200px;width: 15%">
            <div class="pull-left" style="width: 100%;">
                <div id="bottom-nav">
                    {{--<a href="#" id="makeCanvas">Share</a>--}}
                    <a id="shareLink" href="{{ url('reports/print') }}">Share</a>
                    <a href="#" id="printBt">Print</a>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    @include('pages.settings.reports')
                </div>
            </div>
        </div>
        <div class="pull-left" id="mainReport" style="border-bottom: 1px solid #ccc;width: 85%;border-left: 1px solid #ccc;">
            <div class="col-md-12">
                <div class="row text-center">
                    <span class="header">Report Dashboard</span>
                </div>
            </div>
            <div class="col-md-6 padding-40  text-center" style="height: 550px;border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;">
                <span class="section-header">Productivity</span>
                <?php
                    $ranges['today'] = 'Today';
                    $ranges['tomorrow'] = 'Tomorrow';
                    $ranges['yesterday'] = 'Yesterday';
                    $ranges['this-week'] = 'This week';
                    $ranges['next-week'] = 'Next Week';
                    $ranges['last-week'] = 'Last week';
                    $ranges['this-month'] = 'This month';
                    $ranges['next-month'] = 'Next month';
                    $ranges['last-month'] = 'Last month';
                    $ranges['this-quarter'] = 'This quarter';
                    $ranges['last-quarter'] = 'Last quarter';
                    $ranges['next-quarter'] = 'Next quarter';
                    $ranges['this-year'] = 'This year';
                ?>

                {!! Form::select('range',$ranges,'today',['class'=>'selectpicker','id'=>'productivityDateRange']) !!}

                {!! Form::select('range',$userRanges,0,['class'=>'selectpicker','id'=>'productivityUserRange']) !!}

                <canvas id="productivityChart" width="300" height="300"></canvas>
                <div class="legend">
                    <ul>
                        <li><span class="color" style="background-color: #0078b3;"></span>100%</li>
                        <li><span class="color" style="background-color: #ffb943;"></span>+80%</li>
                        <li><span class="color" style="background-color: #da0d81;"></span>+60%</li>
                        <li><span class="color" style="background-color: #2b7c31;"></span>+40%</li>
                        <li><span class="color" style="background-color: #c6c6c6;"></span>-40%</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6 padding-40 text-center" style="height: 550px;border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;">
                <span class="section-header">Deadline</span>

                <?php
                    $dRanges['this-quarter'] = 'This quarter';
                    $dRanges['last-quarter'] = 'Last quarter';
                    $dRanges['next-quarter'] = 'Next quarter';
                    $dRanges['this-year'] = 'This year';
                ?>

                {!! Form::select('range',$dRanges,'this-quarter',['class'=>'selectpicker','id'=>'deadlineDateRange']) !!}

                {{--{!! Form::select('range',$userRanges,'today',['class'=>'selectpicker','id'=>'deadlineUserRange']) !!}--}}

                <canvas id="deadlineChart" width="300" height="300"></canvas>
            </div>
            <div class="col-md-6 padding-40 text-center" style="height: 600px;">

                <span class="section-header">Time Booked</span>
                {!! Form::select('range',$ranges,'today',['class'=>'selectpicker','id'=>'timeDateRange']) !!}
                {!! Form::select('range',$userRanges,0,['class'=>'selectpicker','id'=>'timeUserRange']) !!}

                <canvas id="timeChart" width="300" height="300"></canvas>
                <div class="legend">
                    <ul>
                        <li><span class="color" style="background-color: #0078b3;"></span>Booked</li>
                        <li><span class="color" style="background-color: #c6c6c6;"></span>Unbooked</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6 padding-40 text-center" style="min-height: 600px;border-left: 1px solid #ccc;border-right: 1px solid #ccc;">
                <span class="section-header">Deliverables</span>
                {!! Form::select('range',$ranges,'this-month',['class'=>'selectpicker','id'=>'deliverableDateRange']) !!}
                {!! Form::select('range',$userRanges,0,['class'=>'selectpicker','id'=>'deliverableUserRange']) !!}
                <canvas id="deliverableChart" width="300" height="300"></canvas>
                <div class="legend">
                    <ul>
                        @foreach($deliverables as $index=>$deliverable)
                            <li><span class="color" style="background-color: {{ $colors[$index] }};"></span>{{ $deliverable }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>

        @include('pages.footer')

        </div>
    </div>

    <div id="to-print"></div>
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.bundle.min.js" type="text/javascript"></script>
    <script src="{{ asset('public/plugins/bootstrap-select-1.10.0/js/bootstrap-select.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>
    <script src="https://html2canvas.hertzen.com/build/html2canvas.js"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <script>
        $('.timepicker').timepicker({
            timeFormat: 'H:mm',
            interval: 15,
            minTime: '1',
            maxTime: '11:50pm',
            startTime: '1:00',
            dynamic: false,
            dropdown: true,
            scrollbar: true
        });
    </script>

    <script>
        $('#reportsInterval').on('change',function(){
            $('#week-interval').hide();
            $('#month-interval').hide();
            $('#year-interval').hide();

            if($(this).val()=='w')
                $('#week-interval').show();
            else if($(this).val()=='m')
                $('#month-interval').show();
            else if($(this).val()=='y')
                $('#year-interval').show();

        });

        var printUrl = '{{ url('reports/print') }}/{{ Auth::user()->company->id }}';

        function updateShareLink(){
            link = printUrl+'?';
            departmentRange = 0;

            productivityDateRange = $('#productivityDateRange').val();
            productivityUserRange = $('#productivityUserRange').val();

            if(productivityUserRange.search('department') == 0)
                departmentRange = user.substring(user.lastIndexOf("[")+1,user.lastIndexOf("]"));
            else if(productivityUserRange.search('user') == 0)
                userRange = user.substring(user.lastIndexOf("[")+1,user.lastIndexOf("]"));

            link += 'prodDateRange='+productivityDateRange+'&';
            link += userRange == 0 ? 'prodDepRange='+departmentRange+'&' : 'prodUserRange='+userRange+'&';


            deadlineDateRange = $('#deadlineDateRange').val();

            link += 'deadDateRange='+deadlineDateRange+'&';

            timeDateRange = $('#timeDateRange').val();
            timeUserRange = $('#timeUserRange').val();
            departmentRange = 0;

            if(timeUserRange.search('department') == 0)
                departmentRange = user.substring(user.lastIndexOf("[")+1,user.lastIndexOf("]"));
            else if(timeUserRange.search('user') == 0)
                userRange = user.substring(user.lastIndexOf("[")+1,user.lastIndexOf("]"));

            link += 'timeDateRange='+timeDateRange+'&';
            link += userRange == 0 ? 'timeDepRange='+departmentRange+'&' : 'timeUserRange='+userRange+'&';


            deliverableDateRange = $('#deliverableDateRange').val();
            deliverableUserRange = $('#deliverableUserRange').val();
            departmentRange = 0;

            if(deliverableUserRange.search('department') == 0)
                departmentRange = user.substring(user.lastIndexOf("[")+1,user.lastIndexOf("]"));
            else if(deliverableUserRange.search('user') == 0)
                userRange = user.substring(user.lastIndexOf("[")+1,user.lastIndexOf("]"));

            link += 'delDateRange='+deliverableDateRange+'&';
            link += userRange == 0 ? 'delDepRange='+departmentRange+'&' : 'delUserRange='+userRange+'&';

//
//            $('#mainReport .selectpicker').each(function(){
//                link += $(this).attr('id')+'='+$(this).val()+'&';
//            });
//
            $('#shareLink').attr('href',link);

        }

        function setpixelated(context){
            context['imageSmoothingEnabled'] = true;       /* standard */
            context['mozImageSmoothingEnabled'] = true;    /* Firefox */
            context['oImageSmoothingEnabled'] = true;      /* Opera */
            context['webkitImageSmoothingEnabled'] = true; /* Safari */
            context['msImageSmoothingEnabled'] = true;     /* IE */
        }

        $(document).ready(function(){

            $('#makeCanvas').on('click',function(e){

                e.preventDefault();

                html2canvas($('#mainReport'), {
                    onrendered: function(canvas) {
                        canvas.id = 'Do';

                        document.getElementById('mainReport').appendChild(canvas);

//                        $("#Do").width(780);

                        html2canvas($("#Do"), {
                            onrendered: function(canvas) {

                                setpixelated(canvas.getContext('2d'));
                                var imgData = canvas.toDataURL(
                                        'image/png');

                                var doc = new jsPDF('p', 'mm');
                                doc.addImage(imgData, 'PNG', 1, 1);
//                                doc.save('Report-'+moment().format('D-MM-Y')+'.pdf');
                                window.open(doc, '_blank', 'fullscreen=yes');

//                                window.open(doc);

                                $("#Do").remove();
                            },
                        });
                    },
                    background : '#fff',
                });
            });

            $('#printBt').on('click',function(e){

                e.preventDefault();
                html2canvas($('#mainReport'), {
                    onrendered: function(canvas) {
                        canvas.id = 'Do';
                        document.getElementById('to-print').appendChild(canvas);
                        var context = canvas.getContext('2d');
                        window.print();
                    },
                    background : '#fff',
                });
            });

        });


        var data = [];
        data['labels'] = ["1", "2", "3", "4", "5", "6","8", "9", "10", "11", "12", "13","1", "2", "3", "4", "5", "6","8", "9", "10", "11", "12", "13"];
        data['values'] = [12, 19, 3, 5, 2, 3,12, 19, 3, 5, 2, 3,12, 19, 3, 5, 2, 3,12, 19, 3, 5, 2, 3];
        colors = getColors();

        function getColors(){

            $.ajax({
                type: "GET",
                url: '{{ route('api.colors.get') }}',
                success: function(response){
                    colors = JSON.parse(response);

                    updateDeadlineChart();
                    updateDeliverableChart();
                }
            });
        }

        /*
         ========================================================================
         ============================ END OF PRODUCTIVITY CHART =================
         ========================================================================
         */


        var productivityChart = [];
        function updateProductivityChart(){

            dateRange = $('#productivityDateRange').val();
            user = $('#productivityUserRange').val();

            userRange = 0;
            departmentRange = 0;

            if(user.search('department') == 0)
                departmentRange = user.substring(user.lastIndexOf("[")+1,user.lastIndexOf("]"));
            else if(user.search('user') == 0)
                userRange = user.substring(user.lastIndexOf("[")+1,user.lastIndexOf("]"));

            if(productivityChart.id)
                productivityChart.destroy();

            $.ajax({
                type: "GET",
                url: '{{ route('api.reports.productivity') }}?range='+dateRange+'&department_id='+departmentRange+'&user_id='+userRange,
                success: function(response){
                    if(response){
                        ptx = document.getElementById("productivityChart");
                        productivityChart = new Chart(ptx, {
                            type: 'doughnut',
                            data: {
                                labels: ['100%','+80%','+60%','+40%','-40%'],
                                datasets: [{
                                    data: response,
                                    backgroundColor:[
                                        '#0078b3',
                                        '#ffb943',
                                        '#da0d81',
                                        '#2b7c31',
                                        '#c6c6c6',
                                    ],
                                }]
                            },
                            options: {
                                legend: {
                                    display: false,
                                    position : 'bottom',
                                    labels: {
                                        boxWidth : 10,
                                        fontColor: '#000',
                                        radius: 28
                                    }
                                }
                            }
                        });
                    }
                },
                done : function (){
                    deleteProcessFlag = 1;
                }
            });

        }
        updateProductivityChart();

        $('#productivityDateRange').on('change',function(){
            updateProductivityChart();
            updateShareLink();
        });

        $('#productivityUserRange').on('change',function(){
            updateProductivityChart();
            updateShareLink();
        });

        /*
         ========================================================================
         ============================ END OF PRODUCTIVITY CHART =================
         ========================================================================
         */



        /*
            ========================================================================
            ============================ DEADLINE CHART ============================
            ========================================================================
         */

        var deadlineChart = [];
        function updateDeadlineChart(){

            dateRange = $('#deadlineDateRange').val();
//            user = $('#deadlineUserRange').val();

            userRange = 0;
            departmentRange = 0;

            if(deadlineChart.id)
                deadlineChart.destroy();

            $.ajax({
                type: "GET",
                url: '{{ route('api.reports.deadline') }}?range='+dateRange+'&department_id='+departmentRange+'&user_id='+userRange,
                success: function(response){
                    if(response){
                        dtx = document.getElementById("deadlineChart");
                        deadlineChart = new Chart(dtx, {
                            type: 'bar',
                            data: {
                                labels: response.labels,
                                datasets: [{
                                    label: '# of deadlines',
                                    data: response.values,
                                    backgroundColor: colors
                                }]
                            },
                            options: {
                                legend: {
                                    display: false,
                                }
                            }
                        });
                    }
                },
                done : function (){
                    deleteProcessFlag = 1;
                }
            });

        }

        $('#deadlineDateRange').on('change',function(){
            updateDeadlineChart();
            updateShareLink();
        });

        $('#deadlineUserRange').on('change',function(){
            updateDeadlineChart();
            updateShareLink();
        });

        /*
         ========================================================================
         ============================ END OF DEADLINE CHART =====================
         ========================================================================
         */


        /*
         ========================================================================
         ============================== TIME CHART ==============================
         ========================================================================
         */

        var timeChart = [];
        function updateTimeChart(){

            dateRange = $('#timeDateRange').val();
            user = $('#timeUserRange').val();

            userRange = 0;
            departmentRange = 0;

            if(user.search('department') == 0)
                departmentRange = user.substring(user.lastIndexOf("[")+1,user.lastIndexOf("]"));
            else if(user.search('user') == 0)
                userRange = user.substring(user.lastIndexOf("[")+1,user.lastIndexOf("]"));

            if(timeChart.id)
                timeChart.destroy();

            $.ajax({
                type: "GET",
                url: '{{ route('api.reports.booked') }}?range='+dateRange+'&department_id='+departmentRange+'&user_id='+userRange,
                success: function(response){
                    if(response){
                        ttx = document.getElementById("timeChart");
                        timeChart = new Chart(ttx, {
                            type: 'pie',
                            data: {
                                labels: ['Booked','Unbooked'],
                                datasets: [{
                                    label: '# of Votes',
                                    data: response,
                                    backgroundColor:[
                                        '#0078b3',
                                        '#c6c6c6'
                                    ]
                                }]
                            },
                            options: {
                                legend: {
                                    display: false,
                                    position : 'bottom'
                                }
                            }
                        });
                    }
                },
                done : function (){
                    deleteProcessFlag = 1;
                }
            });

        }
//        updateTimeChart();

        $('#timeDateRange').on('change',function(){
            updateTimeChart();
            updateShareLink();
        });

        $('#timeUserRange').on('change',function(){
            updateTimeChart();
            updateShareLink();
        });

        updateTimeChart();

        /*
         ========================================================================
         ============================ END OF BOOKED CHART =======================
         ========================================================================
         */



        /*
         ========================================================================
         ============================ DELIVERABLES CHART ========================
         ========================================================================
         */

        var deliverableChart = [];
        function updateDeliverableChart(){

            dateRange = $('#deliverableDateRange').val();
            user = $('#deliverableUserRange').val();

            userRange = 0;
            departmentRange = 0;

            if(user.search('department') == 0)
                departmentRange = user.substring(user.lastIndexOf("[")+1,user.lastIndexOf("]"));
            else if(user.search('user') == 0)
                userRange = user.substring(user.lastIndexOf("[")+1,user.lastIndexOf("]"));

            if(deliverableChart.id)
                deliverableChart.destroy();

            $.ajax({
                type: "GET",
                url: '{{ route('api.reports.deliverables') }}?range='+dateRange+'&department_id='+departmentRange+'&user_id='+userRange,
                success: function(response){
                    if(response){
                        var detx = document.getElementById("deliverableChart");
                        deliverableChart = new Chart(detx, {
                            type: 'doughnut',
                            data: {
                                labels: response.labels,
                                datasets: [{
                                    label: '# of devlierables',
                                    data: response.values,
                                    backgroundColor: colors,
                                }]
                            },
                            options: {
                                legend: {
                                    display: false,
                                    position : 'bottom',
                                }
                            }
                        });
                    }
                },
                done : function (){
                    deleteProcessFlag = 1;
                }
            });

        }

        $('#deliverableDateRange').on('change',function(){
            updateDeliverableChart();
            updateShareLink();
        });

        $('#deliverableUserRange').on('change',function(){
            updateDeliverableChart();
            updateShareLink();
        });


        updateShareLink();
        /*
         ========================================================================
         ============================ DELIVERABLES CHART ========================
         ========================================================================
         */

        $(window).load(function(){
            $('#reportsInterval').trigger('change');
        });
    </script>
@endsection
