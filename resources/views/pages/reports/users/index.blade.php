@extends('layouts.app')

@section('page-title','Reports')


@section('style')
    <link rel="stylesheet" href="{{ asset('public/css/reports.css') }}">
@endsection

@section('content')

    @include('pages.header')

    <h1>{{ $user->name }}</h1>

    <div class="col-md-6">
        <h3>Activity Report</h3>

        <table class="table margin-b-0">
            <tr>
                <th>Activity</th>
                <th>Date</th>
            </tr>
        </table>
        <div class="scroller"  style="max-height: 300px;overflow-y: scroll">
            <table class="table">
                @foreach($user->logs as $log)
                    <tr>
                        <td>{{ $log->activity }}</td>
                        <td>{{ $log->created_at->format('d-m-Y') }}</td>
                    </tr>
                @endforeach
            </table>

        </div>
    </div>

    <?php
        $currentMonth = \Carbon\Carbon::now()->setTimezone(Auth::user()->company->timezone)->subMonth(2)->format('m-Y');
    ?>
    <div class="col-md-6">
        <h3>Productivity <span class="pull-right">{{ $user->monthRating($currentMonth) }}/100</span></h3>
        <table class="table">
            <tr>
                <th>Task Name</th>
                <th>Notes</th>
                <th>Date Assigned</th>
                <th>Date Completed</th>
                <th>Status</th>
            </tr>

            @foreach($user->tasksTodayAndPast() as $task)
                @if($task->task)
                    @if($task->task->project)
                        <?php
                            $difference = 0;

                            $date_completed = $task->date_completed ? \Carbon\Carbon::parse($task->date_completed) : null;
                            $assigned_at = \Carbon\Carbon::parse($task->date_assigned);

                        ?>
                        <tr>
                            <td>{{ $task->task->project ? $task->task->project->name : 'Deleted' }}</td>
                            <td>{{ $task->note ? $task->note : 'None' }}</td>
                            <td>{{ $assigned_at->format('d-m-Y') }}</td>
                            <td>{{ $date_completed ? $date_completed->format('d-m-Y') : '' }}</td>
                            <td>
                                <?php
                                    if($task->date_completed)
                                        if($date_completed->format('d-m-Y') == $assigned_at->format('d-m-Y'))
                                        echo 'Completed on time';
                                    elseif(!$task->date_completed)
                                        echo 'Incomplete';
                                    elseif($task->date_completed)
                                        if($date_completed < $assigned_at)
                                        echo 'Completed ahead';
                                ?>
                            </td>
                        </tr>
                    @endif
                @endif
            @endforeach
        </table>
    </div>

    @include('pages.footer')

    </div>
@endsection

@section('js')
@endsection
