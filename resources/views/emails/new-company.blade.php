@extends('emails.main-new')

@section('email_body')
    <p>Company {{ $company->name }} has registered on TapiApp.</p>
    <p><a href="{{ route('backend.companies.edit',$company->id) }}" style="color:#bf1c73;text-decoration: underline;">Click Here</a> to view</p>
@endsection

