@extends('emails.notifications.master')

@section('email_body')
    <br>
    <img src="http://tapiapp.com/app/public/images/email/Today-Schedule.png" width="600" alt="Leading Brands Logo" title="Leading brands Logo"></a>
    <table>
        <tr>
            <td width="600" class="padd">&nbsp;</td>
        </tr>
    </table>
    {{--<br>--}}
    {{--Hello. Here are your tasks for today on TapiApp.  Have a great day.--}}
    {{--<br>--}}
    {{--<br>--}}
    <table style="width:100%;font-size:20px">
        <tr>
            <td width="24" class="padd">&nbsp;</td>
            <td style="font-size: 18px;">Client</td>
            <td style="font-size: 18px;">Project</td>
            <td style="font-size: 18px;">Task</td>
            <td width="24" class="padd">&nbsp;</td>
        </tr>

        @if(isset($tasks))
            @foreach($tasks as $task)
                @if($task)
                    @if($task['task'])
                        <tr>
                            <td width="24" class="padd">&nbsp;</td>
                            <td style="font-size: 18px;">{{ $task['task']['project'] ? ( $task['task']['project']['client'] ? $task['task']['project']['client']['name'] : 'N/A' ) : 'N/A' }}</td>
                            <td style="font-size: 18px;">{{ $task['task']['project'] ? $task['task']['project']['name'] : 'N/A' }}</td>
                            <td style="font-size: 18px;">{{ $task['task']['key'] ? $task['task']['key']['name'] : 'N/A' }}</td>
                            <td width="24" class="padd">&nbsp;</td>
                        </tr>
                    @endif
                @endif
            @endforeach
        @else
            <tr>
                <td width="24" class="padd">&nbsp;</td>
                <td style="font-size: 18px;">XxxXXXXXxXxxxX</td>
                <td style="font-size: 18px;">XxXXX XXXxXX Xx XXXXXXX</td>
                <td style="font-size: 18px;">XXXX XxxxXX</td>
                <td width="24" class="padd">&nbsp;</td>
            </tr>
        @endif
        <tr><td>&nbsp;</td></tr>
    </table>
@endsection

