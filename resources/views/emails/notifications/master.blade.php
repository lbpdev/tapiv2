<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <title>TapiApp Support</title>
</head>
<body>
<style type="text/css">
    #outlook a {
        padding: 0;
    }

    body {
        width: 100% !important;
        -webkit-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%;
        margin: 0;
        padding: 0;
        background-color: #F7F7F7;
    }

    .ExternalClass {
        width: 100%;
    }

    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
        line-height: 100%;
    }

    .bodytbl {
        margin: 0;
        padding: 0;
        width: 100% !important;
    }

    img {
        outline: none;
        text-decoration: none;
        -ms-interpolation-mode: bicubic;
        display: block;
        max-width: 100%;
    }

    a img {
        border: none;
    }

    a {
        color: #ca0b7a;
    }

    p {
        margin: 1em 0;
    }

    table {
        border-collapse: collapse;
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
    }

    table td {
        border-collapse: collapse;
    }

    .o-fix table, .o-fix td {
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
    }

    body {
    }

    table {
        font-family: Helvetica, Arial, sans-serif;
        font-size: 12px;
        color: #585858;
    }

    td, p {
        line-height: 24px;
        color: #000 /*Text*/;
        font-size: 18px;
        font-family: Helvetica;
        font-weight: 300;
    }

    td, tr {
        padding: 0;
    }

    ul, ol {
        margin-top: 24px;
        margin-bottom: 24px;
    }

    li {
        line-height: 24px;
    }

    a {
        color: #000 !important;
        text-decoration: none;
        padding: 2px 0px;
    }

    a:link {
        color: #000;
    }

    a:visited {
        color: #000;
    }

    a:hover {
        color: #000;
    }

    .h1 {
        font-family: Helvetica, Arial, sans-serif;
        font-size: 26px;
        letter-spacing: -1px;
        margin-bottom: 16px;
        margin-top: 2px;
        line-height: 30px;
    }

    .h2 {
        font-family: Helvetica, Arial, sans-serif;
        font-size: 20px;
        letter-spacing: 0;
        margin-top: 2px;
        line-height: 30px;
    }

    h1, h2, h3, h4, h5, h6 {
        font-family: Helvetica, Arial, sans-serif;
        font-weight: normal;
    }

    h1 {
        font-size: 20px;
        letter-spacing: -1px;
        margin-bottom: 16px;
        margin-top: 4px;
        line-height: 24px;
    }

    h2 {
        font-size: 18px;
        margin-bottom: 12px;
        margin-top: 2px;
        line-height: 24px;
    }

    h3 {
        font-size: 14px;
        margin-bottom: 12px;
        margin-top: 2px;
        line-height: 24px;
    }

    h4 {
        font-size: 14px;
        font-weight: bold;
    }

    h5 {
        font-size: 12px;
    }

    h6 {
        font-size: 12px;
        font-weight: bold;
    }

    h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {
        color: #585858;
    }

    h1 a:active, h2 a:active, h3 a:active, h4 a:active, h5 a:active, h6 a:active {
        color: #585858 !important;
    }

    h1 a:visited, h2 a:visited, h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
        color: #585858 !important;
    }

    .wrap.header {
        border-top: 0px solid #E8E8E8 /*Content Border*/;
    }

    .wrap.footer {
        border-bottom: 1px solid #E8E8E8;
    }

    .wrap.body, .wrap.header, .wrap.footer {
        background-color: #ffffff /*Body Background*/;
        border-right: 0px solid #E8E8E8;
        border-left: 0px solid #E8E8E8;
    }

    .padd {
        width: 24px;
    }

    .small {
        font-size: 11px;
        line-height: 18px;
    }

    .separator {
        border-top: 1px dotted #E1E1E1 /*Separator Line*/;
    }

    .btn {
        margin-top: 10px;
        display: block;
    }

    .btn img {
        display: inline;
    }

    .subline {
        line-height: 18px;
        font-size: 16px;
        letter-spacing: -1px;
    }

    table.textbutton td {
        background: #efefef /*Text Button Background*/;
        padding: 1px 14px 4px 14px;
        color: #585858;
        display: block;
        height: 22px;
        border: 1px solid #E8E8E8;
        vertical-align: top;
    }

    table.textbutton a {
        color: #585858;
        font-size: 13px;
        font-weight: normal;
        line-height: 22px;
        width: 100%;
        display: inline-block;
    }

    div.preheader {
        line-height: 0px;
        font-size: 0px;
        height: 0px;
        display: none !important;
        display: none;
        visibility: hidden;
    }

    .lb-logo {
        display: inline-block;
        vertical-align: middle;
    }

    .bg {
        background-color: #F7F7F7;
    }

    .dark-gray {
        background-color: #909090;
    }

    @media only screen and (max-device-width: 480px) {
        body {
            -webkit-text-size-adjust: 120% !important;
            -ms-text-size-adjust: 120% !important;
        }

        table[class=bodytbl] .subline {
            float: left;
        }

        table[class=bodytbl] .padd {
            width: 12px !important;
        }

        table[class=bodytbl] .wrap {
            width: 470px !important;
        }

        table[class=bodytbl] .wrap table {
            width: 100% !important;
        }

        table[class=bodytbl] .wrap img {
            max-width: 100% !important;
            height: auto !important;
        }

        table[class=bodytbl] .wrap .m-100 {
            width: 100% !important;
        }

        table[class=bodytbl] .m-0 {
            width: 0;
            display: none;
        }

        table[class=bodytbl] .m-b {
            display: block;
            width: 100% !important;
        }

        table[class=bodytbl] .m-b-b {
            margin-bottom: 24px !important;
        }

        table[class=bodytbl] .m-1-2 {
            max-width: 264px !important;
        }

        table[class=bodytbl] .m-1-3 {
            max-width: 168px !important;
        }

        table[class=bodytbl] .m-1-4 {
            max-width: 120px !important;
        }

        table[class=bodytbl] .m-1-2 img {
            max-width: 264px !important;
        }

        table[class=bodytbl] .m-1-3 img {
            max-width: 168px !important;
        }

        table[class=bodytbl] .m-1-4 img {
            max-width: 120px !important;
        }
    }

    @media only screen and (max-device-width: 320px) {
        table[class=bodytbl] .wrap {
            width: 310px !important;
        }
    }

</style>
<table class="bodytbl bg" width="100%" cellspacing="0" cellpadding="0" >
    <tbody>
    <tr>
        <td background="" align="center">

            <table width="600" cellspacing="0" cellpadding="0"
                   class="wrap header">
                <tbody>
                <tr>
                    <td height="40" colspan="3" class="bg"></td>
                </tr>
                <tr>
                    <td valign="top" align="center">
                        <table width="100%" cellpadding="0"
                               cellspacing="0" class="o-fix">
                            <tbody>
                            <tr>
                                <td valign="top" align="left">
                                    <table width="600"
                                           cellpadding="0"
                                           cellspacing="0"
                                           align="left"
                                           class="m-b">
                                        <tbody>
                                        <tr>
                                            <td class="small m-b"
                                                align="center"
                                                valign="middle">
                                                <img src="http://tapiapp.com/app/public/images/Tapi-Logo.png" style="margin-top: 20px" height="110">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>

                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="600" class="padd">&nbsp;</td>
                </tr>
                </tbody>
            </table>
            <modules class="ui-sortable">
                <module label="1/1 Column" auto="" class="active"
                        style="display: block;">
                    <table width="600" cellspacing="0" cellpadding="0" class="wrap body" style="min-height:400px">
                        <tbody>
                        <tr>
                            <td width="24" class="padd">&nbsp;</td>
                            <td valign="top" align="center">


                                <multi label="Body" style="font-size: 14px;">
                                    @yield('email_body')
                                </multi>
                            </td>
                            <td width="24" class="padd">&nbsp;</td>
                        </tr>
                        </tbody>
                    </table>
                </module>
            </modules>
            <table width="600" cellspacing="0" cellpadding="0"
                   class="wrap bg">
                <tbody>
                <tr>
                    <td width="600" class="padd">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top" align="center" style="background-color: #414342;">
                        <multi label="Body">
                            <p style="line-height: 15px; margin-top: 10px;font-size: 22px; margin-bottom: 10px; color:#fff">www.tapiapp.com</p>
                            <p style="line-height: 15px; margin-top: 0; margin-bottom: 0;">
                                <a href="#">
                                    <img src="http://tapiapp.com/img/tapi.png"  style="margin-bottom: 10px;" alt="Tapi Logo" title="Tapi Logo" height="60">
                                </a>
                                <a href="https://leadingbrands.me" target="_blank">
                                    <img src="http://tapiapp.com/img/lb.png" height="50" alt="Leading Brands Logo" title="Leading brands Logo"></a>
                                <span class="divider"></span><br>
                                <span id="copyright" style="font-family: Helvetica; font-size: 11px; display: block;color:#fff;"> Copyright © 2017 TapiApp. All rights reserved. </span>
                            </p>
                        </multi>
                        <div class="btn">
                        </div>

                    </td>
                </tr>
                <tr>
                    <td height="32" colspan="3"></td>
                </tr>
                </tbody>
            </table>
            <table width="100%" height="100" cellspacing="0" cellpadding="0"
                   class="wrap">
                <tbody>
                <tr>
                    <td height="24" align="center" valign="middle">
                        <div class="preheader"></div>
                        <a name="top"></a>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>
