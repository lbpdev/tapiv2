@extends('emails.main-new')

@section('email_body')

    <p>It appears that you’ve forgotten your password for TapiApp and have requested to reset it. It happens to the best of us.</p>

    <p>Use the temporary password below with your email address to sign in via the app or website (<a href="{{ url('login') }}">www.tapiapp.com</a>).</p>

    <p>If you did not request a new password, please disregard this email.</p>


    <p>Username: {{isset($email) ? $email : 'test@email.com'}}</p>
    <p>Temporary password: {{isset($temp_password) ? $temp_password : "XXXXXXX"}}</p>

    <p>The temporary password above will expire in two hours.</p>

@endsection
