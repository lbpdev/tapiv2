@extends('emails.main')

@section('email_body')
    <p>You have been invited as an employee for {!! isset($company) ? $company : 'SAMPLE COMPANY NAME'  !!}. Please click <a href="{{ isset($token) ? (url('login') . '/'. $token) : '#' }}">here</a>  to complete the process.</p>
@endsection