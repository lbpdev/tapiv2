<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Tapi App</title>

    <!-- Fonts -->
    {{--<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>--}}

            <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('public/fonts/stylesheet.css') }}" rel="stylesheet">
    <link href="{{ asset('public/fonts/radikal/stylesheet.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/bootstrap-overrides.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/global.css') }}" rel="stylesheet">
    <link href="{{ asset('public/plugins/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet">

    @yield('style')

    <style>

        body {
            font-family: 'nudin';
        }

        .fa-btn {
            margin-right: 6px;
        }

    </style>
</head>
<body id="app-layout">
<div id="overlay-modal"></div>
<div id="spinner">
    <img src="{{ asset('public/images/ripple.svg') }}">
</div>
<div class="alert row-alert" id="globalAlert" style="display: none"></div>
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-left">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ asset('public/images/LB-Logo.png') }}" id="lb-logo" height="82">
                </a>
            </div>
            <div class="col-md-6 text-right">
                <a class="navbar-brand padding-b-0" href="{{ url('/') }}">
                    <img src="{{ asset('public/images/Tapi-Logo.png') }}" id="lb-logo" height="82">
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-9">
                <h3 class="page-title">@yield('page-title')</h3>
            </div>

        </div>
    </div>
</nav>

@yield('content')

        <!-- JavaScripts -->
<script src="{{ asset('public/js/jquery.min.js') }}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="{{ asset('public/plugins/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
<script src="{{ asset('public/js/moment.js') }}"></script>



<script type="text/javascript">

    function doAlert(response){
        var globalAlert = $('#globalAlert');

        if(response.status==200)
            globalAlert.addClass('alert-success').text(response.message).show();
        else
            globalAlert.addClass('alert-danger').text(response.message).show();

        setTimeout(function(){
            globalAlert.fadeOut(2000,function(){ $(this).attr('class','alert row-alert').text(''); });
        },2000);
    }


    var appUrl = '{{ url('/') }}';
</script>

@yield('js')

{{-- <script src="{{ elixir('js/app.js') }}"></script> --}}

<script>

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-1910012-65', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>
