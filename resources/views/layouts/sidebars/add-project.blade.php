<div class="add-project navmenu-default navmenu-fixed-right offcanvas-md offcanvas-lg" style="">
<h3>Add Project</h3>
    {!! Form::open(['id'=>'addProjectForm','route'=>'api.projects.store']) !!}
        {!! Form::hidden('project_id',null,['id'=>'view_project_id']) !!}
        <div class="side-form">
            Client
            <div class="form-group relative clientData">
            {!! Form::input('text','client[name]',null,['id'=>'clients','class'=>'form-control','required']) !!}
            <input id="clientColor" class="form-control" name='client[color]' value='#3355cc'/>
            </div>

            Brand Name
            {!! Form::input('text','brand[name]',null,['class'=>'form-control','required']) !!}
            Project Name
            {!! Form::input('text','project[name]',null,['class'=>'form-control','required']) !!}
            Format
            <div class="gray margin-b-7">
                {!! Form::select('project[format_id]', $formats ? $formats : [] , null,['class'=>'selectpicker','required']) !!}
            </div>
            Version / Issue No.
            {!! Form::input('text','project[version]',null,['class'=>'form-control','required']) !!}
            Language
            <div class="gray margin-b-7">
                {!! Form::select('project[language_id]',$languages, 38 ,['class'=>'selectpicker']) !!}
            </div>
        </div>
        {!! Form::submit('Plan Project',['class'=>'link-bt link-color margin-b-0 pull-right ','id'=>"showPlan"]) !!}
    {!! Form::close() !!}
</div>

@section('add-project-js')
    <script>

        /** SEND CREATE REQUEST **/

    $('#addProjectForm').on('submit',function(e){

        e.preventDefault();
            e.stopPropagation();

        var form = $(this);
        url = form.attr('action');

        if($('#view_project_id').val())
            url = '{{ route('api.projects.update') }}';

        processFlag = 1;


        data = form.serialize();

        if(processFlag) {
            processFlag = 0;

            $.ajax({
              type: "POST",
              url: url,
              data: data,
              success: function(response){

                if(response.status==200) {
                    $(".add-project").offcanvas('hide');
                    $(".plan-project").offcanvas('show');
                    refreshProjects(response.data.items);
                    $('#project_id').val(response.data.project.id);
                    $('#project_name').text(response.data.project.name);

                    start_at = moment(response.data.project.start_at).format('Y-MM-DD');
                    end_at = moment(response.data.project.end_at).format('Y-MM-DD');

                    $('#start_at').val(start_at);
                    $('#end_at').val(end_at);

                    attachDatePickerListeners(end_at,start_at);

                    $.each(response.data.project.stages, function(key,value){
                        start_at = moment(value.start_at).format('Y-MM-DD');
                        end_at = moment(value.end_at).format('Y-MM-DD');

                        $('input[name="stage['+value.stage_id+'][start_at]"]').val(start_at);
                        $('input[name="stage['+value.stage_id+'][end_at]"]').val(end_at);
                    });

                }

                doAlert(response);

              },
              done : function (){
                processFlag = 1;
              }
            });
        }
    });

    function refreshProjects(data){
        var projectList = "";
        var projectStages = "";
        $.each(data, function(key,format){

            if(format.projects.length){

                projectList += '<li class="category">'+
                                '<a href="#">'+format.name+'</a>'+
                              '</li>';

                projectStages +=  '<ul class="stages blank"></ul>';

                $.each(format.projects, function(key,project){
                    projectList += '<li><a href="#">'+project.name+'</a></li>';
                });
            }
        });
        $('#project-list').html('').append(projectList);
        attachViewProjectListener();
    }

    /**
     *
     * VIEW EDIT / PROJECT
     *
     */

    function attachViewProjectListener(){
        $('.viewProjectLink').on('click',function() {
            $('#spinner').fadeIn(500);
            var projectId = $(this).attr('data-project-id');

            $.ajax({
                type: "GET",
                url: '{{ route('api.projects.getDetails') }}?id='+projectId,
                success: function(response){
                    $.each(response,function(key,value){
                        $('#addProjectForm input[name="'+key+'"]').val(value);
                    });
                    $.each(response,function(key,value){
                        $('#addProjectForm select[name="'+key+'"]').val(value);
                    });

                    $('.add-project').offcanvas('show');
                    $('body').css('overflow','hidden').css('padding-right','17px');
                    $('#overlay-modal').fadeIn(10);
                    $('#spinner').hide();
                },
                complete : function(){
                }
            });

            $('#viewModal .scrollableArea').attr('style','width:'+367*20+'px !important');
        });
    }
    attachViewProjectListener();

    </script>
@endsection