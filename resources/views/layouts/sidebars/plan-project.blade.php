<div class="plan-project navmenu-default navmenu-fixed-right offcanvas-md offcanvas-lg" style="">
<a href="#" id="hidePlan">Back</a>
<h3>Plan Project</h3>
<h3 id="project_name">Project Name</h3>
    {!! Form::open(['id'=>'planProjectForm','route'=>'api.projects.stages.store']) !!}
        {!! Form::hidden('project_id', null , ['id'=>'project_id']) !!}
        <div class="side-form small clearfix">
             {{--{!! Form::input('text','date',null ,['id'=>'sessionDate','required' ,'id' => 'daterange' ,'placeholder'=>'Session date','class' => 'form-control']) !!}<br>--}}
             <div class="col-md-6 padding-0 padding-r-10">
             Project Duration :
                {!! Form::input('text','start_at', isset($conference) ? $conference->start_at : null , ['id'=>'start_at','placeholder'=>'Session Start','required','class' => 'project-start show form-control']) !!}
             </div>
             <div class="col-md-6 padding-0">
             &nbsp;
                {!! Form::input('text','end_at', isset($conference) ? $conference->end_at : null , ['readonly','id'=>'end_at','placeholder'=>'Session End','required','class' => 'project-end form-control']) !!}
             </div>
        </div>
        <br>
        Internal Project Stages:
        @if($stagesEx)
            @foreach($stagesIn as $stage)
                {!! Form::hidden('stage['.$stage->id.'][stage_id]', $stage->id) !!}
                <div class="side-form small clearfix" style="background-color: {{ $stage->color }}">
                    <div class="col-md-6 padding-0 padding-r-10">
                    {{ $stage->name }}
                       {!! Form::input('text','stage['.$stage->id.'][start_at]', null , ['id'=>'stage['.$stage->id.'][start_at]','placeholder'=>'Session Start','class' => 'daterangepicker project-start show form-control']) !!}
                    </div>
                    <div class="col-md-6 padding-0">
                        &nbsp;
                       {!! Form::input('text','stage['.$stage->id.'][end_at]', null , ['readonly','id'=>'stage['.$stage->id.'][end_at]','placeholder'=>'Session End','class' => 'project-end form-control']) !!}
                    </div>
                </div>
            @endforeach
        @else
            Please add a project stage at the settings.
        @endif
            <br>
        @if(count($stagesEx))
            External Project Stages:
            @foreach($stagesEx as $stage)
            {!! Form::hidden('stage['.$stage->id.'][stage_id]', $stage->id) !!}
                <div class="side-form small clearfix" style="background-color: {{ $stage->color }}">
                    <div class="col-md-6 padding-0 padding-r-10">
                    {{ $stage->name }}
                       {!! Form::input('text','stage['.$stage->id.'][start_at]', null , ['id'=>'stage['.$stage->id.'][start_at]','placeholder'=>'Session Start','class' => 'daterangepicker project-start show form-control']) !!}
                    </div>
                    <div class="col-md-6 padding-0">
                        &nbsp;
                       {!! Form::input('text','stage['.$stage->id.'][end_at]', null , ['readonly','id'=>'stage['.$stage->id.'][end_at]','placeholder'=>'Session End','class' => 'project-end form-control']) !!}
                    </div>
                </div>
            @endforeach
        @endif
        {!! Form::submit('Plan Project',['class'=>'link-bt link-color margin-b-0 pull-right']) !!}
    {!! Form::close() !!}
</div>

@section('plan-project-js')
    <script>
        /** SEND CREATE REQUEST **/

        $('#planProjectForm').on('submit',function(e){
            var form = $(this);
            processFlag = 1;


        if($('#view_project_id').val()){
            url = '{{ route('api.projects.stages-calendar.update') }}';
            $('#planProjectForm').attr('action',url);
        } else {
            url = '{{ route('api.projects.stages.store') }}';
                $('#planProjectForm').attr('action',url);
        }

//
//            e.preventDefault();
//                e.stopPropagation();
//
//            data = form.serialize();
//            url = form.attr('action');
//
//            if(processFlag) {
//                processFlag = 0;
//
//                $.ajax({
//                  type: "POST",
//                  url: url,
//                  data: data,
//                  success: function(response){
//
//                    console.log(response);
//                    if(response.status==200) {
//                        $(".add-project").offcanvas('hide');
//                        $(".plan-project").offcanvas('show');
//                        refreshProjects(response.data);
//                    }
//
//                    doAlert(response);
//                  },
//                  done : function (){
//                    processFlag = 1;
//                  }
//                });
//            }
        });

        $('#hidePlan').on('click',function(){
            $(".plan-project").offcanvas('hide');
            $(".add-project").offcanvas('show');
        });

    </script>
@endsection