<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Online team scheduling and project management and planning software - TapiApp</title>
    <meta name="description" content="TapiApp is a customizable online team scheduling, project management and planning software.">
    <meta name="keywords" content="TapiApp,scheduling,production,project,management,spreadsheets,software,Excel,app">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">

    <!-- Dublin Core -->
    <meta name="DC.Title" content="Tapi App - Official Website">
    <meta name="DC.Creator" content="Leading Brands JLT">
    <meta name="DC.Description" content="TapiApp is a customizable online team scheduling, project management and planning software.">
    <meta name="DC.Language" content="English">

    <!-- Open Graph data -->
    <meta property="og:title" content="Tapi App - Official Website" />
    <meta property="og:type" content="web page" />
    <meta property="og:url" content="http://www.tapiapp.com/" />
    <meta property="og:description" content="TapiApp is a customizable online team scheduling, project management and planning software." />
    <meta property="og:site_name" content="http://www.tapiapp.com/" />
    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
     <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
     <link href="{{ asset('public/fonts/stylesheet.css') }}" rel="stylesheet">
     <link href="{{ asset('public/css/bootstrap-overrides.css') }}" rel="stylesheet">
     <link href="{{ asset('public/css/global.css') }}" rel="stylesheet">

     <link href="{{ asset('public/css/login.css') }}" rel="stylesheet">

    @yield('style')
</head>
<body id="app-layout">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left">
            </div>
        </div>
    </div>

    @yield('content')


    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    @yield('js')

    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-1910012-65', 'auto');
        ga('send', 'pageview');

    </script>
</body>
</html>
