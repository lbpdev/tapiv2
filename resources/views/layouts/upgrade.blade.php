<div id="plansModal" class="modal fade" role="dialog">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center" style="width: 520px">
            <!-- Modal content-->
            <div class="modal-content clearfix">
                <div class="modal-header clearfix">
                    <h4 class="modal-title">Subscription Plans</h4>
                </div>
                <div class="modal-body clearfix width-full">
                    @inject('subService','App\Services\SubscriptionService')

                    <?php $subscription = $subService->getSubscription(); ?>

                    <div id="plans" class="plan-slider">
                        <div class="col-md-3 text-center">
                            <div class="row">
                                <div class="item {{ $subscription->plan->code!='TAPI_PLAN_1' ? '' : ( $subscription->SubscriptionReference ? "active" : '' ) }}" style="">
                                    <h1>Plan 1</h1>
                                    <div class="content">
                                        <p><strong>5</strong> users</p>
                                        <p>$15/month</p>
                                    </div>
                                    <p>
                                        @if($subscription->plan->code!='TAPI_PLAN_1'&&!$subscription->SubscriptionReference)
                                            <a target="_blank" href="https://secure.avangate.com/order/checkout.php?PRODS=4698769&QTY=1&CART=1&CARD=2&DESIGN_TYPE=2&ADDITIONAL_USER_ID={{ Auth::user()->id }}&BACK_REF=https%3A%2F%2Ftapiapp.com%2Fapp%2Fsettings%3Fm%3Dthank-you">Buy Now</a>
                                        @elseif($subscription->plan->code=='TAPI_PLAN_1'&&$subscription->SubscriptionReference)
                                            @if($subscription->ExpirationDate <= \Carbon\Carbon::now()->setTimezone(Auth::user()->companyTimezone))
                                                <a href="https://secure.avangate.com/renewal/?LICENSE={{ $subscription->SubscriptionReference }}&ADDITIONAL_USER_ID={{Auth::user()->id}}">Renew</a>
                                                <span class="current">Current</span>
                                            @else
                                                <a href="#">Active</a>
                                                <span class="current">Current</span>
                                            @endif
                                        @else
                                            @if($subscription->SubscriptionReference)
                                                <a target="_blank" href="https://secure.avangate.com/order/upgrade.php?LICENSE={{$subscription->SubscriptionReference}}&ADDITIONAL_USER_ID={{ Auth::user()->id }}&BACK_REF=https%3A%2F%2Ftapiapp.com%2Fapp%2Fsettings%3Fm%3Dthank-you">Buy Now</a>
                                            @else
                                                <a target="_blank" href="https://secure.avangate.com/order/checkout.php?PRODS=4698769&QTY=1&CART=1&CARD=2&DESIGN_TYPE=2&ADDITIONAL_USER_ID={{ Auth::user()->id }}&BACK_REF=https%3A%2F%2Ftapiapp.com%2Fapp%2Fsettings%3Fm%3Dthank-you">Buy Now</a>
                                            @endif
                                        @endif
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 text-center">
                            <div class="row">
                                <div class="item {{ $subscription->plan->code!='TAPI_PLAN_2' ? '' : "active" }}" style="">
                                    <h1>Plan 2</h1>
                                    <div class="content">
                                        <p><strong>10</strong> users</p>
                                        <p>$25/month</p>
                                    </div>
                                    <p>
                                        @if($subscription->plan->code!='TAPI_PLAN_2')
                                            @if($subscription->SubscriptionReference)
                                                <a target="_blank" href="https://secure.avangate.com/order/upgrade.php?LICENSE={{$subscription->SubscriptionReference}}&ADDITIONAL_USER_ID={{ Auth::user()->id }}&BACK_REF=https%3A%2F%2Ftapiapp.com%2Fapp%2Fsettings%3Fm%3Dthank-you">Buy Now</a>
                                            @else
                                            <a target="_blank" href="https://secure.avangate.com/order/checkout.php?PRODS=4698853&QTY=1&CART=1&CARD=2&DESIGN_TYPE=2&ADDITIONAL_USER_ID={{ Auth::user()->id }}&BACK_REF=BACK_REF=https%3A%2F%2Ftapiapp.com%2Fapp%2Fsettings%3Fm%3Dthank-you">Buy Now</a>
                                            @endif
                                        @else
                                            <a href="https://secure.avangate.com/renewal/?LICENSE={{ $subscription->SubscriptionReference }}&ADDITIONAL_USER_ID={{Auth::user()->id}}">Renew</a>
                                            <span class="current">Current</span>
                                        @endif
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 text-center">
                            <div class="row">
                                <div class="item {{ $subscription->plan->code!='TAPI_PLAN_3' ? '' : "active" }}" style="">
                                    <h1>Plan 3</h1>
                                    <div class="content">
                                        <p><strong>20</strong> users</p>
                                        <p>$40/month</p>
                                    </div>
                                    <p>
                                        @if($subscription->plan->code!='TAPI_PLAN_3')
                                            @if($subscription->SubscriptionReference)
                                                <a target="_blank" href="https://secure.avangate.com/order/upgrade.php?LICENSE={{$subscription->SubscriptionReference}}&ADDITIONAL_USER_ID={{ Auth::user()->id }}&BACK_REF=https%3A%2F%2Ftapiapp.com%2Fapp%2Fsettings%3Fm%3Dthank-you">Buy Now</a>
                                            @else
                                            <a target="_blank" href="https://secure.avangate.com/order/checkout.php?PRODS=4698854&QTY=1&CART=1&CARD=2&DESIGN_TYPE=2&ADDITIONAL_USER_ID={{ Auth::user()->id }}&BACK_REF=https%3A%2F%2Ftapiapp.com%2Fapp%2Fsettings%3Fm%3Dthank-you">Buy Now</a>
                                            @endif
                                        @else
                                            <a href="https://secure.avangate.com/renewal/?LICENSE={{ $subscription->SubscriptionReference }}&ADDITIONAL_USER_ID={{Auth::user()->id}}">Renew</a>
                                            <span class="current">Current</span>
                                        @endif
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 text-center">
                            <div class="row">
                                <div class="item {{ $subscription->plan->code!='TAPI_PLAN_4' ? '' : "active" }}" style="border: 0;">
                                    <h1>Plan 4</h1>
                                    <div class="content">
                                        <p><strong>35</strong> users</p>
                                        <p>$60/month</p>
                                    </div>
                                    <p>
                                        @if($subscription->plan->code!='TAPI_PLAN_4')
                                            @if($subscription->SubscriptionReference)
                                                <a target="_blank" href="https://secure.avangate.com/order/upgrade.php?LICENSE={{$subscription->SubscriptionReference}}&ADDITIONAL_USER_ID={{ Auth::user()->id }}&BACK_REF=https%3A%2F%2Ftapiapp.com%2Fapp%2Fsettings%3Fm%3Dthank-you">Buy Now</a>
                                            @else
                                                <a target="_blank" href="https://secure.avangate.com/order/checkout.php?PRODS=4698855&QTY=1&CART=1&CARD=2&DESIGN_TYPE=2&ADDITIONAL_USER_ID={{ Auth::user()->id }}&BACK_REF=https%3A%2F%2Ftapiapp.com%2Fapp%2Fsettings%3Fm%3Dthank-you">Buy Now</a>
                                            @endif
                                        @else
                                            <a href="https://secure.avangate.com/renewal/?LICENSE={{ $subscription->SubscriptionReference }}&ADDITIONAL_USER_ID={{Auth::user()->id}}">Renew</a>
                                            <span class="current">Current</span>
                                        @endif
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer clearfix pull-left width-full">
                </div>
            </div>

        </div>
    </div>
</div>