<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Tapi App</title>

    <!-- Fonts -->
    {{--<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>--}}

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
     <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
     <link href="{{ asset('public/fonts/stylesheet.css') }}" rel="stylesheet">
     <link href="{{ asset('public/fonts/radikal/stylesheet.css') }}" rel="stylesheet">
     <link href="{{ asset('public/css/bootstrap-overrides.css') }}" rel="stylesheet">
     <link href="{{ asset('public/css/global.css') }}" rel="stylesheet">
     <link href="{{ asset('public/plugins/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet">

    @yield('style')

    <style>
        html,
        body {
            height: 100%;
            min-height: 100%;
            padding: 0 !important;
        }

        body {
            font-family: Helvetica;
            font-weight: 300;
        }

        .fa-btn {
            margin-right: 6px;
        }

        #addProject {
            font-size: 18px;
            margin-top: 5px;
        }
        
        #subscription-overlay {
            background-color: rgba(0,0,0,0.8);
            height: 100%;
            width: 100%;
            position: fixed;
            top: 22px;
            left: 0;
            z-index:3000;
        }

        #subscription-overlay .message {
            width: 400px;
            height: 100px;
            position: absolute;
            top: 50%;
            left: 50%;
            margin-left: -200px;
            margin-top: -50px;
            background-color: transparent;
            font-size: 24px;
            color: #fff;
            text-align: center;
            line-height: 30px;
        }

        #mobile-overlay {
            display: none;
        }

        @media screen and (max-width: 991px){
            #mobile-overlay {
                display: block;
                background-color: rgba(0,0,0,0.9);
                height: 100%;
                width: 100%;
                position: fixed;
                top: 0;
                left: 0;
                z-index:3000;
            }

            #mobile-overlay .message {
                width: 400px;
                height: 100px;
                position: absolute;
                top: 20%;
                left: 50%;
                margin-left: -200px;
                margin-top: -50px;
                background-color: transparent;
                font-size: 19px;
                color: #fff;
                text-align: center;
                line-height: 30px;
                padding: 30px;
            }
        }

        #plansModal {
            z-index: 30001;
        }
    </style>

    <style>
        .supportButton {
            line-height: 100%;
            font-size: 14px;
            font-weight: 300;
            background-color: #cf2982;
            color: #fff;
            border-width: 1px;
            border-style: solid;
            border-color: rgb(191, 28, 115);
            border-image: initial;
            padding: 0;
            border-radius: 999rem;
            z-index: 999998;
            transform: translateZ(0px);
            position: fixed;
            opacity: 1;
            right: 0px;
            bottom: 0px;
            width: 80px;
            height: 30px;
            margin: 10px 20px;
            font-family: Helvetica;
        }
    </style>
</head>


@inject('subService','App\Services\SubscriptionService')

<?php $subscription = $subService->getSubscription(); ?>


<body id="app-layout" style="{{ !is_null($subscription) ? ( $subscription->ExpirationDate <= \Carbon\Carbon::now()->setTimezone(Auth::user()->companyTimezone) ? 'overflow:hidden' : '' ) :  '' }}">
    <div id="globalAlert" class="alert"></div>
    <div id="overlay-modal" style="display: block">
        <div id="spinner" style="display: block">
            <img src="{{ asset('public/images/ripple.svg') }}">
        </div>
    </div>

    <div class="col-md-12 top-nav no-print">
        <div class="row">
            <div class="container">

                @if(count($subscription))
                    <div class="pull-left nav-menu">
                        <span class="pull-left">
                            <span class="font-12" style="margin-right: 5px">{{ $subscription->plan->name }} {{ $subscription->SubscriptionReference ? '' : ' - Trial' }}
                            (Expires on {{ $subscription->ExpirationDate->format('M d Y') }})</span>

                            @if(Auth::user()->isAdmin || Auth::user()->isScheduler)
                            <span class="font-12"> |

                                @if($subscription->SubscriptionReference)
                                    {{--<a href="https://secure.avangate.com/renewal/?LICENSE={{ $subscription->SubscriptionReference }}&DOTEST=1&ADDITIONAL_USER_ID={{Auth::user()->id}}"><button>Renew</button></a> |--}}
                                    <a href="#" class="show-plans"><button>Change Plan</button></a>
{{--                                    <a href="{{ route('api.company.toggleAutoRenew',$subscription->SubscriptionReference) }}">{!! $subscription->RecurringEnabled ? '<button>Disable Auto-renew</button>' : '<button>Enable Auto-renew </button>'  !!}</a>--}}
                                @else
                                    <a href="#" class="show-plans"><button>Subscribe Now</button></a>
                                @endif
                                 </span>
                            @endif

                        </span>
                    </div>
                @endif

                <div class="pull-right nav-menu">
                    @if(Auth::user()->isAdmin)
                        <a href="{{ route('calendar.year') }}" id="sched-link">
                            Scheduling
                        </a>
                        <a href="{{ route('reports.index') }}" id="report-link">
                            Reports
                        </a>
                        <a href="{{ route('settings.info') }}">Settings</a>
                    @endif

                    <span id="activeUser">({{ Auth::user()->name }})</span>
                    <a href="{{ url('logout') }}">Logout</a>
                </div>
            </div>
        </div>
    </div>
    <nav class="navbar navbar-default navbar-static-top no-print">
        <div class="container">
            <div class="row">

                <div class="col-md-6 text-left paddring-r-0">
                    @inject('company', 'App\Services\CompanyService')

                    @if(Request::path()!="settings")
                        <a class="navbar-brand" href="{{ url('/') }}">
                    @endif
                        @if($company->getLogo())
                          <img src="{{ $company->getLogo() }}" id="lb-logo" height="82">
                        @else
                          <div id="lb-logo" class="logo-placeholder"></div>
                        @endif
                    @if(Request::path()!="settings")
                        </a>
                    @endif
                </div>
                <div class="col-md-6 text-right">
                    <a class="navbar-brand padding-b-0 padding-r-0" href="{{ url('/') }}">
                        <img src="{{ asset('public/images/Tapi-Logo.png') }}" id="lb-logo" height="96">
                    </a>
                </div>
            </div>
            @if(Request::path()!="settings" && Request::path()!="reports")
                @if(Auth::user()->isAdmin || Auth::user()->isScheduler)
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <ul class="view-nav">
                                <li class="{{ str_contains(Request::path(), 'calendar/year') ? 'active' : '' }}">
                                    <a href="{{ str_contains(Request::path(), 'calendar/year') ? '#' : route('calendar.year') }}">Year View</a>
                                </li>
                                <li class="{{ str_contains(Request::path(), 'calendar/week') ? 'active' : '' }}">
                                    <a  href="{{ str_contains(Request::path(), 'calendar/week') ? '#' : route('calendar.week') }}" >Week View</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                @endif
            @endif
        </div>
    </nav>

    <div id="main-content" class="relative">
        @yield('content')
    </div>

    @if($subscription->ExpirationDate <= \Carbon\Carbon::now()->setTimezone(Auth::user()->companyTimezone))
        @if(Auth::user()->isAdmin || Auth::user()->isScheduler)
            <div id="subscription-overlay">
                <div class="message">
                    Your subscription has expired. Please renew your subscription to continue using TapiApp.<br><br>
                    <a href="#" class="show-plans"><button>Subscribe Now</button></a>
                </div>
            </div>

        @endif
    @endif

    <div id="mobile-overlay">
        <div class="message">
            Sorry but this app is only currently compatible on desktop. If you are on mobile then please download the mobile app below: <br><br>
            <img src="{{ asset('public/images/Apple-Store-min.png') }}" width="160">
            <img src="{{ asset('public/images/Play-Store-min.png') }}" width="160">
        </div>
    </div>

    @include('layouts.upgrade')


    <!-- JavaScripts -->
    <script src="{{ asset('public/js/jquery.min.js') }}"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="{{ asset('public/js/moment.js') }}"></script>



    <script type="text/javascript">
        var globalAlert = $('#globalAlert');
        var rightOffset = ($('#main-content .container')[0].offsetLeft ) - 50;

        $(window).load(function(){
            $('#overlay-modal').fadeOut(500);
        });

        $(window).resize(function(){
            setTimeout(function(){
                rightOffset = ($('#main-content .container')[0].offsetLeft ) - 50;
            },500);
        });

        var my_timer;

        function alertFade() {
            my_timer = setTimeout(function () {
                globalAlert.animate({
                    'top' : '-150%',
                    'opacity' : '0',
                    'display' : 'none',
                }, 1000,function(){ $(this).attr('class','alert').text(''); });
            }, 5000);
        };

        function doAlert(response){

            clearTimeout(my_timer);

            $(globalAlert).dequeue().stop(true,true).finish().css('top','-150%');

            globalAlert.text(response.message).stop(true,true).css('margin-right',(-($(globalAlert).outerWidth() / 2)+'px'));

            if(response.status==200)
                globalAlert.addClass('alert-success');
            else
                globalAlert.addClass('alert-danger');

            globalAlert.stop(true,true).animate({
                'top' : '10px',
                'opacity' : '1',
                'display' : 'block',
            },50);

            alertFade();
        }

        $('.show-plans').on('click',function(){
            $('#plansModal').modal('show');
        });

        var appUrl = '{{ url('/') }}';


        function showLoader() {
            $('#overlay-modal').show();
            $('#spinner').show();
        }


        function hideLoader() {
            $('#overlay-modal').hide();
            $('#spinner').hide();
        }

    </script>

    @yield('js')
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}


    <script>

        @if($subscription->ExpirationDate <= \Carbon\Carbon::now()->setTimezone(Auth::user()->companyTimezone))
            setInterval(function(){
                $("#main-content button").unbind('click');
                $("#main-content a").unbind('click');
                $("#main-content a").on('click',function(e) {
                    // stop propagation and prevent default by returning false
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                });
            },1000);
        @endif


        @if(count($subscription))
            @if($subscription->SubscriptionReference)
                    $.ajax({
                type: "GET",
                url: '{{ route('subscriptions.get.ajax') }}?SubscriptionReference={{$subscription->SubscriptionReference}}',
                success: function(response){
                    console.log(response);
                }
            });
            @endif
        @endif

        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-1910012-65', 'auto');
        ga('send', 'pageview');

    </script>
    <!-- Start of TapiApp Zendesk Widget script -->
    <script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(e){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var o=this.createElement("script");n&&(this.domain=n),o.id="js-iframe-async",o.src=e,this.t=+new Date,this.zendeskHost=t,this.zEQueue=a,this.body.appendChild(o)},o.write('<body onload="document._l();">'),o.close()}("https://assets.zendesk.com/embeddable_framework/main.js","tapiapp.zendesk.com");
        /* Cloaking Mode: On */
        zE(function() {
            zE.hide();
        });
        /*]]>*/</script>

    <!-- End of TapiApp Zendesk Widget script -->
    <button class="supportButton" onClick="zE.activate({hideOnClose: true});">Support</button>

</body>
</html>
