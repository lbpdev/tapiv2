<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Tapi App</title>

    <!-- Fonts -->
    {{--<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>--}}

            <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('public/fonts/stylesheet.css') }}" rel="stylesheet">
    <link href="{{ asset('public/fonts/radikal/stylesheet.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/bootstrap-overrides.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/global.css') }}" rel="stylesheet">
    <link href="{{ asset('public/plugins/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet">

    @yield('style')

    <style>
        html,
        body {
            height: 100%;
            min-height: 100%;
            padding: 0 !important;
        }

        body {
            font-family: Helvetica;
            font-weight: 300;
        }

        .fa-btn {
            margin-right: 6px;
        }

        #addProject {
            font-size: 18px;
            margin-top: 5px;
        }

        #subscription-overlay {
            background-color: rgba(0,0,0,0.8);
            height: 100%;
            width: 100%;
            position: fixed;
            top: 30px;
            left: 0;
            z-index:3000;
        }

        #subscription-overlay .message {
            width: 400px;
            height: 100px;
            position: absolute;
            top: 50%;
            left: 50%;
            margin-left: -200px;
            margin-top: -50px;
            background-color: transparent;
            font-size: 24px;
            color: #fff;
            text-align: center;
            line-height: 30px;
        }

        #mobile-overlay {
            display: none;
        }

        @media screen and (max-width: 991px){
            #mobile-overlay {
                display: block;
                background-color: rgba(0,0,0,0.9);
                height: 100%;
                width: 100%;
                position: fixed;
                top: 0;
                left: 0;
                z-index:3000;
            }

            #mobile-overlay .message {
                width: 400px;
                height: 100px;
                position: absolute;
                top: 20%;
                left: 50%;
                margin-left: -200px;
                margin-top: -50px;
                background-color: transparent;
                font-size: 19px;
                color: #fff;
                text-align: center;
                line-height: 30px;
                padding: 30px;
            }
        }

        #plansModal {
            z-index: 30001;
        }
    </style>
</head>


<body id="app-layout">

<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <div style="height: 300px;">
                <h1 style="margin-top: 300px">Oops!</h1>
                <div>
                    {!! $error !!}<br>
                    Click <a href="{{ url()->previous() }}">here</a> to go back!
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
