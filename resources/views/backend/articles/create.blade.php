@extends('admin.master')

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{ isset($_GET['cat_name']) ? $_GET['cat_name'] : 'Articles' }}
        <small>Control panel</small>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header">
            <div class="col-md-12">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif

            <h3 class="box-title">{{ isset($article) ? 'Edit' : 'Add' }} {{ isset($_GET['cat_name']) ? $_GET['cat_name'] : 'Article' }} </h3>

            <!-- /. tools -->
            </div>
        </div>
        <!-- /.box-header -->

        <div class="row">
            <div class="col-md-12">
                <div class="box-body pad">
                    {!! Form::open(['route'=> isset($article) ? 'admin.articles.update' : 'admin.articles.store','files'=>true]) !!}

                    @if(isset($article))
                        <div class="col-md-12">
                            <img src="{{ $article->thumbnail }}" height="100">
                            <br>
                            Remove Thumbnail &nbsp;<input type="checkbox" name="remove_thumb">
                            <br>
                            <br>
                        </div>
                    @endif

                        @if(isset($article))
                            <input type="hidden" name="id" value="{{ $article->id }}">
                        @endif
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" name="title" required class="form-control" value="{{ isset($article) ? $article->title : '' }}">
                            </div>
                            <div class="form-group">
                                <label>Sub-title (Optional)</label>
                                <input type="text" name="subtitle" value="{{ isset($article) ? $article->subtitle : '' }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">

                            @if(!isset($_GET['category']))
                            <div class="form-group">
                                <label>Category</label>

                                @inject('catFetcher','App\Services\CategoryFetcher')
                                <?php $categories = $catFetcher->getList(); ?>
                                {!! Form::select('category_id',$categories, isset($article) ? $article->category_id : 0 ,['class'=>'form-control']) !!}
                            </div>
                            @else
                                {!! Form::hidden('category_id',$_GET['category']) !!}
                            @endif

                            <div class="form-group">
                                <label>Thumbnail</label>
                                <input type="file" class="form-control"  value="" name="thumbnail">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Content</label>
                                <textarea class="editor" name="content" rows="10" cols="80">{{ isset($article) ? $article->content : '' }}</textarea>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>External Link (Optional)</label>
                                <input type="text" class="form-control"  value="{{ isset($article) ? $article->external_link : '' }}" name="external_link">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Post Date</label>
                                <input type="text"  name="created_at" value="{{ isset($article) ? $article->created_at->format('m/d/Y') : \Carbon\Carbon::now()->setTimezone(Auth::user()->companyTimezone)->format('m/d/Y') }}" class="datepicker form-control">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="submit" class="form-control">
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

@endsection

@section('custom-js')
    <script>
        $('.datepicker').datepicker();
    </script>
@endsection