<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            {{--<li class="treeview">--}}
                {{--<a href="#">--}}
                    {{--<i class="fa fa-dashboard"></i> <span>Gallery</span>--}}
                    {{--<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li><a href="index.html"><i class="fa fa-circle-o"></i> Photos</a></li>--}}
                    {{--<li><a href="index2.html"><i class="fa fa-circle-o"></i> Videos</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            <li><a href="{{ route('backend.companies.index') }}"><i class="fa fa-circle-o"></i> Companies</a></li>
            <li><a href="{{ route('backend.users.index') }}"><i class="fa fa-circle-o"></i> Users</a></li>
            {{--<li><a href="{{ route('backend.plans.index') }}"><i class="fa fa-circle-o"></i> Plans</a></li>--}}
            {{--<li><a href="{{ route('backend.subscriptions.index') }}"><i class="fa fa-circle-o"></i> Subscriptions</a></li>--}}
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>