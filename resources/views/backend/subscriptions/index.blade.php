@extends('backend.master')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('public/admin') }}/plugins/datatables/dataTables.bootstrap.css">
    @endsection

    @section('content')
            <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Companies
            <small>Control panel</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="box">
            <div class="box-header"></div>
            <div class="box-body">
                {{--<a href="{{ route('backend.plans.create') }}">+ Add New</a>--}}
                @if(Session::has('success'))
                    <div class="col-md-12 alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                @endif
                <table width="100%" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Reference ID</th>
                        <th>Date Purchased</th>
                        <th>Start</th>
                        <th>Expiration</th>
                        <th>Recurring</th>
                        <th>Company</th>
                        <th>Plan</th>
                        <th>Create at</th>
                        <th>Updated at</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $row)
                        <tr>
                            <td>{{ $row->SubscriptionReference }}</td>
                            <td>{{ $row->PurchaseDate ? $row->PurchaseDate->format('d-m-Y') : 'N/A' }}</td>
                            <td>{{ $row->SubscriptionStartDate ? $row->SubscriptionStartDate->format('d-m-Y')  : 'N/A' }}</td>
                            <td>{{ $row->ExpirationDate ? $row->ExpirationDate->format('d-m-Y')  : 'N/A' }}</td>
                            <td>{{ $row->RecurringEnabled ? 'Yes' : 'No' }}</td>
                            <td>{{ $row->company->name }}</td>
                            <td>{{ $row->plan->name }}</td>
                            <td>{{ $row->created_at->format('d-m-Y') }}</td>
                            <td>{{ $row->updated_at->format('d-m-Y') }}</td>
                            <td>
                                <a href="{{ route('backend.plans.edit',$row->id) }}">Edit</a>
                                |
                                <a href="{{ route('backend.plans.delete',$row->id) }}">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>Current Plan</th>
                        <th>Expiration</th>
                        <th>Actions</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>

    </section>
    <!-- /.content -->

@endsection

@section('custom-js')
    <script src="{{ asset('public/admin') }}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('public/admin') }}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $('table').DataTable({
            "order": [[ 3, "desc" ]],
            "pageLength": 14,
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    </script>
@endsection