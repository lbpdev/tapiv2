@extends('backend.master')


@section('custom-css')
    <style>
        hr {
            display: inline-block;
            width: 100%;
        }

        .col-md-8 {
            width: 100% !important;
        }
    </style>
@endsection

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{ isset($options['name']) ? $options['name'] : '' }}
        <small>Control panel</small>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="box">
        <div class="box-header"></div>
        <div class="box-body">

        <div class="col-md-12 relative">
            <h4>Edit Plan "{{ $data->name }}"</h4>

            {{ Form::open(['route'=>'backend.plans.update']) }}
                <div class="col-md-4">
                    <div class="row">
                        <div class="form-group">
                            {{ Form::input('text','name',$data->name,['class'=>'form-control']) }}
                            {{ Form::input('text','code',$data->code,['class'=>'form-control']) }}
                            {{ Form::input('number','max_users',$data->max_users,['class'=>'form-control']) }}
                            {{ Form::submit('Update',['class'=>'form-control']) }}
                        </div>
                    </div>
                </div>
            {{ Form::close() }}

        </div>
    </div>
    </div>
</section>
<!-- /.content -->

@endsection

@section('custom-js')

@endsection











