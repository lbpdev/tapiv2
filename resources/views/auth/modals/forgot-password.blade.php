<!-- Modal -->
<div class="modal fade" id="changePassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Create a new password</h4>
                </div>
                <div class="modal-body">
                    <div id="change-password">
                        {!! Form::hidden('reset_id',null) !!}
                        {!! Form::hidden('temp_password',null,['id'=>'temp_password']) !!}
                        Please enter your new password:
                        {!! Form::input('password','password[1]',null,['required','id'=>'new-password','placeholder'=>'New password','class'=>'form-control']) !!}
                        <br>
                        Confirm new password:
                        {!! Form::input('password','password[2]',null,['required','id'=>'new-password-confirm','placeholder'=>'Confirm new password','class'=>'form-control']) !!}
                    </div>
                    <div class="alert-success alert temp-hide">
                        You have successfully changed you password. Please click <a href="#" id="loginLink">here</a>  to login.
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" id="forgot-password-submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>


@section('forgot-password-js')
    <script>
        $('#forgot-password-submit').on('click',function(){
            $(this).attr('disabled','disabled');

            var email = $('#loginForm input[name=email]').val();
            var password = $('#new-password').val();
            var password_confirm = $('#new-password-confirm').val();
            var temp_password = $('#temp_password').val();

            if(!password_confirm && !password){
                $('#changePassword .alert').html('Please fill up the required fields.').attr('class','alert alert-danger');
                $('#forgot-password-submit').removeAttr('disabled');
                return false;
            }

            if(password_confirm==password){
                $.ajax({
                    type: "POST",
                    url: '{{ route('mobile.reset-password') }}',
                    data : {
                        password : password,
                        password_confirm : password_confirm,
                        email : email,
                        temp_password : temp_password
                    },
                    success: function(response){
                        console.log(response);

                        if(response.status==200){
                            $('#changePassword .modal-footer').hide();
                            $('#change-password').hide();
                            $('#password-change-success').show();

                            $('#loginForm input[name=password]').val(password);

                            $('#changePassword .alert').html('You have successfully changed you password. Please click <a href="#" id="loginLink">here</a>  to login.').attr('class','alert alert-success');

                            $('#loginLink').on('click',function(){
                                $('#loginBt').trigger('click');
                            });

                            $('#forgot-password-submit').removeAttr('disabled');

                        } else if (response.status == 400)
                            $('#changePassword .alert').html(response.message).attr('class','alert alert-danger');

                        $('#forgot-password-submit').removeAttr('disabled');
                    },
                    complete : function(){
                    }
                });
            } else {
                $('#changePassword .alert').html('Password did not match. Please try again.').attr('class','alert alert-danger');

                $('#forgot-password-submit').removeAttr('disabled');
            }

        });
    </script>
@endsection