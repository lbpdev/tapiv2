<!-- Modal -->
<div class="modal fade" id="forgotEmail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Forgot Password</h4>
                </div>
                <div class="modal-body">

                    <div class="form">
                        Enter your email address to reset your password:
                        {!! Form::input('email','email',null,['required','id'=>'forgot-email','placeholder'=>'Email address','class'=>'form-control']) !!}

                    </div>
                    <div class="alert-danger alert temp-hide margin-t-10" >
                        Email is not registered.
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" id="forgot-email-submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>


@section('forgot-email-js')
    <script>
        $('#forgot-email-submit').on('click',function(){
        $(this).attr('disabled','disabled');

        var email = $('#forgot-email').val();

        $.ajax({
            type: "POST",
            url: '{{ route('mobile.reset-email') }}',
            data : {
                email : email
            },
            success: function(response){
                console.log(response);

                if(response.status==200){
                    $('#forgotEmail .form').hide();
                    $('#forgotEmail .modal-footer').hide();
                    $('#forgotEmail .alert').show().attr('class','alert alert-success').html('We’ve sent you an email to the address registered with your account. Please check your inbox.');
                }
                else if(response.status==400)
                    $('#forgotEmail .alert').show().html(response.message);

                $('#forgot-email-submit').removeAttr('disabled');
            },
            complete : function(){
            }
        });

        });
    </script>
@endsection