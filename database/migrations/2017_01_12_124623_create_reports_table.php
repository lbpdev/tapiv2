<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //'company_id','interval','time','day','week_day','timezone','type'
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned()->index();
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->string('interval',1);
            $table->time('time');
            $table->string('day',2)->nullable();
            $table->string('week_day',3)->nullable();
            $table->string('timezone',32);
            $table->string('type',64);
            $table->timestamp('last_send')->nullable();
            $table->timestamp('next_send')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reports');
    }
}
