<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\ClientBrand;

class AddCompanyIdToClientBrands extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_brands', function (Blueprint $table) {
            $table->string('company_id')->nullable();
        });

        $brands = ClientBrand::get();
        
        foreach ($brands as $brand){
            if($brand->client){
                $brand->company_id = $brand->client->company_id;
                $brand->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_brands', function (Blueprint $table) {
            $table->dropColumn('company_id');
        });
    }
}
