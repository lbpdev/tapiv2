<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeDatesToSubscriptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('subscriptions', function($table)
        {
            $table->dateTime('PurchaseDate')->change();
            $table->dateTime('SubscriptionStartDate')->change();
            $table->dateTime('ExpirationDate')->change();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscriptions', function($table)
        {
            $table->string('PurchaseDate')->change();
            $table->string('SubscriptionStartDate')->change();
            $table->string('ExpirationDate')->change();
        });
    }
}
