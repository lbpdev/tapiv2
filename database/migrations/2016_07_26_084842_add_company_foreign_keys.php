<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanyForeignKeys extends Migration
{

    public function __construct()
    {
        $this->tables = ['users','options','projects','clients','departments','formats','keys','stages','invites'];
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        foreach ($this->tables as $table)
            Schema::table($table, function($table)
            {
                $table->integer('company_id')->unsigned()->index();
                $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        foreach ($this->tables as $tableName)
            Schema::table('users', function($t)
            {
                $t->dropForeign('users_company_id_foreign');
                $t->dropColumn('company_id');
            });

            Schema::table('options', function($t)
            {
                $t->dropForeign('options_company_id_foreign');
                $t->dropColumn('company_id');
            });

            Schema::table('projects', function($t)
            {
                $t->dropForeign('projects_company_id_foreign');
                $t->dropColumn('company_id');
            });

            Schema::table('clients', function($t)
            {
                $t->dropForeign('clients_company_id_foreign');
                $t->dropColumn('company_id');
            });

            Schema::table('departments', function($t)
            {
                $t->dropForeign('departments_company_id_foreign');
                $t->dropColumn('company_id');
            });

            Schema::table('formats', function($t)
            {
                $t->dropForeign('formats_company_id_foreign');
                $t->dropColumn('company_id');
            });

            Schema::table('keys', function($t)
            {
                $t->dropForeign('keys_company_id_foreign');
                $t->dropColumn('company_id');
            });

            Schema::table('stages', function($t)
            {
                $t->dropForeign('stages_company_id_foreign');
                $t->dropColumn('company_id');
            });

            Schema::table('invites', function($t)
            {
                $t->dropForeign('invites_company_id_foreign');
                $t->dropColumn('company_id');
            });
    }
}
