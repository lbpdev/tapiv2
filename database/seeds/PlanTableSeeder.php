<?php

use Illuminate\Database\Seeder;
use App\Models\Plan;

class PlanTableSeeder extends Seeder
{

    public function __construct(Plan $plan){
        $this->model = $plan;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->model->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $data = [
            ['name' => 'Plan 1' , 'code' => 'plan-1'],
            ['name' => 'Plan 2' , 'code' => 'plan-2'],
            ['name' => 'Plan 3' , 'code' => 'plan-3'],
            ['name' => 'Plan 4' , 'code' => 'plan-4'],
        ];

        foreach($data as $drow)
            Plan::create($drow);
    }
}
