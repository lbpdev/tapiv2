<?php

use Illuminate\Database\Seeder;
use App\Models\Client;

class ClientSeeder extends Seeder
{

    public function __construct(Client $client){
        $this->model = $client;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->model->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $data = [
            [
                'name'  => 'Astrazeneca' ,
                'color' => '#3355cc',
                'company_id' => 1
            ],
            [
                'name'  => 'Abbott' ,
                'color' => '#e5ff00',
                'company_id' => 1
            ],
            [
                'name'  => 'Bayer' ,
                'color' => '#3cbaff',
                'company_id' => 1
            ],
            [
                'name'  => 'Novartis' ,
                'color' => '#f19fff',
                'company_id' => 1
            ]
        ];

        foreach($data as $index=>$item){
            $this->model->create($item);
        }
    }
}
