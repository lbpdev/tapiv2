<?php

use Illuminate\Database\Seeder;
use App\Models\Format;

class FormatSeeder extends Seeder
{

    public function __construct(Format $format){
        $this->model = $format;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->model->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $formats = [
            [
                'name'  => 'Newsletter' ,
                'slug' => 'newsletter',
                'company_id' => 1
            ],
            [
                'name'  => 'Journal' ,
                'slug' => 'journal',
                'company_id' => 1
            ],
            [
                'name'  => 'App' ,
                'slug' => 'app',
                'company_id' => 1
            ],
            [
                'name'  => 'E-detail' ,
                'slug' => 'e-detail',
                'company_id' => 1
            ],
            [
                'name'  => 'Website' ,
                'slug' => 'Website',
                'company_id' => 1
            ],
            [
                'name'  => 'Web App' ,
                'slug' => 'web-app',
                'company_id' => 1
            ],
            [
                'name'  => 'HTML Mailer' ,
                'slug' => 'html-mailer',
                'company_id' => 1
            ]
        ];

        foreach($formats as $index=>$format){
            $this->model->create($format);
        }
    }
}
