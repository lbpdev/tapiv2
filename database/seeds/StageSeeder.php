<?php

use Illuminate\Database\Seeder;
use App\Models\Stage;

class StageSeeder extends Seeder
{

    public function __construct(Stage $stage){
        $this->model = $stage;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->model->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $stages = [
            [
                'data' => [
                    'name'  => 'Coding' ,
                    'color' => '#ff8083',
                    'internal' => 1,
                    'company_id' => 1
                ],
                'department' => 1,
            ],

            [
                'data' => [
                    'name'  => 'Editorial' ,
                    'color' => '#6fd1dc',
                    'internal' => 1,
                    'company_id' => 1
                ],
                'department' => 1,
            ],
            [
                'data' => [
                    'name'  => 'Design' ,
                    'color' => '#bfdd35',
                    'internal' => 1,
                    'company_id' => 1
                ],
                'department' => 1,
            ],
            [
                'data' => [
                    'name'  => 'Approval' ,
                    'color' => '#9495a4',
                    'internal' => 1,
                    'company_id' => 1
                ],
                'department' => 1,
            ]
        ];

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        foreach($stages as $index=>$stage){
            $newStage = $this->model->create($stage['data']);

            if($newStage)
                $newStage->department()->sync([$stage['department']]);

        }
    }
}
