<?php

use Illuminate\Database\Seeder;
use App\Models\Department;

class DepartmentsTableSeeder extends Seeder
{

    public function __construct(Department $department){
        $this->department = $department;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->department->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $departments = [
            [
                'name'  => 'Coding' ,
                'color' => '#00e3cc',
                'company_id' => 1,
                'work_hours' => 8
            ],
            [
                'name'  => 'Editorial' ,
                'color' => '#ff8083',
                'company_id' => 1,
                'work_hours' => 8
            ],
            [
                'name'  => 'Design' ,
                'color' => '#6fd1dc',
                'company_id' => 1,
                'work_hours' => 8
            ],
            [
                'name'  => 'Accounts' ,
                'color' => '#bfdd35',
                'company_id' => 1,
                'work_hours' => 8
            ],
            [
                'name'  => 'Production' ,
                'color' => '#9495a4',
                'company_id' => 1,
                'work_hours' => 8
            ],
            [
                'name'  => 'Management' ,
                'color' => '#38a4ff',
                'company_id' => 1,
                'work_hours' => 8
            ],
            [
                'name'  => 'Reception' ,
                'color' => '#da72e1',
                'company_id' => 1,
                'work_hours' => 8
            ],
        ];

        foreach($departments as $index=>$department){
            Department::create($department);
        }
    }
}
