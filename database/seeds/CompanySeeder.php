<?php

use Illuminate\Database\Seeder;
use App\Models\Client;

use App\Models\Company;

class CompanySeeder extends Seeder
{

    public function __construct(Company $company){
        $this->model = $company;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->model->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $data = [
            [
                'name'  => 'Leading Brands'
            ],
            [
                'name'  => 'Pixelhub'
            ]
        ];

        foreach($data as $index=>$item){
            $this->model->create($item);
        }
    }
}
