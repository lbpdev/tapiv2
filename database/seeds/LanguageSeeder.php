<?php

use Illuminate\Database\Seeder;
use App\Models\Language;

class LanguageSeeder extends Seeder
{

    public function __construct(Language $languages){
        $this->language = $languages;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->language->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $languages = [
            "ar" => "Arabic",
            "de" => "German",
            "en" => "English",
            "la" => "Latin",
            "nl" => "Dutch",
            "pl" => "Polish",
        ];

        foreach($languages as $code=>$language)
            Language::create(['code'=>$code,'name'=>$language]);
    }
}
