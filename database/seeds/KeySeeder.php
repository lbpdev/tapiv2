<?php

use Illuminate\Database\Seeder;
use App\Models\Key;

class KeySeeder extends Seeder
{


    public function __construct(Key $key){
        $this->model = $key;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //disable foreign key check for this connection before running seeders
//        $this->model->truncate();

        $keys = [
            [
                'name' => 'Front-End',
                'department_id' => 1,
                'company_id' => 1
            ],
            [
                'name' => 'Back-End',
                'department_id' => 1,
                'company_id' => 1
            ],
            [
                'name' => 'Content',
                'department_id' => 1,
                'company_id' => 1
            ]
        ];

        foreach($keys as $index=>$key)
            $this->model->create($key);

    }
}
