<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ColorSeeder::class);
        $this->call(CompanySeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(DepartmentsTableSeeder::class);
        $this->call(StageSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(FormatSeeder::class);
        $this->call(ClientSeeder::class);
        $this->call(LanguageSeeder::class);
        $this->call(KeySeeder::class);
        $this->call(OptionSeeder::class);
        $this->call(PlanTableSeeder::class);
        $this->call(SubscriptionTableSeeder::class);
    }
}
