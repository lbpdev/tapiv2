<?php

use Illuminate\Database\Seeder;
use App\Models\Subscription;

class SubscriptionTableSeeder extends Seeder
{

    public function __construct(Subscription $model){
        $this->model = $model;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->model->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $data = [
            ['company_id' => 1 , 'SubscriptionStartDate' => '2016-11-01 00:00:00','ExpirationDate' => '2020-11-01 00:00:00', 'plan_id' => 1],
        ];

        foreach($data as $row)
            $this->model->create($row);
    }
}
