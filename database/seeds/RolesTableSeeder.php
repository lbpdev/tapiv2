<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesTableSeeder extends Seeder
{

    public function __construct(Role $role){
        $this->roles = $role;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->roles->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $roles = [
            ['name' => 'Administrator' , 'slug' => 'admin','icon' => 'Star.png'],
            ['name' => 'Scheduler' , 'slug' => 'scheduler','icon' => 'Ribbon.png'],
            ['name' => 'Employee' , 'slug' => 'employee','icon' => 'User.png'],
        ];

        foreach($roles as $role)
            Role::create($role);
    }
}
