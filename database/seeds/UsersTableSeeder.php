<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{

    public function __construct(User $user){
        $this->user = $user;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->user->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        // Roles | 1 = Admin , 2 = Scheduler , 3 = Employee
        // Departments | 1 = Coding , 2 = Editorial , 3 = Design , 4 = Accounts, 5 = Production , 6 = Management , 7 = Reception

        $users =
            [
                [
                    'data' => [
                        'username' => 'mars' ,
                        'name' => 'Mars Mlodzinski' ,
                        'email' => 'mars@leadingbrands.me' ,
                        'password' => Hash::make('secret'),
                        'company_id' => 1,
                        'is_active' => 1
                    ],
                    'role' => 1,
                    'department' => 6
                ],
                [
                    'data' => [
                        'username' => 'kirsten' ,
                        'name' => 'Kirsten Campbell-Morris' ,
                        'email' => 'kirsten@leadingbrands.me' ,
                        'password' => Hash::make('secret'),
                        'company_id' => 1,
                        'is_active' => 1
                    ],
                    'role' => 1,
                    'department' => 6
                ],
                [
                    'data' => [
                        'username' => 'yesa' ,
                        'name' => 'Yesa Calapre' ,
                        'email' => 'yesa@leadingbrands.me' ,
                        'password' => Hash::make('secret'),
                        'company_id' => 1,
                        'is_active' => 1
                    ],
                    'role' => 1,
                    'department' => 5
                ],
                [
                    'data' => [
                        'username' => 'iona' ,
                        'name' => 'Iona Stonehouse' ,
                        'email' => 'iona@leadingbrands.me' ,
                        'password' => Hash::make('secret'),
                        'company_id' => 1,
                        'is_active' => 1
                    ],
                    'role' => 1,
                    'department' => 5
                ],
                [
                    'data' => [
                        'username' => 'mike' ,
                        'name' => 'Michael Gordon' ,
                        'email' => 'mike@leadingbrands.me' ,
                        'password' => Hash::make('secret'),
                        'company_id' => 1,
                        'is_active' => 1
                    ],
                    'role' => 3,
                    'department' => 3
                ],
                [
                    'data' => [
                        'username' => 'gene' ,
                        'name' => 'Gene Ellorin' ,
                        'email' => 'gene@leadingbrands.me' ,
                        'password' => Hash::make('secret'),
                        'company_id' => 1,
                        'is_active' => 1
                    ],
                    'role' => 3,
                    'department' => 1
                ],
                [
                    'data' => [
                        'username' => 'jeffrey' ,
                        'name' => 'Jeffrey Belo' ,
                        'email' => 'jeffrey@leadingbrands.me' ,
                        'password' => Hash::make('secret'),
                        'company_id' => 1,
                        'is_active' => 1
                    ],
                    'role' => 3,
                    'department' => 1
                ],
                [
                    'data' => [
                        'username' => 'lance' ,
                        'name' => 'Lance Andres' ,
                        'email' => 'lance@leadingbrands.me' ,
                        'password' => Hash::make('secret'),
                        'company_id' => 1,
                        'is_active' => 1
                    ],
                    'role' => 3,
                    'department' => 1
                ],
                [
                    'data' => [
                        'username' => 'emily' ,
                        'name' => 'Emily Smith' ,
                        'email' => 'emily@leadingbrands.me' ,
                        'password' => Hash::make('secret'),
                        'company_id' => 1,
                        'is_active' => 1
                    ],
                    'role' => 3,
                    'department' => 2
                ],
                [
                    'data' => [
                        'username' => 'lindsay' ,
                        'name' => 'Lindsay Tarasovic' ,
                        'email' => 'lindsay@leadingbrands.me' ,
                        'password' => Hash::make('secret'),
                        'company_id' => 1,
                        'is_active' => 1
                    ],
                    'role' => 3,
                    'department' => 2
                ],
                [
                    'data' => [
                        'username' => 'migo' ,
                        'name' => 'Migo Badalles' ,
                        'email' => 'migo@leadingbrands.me' ,
                        'password' => Hash::make('secret'),
                        'company_id' => 1,
                        'is_active' => 1
                    ],
                    'role' => 3,
                    'department' => 7
                ]
            ];

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        foreach($users as $index=>$user){
            $newUser = User::create($user['data']);
            $newUser->role()->sync([$user['role']]);
            $newUser->department()->sync([$user['department']]);
        }
    }
}
