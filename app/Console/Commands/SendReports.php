<?php

namespace App\Console\Commands;

use App\Models\Color;
use App\Models\Company;
use App\Models\Department;
use App\Models\Format;
use App\Models\Project;
use App\Models\Report;
use App\Models\UserTask;
use App\Models\User;
use App\Services\PushNotificationService;
use App\Services\ReportService;
use Carbon\Carbon;
use Davibennun\LaravelPushNotification\Facades\PushNotification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Support\Facades\Mail;


class SendReports extends Command
{
    use ReportService;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reports:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Log $log, PushNotificationService $push)
    {
        $this->log = $log;
        $this->push = $push;

        $this->log->useDailyFiles(storage_path().'/logs/notifications.log');
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->log->info('Checking Reports: '.Carbon::now()->format('Y-m-d H:i'));

        echo Carbon::now()->format('Y-m-d H:i');

        $queue = Report::where('next_send','>=',Carbon::now()->format('Y-m-d H:i'))
            ->where('next_send','<=',Carbon::now()->addMinute(5)->format('Y-m-d H:i'))
            ->get();

        $this->log->info('Reports found: '.count($queue));

        if($queue){
            foreach ($queue as $task){
                $company = Company::where('id',$task->company_id)->first();

                $this->log->info('Report Type: '.$task->type);
                
                if($task->type=='tasks-report'){

                    $companyAdmins = User::where('company_id',$task->company_id)->whereHas('role',function($query){
                        $query->where('slug','admin');
                    })->pluck('email');

                    $this->log->info('Company admins Found: '.count($companyAdmins));

                    $companyEmployees = User::where('company_id',$task->company_id)->whereHas('role',function($query){
                        $query->where('slug','employee');
                    })->whereHas('department',function($query){})->get();

                    $users = [];
                    foreach($companyEmployees as $index=>$employee){

                        $tasks = UserTask::where('user_id',$employee->id)
                            ->whereDate('date_assigned','=',Carbon::now()->setTimezone($employee->company->timezone)->format('Y-m-d'))
                            ->count();

                        $completed = UserTask::where('user_id',$employee->id)
                            ->whereDate('date_assigned','=',Carbon::now()->setTimezone($employee->company->timezone)->format('Y-m-d'))
                            ->whereNotNull('date_completed')
                            ->count();

                        if($tasks || $completed){
                            $users[$index]['name'] = $employee->name;
                            $users[$index]['tasks'] = $tasks;
                            $users[$index]['completed'] = $completed;
                            $users[$index]['id'] = $employee->id;
                            $users[$index]['department'] = $employee->department ? $employee->department->id : '';
                        }
                    }

                    if(count($users)){
                        $date = Carbon::now()->setTimezone($employee->company->timezone)->format('d-m-Y');

                        $this->log->info('Sending Task Report');

                        $adminEmails = [];

                        foreach($companyAdmins as $admin){
                            array_push($adminEmails,$admin);
                            $this->log->info('Sending Reports to:'. $admin);
                        }

                        Mail::queue('emails.notifications.tasks-report', ['users'=>$users,'date'=>$date], function($message) use($adminEmails)
                        {
                            $message->to($adminEmails, 'Administrator')->subject('Today’s summary report');
                        });
                    } else {
                        $this->log->info('No Tasks today');
                    }
                }

                if($task->type=='today-tasks'){

                    $companyEmployees = User::where('company_id',$task->company_id)->whereHas('role',function($query){
                        $query->where('slug','employee');
                    })->get();

                    foreach($companyEmployees as $employee){

                        $tasks = UserTask::with('task.project.client','task.key')->where('user_id',$employee->id)
                            ->whereDate('date_assigned','=',Carbon::now()->setTimezone($employee->company->timezone)->format('Y-m-d'))
                            ->get();

//                        return view('emails.notifications.today-tasks',compact('user','tasks'));

                        if(count($tasks)){
                            $this->log->info('Sending Task Ahead Reminders PUSH');
                            $message = PushNotification::Message('Hello. You have '.count($tasks).' tasks for today. Have a great day ahead.',array(
                                'custom' => array('intent' => 'task-today-reminder')
                            ));

                            $this->push->send($employee,$message);

                            $this->log->info('Sending Task Ahead Reminders Email to:'. $employee->email);
                            Mail::queue('emails.notifications.today-tasks', ['user'=>$employee,'tasks'=>$tasks], function($message) use($employee)
                            {
                                $message->to($employee->email, $employee->name)->subject('Today’s schedule');
                            });
                        }

                    }
                }

                if($task->type=='incomplete-tasks'){

                    $companyEmployees = User::where('company_id',$task->company_id)->whereHas('role',function($query){
                        $query->where('slug','employee');
                    })->whereHas('department',function($query){})->get();

                    $company = Company::where('id',$task->company_id)->first();
                    $currentDay = Carbon::now()->setTimezone($company->timezone)->format('Y-m-d');

                    foreach ($companyEmployees as $employee){

                        $unfinishedTasks = $employee->tasks()->where('date_completed','=',null)->where('date_assigned',$currentDay)->count();
                        $finishedTasks = $employee->tasks()->where('date_completed','!=',null)->where('date_assigned',$currentDay)->count();
                        $totalTasks = $employee->tasks()->where('date_assigned',$currentDay)->count();

                        if($unfinishedTasks) {

                            $this->log->info('Sending Unfinished Task Reminders');
                            
                            $message = PushNotification::Message('Hello. This is quick reminder that you have '.$unfinishedTasks.' tasks to complete today.',array(
                                'custom' => array('intent' => 'task-complete-reminder')
                            ));

                            $this->push->send($employee,$message);

                        } else if($totalTasks) {
                            if($finishedTasks == $totalTasks){

                                $this->log->info('Sending All finished Task Notification');

                                $message = PushNotification::Message('Well done you have completed all your tasks today. ',array(
                                    'custom' => array('intent' => 'task-all-complete-reminder')
                                ));

                                $this->push->send($employee,$message);

                                $this->log->info('All Task Completed Email to:'. $employee->email);
                                Mail::queue('emails.notifications.todays-tasks-completed',[],function($message) use($employee)
                                {
                                    $message->to($employee->email, $employee->name)->subject('All your tasks for today are completed.');
                                });

                            }
                        }
                    }
                }

                if($task->type=='email-reports'){
                    $companyAdmins = User::where('company_id',$task->company_id)->whereHas('role',function($query){
                        $query->where('slug','admin');
                    })->pluck('email');

                    $company = Company::where('id',$task->company_id)->first();
                    $currentDay = Carbon::now()->setTimezone($company->timezone)->format('Y-m-d');

                    if($company){
                        if( // CHECK IF COMPANY HAS REPORT ENABLED
                            count($company->getOption('productivity-report')) ||
                            count($company->getOption('deadline-report')) ||
                            count($company->getOption('time-report')) ||
                            count($company->getOption('deliverable-report'))
                        )
                        {
                            $pdf = $this->generateReport($company->id, $task->interval);

                            $adminEmails = [];

                            foreach($companyAdmins as $admin){
                                array_push($adminEmails,$admin);
                                $this->log->info('Sending Reports to:'. $admin);
                            }

                            Mail::queue('emails.notifications.reports',[],function($message) use($pdf,$adminEmails)
                            {
                                $message->to($adminEmails, 'Administrator')->subject('Your TapiApp reports ');
                                $message->attachData($pdf->output(), "Report.pdf");
                            });
                        }
                    }
                }

                $next_send = Carbon::now();

                switch($task->interval){
                    case 'd':
                        $next_send = Carbon::now()->addDay()->format('Y-m-d'); break;
                    case 'm':
                        $next_send = Carbon::now()->addMonth()->format('Y-m-d'); break;
                    case 'w':
                        $next_send = Carbon::now()->addDays(7)->format('Y-m-d'); break;
                }

                $next_send = Carbon::parse($next_send.' '.$task->time);

                $this->log->info('Updating Next Send Report: '.$next_send);
                $task->last_send = $task->next_send;
                $task->next_send = $next_send;
                $task->save();
            }
        }
    }

    public function generateReport($company_id,$interval)
    {

        $range = 'today';

        switch($interval){
            case 'd':
                $range = 'today';
                break;
            case 'w':
                $range = 'this-week';
                break;
            case 'm':
                $range = 'this-month';
                break;
            case 'y':
                $range = 'this-year';
                break;

        }
        $department = 0;
        $user = 0;

        $userRanges = [];
        $userRanges[0] = 'Whole company';

        $departments = Department::where('company_id',$company_id)->lists('name','id');
        $users = User::where('company_id',$company_id)->lists('name','id');

        foreach ($departments as $id=>$department)
            $userRanges['department['.$id.']'] = $department;

        foreach ($users as $id=>$user)
            $userRanges['user['.$id.']'] = $user;

        $deliverables = Format::where('company_id',$company_id)->orderBy('name','ASC')->lists('name');

        $colors = Color::lists('value');

        $company = Company::where('id',$company_id)->first();
//        $company_logo = $company->getLogo();

        $productivityData = count($company->getOption('productivity-report')) ? json_encode($this->productivity(true,$company_id,$range,$department,$user)) : null;

        if(count($company->getOption('deadline-report'))){
            $deadline = $this->deadline(true,$company_id,$range != 'this-year' ? 'this-quarter' : 'this-year');
            $deadlineData = json_encode($deadline['values']);
            $deadlineLabels = json_encode($deadline['labels']);
        }

        if(count($company->getOption('time-report'))){
            $timeData = json_encode($this->booked(true,$company_id,$range,$department,$user));
        }

        if(count($company->getOption('deliverable-report'))){
            $deliverable = $this->deliverables(true,$company_id,$range,$department,$user);
            $deliverableData = json_encode($deliverable['values']);
            $deliverableLabels = json_encode($deliverable['labels']);
        }

        $userRanges = [];
        $userRanges[0] = 'Whole company';

        $departments = Department::where('company_id',$company_id)->lists('name','id');
        $users = User::where('company_id',$company_id)->lists('name','id');

        foreach ($departments as $id=>$department)
            $userRanges['department['.$id.']'] = $department;

        foreach ($users as $id=>$user)
            $userRanges['user['.$id.']'] = $user;

//        return view('pages.reports.print', compact('projects','userRanges','deliverables','colors','productivityData','deadlineData','deadlineLabels','timeData','timeLabels','deliverableData','deliverableLabels'));

        $pdf = App::make('snappy.pdf.wrapper');
        $pdf->loadView('pages.reports.print', compact('projects','userRanges','deliverables','colors','productivityData','deadlineData','deadlineLabels','timeData','timeLabels','deliverableData','deliverableLabels','company_logo'))->setPaper('a4')->setOrientation('portrait')->setOption('margin-bottom', 0);

        return $pdf;
    }
}
