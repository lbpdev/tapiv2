<?php

namespace App\Console\Commands;

use App\Models\Color;
use App\Models\Company;
use App\Models\Department;
use App\Models\Format;
use App\Models\Project;
use App\Models\Report;
use App\Models\TaskChanges;
use App\Models\UserTask;
use App\Models\User;
use App\Services\PushNotificationService;
use App\Services\ReportService;
use Carbon\Carbon;
use Davibennun\LaravelPushNotification\Facades\PushNotification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Support\Facades\Mail;

class SendTasksPushNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'push-notification:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send push notification to a user based on tasks changes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Log $log, PushNotificationService $push)
    {
        $this->log = $log;
        $this->push = $push;
        $this->log->useDailyFiles(storage_path().'/logs/push.log');

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $users = TaskChanges::groupBy('user_id')->lists('user_id');

        if(count($users))
            $this->log->info('Found '. count($users) .' changes');

        foreach ($users as $user){
            $changes = TaskChanges::where('user_id',$user)->orderBy('created_at','ASC')->get();

            if(count($changes)>1){

                $firstChange = $changes[0]->created_at;

                $changes = TaskChanges::where('user_id',$user)->whereDate('created_at' ,'<=', Carbon::parse($firstChange)->addMinutes(3) )->get();

                if($changes){
                    $this->log->info('Sending Group Change');

                    $message = PushNotification::Message('Hello. Your tasks today have been updated.',array(
                        'custom' => array('intent' => 'task-updated')
                    ));

                    $user = User::find($user);
                    $this->push->send($user,$message);

                    foreach ($changes as $change)
                        TaskChanges::where('id',$change->id)->delete();
                }

            } elseif(count($changes)==1) {

                if(Carbon::parse($changes[0]->created_at->format('Y-m-d H:i'))->diffInMinutes(Carbon::now()) > 3){
                    $message = 'Hello. One of your tasks today has been updated.';
                    $intent = 'updated';

                    switch ($changes[0]->type){
                        case 'deleted':
                            $message = 'Hello. One of your tasks today has been deleted.';
                            $intent = 'task-deleted';
                            break;
                        case 'added':
                            $message = 'A task has been added to your schedule today.';
                            $intent = 'task-added'; break;
                    }


                    $this->log->info('Sending '. $message);

                    $message = PushNotification::Message($message,array(
                        'custom' => array('intent' => $intent)
                    ));

                    $user = User::find($user);
                    $this->push->send($user,$message);

                    TaskChanges::where('id',$changes[0]->id)->delete();
                }
            }
        }


    }
}
