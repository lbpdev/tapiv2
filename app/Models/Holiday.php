<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\Eloquent\HasCompanyScopes;
use Illuminate\Support\Facades\Auth;

class Holiday extends Model
{
    use HasCompanyScopes;

    protected $fillable = ['from','to','name','is_annual','company_id'];

    public $dates = ['from','to'];

    public function getRangeAttribute(){
        $date = Carbon::parse($this->from)->setTimezone(Auth::user()->companyTimezone)->format('m/d/Y') . ' - ' . $this->to->format('m/d/Y');
        return $date;
    }

}
