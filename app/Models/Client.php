<?php

namespace App\Models;

use App\Repositories\Eloquent\HasCompanyScopes;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasCompanyScopes;

    protected $fillable = ['color','name','company_id'];

    public function brands(){
        return $this->hasMany('App\Models\ClientBrand');
    }

    public function projects(){
        return $this->hasMany('App\Models\Project');
    }
}
