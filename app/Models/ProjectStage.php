<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectStage extends Model
{
    public $fillable = ['start_at','end_at','stage_id','time','project_id'];

    protected $dates = ['start_at','end_at'];

    public function stage(){
        return $this->hasOne('App\Models\Stage','id','stage_id');
    }

    public function project(){
        return $this->hasOne('App\Models\Project','id','project_id');
    }

    public function getColorAttribute(){
        return $this->stage->color;
    }
}
