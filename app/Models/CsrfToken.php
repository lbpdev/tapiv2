<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CsrfToken extends Model
{
    protected $fillable = ['token','session_id'];
}
