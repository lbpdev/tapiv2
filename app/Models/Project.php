<?php

namespace App\Models;

use App\Repositories\Eloquent\HasCompanyScopes;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasCompanyScopes;
    
    protected $fillable = ['name','version','format_id','client_id','client_brand_id','language_id','start_at','end_at','completed_at','time','company_id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['start_at','end_at','completed_at'];

    public function format(){
        return $this->hasOne('App\Models\Format','id','format_id');
    }

    public function stages(){
        return $this->hasMany('App\Models\ProjectStage');
    }

    public function languages(){
        return $this->hasMany('App\Models\ProjectLanguage','project_id');
    }

    public function client(){
        return $this->belongsTo('App\Models\Client');
    }

    public function getClientNameAttribute(){
        return $this->client->name;
    }

    public function brand(){
        return $this->belongsTo('App\Models\ClientBrand','client_brand_id');
    }

    public function tasks(){
        return $this->belongsTo('App\Models\Task','id','project_id');
    }

    public function getTasksAttribute(){
        return $this->tasks()->get();
    }

    public function getBrandNameAttribute(){
        return $this->brand->name;
    }

    public function language(){
        return $this->hasOne('App\Models\Language','id');
    }

    public function getLanguagesAttribute(){
        $languages = $this->languages()->get();
        $languages->load('language');
        return count($languages) ? $languages : 'N/A';
    }

    public function getLanguagesStringAttribute(){
        $languages = $this->languages;
        $data = '';

        foreach ($languages as $key=>$language){
            $data .= $language->language->name;
            if(isset($languages[$key+1]))
                $data .= ',';
        }

        return $data;
    }

    public function getDurationAttribute(){
        $data = Carbon::parse($this->start_at)->format('d/m/Y') . ' - ' . Carbon::parse($this->end_at)->format('d/m/Y') ;

        return $data;
    }

    public function scopeFromYear($query,$year){
        $year = intval($year);
        $currentYear = Carbon::parse($year.'-1-1');
        $to = $currentYear->addYears(2);
        $from = Carbon::parse(($year-1).'-1-1');

        return $query->where('start_at', '>=' , $from->format('Y-m-d'))->where('start_at', '<=' , $to->format('Y-m-d'));

    }
}
