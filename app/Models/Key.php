<?php

namespace App\Models;

use App\Repositories\Eloquent\HasCompanyScopes;
use Illuminate\Database\Eloquent\Model;

class Key extends Model
{
    use HasCompanyScopes;
    
    protected $fillable = ['name','department_id','company_id'];

    public $timestamps = false;

    public function department(){
        return $this->belongsTo('App\Models\Department','department_id');
    }

    public function company(){
        return $this->hasOne('App\Models\Company');
    }

    public function tasks(){
        return $this->hasMany('App\Models\Task');
    }

    public function projectsLinked(){
        return $this->hasMany('App\Models\Task');
    }
}
