<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StageDepartment extends Model
{
    public function department(){
        return $this->hasOne('App\Models\Department','department_id');
    }
    public function stage(){
        return $this->hasOne('App\Models\Stage','stage_id');
    }
}
