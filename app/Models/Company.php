<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = ['name'];

    public function users(){
        return $this->hasMany('App\Models\User');
    }
//
//    public function keys(){
//        return $this->hasMany('App\Models\Key','department_id');
//    }
//
//    public function tasks(){
//        return $this->hasMany('App\Models\Task','department_id');
//    }
//
//    public function stages(){
//        return $this->hasMany('App\Models\Stage','stage_departments');
//    }

    public function subscriptions(){
        return $this->hasMany('App\Models\Subscription','company_id');
    }

    public function getSubscriptionAttribute(){
        return $this->subscriptions()->orderBy('created_at','DESC')->first();
    }

    public function getActiveUsersAttribute(){
        return $this->users()->where('is_active',1)->get();
    }

    public function getUserLimitAttribute(){
        return $this->subscription->plan->max_users;
    }

    public function getWorkHoursAttribute(){
        $workHours = $this->options()->where('name','workhours')->first();

        if($workHours)
            return $workHours->value;

        return 8;
    }

    public function getWorkDaysCountAttribute(){
        $data = 5;

        $workDays = $this->options()->where('name','workdays')->first();

        if($workDays)
            $data = count(json_decode($workDays->value));

        return $data;
    }

    public function getWorkDaysAttribute(){
        $data = ["Mon","Tue","Wed","Thu","Fri"];

        $workDays = $this->options()->where('name','workdays')->first();

        if($workDays)
            $data = json_decode($workDays->value);

        return $data;
    }

    public function getTimezoneAttribute(){
        $timezone = 'America/Chicago';

        $option = $this->options()->where('name','timezone')->first();

        if($option)
            $timezone = $option->value;

        return $timezone;
    }

    public function getFirstDayAttribute(){

        $data = $this->options()->where('name','first-day')->first();

        return $data ? $data->value : 'Sun';
    }


    public function getAdminCountAttribute(){
        return $this->users()->whereHas('role',function($query){
           $query->where('slug','admin');
        })->count();
    }


    public function getLogoAttribute(){
        $logo = $this->options()->where('name','logo')->first();

        if($logo)
            return $logo->value;

        return null;
    }


    public function getActiveAdminCountAttribute(){
        return $this->users()->whereHas('role',function($query){
           $query->where('slug','admin');
        })->where('is_active',1)->count();
    }


    public function getHasActiveSubscriptionAttribute(){
        return $this->subscription->ExpirationDate > new \DateTime() ;
    }

    public function options(){
        return $this->hasMany('App\Models\Option');
    }

    public function reports(){
        return $this->hasMany('App\Models\Report');
    }


    public function scopeGetOption($query,$option_name){
        return $this->options()->where('name',$option_name)->where('company_id',$this->id)->get();
    }
}
