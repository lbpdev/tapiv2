<?php

namespace App\Models;

use App\Repositories\Eloquent\HasCompanyScopes;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    use HasCompanyScopes;
    
    protected $fillable = ['company_id','interval','time','month','day','week_day','timezone','type','next_send','last_send'];
}
