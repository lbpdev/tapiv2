<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserTask extends Model
{
    protected $fillable = [
        'task_id',
        'user_id',
        'time',
        'podio',
        'note',
        'is_urgent',
        'is_pushed',
        'date_assigned',
        'date_completed',
        'is_extra',
        'order'
    ];

    public $dates = [
        'date_assigned',
        'date_completed'
    ];

    public function task(){
        return $this->hasOne('App\Models\Task','id','task_id');
    }

    public function scopeThisMonth(){
        $month = Carbon::now()->format('m');
        $year = Carbon::now()->format('Y');
        $currentMonth = Carbon::now()->parse('01-'.$month.'-'.$year);
        $currentMonthEnd = Carbon::now()->parse('01-'.$month.'-'.$year)->addMonth()->subDay();

        return $this->where('date_assigned','>=',$currentMonth)->where('date_assigned','<=',$currentMonthEnd)->get();
    }

    public function scopeTodayAndPast(){
        $currentDay = Carbon::now()->format('Y-m-d');

        return $this->where('date_assigned','<=',$currentDay)->orderBy('date_assigned','DESC')->get();
    }
}
