<?php

namespace App\Models;

use App\Repositories\Eloquent\HasCompanyScopes;
use Illuminate\Database\Eloquent\Model;

class Stage extends Model
{
    use HasCompanyScopes;
    
    protected $fillable = ['color','name','internal','company_id','order','hasTime'];

    public $timestamps = false;

    public function project_stages(){
        return $this->hasMany('App\Models\ProjectStage');
    }

    public function department(){
        return $this->belongsToMany('App\Models\Department','stage_departments');
    }

    public function getDepartmentAttribute(){
        return $this->department()->first();
    }
}
