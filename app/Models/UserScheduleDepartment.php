<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserScheduleDepartment extends Model
{
    protected $fillable = ['user_id','department_id'];

    public function department(){
        return $this->hasOne('App\Models\Department');
    }

    public function user(){
        return $this->hasOne('App\Models\User');
    }

}
