<?php

namespace App\Models;

use App\Repositories\Eloquent\HasCompanyScopes;
use Illuminate\Database\Eloquent\Model;

class Invite extends Model
{
    use HasCompanyScopes;
    
    protected $fillable = ['name','email','role_id','department_id','token','company_id'];
}
