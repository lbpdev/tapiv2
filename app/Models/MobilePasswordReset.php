<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MobilePasswordReset extends Model
{
    protected $fillable = ['email','temp_password','status'];
}
