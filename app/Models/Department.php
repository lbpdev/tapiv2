<?php

namespace App\Models;

use App\Repositories\Eloquent\HasCompanyScopes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Department extends Model
{
    use HasCompanyScopes;
    
    protected $fillable = ['color','name','company_id','work_hours'];

    public $timestamps = false;

    public function users(){
    	return $this->belongsToMany('App\Models\User','user_departments');
    }

    public function keys(){
    	return $this->hasMany('App\Models\Key','department_id');
    }

    public function company(){
    	return $this->hasOne('App\Models\Company');
    }

    public function tasks(){
    	return $this->hasMany('App\Models\Task','department_id');
    }

    public function stages(){
    	return $this->hasMany('App\Models\Stage','stage_departments');
    }

    public function scopeWithEmployeesOnly($query){

        $query->whereHas('users', function ($users) {
            $users->where('company_id',Auth::user()->company_id)->whereHas('role', function ($roles) {
                $roles->where('id', 3);
            })->where('is_active',1);
        });

        return $query;
    }

    /** Jeffrey Update 2 */
    public function scopeWithMembersOnly($query,$company_id=null){

        $query->whereHas('users', function ($users) use($company_id){
            $users->where('company_id', $company_id ? $company_id : Auth::user()->company_id)->whereHas('role', function ($roles) {
                $roles->where('id', 3)
                      ->orWhere('id', 1);
            })->where('is_active',1)->whereHas('department',function($q){});
        });

        return $query;
    }

    public function getUsersAttribute(){
        return $this->users()->where('is_active',1)->get();
    }
}
