<?php

namespace App\Models;

use App\Repositories\Eloquent\HasCompanyScopes;
use Illuminate\Database\Eloquent\Model;

class Format extends Model
{
    use HasCompanyScopes;
    
    protected $fillable = ['name','slug','company_id'];

    public $timestamps = false;

    public  function projects(){
        return $this->hasMany('App\Models\Project');
    }
}
