<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Leave extends Model
{
    protected $fillable = ['from','to','user_id','type','holiday_id','time'];

    public $dates = ['from','to'];

}
