<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MobileApiToken extends Model
{
    protected $fillable = ['uuid','token','user_id'];

    protected $hidden = ['user_id','id'];
}
