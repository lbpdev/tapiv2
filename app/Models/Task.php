<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Task extends Model
{
    protected $fillable = ['department_id','key_id','project_id','time','note','date','is_extra','is_urgent'];

    public $dates = ['date'];

    public function department(){
        return $this->belongsTo('App\Models\Department','department_id');
    }

    public function project(){
        return $this->belongsTo('App\Models\Project','project_id');
    }

    public function key(){
        return $this->belongsTo('App\Models\Key','key_id');
    }

    public function utasks(){
        return $this->hasMany('App\Models\UserTask');
    }

    public function getUtasksAttribute(){
        return $this->utasks()->get();
    }

    public function scopeFromCurrentCompany($query)
    {
        return $query->whereHas('project',function ($query) {
            $query->where('company_id', Auth::user()->company_id);
        });
    }

}
