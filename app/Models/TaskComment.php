<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaskComment extends Model
{
    protected $fillable = ['comment','author_id','user_id','date'];

    public $timestamps = ['date'];

    public function author(){
        return $this->hasOne('App\Models\User','author_id');
    }

    public function user(){
        return $this->hasOne('App\Models\User','user_id');
    }
}
