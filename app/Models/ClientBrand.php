<?php

namespace App\Models;

use App\Repositories\Eloquent\HasCompanyScopes;
use Illuminate\Database\Eloquent\Model;

class ClientBrand extends Model
{
    use HasCompanyScopes;

    protected $fillable = ['name','company_id'];

    public $timestamps = false;

    public function company(){
        return $this->hasOne('App\Models\Company');
    }
}
