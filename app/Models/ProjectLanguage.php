<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectLanguage extends Model
{
    protected $fillable = ['project_id','language_id'];

    public $timestamps = false;

    public function language(){
        return $this->hasOne('App\Models\Language','id','language_id');
    }
}
