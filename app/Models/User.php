<?php

namespace App\Models;

use App\Models\UserLog;
use App\Repositories\Eloquent\HasCompanyScopes;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;


class User extends Authenticatable
{
    use HasCompanyScopes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','username','token','company_id','is_active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function scopeWithAll($query){
        return $query->with('role','tokens','leaves','logs','department','company');
    }

    public function role(){
        return $this->belongsToMany('App\Models\Role','user_roles');
    }


    public function tokens(){
        return $this->hasMany('App\Models\MobileApiToken');
    }

    public function leaves(){
        return $this->hasMany('App\Models\Leave');
    }

    public function logs(){
        return $this->hasMany('App\Models\UserLog');
    }

    public function devices(){
        return $this->hasMany('App\Models\DeviceToken');
    }

    public function tasks(){
        return $this->hasMany('App\Models\UserTask');
    }

    public function getRoleAttribute(){
        return $this->role()->first();
    }

    public function getIsAdminAttribute(){
        $role =  $this->role()->first();

        if($role->slug=='admin')
            return true;

        return false;
    }

    public function getIsEmployeeAttribute(){
        $role =  $this->role()->first();

        if($role->slug=='employee')
            return true;

        return false;

    }

    public function getIsSchedulerAttribute(){
        $role =  $this->role()->first();

        if($role->slug=='scheduler')
            return true;

        return false;
    }

    public function department(){
        return $this->belongsToMany('App\Models\Department','user_departments','user_id','department_id');
    }

    public function scheduleDepartment(){
        return $this->belongsToMany('App\Models\Department','user_schedule_departments','user_id','department_id');
    }

    public function company(){
        return $this->hasOne('App\Models\Company','id','company_id');
    }

    public function getDepartmentAttribute(){
        return $this->department()->first();
    }

    public function getDevicesAttribute(){
        return $this->devices()->get();
    }

    public function getMaxHoursAttribute(){
        $department_hours = $this->department->work_hours;

        if($department_hours)
            return $department_hours;

        $company_workhours = $this->company->options()->where('name','workhours')->first();

        if($company_workhours)
            return $company_workhours->value;

        return 8;
    }

    public function getScheduleDepartmentAttribute(){
        return $this->scheduleDepartment()->first() ? $this->scheduleDepartment()->first() : null;
    }

    public function getLogsAttribute(){
        return $this->logs()->orderBy('created_at','DESC')->get();
    }

    public function getCompanyAttribute(){
        return $this->company()->first();
    }

    protected function createLog($activity){
        return $this->logs()->create(array('activity'=>$activity));
    }

    protected function getLastLoginAttribute(){
        return $this->logs()->where('activity','login')->orderBy('created_at','DESC')->first();
    }

    protected function getLoggedInTodayAttribute(){
        return $this->logs()->where('activity','login')->whereDate('created_at','=',Carbon::now()->format('Y-m-d'))->get();
    }

    public function getCompanyUserLimitAttribute(){
        $company = Auth::user()->company;

        if($company){
            $subscription = $company->subscription;

            return $subscription->plan->max_users;
        }

        return null;
    }
    public function getTasksAttribute(){
        return $this->tasks()->orderBy('date_assigned','DESC')->get();
    }

    public function getTasksTodayAttribute(){

        $tasks = $this->tasks()->where('date_assigned',Carbon::now()->format('Y-m-d'))->get();

        return $tasks;
    }

    public function scopeTasksOnMonth($query, $monthYear){

        $year = Carbon::parse('01-'.$monthYear)->setTimezone(Auth::user()->company->timezone)->format('Y');
        $month = Carbon::parse('01-'.$monthYear)->setTimezone(Auth::user()->company->timezone)->format('m');

        $start = Carbon::parse('01-'.$month.'-'.$year)->format('Y-m-d');
        $end = Carbon::parse('01-'.$month.'-'.$year)->addMonth()->subDay()->format('Y-m-d');

        $total_tasks = $this->tasks()->where('date_assigned','>=',$start)->where('date_assigned','<=',$end)->get();

        return $total_tasks;
    }

    public function scopeTasksTodayAndPast(){

        $currentDay = Carbon::now()->setTimezone($this->company->timezone)->format('Y-m-d');

        return $this->tasks()->where('date_assigned','<=',$currentDay)->orderBy('date_assigned','DESC')->get();
    }

    public function getTasksCompletedTodayAttribute(){

        $tasks = $this->tasks()->where('date_assigned',Carbon::now()->format('Y-m-d'))->where('date_completed','!=',null)->get();

        return $tasks;
    }

    public function getTasksUncompletedTodayAttribute(){

        $tasks = $this->tasks()->where('date_assigned',Carbon::now()->format('Y-m-d'))->where('date_completed','=',null)->get();

        return $tasks;
    }

    public function getTasksYesterdayAttribute(){

        $tasks = $this->tasks()->where('date_assigned',Carbon::now()->subDay()->format('Y-m-d'))->get();

        return $tasks;
    }

    public function getTasksCompletedYesterdayAttribute(){

        $tasks = $this->tasks()->where('date_assigned',Carbon::now()->subDay()->format('Y-m-d'))->where('date_completed','!=',null)->get();

        return $tasks;
    }

    public function getCompanyTimezoneAttribute(){
        $timezone = 'America/Chicago';

        $option = $this->company->options()->where('name','timezone')->first();

        if($option)
            $timezone = $option->value;

        return $timezone;
    }

    public function scopeLoggedInOn($query,$date){
        return $this->logs()->where('activity','login')->whereDate('created_at','=',Carbon::parse($date)->format('Y-m-d'))->get();
    }

    public function getOverallRatingAttribute(){
        $total_tasks = $this->tasks()->count();

        if($total_tasks){
            $completed_taks = $this->tasks()->where('date_completed','!=',null)->count();

            return ($completed_taks / $total_tasks) * 100;
        }

        return 0;
    }

    public function scopeWeekRating(){
        $total_tasks = $this->tasks()->count();

        if($total_tasks){
            $completed_taks = $this->tasks()->where('date_completed','!=',null)->count();

            return ($completed_taks / $total_tasks) * 100;
        }

        return 0;
    }

    public function scopeMonthRating($query, $monthYear){

        $year = Carbon::parse('01-'.$monthYear)->setTimezone(Auth::user()->company->timezone)->format('Y');
        $month = Carbon::parse('01-'.$monthYear)->setTimezone(Auth::user()->company->timezone)->format('m');

        $start = Carbon::parse('01-'.$month.'-'.$year)->format('Y-m-d');
        $end = Carbon::parse('01-'.$month.'-'.$year)->addMonth()->subDay()->format('Y-m-d');

        $query = $this->tasks()->where('date_assigned','>=',$start)->where('date_assigned','<=',$end);

        $total_tasks = $query->count();

        $completed_tasks = $query->where('date_completed','!=',null)->count();

        return $total_tasks > 0 ? ($completed_tasks / $total_tasks) * 100 : 'N/A';
    }


    public function getWholeLeavesAttribute(){

        $leaves = Leave::where('user_id',$this->id)->where('time',0)->get();

        $dates = [];

        foreach ($leaves  as $key => $leave) {
            $begin = new \DateTime($leave->from);
            $end = new \DateTime($leave->to);

            $end = $end->modify( '+1 day' );

            $interval = new \DateInterval('P1D');
            $daterange = new \DatePeriod($begin, $interval ,$end);

            foreach($daterange as $date){
                $dates[] = $date->format("Y-m-d");
            }

        }

        return $dates;
    }


    public function getPartialLeavesAttribute(){

        $leaves = Leave::where('user_id',$this->id)->where('time','>',0)->get();

        $dates = [];

        foreach ($leaves  as $key => $leave) {
            $begin = new \DateTime($leave->from);
            $end = new \DateTime($leave->to);

            $end = $end->modify( '+1 day' );

            $interval = new \DateInterval('P1D');
            $daterange = new \DatePeriod($begin, $interval ,$end);

            foreach($daterange as $date){
                $dates[] = $date->format("Y-m-d");
            }

        }

        return $dates;
    }
}
