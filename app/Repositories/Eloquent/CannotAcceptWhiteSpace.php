<?php namespace App\Repositories\Eloquent;

use Illuminate\Support\Str;
trait CannotAcceptWhiteSpace {

    public function checkWhiteSpaces($string){

        if(str_replace(' ', '', $string)=="")
            return false;

        return trim($string);
    }
}