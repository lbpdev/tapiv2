<?php namespace App\Repositories\Eloquent;

use Illuminate\Support\Str;
trait CanCreateResponseCode {

    public function generateResponseWithData($slug, $postData){

        $data = $this->make($slug);
        $data['data'] = $postData;

        return $data;
    }

    public function generateResponseWithParameter($slug, $param){

        switch($slug){
            case 'work-hour-exceeded-dept':
                $data['status'] = 500;
                $data['message'] = 'Time entered exceeds working hours of '.$param.' hour(s).';
                break;
        }

        return $data;
    }

    public function generateResponse($slug){
        $data = $this->make($slug);
        return $data;
    }

    private function make($slug){

        $data['status'] = 200;

        switch($slug){
            case 'create-success':
                $data['message'] = 'New record created successfully.';
                break;

            case 'invite-success':
                $data['message'] = 'Your message has been sent.';
                break;

            case 'update-success':
                $data['message'] = 'Record updated successfully.';
                break;

            case 'invite-sent-success':
                $data['message'] = 'An invitation has been sent to ';
                break;

            case 'delete-success':
                $data['message'] = 'Record deleted successfully.';
                break;

            case 'token-found':
                $data['message'] = 'Token validated. Returning data.';
                break;

            case 'success':
                $data['message'] = 'Operation was successful.';
                break;



            /*
             * Departments
             */
            case 'create-department-success':
                $data['message'] = 'New department added.';
                break;

            case 'update-department-success':
                $data['message'] = 'Department updated.';
                break;

            case 'delete-department-success':
                $data['message'] = 'Department deleted.';
                break;

            /*
             * User
             */
            case 'create-user-success':
                $data['message'] = 'New User added.';
                break;

            case 'update-user-success':
                $data['message'] = 'User updated.';
                break;

            case 'delete-user-success':
                $data['message'] = 'User deleted.';
                break;

            /*
             * Holiday
             */
            case 'create-holiday-success':
                $data['message'] = 'New holiday added.';
                break;

            case 'update-holiday-success':
                $data['message'] = 'Holiday updated.';
                break;

            case 'delete-holiday-success':
                $data['message'] = 'Holiday deleted.';
                break;

            /*
             * Services
             */
            case 'create-stage-success':
                $data['message'] = 'New service added.';
                break;

            case 'update-stage-success':
                $data['message'] = 'Service updated.';
                break;

            case 'delete-stage-success':
                $data['message'] = 'Service deleted.';
                break;

            /*
             * Formats
             */
            case 'create-format-success':
                $data['message'] = 'New deliverable added.';
                break;

            case 'update-format-success':
                $data['message'] = 'Deliverable updated.';
                break;

            case 'delete-format-success':
                $data['message'] = 'Deliverable deleted.';
                break;

            /*
             * Clients
             */
            case 'create-client-success':
                $data['message'] = 'New client added.';
                break;

            case 'update-client-success':
                $data['message'] = 'Client updated.';
                break;

            case 'delete-client-success':
                $data['message'] = 'Client deleted.';
                break;

            /*
             * Task Type
             */
            case 'create-key-success':
                $data['message'] = 'New task type added.';
                break;

            case 'update-key-success':
                $data['message'] = 'Task type updated.';
                break;

            case 'delete-key-success':
                $data['message'] = 'Task type deleted.';
                break;

            /*
             * Device Token
             */
            case 'store-device-token-success':
                $data['message'] = 'Device token stored.';
                break;

            case 'delete-device-token-success':
                $data['message'] = 'Device token deleted.';
                break;


            /*
             * Users
             */
            case 'user-updated-success':
                $data['message'] = 'User details updated successfully.';
                break;

            /*
             * Keys
             */

            case 'key-name-exists':
                $data['status'] = 500;
                $data['message'] = 'Key name already exists.';
                break;

            case 'key-code-exists':
                $data['status'] = 500;
                $data['message'] = 'Key code already exists.';
                break;

            /*
             * ERRORS
             */

            case 'wrong-current-password':
                $data['status'] = 500;
                $data['message'] = 'Current password is incorrect.';
                break;

            case 'role-exists':
                $data['status'] = 500;
                $data['message'] = 'Role name already exists.';
                break;

            case 'key-exists':
                $data['status'] = 500;
                $data['message'] = 'Task Type already exists.';
                break;

            case 'role-not-exists':
                $data['status'] = 500;
                $data['message'] = 'Role does not exist anymore.';
                break;

            case 'user-not-exists':
                $data['status'] = 500;
                $data['message'] = 'User does not exist anymore.';
                break;

            case 'user-email-exists':
                $data['status'] = 500;
                $data['message'] = 'User with this email already exists.';
                break;

            case 'username-exists':
                $data['status'] = 500;
                $data['message'] = 'User with this username already exists.';
                break;

            case 'department-exists':
                $data['status'] = 500;
                $data['message'] = 'Department name already exists.';
                break;

            case 'department-not-exists':
                $data['status'] = 500;
                $data['message'] = 'Department does not exists.';
                break;

            case 'client-not-exists':
                $data['status'] = 500;
                $data['message'] = 'Client does not exists.';
                break;

            case 'format-exists':
                $data['status'] = 500;
                $data['message'] = 'Format name already exists.';
                break;

            case 'format-not-exists':
                $data['status'] = 500;
                $data['message'] = 'Format does not exists.';
                break;

            case 'stage-exists':
                $data['status'] = 500;
                $data['message'] = 'Project stage name already exists.';
                break;

            case 'client-exists':
                $data['status'] = 500;
                $data['message'] = 'Client name already exists.';
                break;

            case 'company-name-exists':
                $data['status'] = 500;
                $data['message'] = 'Company name already registered.';
                break;

            case 'password-too-short':
                $data['status'] = 500;
                $data['message'] = 'Password is too short. Please use at least 8 characters.';
                break;

            case 'stage-not-exist':
                $data['status'] = 500;
                $data['message'] = 'Project stage does not exist.';
                break;

            case 'key-not-exists':
                $data['status'] = 500;
                $data['message'] = 'Task Type does not exist.';
                break;

            case 'not-enough-time':
                $data['status'] = 500;
                $data['message'] = 'Time entered exceeds remaining time set for the task.';
                break;

            case 'work-hour-exceeded':
                $data['status'] = 500;
                $data['message'] = 'Time entered exceeds working hours of '.Auth()->user()->company->workHours.' hour(s).';
                break;

            case 'error':
                $data['status'] = 500;
                $data['message'] = 'There was an unexpected error. Please try again.';
                break;

            case 'user-limit-exeeded':
                $data['status'] = 500;
                $data['message'] = 'User limit exeeded. Please deactivate inactive users or upgrade your plan.';
                break;

            case 'no-time-assigned':
                $data['status'] = 500;
                $data['message'] = 'You cannot assign tasks without time.';
                break;

            case 'invalid-project-selected':
                $data['status'] = 500;
                $data['message'] = 'Please select a valid project for the task.';
                break;

            case 'no-key-selected':
                $data['status'] = 500;
                $data['message'] = 'Please select or add a project type for the task.';
                break;

            case 'create-no-time-assigned':
                $data['status'] = 500;
                $data['message'] = 'Please assign time for the task.';
                break;

            case 'white-spaces':
                $data['status'] = 500;
                $data['message'] = 'Please add a non-whitespace value.';
                break;

            case 'invalid-email':
                $data['status'] = 500;
                $data['message'] = 'Please enter a valid email address';
                break;

            case 'department-has-admins':
                $data['status'] = 500;
                $data['message'] = 'Departments with assigned administrators cannot be deleted.';
                break;

            case 'one-admin-required':
                $data['status'] = 500;
                $data['message'] = 'Atleast one user must be admin for the company.';
                break;

            case 'invalid-format-name':
                $data['status'] = 500;
                $data['message'] = 'Invalid format name.';
                break;

            case 'name-exists':
                $data['status'] = 500;
                $data['message'] = 'Name entered is already in the records. Please a different one.';
                break;

            case 'invalid-name':
                $data['status'] = 500;
                $data['message'] = 'Invalid name. Please enter a valid name.';
                break;

            case 'invalid-date-range':
                $data['status'] = 500;
                $data['message'] = 'Invalid date. Please enter a valid date range.';
                break;

            case 'cannot-deactivate-last-admin':
                $data['status'] = 500;
                $data['message'] = 'This user account cannot be deactivated until a new administrator has been added or appointed.';
                break;

            case 'cannot-delete-last-admin':
                $data['status'] = 500;
                $data['message'] = 'This user account cannot be deleted until a new administrator has been added or appointed.';
                break;

            case 'record-not-exists':
                $data['status'] = 500;
                $data['message'] = 'Record is either not existing or does not belong to you.';
                break;

            // MOBILE API

            case 'invalid-api-token':
                $data['status'] = 401;
                $data['message'] = 'Invalid token.';
                break;

            case 'existing-token':
                $data['status'] = 500;
                $data['message'] = 'Token already exists in database.';
                break;
        }

        return $data;
    }
}