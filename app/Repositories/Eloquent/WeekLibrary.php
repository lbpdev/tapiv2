<?php namespace App\Repositories\Eloquent;

use App\Models\Format;
use App\Models\Holiday;
use App\Models\Project;
use App\Models\Option;
use App\Models\Leave;
use App\Models\Task;
use App\Models\TaskComment;
use App\Models\UserTask;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

trait WeekLibrary {

    public function generateProjectList(){
        $projects = Project::with('client','brand','format')->where('completed_at',null)->orderBy('name','ASC')->fromCurrentCompany()->get();

        $data = [];
        foreach($projects as $index=>$project){
            $project->load('format');
            $data[$project->id]['label'] = $project->name;
            $data[$project->id]['value'] = $project->id;
            $data[$project->id]['brand'] =  $project->brand ? $project->brand->name : null;
            $data[$project->id]['client'] = $project->client ? $project->client->name : null;
            $data[$project->id]['version'] = $project->version;
            $data[$project->id]['language'] = $project->languagesString;

            $format = Format::where('id',$project->format_id)->first();
            $data[$project->id]['format'] = $format ? $format->name : 'N/A';
            $data[$project->id]['duration'] = ($project->end_at && $project->start_at) ? $project->start_at->format('m/d/Y') . ' - ' . $project->end_at->format('m/d/Y'): "N/A";
            $data[$project->id]['deadline'] = $project->end_at ? $project->end_at->format('m/d/Y'): "N/A";
            $data[$project->id]['color'] = $project->client ? ( $project->client->color ? $project->client->color : false ) : "N/A";
        }

        return json_encode($data);

    }

    private function getWorkDaysInWeeks(){

        $workDays = Option::fromCurrentCompany()->where('name','workdays')->first();

        if($workDays && $workDays->value)
            $workDays = json_decode($workDays->value);
        else
            $workDays = $this->getWorkDays($this->currentCompanyId());

        $year = isset($_GET['day']) ? Carbon::parse($_GET['day'])->format('Y') : Carbon::now()->setTimezone($this->currentCompany()->timezone)->format('Y');

        // if(!$workDays){
        $begin = new \DateTime( $year.'-01-01' );
        $end = new \DateTime( (intval($year)+1).'-01-01' );
        $end = $end->modify( '+1 day' );

        $interval = new \DateInterval('P1D');
        $daterange = new \DatePeriod($begin, $interval ,$end);

        $end = null;
        $start = null;
        $lastDate = null;
        $weekFlag = 0;
        $weekIndex = 0;
        $data = array();

        foreach($daterange as $date){
            if(in_array($date->format("D"),$workDays)){
                if($weekFlag>6){
                    $weekIndex++;
                    $weekFlag=0;
                }

                $data[$weekIndex][] = $date->format('d-m-Y');
                $weekFlag++;
            }
        }

        // Option::create(array('name'=>'work-days','value'=>json_encode($workDays)));
        return $data;
        // }

        // return json_decode($workDays->value);
    }

    private function getWorkDays($company_id){

        $workDays = Option::fromCompany($company_id)->where('name','workdays')->first();
        $firstWorkDay = Option::fromCompany($company_id)->where('name','first-day')->pluck('value')->first();

        $firstWorkDay = $firstWorkDay ? $firstWorkDay : 'Mon';

        if($workDays)
            $workDays = json_decode($workDays->value);
        else
            $workDays = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'];

        if(isset($_POST['date']))
            $year = Carbon::parse($_POST['date'])->format('Y');
        else
            $year = isset($_GET['day']) ? Carbon::parse($_GET['day'])->format('Y') : Carbon::now()->setTimezone($this->currentCompany()->timezone)->format('Y');

        // if(!$workDays){

        $begin = new \DateTime( $year.'-01-01' );

//        while($begin->format('D') != $firstWorkDay){
//            $begin->sub(new \DateInterval('P1D'));
//        }

        /** Jeffrey Update 3 */
//         $end = new \DateTime( (intval($year)+1).'-01-01' );
        $end = new \DateTime( (intval($year)+1).'-12-31' );

//        while($end->format('D') != $firstWorkDay){
//            $begin->add(new \DateInterval('P1D'));
//        }

        $end = $end->modify( '+1 day' );

        $interval = new \DateInterval('P1D');
        $daterange = new \DatePeriod($begin, $interval ,$end);

        $end = null;
        $start = null;
        $lastDate = null;
        $data = array();

        foreach($daterange as $date){
            if(in_array($date->format("D"),$workDays)){
                $data[] = $date->format('d-m-Y');
            }
        }

        // Option::create(array('name'=>'work-days','value'=>json_encode($workDays)));

        return $data;
        // }

        // return json_decode($workDays->value);
    }

    private function getWorkDaysInt(){

        $workDays = Option::fromCurrentCompany()->where('name','workdays')->first();
        $data = [];

        if($workDays){
            foreach (json_decode($workDays->value) as $index=>$day){
                switch ($day):
                    case "Sun":
                        $data[] = 0; break;
                    case "Mon":
                        $data[]= 1; break;
                    case "Tue":
                        $data[] = 2; break;
                    case "Wed":
                        $data[] = 3; break;
                    case "Thu":
                        $data[] = 4; break;
                    case "Fri":
                        $data[] = 5; break;
                    case "Sat":
                        $data[] = 6; break;
                endswitch;
            }
        } else {
            $data = [0,1,2,3,4,5,6];
        }

        return json_encode($data);
    }

    private function getCurrentWeek($currentDay, $workdays , $currentUser){
        $data= [];
        $userLeaves = $this->getUserLeaves($currentUser->id);

        $key = array_search($currentDay, $workdays);

        /** Jeffrey Update 3 */
        // If currentDay is found on workDays
        for($x=($key > 0 ? $key-1 : 0); $x<=($key+3); $x++){

            // If such workdays exists
            if (array_key_exists($x, $workdays)) {
                $d = strtotime($workdays[$x]);
                array_push($data , ['day'=>$workdays[$x],'daym'=>date("d", $d).'-'.date("m", $d), 'year'=> Carbon::parse($workdays[$x])->format('Y')]);
            }

        }

        foreach($data as $index=>$week){
            $data[$index]['tasks'] = UserTask::with('task.project.client','task.key')->has('task.project')->where('user_id',$currentUser->id)->where('date_assigned',Carbon::parse($week['day'])->format('Y-m-d'))->orderBy('order','ASC')->get();

            if(in_array(Carbon::parse($week['day'])->format('Y-m-d'),$userLeaves)){
                $data[$index]['on_leave'] = true;
                $data[$index]['leave_type'] = $this->checkLeaveType($currentUser->id,Carbon::parse($week['day'])->format('Y-m-d'));

                $partial_leave = $currentUser->leaves()->where('time','>',0)
                    ->whereDate('from','<=',$currentDay)
                    ->whereDate('to','>=',$currentDay)
                    ->orderBy('create_at','DESC')->first();

                if($partial_leave)
                    $data[$index]['on_leave_time'] = $partial_leave->time;

                if(Leave::where('from','>=',Carbon::parse($week['day'])->format('Y-m-d'))->
                       where('to','>=',Carbon::parse($week['day'])->format('Y-m-d'))->
                       where('holiday_id','!=',null)->
                       count())
                    $data[$index]['is_holiday'] = true;
            }
            else
                $data[$index]['on_leave'] = false;

            if(count($data[$index]['tasks'])){
                foreach($data[$index]['tasks'] as $i=>$task){
                    $data[$index]['tasks'][$i]->hours = intval($task->time/60);
                    $data[$index]['tasks'][$i]->minutes  = $task->time%60;
                }
            }
        }

        return $data;
    }


    private function getCurrentWeekFull($currentDay, $weeks , $currentUser){
        $data= [];
        $userLeaves = $this->getUserLeaves($currentUser->id);
        $workDays = Auth::user()->company->workDays;
        $currentWeek = [];
        $key = false;

        foreach ($weeks as $index=>$week){
            if($key)
                break;

            foreach ($week as $day){
                if($day==$currentDay){
                    $currentWeek = $weeks[$index];
                }
            }
        }

        foreach($currentWeek as $day){
            $d = Carbon::parse($day);

//            if(in_array($d->format('D'),$workDays))

            array_push($data , ['day'=>$day,'daym'=>$d->format('d-m'), 'year'=> $d->format('Y')]);
        }

        $annualHolidays = json_decode($this->currentCompany()->options()->where('name','annual-holidays')->pluck('value')->first());

        foreach($data as $index=>$week){
            $data[$index]['tasks'] = UserTask::with('task.project.client','task.key')->has('task.project')->where('user_id',$currentUser->id)->where('date_assigned',Carbon::parse($week['day'])->format('Y-m-d'))->orderBy('order','ASC')->get();

            if(in_array(Carbon::parse($week['day'])->format('Y-m-d'),$userLeaves)){
                $data[$index]['on_leave'] = true;
                $data[$index]['leave_type'] = $this->checkLeaveType($currentUser->id,Carbon::parse($week['day'])->format('Y-m-d'));

                $holidays = Holiday::
                where('from','<=',Carbon::parse($week['day'])->format('Y-m-d'))->
                where('to','>=',Carbon::parse($week['day'])->format('Y-m-d'))->
                where('company_id',$this->currentCompanyId())->
                get();

                if($holidays){
                    foreach ($holidays as $holiday){
                        if(Leave::where('holiday_id',$holiday->id)->where('user_id',$currentUser->id)->count()){
                            $data[$index]['is_holiday'] = true;
                            break;
                        }
                    }
                }
            }
            elseif($this->checkIfAnnualHoliday($annualHolidays,Carbon::parse($week['day'])->format('Y-m-d'))){
                $data[$index]['is_holiday'] = true;
                $data[$index]['on_leave'] = true;
            }
            else
                $data[$index]['on_leave'] = false;

            // GET LEAVE TIME IF PARTIAL
            if($data[$index]['on_leave']){
                $partial_leave = $currentUser->leaves()->where('time','>',0)
                    ->whereDate('from','<=',Carbon::parse($week['day'])->format('Y-m-d'))
                    ->whereDate('to','>=',Carbon::parse($week['day'])->format('Y-m-d'))
                    ->orderBy('created_at','DESC')->first();

                if($partial_leave)
                    $data[$index]['on_leave_time'] = $partial_leave->time;
            }

            if(count($data[$index]['tasks'])){
                foreach($data[$index]['tasks'] as $i=>$task){
                    $data[$index]['tasks'][$i]->hours = intval($task->time/60);
                    $data[$index]['tasks'][$i]->minutes  = $task->time%60;
                }
            }


            $data[$index]['comments'] = TaskComment::
                                            where('date',Carbon::parse($week['day'])->format('Y-m-d'))->
                                            where('user_id',$currentUser->id)->
                                            get();
        }

        return $data;
    }

    private function getUserLeaves($user_id)
    {
        $leaves = Leave::where('user_id',$user_id)->get();

        $dates = [];

        foreach ($leaves  as $key => $leave) {
            $begin = new \DateTime($leave->from);
            $end = new \DateTime($leave->to);

            $end = $end->modify( '+1 day' );

            $interval = new \DateInterval('P1D');
            $daterange = new \DatePeriod($begin, $interval ,$end);

            foreach($daterange as $date){
                $dates[] = $date->format("Y-m-d");
            }

        }

        return $dates;
    }

    private function getCurrentWeekTasks($currentWeek,$departments,$currentDepartment){
        $tasks = [];

//               $uncompleted = Task::with('department','project.client','key')
//            ->whereHas('project',function($query){
//                $query->where('end_at','>=',Carbon::now()->format('Y-m-d'));
//            })
//            ->where('department_id',$currentDepartment->id)
//            ->where('date','<',Carbon::parse($currentWeek[0]['day'])->format('Y-m-d'))
//            ->where('time','>',0)
//            ->fromCurrentCompany()
//            ->get()->toArray();
//
//        foreach ($uncompleted as $u){
//            $u['hours'] = intval($u['time']/60);
//            $u['minutes'] = $u['time']%60;
//            array_push($tasks,$u);
//        }
//
//        $generalTasks = Task::with('department','project.client','key')
//            ->whereHas('project',function($query){
//                $query->where('end_at','>=',Carbon::now()->format('Y-m-d'));
//            })
//            ->whereNull('department_id')
//            ->where('date','<',Carbon::parse($currentWeek[count($currentWeek)-1]['day'])->format('Y-m-d'))
//            ->where('time','>',0)
//            ->fromCurrentCompany()
//            ->get()->toArray();
//
//        foreach ($generalTasks as $u){
//            $u['hours'] = intval($u['time']/60);
//            $u['minutes'] = $u['time']%60;
//            array_push($tasks,$u);
//        }

//        foreach($currentWeek as $index=>$week){
            $task = Task::with('department','project.client','key')
                ->whereHas('project', function($query){
                    $query->where('completed_at',null);
//                    $query->where('end_at','>=',Carbon::now()->format('Y-m-d'));
                })
                ->where('department_id',$currentDepartment->id)
                ->where('time','>',0)
                ->fromCurrentCompany()
//                ->where('date',Carbon::parse($week['day'])->format('Y-m-d'))
                ->get()->toArray();

            if($task){
                foreach ($task as $index=>$t){
                    if(!in_array($t, $tasks, true)){
                        $t['hours'] = intval($t['time']/60);
                        $t['minutes'] = $t['time']%60;
                        array_push($tasks,$t);
                    }
                }
            }
//        }

        return $tasks;
    }

    function getWeeks()
    {
        $data = "";

        $year = Carbon::now()->setTimezone($this->currentCompany()->timezone);

        if(isset($_GET['year'])){
            $checkYear = date_parse($_GET['year']);

            if($checkYear['error_count']==0){
                $year = Carbon::parse($_GET['year']);
            }
        } elseif(isset($_GET['day'])) {
            $year = Carbon::parse($_GET['day']);
        }

        $begin = new \DateTime( $year->format('Y').'-01-01' );
        $end = new \DateTime( $year->addYear()->format('Y').'-01-01' );
        $end = $end->modify( '+1 day' );

        $interval = new \DateInterval('P1D');
        $daterange = new \DatePeriod($begin, $interval ,$end);

        $end = null;
        $start = null;
        $lastDate = null;
        $workDays = array();

        foreach($daterange as $date)
            array_push($workDays, $date);

        $data = [];
        $startAdd = false;
        $week = 0;
        $daysCount = 0;

        foreach($workDays as $index=>$day){

            if(isset($workDays[$index+1])){
                $first_day =  Auth::user()->company->firstDay;

                if($day->format('D')==$first_day){
                    $daysCount = 0;
                    $week++;
                    $startAdd = true;
                }

                if($startAdd){

                    if($daysCount<7){
                        $data[$week][] = $day->format('d-m-Y');
                        $daysCount++;
                    }
                    else {
                        $week++;
                        $daysCount=1;

                        $data[$week][] = $day->format('d-m-Y');
                    }

                } else {
                    $data[$week][] = $day->format('d-m-Y');
                }
            }
        }

        if(isset($data[0])){
            while(count($data[0])<7){
                $date = $data[0];

                $date = Carbon::parse($data[0][0])->subDay(1)->format('d-m-Y');
                array_unshift($data[0],$date);
            }
        }
        
        if(isset($data[count($data)-1])){
            $lastDate = $data[count($data)-1][0];

            while(count($data[count($data)-1])<7){
                $lastDate = Carbon::parse($lastDate)->addDay(1)->format('d-m-Y');
                array_push($data[count($data)-1],$lastDate);
            }
        }

        return $data;
    }

    public function checkLeaveType($user_id,$date){

        $leave= Leave::where('user_id',$user_id)->whereDate('from','<=',Carbon::parse($date)->format('Y-m-d'))->whereDate('to','>=',Carbon::parse($date)->format('Y-m-d'))->first();

        if($leave)
            return $leave->type;

        return null;
    }


    public function checkIfAnnualHoliday($annualHolidays,$date){
        $date = Carbon::parse($date)->format('m-d');

        if($annualHolidays){
            foreach ($annualHolidays as $holiday){
                $flag = Carbon::parse($holiday)->format('m-d');

                if($flag==$date)
                    return true;
            }
        }

        return false;
    }
}