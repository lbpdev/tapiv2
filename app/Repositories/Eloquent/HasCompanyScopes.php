<?php namespace App\Repositories\Eloquent;

use Illuminate\Support\Facades\Auth;

trait HasCompanyScopes {

    public function scopeFromCompany($query,$company_id)
    {
        return $query->where('company_id', $company_id);
    }

    public function scopeFromCurrentCompany($query)
    {
        return $query->where('company_id', Auth::user()->company_id);
    }
}