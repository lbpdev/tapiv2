<?php

namespace App\Repositories\Eloquent;

use App\Models\MobileApiToken;
use App\Models\User;
use Illuminate\Support\Str;

class MobileApiTokenLibrary {

    use CanCreateResponseCode;

    public function __construct(MobileApiToken $model)
    {
        $this->model = $model;
    }

    public function generateToken(User $user,$uuid){
        $unique = false;

        while(!$unique){
            $tempToken = Str::random(32);

            if($this->checkDuplicate($tempToken))
                $unique = true;
        }

        $token = $user->tokens()->where('uuid',$uuid)->first();

        if($token){
            $token->token = $tempToken;
            $token->save();
        }
        else
        {
            $token = $user->tokens()->create([
                'token' => $tempToken,
                'uuid' => $uuid
            ]);
        }

        return $token;
    }

    public function checkDuplicate($token){

        if($this->model->where('token',$token)->count())
            return false;

        return true;
    }

    public function getToken($request){
        $client = $request->input();

        $token = $this->model->where('uuid',$client['uuid'])->where('token',$client['token'])->first();

        return $token;
    }
}