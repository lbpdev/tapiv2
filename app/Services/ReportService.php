<?php namespace App\Services;

use App\Models\Color;
use App\Models\Company;
use App\Models\Department;
use App\Models\Format;
use App\Models\Project;
use App\Models\Subscription;
use App\Models\User;
use App\Models\UserTask;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

trait ReportService {

    public function productivity($returnJson = null,$company_id = null,$range = null, $department = null,$user = null){

        if(!$company_id)
            $company_id = Auth::user()->company->id;

        // 100% , 80% , 60% , 40% , -0%

        $percent100 = 0;
        $percent80 = 0;
        $percent60 = 0;
        $percent40 = 0;
        $percent0 = 0;

        if(!$range)
            $range = isset($_GET['range']) ? $_GET['range'] : 'today';

        $dataRange = $this->getRange($range,$company_id);

        if(!$department)
            $department = isset($_GET['department_id']) ? $_GET['department_id'] : 0;

        if(!$user)
            $user = isset($_GET['user_id']) ? $_GET['user_id'] : 0;

        if($user > 1){

            $allTasks = UserTask::where('user_id',$user)->whereDate('date_assigned','>=',$dataRange['start'])->whereDate('date_assigned','<=',$dataRange['end'])->groupBy('date_assigned')->lists('date_assigned');

            foreach ($allTasks as $taskDate){

                $totalTasks = UserTask::where('user_id',$user)->whereDate('date_assigned','=',$taskDate)->count();
                $completedTasks = UserTask::where('user_id',$user)->whereDate('date_assigned','=',$taskDate)->whereNotNull('date_completed')->count();

                if($totalTasks > 0){
                    $rate = $completedTasks > 0 ? $completedTasks/$totalTasks : 0;

                    if($rate <= .40)
                        $percent0++;

                    elseif($rate >= .41 && $rate <= .60)
                        $percent40++;

                    elseif($rate >= .61 && $rate <= .80)
                        $percent60++;

                    elseif($rate >= .81 && $rate <= .99)
                        $percent80++;

                    elseif($rate >= 1)
                        $percent100++;
                }
            }

            $results = [
                $percent100 ,
                $percent80 ,
                $percent60 ,
                $percent40 ,
                $percent0
            ];

            $results = $this->checkEmpty($results);

            if(!$returnJson)
                return response()->json($results);
            else
                return $results;
        }

        $departmentsQuery = Department::query();

        if($department>0)
            $departments = $departmentsQuery->with('users')->where('id',$department)->get();
        else
            $departments = $departmentsQuery->where('company_id',$company_id)->with('users')->get();

        foreach ($departments as $department){
            foreach ($department->users as $user){

                $allTasks = UserTask::where('user_id',$user->id)->whereDate('date_assigned','>=',$dataRange['start'])->whereDate('date_assigned','<=',$dataRange['end'])->groupBy('date_assigned')->lists('date_assigned');

                foreach ($allTasks as $taskDate){
                    $rate = 0;
                    $totalTasks = UserTask::where('user_id',$user->id)->whereDate('date_assigned','=',$taskDate)->count();
                    $completedTasks = UserTask::where('user_id',$user->id)->whereDate('date_assigned','=',$taskDate)->whereNotNull('date_completed')->count();

                    if($totalTasks > 0){
                        $rate = $completedTasks > 0 ? $completedTasks/$totalTasks : 0;

                        if($rate <= .40)
                            $percent0++;

                        elseif($rate >= .41 && $rate <= .60)
                            $percent40++;

                        elseif($rate >= .61 && $rate <= .80)
                            $percent60++;

                        elseif($rate >= .81 && $rate <= .99)
                            $percent80++;
                        else
                            $percent100++;
                    }
                }

            }

        }

        $results = [
            $percent100 ,
            $percent80 ,
            $percent60 ,
            $percent40 ,
            $percent0
        ];

        $results = $this->checkEmpty($results);

        if(!$returnJson)
            return response()->json($results);
        else
            return $results;
    }

    /*
            ========================================================================
            ============================ DEADLINE ==================================
            ========================================================================
     */

    public function deadline($returnJson = null,$company_id = null,$range = null){

        if(!$company_id)
            $company_id = Auth::user()->company->id;
        // Months & Values

        $data['labels'] = [];
        $data['values'] = [];

        $allowedRanges = [
            'this-quarter',
            'last-quarter',
            'next-quarter',
            'this-year',
        ];

        if(!$range)
            $range = isset($_GET['range']) ? (in_array($_GET['range'],$allowedRanges) ? $_GET['range'] : 'this-quarter') : 'this-quarter';

        $dataRange = $this->getRange($range,$company_id);

        $department = isset($_GET['department_id']) ? $_GET['department_id'] : 0;
        $user = isset($_GET['user_id']) ? $_GET['user_id'] : 0;

        $start    = (new \DateTime($dataRange['start']))->modify('first day of this month');
        $end      = (new \DateTime($dataRange['end']))->modify('first day of next month');
        $interval = \DateInterval::createFromDateString('1 month');
        $period   = new \DatePeriod($start, $interval, $end);
        $index = 0;

        foreach ($period as $dt) {
            $data['labels'][] = $dt->format("M");
            $date = Carbon::parse($dt->format("Y-m-d"));
            $months[$index]['start'] = $date->startOfMonth()->format("Y-m-d");
            $months[$index]['end'] = $date->endOfMonth()->format("Y-m-d");
            $index++;

            $data['values'][] = 0;
        }

//        if($user > 1){
//
//            foreach($months as $index=>$month){
//                $data['values'][$index] = 0;
//
//                $allProjects = Project::where('company_id',$company_id)->whereHas('tasks',function($taskQuery) use($user){
//                    $taskQuery->whereHas('utasks',function($uTaskQuery)use($user){
//                        $uTaskQuery->where('user_id',$user);
//                    });
//                })->whereDate('end_at','>=',$month['start'])->whereDate('end_at','<=',$month['end'])->count();
//
//                $data['values'][$index] = $allProjects;
//            }
//
//            return response()->json($data);
//        }
//
//        $departmentsQuery = Department::query();
//
//        if($department>0)
//            $departments = $departmentsQuery->with('users')->where('id',$department)->get();
//        else
//            $departments = $departmentsQuery->where('company_id',$company_id)->with('users')->get();
//
//        foreach ($departments as $department){
//            foreach ($department->users as $user){
//                foreach($months as $index=>$month){
//                    $allProjects = Project::where('company_id',$company_id)->whereDate('end_at','>=',$month['start'])->whereDate('end_at','<=',$month['end'])->count();
//                    return response()->json($allProjects);
//
//                    $data['values'][$index] += $allProjects;
//                }
//            }
//        }

        foreach($months as $index=>$month){
            $allProjects = Project::where('company_id',$company_id)->whereDate('end_at','>=',$month['start'])->whereDate('end_at','<=',$month['end'])->count();
            $data['values'][$index] += $allProjects;
        }

        if(!$returnJson)
            return response()->json($data);
        else
            return $data;
    }

    public function booked($returnJson = null,$company_id = null,$range = null, $department = null,$user = null){

        if(!$company_id)
            $company_id = Auth::user()->company->id;

        // 100% , 80% , 60% , 40% , -0%

        $booked = 0;
        $maxBook = 0;
        $unbooked = 0;

        if(!$range)
            $range = isset($_GET['range']) ? $_GET['range'] : 'today';

        $dataRange = $this->getRange($range,$company_id);

        if(!$department)
            $department = isset($_GET['department_id']) ? $_GET['department_id'] : 0;

        if(!$user)
            $user = isset($_GET['user_id']) ? $_GET['user_id'] : 0;

        $company = Company::where('id',$company_id)->first();
        $workDays = $company->workDays;

        $start    = (new \DateTime($dataRange['start']));
        $end      = (new \DateTime($dataRange['end']));
        $interval = \DateInterval::createFromDateString('1 day');
        $period   = new \DatePeriod($start, $interval, $end);
        $index = 0;
        $userWorkDays = [];

        if($user > 1){
            $user = User::find($user);

            $userLeaves = $user->wholeLeaves;
            $userPartialLeaves = $user->partialLeaves;

            foreach ($period as $dt) {
                if(!in_array($dt->format('Y-m-d'),$userLeaves) && in_array($dt->format('D'),$workDays))
                    $userWorkDays[] = $dt->format('Y-m-d');
            }

            if(!$userWorkDays)
                $userWorkDays[] = $start->format('Y-m-d');

            $userMaxHours = $user->company->workHours;

            $hours = 0;
            foreach ($userWorkDays as $day){
                $tasks = UserTask::where('user_id',$user->id)->whereDate('date_assigned','=',$day)->get();

                foreach ($tasks as $task)
                    $booked += $task->time;


                if(in_array($day,$userPartialLeaves)){
                    $leave = Leave::whereDate('from','<=',$day)->whereDate('to','>=',$day)->first();

                    if($leave)
                        $maxBook += ( ($userMaxHours * 60)  - $leave->time);
                } else {
                    $maxBook += ( $userMaxHours * 60 );
                }
            }

            $data[] = $booked / 60;
            $data[] = ( $maxBook - $booked ) / 60;

            if(!$returnJson)
                return response()->json($data);
            else
                return $data;
        }

        $departmentsQuery = Department::query();

        if($department>0)
            $departments = $departmentsQuery->with('users')->where('id',$department)->get();
        else
            $departments = $departmentsQuery->where('company_id',$company_id)->with('users')->get();

        $data[0] = 0;
        $data[1] = 0;

        foreach ($departments as $department){
            foreach ($department->users as $user){

                $userWorkDays = [];

                $userLeaves = $user->wholeLeaves;
                $userPartialLeaves = $user->partialLeaves;

                foreach ($period as $dt) {
                    if(!in_array($dt->format('Y-m-d'),$userLeaves) && in_array($dt->format('D'),$workDays))
                        $userWorkDays[] = $dt->format('Y-m-d');
                }

                if(!$userWorkDays)
                    $userWorkDays[] = $start->format('Y-m-d');

                $userMaxHours = $user->company->workHours;

                $hours = 0;
                foreach ($userWorkDays as $day){
                    $tasks = UserTask::where('user_id',$user->id)->whereDate('date_assigned','=',$day)->get();

                    foreach ($tasks as $task)
                        $booked += $task->time;

                    $maxBook += ( $userMaxHours * 60 );
                }

                $data[0] += $booked / 60;
                $data[1] += ( $maxBook - $booked ) / 60;

            }
        }

        if(!$returnJson)
            return response()->json($data);
        else
            return $data;
    }

    /*
            ========================================================================
            ============================ DELIVERABLES ==============================
            ========================================================================
     */

    public function deliverables($returnJson = null,$company_id = null,$range = null, $department = null,$user = null){

        if(!$company_id)
            $company_id = Auth::user()->company->id;
        
        // Months & Values

        $data['labels'] = [];
        $data['values'] = [];

        if(!$range)
            $range = isset($_GET['range']) ? $_GET['range'] : 'today';

        $dataRange = $this->getRange($range,$company_id);

        if(!$department)
            $department = isset($_GET['department_id']) ? $_GET['department_id'] : 0;

        if(!$user)
            $user = isset($_GET['user_id']) ? $_GET['user_id'] : 0;

        $deliverables = Format::where('company_id',$company_id)->get();

        foreach ($deliverables as $deliverable){
            $data['labels'][] = $deliverable->name;
            $data['values'][] = 0;
        }

        if($user > 1){

            foreach($deliverables as $index=>$deliverable){

                $allProjects = Project::where('company_id',$company_id)
                    ->where(function($query) use ($dataRange,$user,$deliverable){
                        $query->whereHas('tasks',function($taskQuery) use($user,$dataRange){
                            $taskQuery->whereHas('utasks',function($uTaskQuery)use($user,$dataRange){
                                $uTaskQuery->where('user_id',$user)->whereDate('date_assigned','>=',$dataRange['start'])->whereDate('date_assigned','<=',$dataRange['end']);
                            });
                        })->where('format_id',$deliverable->id);
                    })->count();

                $data['values'][$index] = $allProjects;

            }

            if(!$returnJson)
                return response()->json($data);
            else
                return $data;
        }

        $departmentsQuery = Department::query();

        if($department>0)
            $departments = $departmentsQuery->with('users')->where('id',$department)->get();
        else
            $departments = $departmentsQuery->where('company_id',$company_id)->with('users')->get();

        foreach ($departments as $department){
            foreach ($department->users as $user){
                foreach($deliverables as $index=>$deliverable){


                    $allProjects = Project::where('company_id',$company_id)
                        ->where(function($query) use ($dataRange,$user,$deliverable){
                            $query->whereHas('tasks',function($taskQuery) use($user,$dataRange){
                                $taskQuery->whereHas('utasks',function($uTaskQuery)use($user,$dataRange){
                                    $uTaskQuery->where('user_id',$user->id)->whereDate('date_assigned','>=',$dataRange['start'])->whereDate('date_assigned','<=',$dataRange['end']);
                                });
                            })->where('format_id',$deliverable->id);
                        })->count();

                    $data['values'][$index] += $allProjects;
                }
            }
        }

        if(!$returnJson)
            return response()->json($data);
        else
            return $data;
    }

    public function getRange($range,$company_id = null){

        $data = [];


        if($company_id){
            $company = Company::find($company_id);
            $companyTimezone = $company->timezone;
        } else {
            $companyTimezone =  Auth::user()->company->timezone;
        }

        $rangeStart = Carbon::now()->setTimezone($companyTimezone);
        $rangeEnd = Carbon::now()->setTimezone($companyTimezone);

        if($range=='today'){
            $data['start'] = $rangeStart->format('Y-m-d');
            $data['end']= $rangeEnd->format('Y-m-d');
        }
        elseif($range=='tomorrow'){
            $data['start']= $rangeStart->addDay()->format('Y-m-d');
            $data['end']= $rangeEnd->addDay()->format('Y-m-d');
        }
        elseif($range=='yesterday'){
            $data['start']= $rangeStart->subDay()->format('Y-m-d');
            $data['end']= $rangeEnd->subDay()->format('Y-m-d');
        }
        elseif($range=='this-week'){
            $data['start']= $rangeStart->startOfWeek()->format('Y-m-d');
            $data['end']= $rangeEnd->endOfWeek()->format('Y-m-d');
        }
        elseif($range=='next-week'){
            $data['start']= $rangeStart->addWeek()->startOfWeek()->format('Y-m-d');
            $data['end']= $rangeEnd->addWeek()->endOfWeek()->format('Y-m-d');
        }
        elseif($range=='last-week'){
            $data['start']= $rangeStart->subWeek()->startOfWeek()->format('Y-m-d');
            $data['end']= $rangeEnd->subWeek()->endOfWeek()->format('Y-m-d');
        }
        elseif($range=='this-month'){
            $data['start']= $rangeStart->startOfMonth()->format('Y-m-d');
            $data['end']= $rangeEnd->endOfMonth()->format('Y-m-d');
        }
        elseif($range=='next-month'){
            $data['start']= $rangeStart->startOfMonth()->addMonth()->format('Y-m-d');
            $data['end']= $rangeEnd->addMonth()->endOfMonth()->format('Y-m-d');
        }
        elseif($range=='last-month'){
            $data['start']= $rangeStart->subMonth()->startOfMonth()->format('Y-m-d');
            $data['end']= $rangeEnd->subMonth()->endOfMonth()->format('Y-m-d');
        }
        elseif($range=='this-quarter'){
            $data['start']= $rangeStart->startOfQuarter()->format('Y-m-d');
            $data['end']= $rangeEnd->endOfQuarter()->format('Y-m-d');
        }
        elseif($range=='last-quarter'){
            $data['start']= $rangeStart->startOfQuarter()->subQuarter()->format('Y-m-d');
            $data['end']= $rangeEnd->subQuarter()->endOfQuarter()->format('Y-m-d');
        }
        elseif($range=='next-quarter'){
            $data['start']= $rangeStart->startOfQuarter()->addQuarter()->format('Y-m-d');
            $data['end']= $rangeEnd->addQuarter()->endOfQuarter()->format('Y-m-d');
        }
        elseif($range=='this-year'){
            $data['start']= $rangeStart->startOfYear()->format('Y-m-d');
            $data['end']= $rangeEnd->endOfYear()->format('Y-m-d');
        }

        return $data;
    }


    public function checkEmpty($data){

        $notEmptyFlag = 0;

        for($x=0;$x<count($data);$x++){
            if($data[$x]){
                $notEmptyFlag = 1;
                break;
            }
        }

        if(!$notEmptyFlag)
            return [0,0,0,0,1];

        return $data;
    }


    public function printView($company_id){

        $range = null;
        $department = 0;
        $user = null;

        $company = Company::find($company_id);
        $userRanges = [];
        $userRanges[0] = 'Whole company';

        $departments = Department::where('company_id',$company_id)->lists('name','id');
        $users = User::where('company_id',$company_id)->lists('name','id');

        foreach ($departments as $id=>$department)
            $userRanges['department['.$id.']'] = $department;

        foreach ($users as $id=>$user)
            $userRanges['user['.$id.']'] = $user;

        $deliverables = Format::where('company_id',$company_id)->orderBy('name','ASC')->lists('name');

        $colors = Color::lists('value');

        $range = isset($_GET['prodDateRange']) ? $_GET['prodDateRange'] : null;
        $department = isset($_GET['prodDepRange']) ? $_GET['prodDepRange'] : null;
        $user = isset($_GET['prodUserRange']) ? $_GET['prodUserRange'] : null;

        $productivityData = count($company->getOption('productivity-report')) ? json_encode($this->productivity(true,$company_id,$range,$department,$user)) : null;

        $range = isset($_GET['deadDateRange']) ? $_GET['deadDateRange'] : null;

        if(count($company->getOption('deadline-report'))){
            $deadline = $this->deadline(true,$company_id,$range);
            $deadlineData = json_encode($deadline['values']);
            $deadlineLabels = json_encode($deadline['labels']);
        }

        if(count($company->getOption('time-report'))){
            $range = isset($_GET['timeDateRange']) ? $_GET['timeDateRange'] : null;
            $department = isset($_GET['timeDepRange']) ? $_GET['timeDepRange'] : null;
            $user = isset($_GET['timeUserRange']) ? $_GET['timeUserRange'] : null;

            $timeData = json_encode($this->booked(true,$company_id,$range,$department,$user));
        }

        if(count($company->getOption('deliverable-report'))){
            $range = isset($_GET['delDateRange']) ? $_GET['delDateRange'] : null;
            $department = isset($_GET['delDepRange']) ? $_GET['delDepRange'] : null;
            $user = isset($_GET['delUserRange']) ? $_GET['delUserRange'] : null;

            $deliverable = $this->deliverables(true,$company_id,$range,$department,$user);
            $deliverableData = json_encode($deliverable['values']);
            $deliverableLabels = json_encode($deliverable['labels']);
        }

        $userRanges = [];
        $userRanges[0] = 'Whole company';

        $departments = Department::fromCurrentCompany()->lists('name','id');
        $users = User::fromCurrentCompany()->lists('name','id');

        foreach ($departments as $id=>$department)
            $userRanges['department['.$id.']'] = $department;

        foreach ($users as $id=>$user)
            $userRanges['user['.$id.']'] = $user;

//        return view('pages.reports.print', compact('userRanges','projects','userRanges','deliverables','colors','productivityData','deadlineData','deadlineLabels','timeData','timeLabels','deliverableData','deliverableLabels'));

        $pdf = App::make('snappy.pdf.wrapper');
        $pdf->loadView('pages.reports.print', compact('projects','userRanges','deliverables','colors','productivityData','deadlineData','deadlineLabels','timeData','timeLabels','deliverableData','deliverableLabels'))->setPaper('a4')->setOrientation('portrait')->setOption('margin-bottom', 0);

        return $pdf->inline();
    }
}