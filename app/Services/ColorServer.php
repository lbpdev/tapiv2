<?php namespace App\Services;

use App\Models\Color;

trait ColorServer {

    public function getColors(){
        $colors = Color::get();
        $data = [];

        foreach ($colors as $color)
            $data[] = $color->value;

        return json_encode($data);

    }
}