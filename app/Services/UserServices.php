<?php namespace App\Services;

use App\Models\Color;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserServices {

    public function getAll(){
        $data = User::with('department','role','tasks')->where('company_id',Auth::user()->company->id)->get();
        return $data;
    }
}