<?php


/**
 * Mobile API Routes
 */


/**
 *
 *  VERSION 1
 *
 */

Route::group(['prefix' => 'mobile-api/v1'], function(){

    Route::post('/login',['as'=>'mobile.login','uses'=>'Mobile\AuthController@login']);

    Route::get('/reset-email',['as'=>'mobile.reset-email','uses'=>'Mobile\AuthController@resetEmail']);
    Route::post('/reset-email',['as'=>'mobile.reset-email','uses'=>'Mobile\AuthController@resetEmail']);

    Route::get('/reset-password',['as'=>'mobile.reset-password','uses'=>'Mobile\AuthController@resetPassword']);
    Route::post('/reset-password',['as'=>'mobile.reset-password','uses'=>'Mobile\AuthController@resetPassword']);

    /**
     * Tasks
     */
    Route::group(['prefix' => 'tasks'], function(){

        Route::post('/store',['as' => 'mobile.usertasks.store' , 'uses' =>'Mobile\UserTaskController@store']);
//        Route::get('/get-user-week',['as' => 'mobile.usertasks.get.week' , 'uses' =>'Mobile\UserTaskController@getByUserWeek']);
        Route::post('/get-user-week',['as' => 'mobile.usertasks.get.week' , 'uses' =>'Mobile\UserTaskController@getByUserWeek']);
        Route::post('/get-user-day',['as' => 'mobile.usertasks.get.day' , 'uses' =>'Mobile\UserTaskController@getByUserDay']);
        Route::get('/get-user-day',['as' => 'mobile.usertasks.get.day' , 'uses' =>'Mobile\UserTaskController@getByUserDay']);
//        Route::get('/toggle-status',['as' => 'mobile.usertasks.status.toggle' , 'uses' =>'Mobile\UserTaskController@toggleStatus']);
//        Route::post('/toggle-status',['as' => 'mobile.usertasks.status.toggle' , 'uses' =>'Mobile\UserTaskController@toggleStatus']);

        Route::post('/complete',['as' => 'mobile.utasks.complete' , 'uses' =>'Mobile\UserTaskController@complete']);
        Route::post('/incomplete',['as' => 'mobile.utasks.incomplete' , 'uses' =>'Mobile\UserTaskController@incomplete']);
    });

    /**
     * Calendar
     */
    Route::group(['prefix' => 'calendar'], function(){

        Route::post('/get-work-days',['as' => 'mobile.calendar.get.days' , 'uses' =>'Mobile\CalendarController@getWorkDays']);
        Route::get('/get-work-days',['as' => 'mobile.calendar.get.days' , 'uses' =>'Mobile\CalendarController@getWorkDays']);
    });

    /**
     * Projects
     */
    Route::group(['prefix' => 'projects'], function(){

        Route::get('/get-all',['as' => 'mobile.projects.get.all' , 'uses' =>'Mobile\ProjectController@getAll']);
        Route::post('/get-all',['as' => 'mobile.projects.get.all' , 'uses' =>'Mobile\ProjectController@getAll']);
        Route::get('/get',['as' => 'mobile.projects.get' , 'uses' =>'Mobile\ProjectController@get']);
    });

    /**
     * Department Keys
     */
    Route::group(['prefix' => 'keys'], function(){

        Route::get('/get-all',['as' => 'mobile.keys.get.all' , 'uses' =>'Mobile\KeyController@getAll']);
        Route::post('/get-all',['as' => 'mobile.keys.get.all' , 'uses' =>'Mobile\KeyController@getAll']);
    });
});

/**
 *
 *  VERSION 2
 *
 */

Route::group(['prefix' => 'mobile-api/v2'], function(){

    Route::post('/login',['as'=>'mobile.login','uses'=>'Mobile\AuthController@login']);
    Route::post('/logout',['as'=>'mobile.logout','uses'=>'Mobile\AuthController@logout']);
    Route::get('/reset-email',['as'=>'mobile.reset-email','uses'=>'Mobile\AuthController@resetEmail']);
    Route::post('/reset-email',['as'=>'mobile.reset-email','uses'=>'Mobile\AuthController@resetEmail']);

    Route::get('/reset-password',['as'=>'mobile.reset-password','uses'=>'Mobile\AuthController@resetPassword']);
    Route::post('/reset-password',['as'=>'mobile.reset-password','uses'=>'Mobile\AuthController@resetPassword']);

    Route::group(['middleware'=>'auth.api'], function(){

        Route::post('/refresh-token',['as'=>'mobile.tokens.refresh','uses'=>'Mobile\TokenController@get']);

        /**
         * Tasks
         */
        Route::group(['prefix' => 'tasks'], function(){

            Route::post('/store',['as' => 'mobile.usertasks.store' , 'uses' =>'Mobile\UserTaskController@store']);
//        Route::get('/get-user-week',['as' => 'mobile.usertasks.get.week' , 'uses' =>'Mobile\UserTaskController@getByUserWeek']);
            Route::post('/get-user-week',['as' => 'mobile.usertasks.get.week' , 'uses' =>'Mobile\UserTaskController@getByUserWeek']);
            Route::post('/get-user-day',['as' => 'mobile.usertasks.get.day' , 'uses' =>'Mobile\UserTaskController@getByUserDay']);
            Route::get('/get-user-day',['as' => 'mobile.usertasks.get.day' , 'uses' =>'Mobile\UserTaskController@getByUserDay']);

            Route::post('/complete',['as' => 'mobile.utasks.complete' , 'uses' =>'Mobile\UserTaskController@complete']);
            Route::post('/incomplete',['as' => 'mobile.utasks.incomplete' , 'uses' =>'Mobile\UserTaskController@incomplete']);

            Route::post('/get-user-task',['as' => 'mobile.utasks.get' , 'uses' =>'Mobile\UserTaskController@get']);
        });

        /**
         * Calendar
         */
        Route::group(['prefix' => 'calendar'], function(){

            Route::post('/get-work-days',['as' => 'mobile.calendar.get.days' , 'uses' =>'Mobile\CalendarController@getWorkDays']);
            Route::get('/get-work-days',['as' => 'mobile.calendar.get.days' , 'uses' =>'Mobile\CalendarController@getWorkDays']);
        });

        /**
         * Projects
         */
        Route::group(['prefix' => 'projects'], function(){

            Route::get('/get-all',['as' => 'mobile.projects.get.all' , 'uses' =>'Mobile\ProjectController@getAll']);
            Route::post('/get-all',['as' => 'mobile.projects.get.all' , 'uses' =>'Mobile\ProjectController@getAll']);
            Route::get('/get',['as' => 'mobile.projects.get' , 'uses' =>'Mobile\ProjectController@get']);
        });

        /**
         * Department Keys
         */
        Route::group(['prefix' => 'keys'], function(){

            Route::get('/get-all',['as' => 'mobile.keys.get.all' , 'uses' =>'Mobile\KeyController@getAll']);
            Route::post('/get-all',['as' => 'mobile.keys.get.all' , 'uses' =>'Mobile\KeyController@getAll']);
        });

        /**
         * Comments
         */
        Route::group(['prefix' => 'comments'], function(){
            Route::post('/get',['as' => 'mobile.comments.get' , 'uses' =>'Mobile\CommentController@get']);
            Route::post('/store',['as' => 'mobile.comments.store' , 'uses' =>'Mobile\CommentController@store']);
        });


    });

    /**
     * Users
     */
    Route::group(['prefix' => 'users'], function(){
        Route::post('/get',['as' => 'mobile.users.get' , 'uses' =>'Mobile\UserController@get']);
        Route::post('/update/name',['as' => 'mobile.users.name.update' , 'uses' =>'Mobile\UserController@updateName']);
        Route::post('/update/password',['as' => 'mobile.users.password.update' , 'uses' =>'Mobile\UserController@updatePw']);
        Route::post('/update-password',['as' => 'mobile.users.password.update-password' , 'uses' =>'Mobile\PasswordController@updatePw']);
    });

    /**
     * Comments
     */
    Route::group(['prefix' => 'device-tokens'], function(){
        Route::post('/destroy',['as' => 'mobile.device-tokens.destroy' , 'uses' =>'Mobile\DeviceTokenController@destroy']);
        Route::post('/store',['as' => 'mobile.device-tokens.store' , 'uses' =>'Mobile\DeviceTokenController@store']);
    });


});