<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/',['middleware' => 'guest','uses'=>'PageController@login']);

Route::get('/logout','Auth\AuthController@logout');

Route::get('/login',['as'=>'login','middleware' => 'guest','uses'=>'PageController@login']);
Route::get('/login/{token}','Auth\AuthController@inviteLogin');

//Route::post('/login','Auth\AuthController@login');

Route::get('/pdf-test','Admin\SettingsController@samplePDF');

/**
 * User Register
 */
Route::post('register',['as' => 'users.register' , 'uses' =>'UsersController@store']);


/**
 * Settings Routes
 */

Route::group(['prefix' => 'settings','middleware' => ['forceHTTPS','auth','adminOnly','logcheck']], function(){
    Route::get('/',['as' => 'settings.info' , 'uses' =>'Admin\SettingsController@index']);
    Route::post('/store',['as' => 'settings.info.store' , 'uses' =>'Admin\SettingsController@store']);
});


/**
 * Reports Routes
 */

Route::group(['prefix' => 'reports','middleware' => ['auth','adminOnly','logcheck']], function(){
    Route::get('/',['as' => 'reports.index' , 'uses' =>'Admin\ReportController@index']);
    Route::get('/print/{company_id}',['as' => 'reports.print' , 'uses' =>'ReportsController@printView']);
    Route::post('/store',['as' => 'reports.store' , 'uses' =>'Admin\ReportController@store']);
    Route::get('/test/{company_id}',['as' => 'api.reports.testReport' , 'uses' =>'Admin\ReportController@generateReport']);

    Route::group(['prefix' => 'users'], function(){
        Route::get('/single/{user_id}',['as' => 'reports.users.single' , 'uses' =>'Admin\ReportController@user']);
    });

    Route::group(['prefix' => 'projects'], function(){
        Route::get('/single/{project_id}',['as' => 'reports.projects.single' , 'uses' =>'Admin\ReportController@project']);
    });
});

/**
 * Calendar Routes
 */

Route::group(['prefix' => 'calendar','middleware' => ['forceHTTPS','auth','noEmployees','logcheck']], function(){
    Route::get('/year',['as' => 'calendar.year' , 'uses' =>'Admin\CalendarController@index']);
//    Route::get('/year-print',['as' => 'calendar.year.print' , 'uses' =>'PrintController@generateYearPrint']);
    Route::post('/year-print',['as' => 'calendar.year.print' , 'uses' =>'PrintController@generateYearPrint']);
    Route::post('/year/search',['as' => 'calendar.year.search' , 'uses' =>'Admin\CalendarController@search']);
    Route::get('/week',['as' => 'calendar.week' , 'uses' =>'Admin\WeekViewController@index']);
});

/**
 * Calendar Routes
 */

Route::group(['prefix' => 'employee','middleware' => 'auth','logcheck'], function(){
    Route::group(['prefix' => 'calendar'], function(){
        Route::get('/week',['as' => 'user.calendar.week' , 'uses' =>'User\WeekViewController@index']);
    });
});


/**
 * User Tasks
 */
Route::group(['prefix' => 'utasks','middleware' => 'auth','logcheck'], function(){
    Route::get('delete',['as' => 'utasks.delete' , 'uses' =>'UserTasksController@delete']);
});

/**
 * Tasks
 */
Route::group(['prefix' => 'tasks','middleware' => 'auth','logcheck'], function(){
    Route::get('delete',['as' => 'tasks.delete' , 'uses' =>'TasksController@delete']);
});

/**
 * API Routes
 */

Route::group(['prefix' => 'api'], function(){

    /**
     * Roles
     */
    Route::group(['prefix' => 'roles'], function(){
        Route::post('/store',['as' => 'api.roles.store' , 'uses' =>'RolesController@store']);
        Route::post('/delete',['as' => 'api.roles.delete' , 'uses' =>'RolesController@delete']);
    });

    /**
     * Departments
     */
    Route::group(['prefix' => 'reports'], function(){
        Route::get('/productivity',['as' => 'api.reports.productivity' , 'uses' =>'ReportsController@productivity']);
        Route::get('/deadline',['as' => 'api.reports.deadline' , 'uses' =>'ReportsController@deadline']);
        Route::get('/booked',['as' => 'api.reports.booked' , 'uses' =>'ReportsController@booked']);
        Route::get('/deliverables',['as' => 'api.reports.deliverables' , 'uses' =>'ReportsController@deliverables']);
    });


    /**
     * Departments
     */
    Route::group(['prefix' => 'departments'], function(){
        Route::get('/get',['as' => 'api.departments.get' , 'uses' =>'DepartmentsController@get']);
        Route::post('/store',['as' => 'api.departments.store' , 'uses' =>'DepartmentsController@store']);
        Route::get('/get-keys',['as' => 'api.departments.getkeys' , 'uses' =>'DepartmentsController@getKeys']);
        Route::get('/delete',['as' => 'api.departments.delete' , 'uses' =>'DepartmentsController@delete']);
        Route::post('/update',['as' => 'api.departments.update' , 'uses' =>'DepartmentsController@update']);
    });

    /**
     * Users
     */
    Route::group(['prefix' => 'users'], function(){
        Route::get('/get',['as' => 'api.users.get' , 'uses' =>'UsersController@get']);
        Route::post('/invite',['as' => 'api.users.invite' , 'uses' =>'UsersController@invite']);
        Route::get('/toggleStatus',['as' => 'api.users.toggleStatus' , 'uses' =>'UsersController@toggleStatus']);
        Route::post('/update',['as' => 'api.users.update' , 'uses' =>'UsersController@update']);
        Route::get('/delete',['as' => 'api.users.delete' , 'uses' =>'UsersController@delete']);
        Route::get('/validateEmail',['as' => 'api.users.validateEmail' , 'uses' =>'UsersController@validateEmail']);
        Route::get('/validateRegisterCompany',['as' => 'api.users.validateRegisterCompany' , 'uses' =>'UsersController@validateRegisterCompany']);
        Route::get('/generate-csrf',['uses' =>'Customers\CompanyController@generateCSRF']);
    });

    Route::group(['prefix' => 'company'], function(){
        Route::post('/register', ['uses' => 'Customers\CompanyController@register']);
    });

    /**
     * Stage
     */
    Route::group(['prefix' => 'stages'], function(){
        Route::get('/get',['as' => 'api.stages.get' , 'uses' =>'StagesController@get']);
        Route::post('/store',['as' => 'api.stages.store' , 'uses' =>'StagesController@store']);
        Route::post('/delete',['as' => 'api.stages.delete' , 'uses' =>'StagesController@delete']);
        Route::post('/update',['as' => 'api.stages.update' , 'uses' =>'StagesController@update']);
        Route::post('/update-order',['as' => 'api.stages.order.update' , 'uses' =>'StagesController@updateOrder']);
    });


    /**
     * Formats
     */
    Route::group(['prefix' => 'formats'], function(){
        Route::get('/get',['as' => 'api.formats.get' , 'uses' =>'FormatsController@get']);
        Route::get('/getForSelect',['as' => 'api.formats.getForSelect' , 'uses' =>'FormatsController@getForSelect']);
        Route::post('/store',['as' => 'api.formats.store' , 'uses' =>'FormatsController@store']);
        Route::POST('/delete',['as' => 'api.formats.delete' , 'uses' =>'FormatsController@delete']);
        Route::post('/update',['as' => 'api.formats.update' , 'uses' =>'FormatsController@update']);
    });

    /**
     * Clients
     */
    Route::group(['prefix' => 'clients'], function(){
        Route::get('/get-json',['as' => 'api.clients.getJson' , 'uses' =>'ClientsController@getAvailableClientsJson']);
        Route::get('/get',['as' => 'api.clients.get' , 'uses' =>'ClientsController@get']);
        Route::post('/store',['as' => 'api.clients.store' , 'uses' =>'ClientsController@store']);
        Route::get('/delete',['as' => 'api.clients.delete' , 'uses' =>'ClientsController@delete']);
        Route::post('/update',['as' => 'api.clients.update' , 'uses' =>'ClientsController@update']);
    });


    /**
     * Projects
     */
    Route::group(['prefix' => 'projects'], function(){
        Route::get('/get-json',['as' => 'api.projects.getJson' , 'uses' =>'Admin\WeekViewController@getAvailableProjectsJson']);
        Route::get('/get',['as' => 'api.projects.get' , 'uses' =>'ProjectsController@get']);
        Route::get('/get-details',['as' => 'api.projects.getDetails' , 'uses' =>'ProjectsController@getDetails']);
        Route::post('/store',['as' => 'api.projects.store' , 'uses' =>'ProjectsController@store']);
        Route::post('/store-stage',['as' => 'api.projects.stages.store' , 'uses' =>'ProjectsController@storeStages']);
        Route::post('/update-stage',['as' => 'api.projects.stages.update' , 'uses' =>'ProjectsController@updateStages']);
        Route::post('/update-stage-calendar',['as' => 'api.projects.stages-calendar.update' , 'uses' =>'ProjectsController@updateStagesCalendar']);
        Route::post('/delete',['as' => 'api.projects.delete' , 'uses' =>'ProjectsController@delete']);
        Route::get('/complete',['as' => 'api.projects.complete' , 'uses' =>'ProjectsController@complete']);
        Route::post('/update',['as' => 'api.projects.update' , 'uses' =>'ProjectsController@update']);
        Route::get('/generate-excel',['as' => 'api.projects.excel' , 'uses' =>'ProjectsController@generateExcel']);
        Route::get('/generate-pdf',['as' => 'api.projects.pdf' , 'uses' =>'Admin\CalendarController@generatePdf']);
    });

    /**
     * Keys
     */
    Route::group(['prefix' => 'keys'], function(){
        Route::get('/get',['as' => 'api.keys.get' , 'uses' =>'KeysController@get']);
        Route::post('/store',['as' => 'api.keys.store' , 'uses' =>'KeysController@store']);
        Route::post('/delete',['as' => 'api.keys.delete' , 'uses' =>'KeysController@delete']);
        Route::post('/update',['as' => 'api.keys.update' , 'uses' =>'KeysController@update']);
    });

    /**
     * Tasks
     */
    Route::group(['prefix' => 'tasks'], function(){
        Route::get('/get',['as' => 'api.tasks.get' , 'uses' =>'TasksController@get']);
        Route::get('/get-item',['as' => 'api.tasks.getItem' , 'uses' =>'TasksController@getTaskItem']);
        Route::post('/store',['as' => 'api.tasks.store' , 'uses' =>'TasksController@store']);
        Route::post('/store/user',['as' => 'api.user.tasks.store' , 'uses' =>'User\TasksController@store']);
        Route::post('/assign',['as' => 'api.tasks.assign' , 'uses' =>'TasksController@assign']);
        Route::get('/delete',['as' => 'api.tasks.delete' , 'uses' =>'TasksController@delete']);
        Route::post('/update',['as' => 'api.tasks.update' , 'uses' =>'TasksController@update']);

        Route::get('/get-utask',['as' => 'api.utasks.get' , 'uses' =>'UserTasksController@get']);
        Route::get('/view-utask',['as' => 'api.utasks.view' , 'uses' =>'Mobile\UserTaskController@view']);
        Route::post('/update-utask',['as' => 'api.utasks.update' , 'uses' =>'UserTasksController@update']);
        Route::get('/complete',['as' => 'api.utasks.complete' , 'uses' =>'UserTasksController@complete']);
        Route::get('/incomplete',['as' => 'api.utasks.incomplete' , 'uses' =>'UserTasksController@incomplete']);
        Route::get('/delete-utask',['as' => 'api.utasks.delete' , 'uses' =>'UserTasksController@delete']);
        Route::get('/reorder',['as' => 'api.utasks.updateOrder' , 'uses' =>'UserTasksController@updateOrder']);
    });

    /**
     * C
     */
    Route::group(['prefix' => 'comments'], function(){
        Route::get('/get',['as' => 'api.comments.get' , 'uses' =>'CommentController@get']);
        Route::post('/store',['as' => 'api.comments.store' , 'uses' =>'CommentController@store']);
    });


    /**
     * Holidays
     */
    Route::group(['prefix' => 'holidays'], function(){
        Route::get('/get',['as' => 'api.holidays.get' , 'uses' =>'HolidayController@get']);
        Route::post('/store',['as' => 'api.holidays.store' , 'uses' =>'HolidayController@store']);
        Route::post('/update',['as' => 'api.holidays.update' , 'uses' =>'HolidayController@update']);
        Route::post('/delete',['as' => 'api.holidays.delete' , 'uses' =>'HolidayController@delete']);
    });


    /**
     * Leaves
     */
    Route::group(['prefix' => 'leaves'], function(){
        Route::post('/store',['as' => 'api.leaves.store' , 'uses' =>'LeavesController@store']);
        Route::get('/delete',['as' => 'api.leaves.delete' , 'uses' =>'LeavesController@delete']);
        Route::get('/get',['as' => 'api.leaves.get' , 'uses' =>'LeavesController@get']);
    });

    /**
     * Company
     */
    Route::group(['prefix' => 'company'], function(){
        Route::get('/get-user-limit',['as' => 'api.company.getUserLimit' , 'uses' =>'Customers\CompanyController@getUserLimit']);
        Route::get('/toggle-autorenew/{SubscriptionReference}',['as' => 'api.company.toggleAutoRenew' , 'uses' =>'Customers\CompanyController@toggleAutoRenew']);
    });


    /**
     * Colors
     */
    Route::group(['prefix' => 'colors'], function(){
        Route::get('/get',['as' => 'api.colors.get' , 'uses' =>'Admin\CalendarController@getColorPalette']);
    });


    /**
     * Options
     */
    Route::group(['prefix' => 'options'], function(){
        Route::get('/get-workdays',['as' => 'api.options.workdays.get' , 'uses' =>'Admin\WeekViewController@getWorkDaysJson']);
    });

});


/**
 * Customer Routes
 */

Route::group(['prefix' => 'register'], function() {

    Route::get('/', ['as' => 'company.index', 'uses' => 'Customers\CompanyController@index']);
    Route::post('/register', ['as' => 'company.register', 'uses' => 'Customers\CompanyController@register']);
//    Route::post('/delete', ['as' => 'api.roles.delete', 'uses' => 'RolesController@delete']);
});



/**
 * Customer Routes
 */

Route::group(['prefix' => 'cp','middleware' => ['siteAdminsOnly']], function() {

    Route::get('/', ['as' => 'backend.index', 'uses' => 'Backend\AdminController@index']);
    Route::get('/login',['as'=>'backend.login','uses'=>'Backend\AdminController@login','middleware' => 'guest']);
    Route::get('/logout',['as'=>'backend.logout','uses'=>'Auth\AuthController@logout']);
    Route::post('/login-post',['as'=>'backend.login.validate','uses'=>'Auth\AuthController@login']);


    Route::group(['prefix' => 'companies'], function() {
        Route::get('/',                     ['as'=>'backend.companies.index','uses'=>'Backend\CompanyController@index']);
        Route::get('/create',               ['as'=>'backend.companies.create','uses'=>'Backend\CompanyController@create']);
        Route::post('/store',               ['as'=>'backend.companies.store','uses'=>'Backend\CompanyController@store']);
        Route::get('/edit/{id}',            ['as'=>'backend.companies.edit','uses'=>'Backend\CompanyController@edit']);
        Route::post('/update',              ['as'=>'backend.companies.update','uses'=>'Backend\CompanyController@update']);
        Route::get('/delete/{id}',          ['as'=>'backend.companies.delete','uses'=>'Backend\CompanyController@delete']);
    });

    Route::group(['prefix' => 'users'], function() {
        Route::get('/',                     ['as'=>'backend.users.index','uses'=>'Backend\UsersController@index']);
        Route::get('/create',               ['as'=>'backend.users.create','uses'=>'Backend\UsersController@create']);
        Route::post('/store',               ['as'=>'backend.users.store','uses'=>'Backend\UsersController@store']);
        Route::get('/edit/{id}',            ['as'=>'backend.users.edit','uses'=>'Backend\UsersController@edit']);
        Route::post('/update',              ['as'=>'backend.users.update','uses'=>'Backend\UsersController@update']);
        Route::get('/delete/{id}',          ['as'=>'backend.users.delete','uses'=>'Backend\UsersController@delete']);
    });

    Route::group(['prefix' => 'plans'], function() {
        Route::get('/',                     ['as'=>'backend.plans.index','uses'=>'Backend\PlanController@index']);
        Route::get('/create',               ['as'=>'backend.plans.create','uses'=>'Backend\PlanController@create']);
        Route::post('/store',               ['as'=>'backend.plans.store','uses'=>'Backend\PlanController@store']);
        Route::get('/edit/{id}',            ['as'=>'backend.plans.edit','uses'=>'Backend\PlanController@edit']);
        Route::post('/update',              ['as'=>'backend.plans.update','uses'=>'Backend\PlanController@update']);
        Route::get('/delete/{id}',          ['as'=>'backend.plans.delete','uses'=>'Backend\PlanController@delete']);
    });

    Route::group(['prefix' => 'subscriptions'], function() {
        Route::get('/',                     ['as'=>'backend.subscriptions.index','uses'=>'Backend\SubscriptionController@index']);
        Route::get('/create',               ['as'=>'backend.subscriptions.create','uses'=>'Backend\SubscriptionController@create']);
        Route::post('/store',               ['as'=>'backend.subscriptions.store','uses'=>'Backend\SubscriptionController@store']);
        Route::get('/edit/{id}',            ['as'=>'backend.subscriptions.edit','uses'=>'Backend\SubscriptionController@edit']);
        Route::post('/update',              ['as'=>'backend.subscriptions.update','uses'=>'Backend\SubscriptionController@update']);
        Route::get('/delete/{id}',          ['as'=>'backend.subscriptions.delete','uses'=>'Backend\SubscriptionController@delete']);
    });

//    Route::post('/delete', ['as' => 'api.roles.delete', 'uses' => 'RolesController@delete']);
});


Route::group(['prefix' => 'notifications'], function() {
//    Route::get('/ipn', ['as' => 'payments.ipn.get', 'uses' => 'Admin\PaymentController@post']);
    Route::get('/test', ['as' => 'notifications.test', 'uses' => 'PushNotificationController@test']);
    Route::post('/settings/store', ['as' => 'notifications.settings.store', 'uses' => 'PushNotificationController@storeSettings']);
    Route::post('/reports/store', ['as' => 'notifications.reports.store', 'uses' => 'Admin\SettingsController@storeReports']);
    Route::get('/report-test', ['as' => 'notifications.reports.test', 'uses' => 'PushNotificationController@handle']);
    Route::get('/check-roles', ['as' => 'notifications.reports.roles', 'uses' => 'PushNotificationController@checkRoles']);
});



/**
 * Customer Routes
 */

Route::group(['prefix' => 'payments'], function() {
//    Route::get('/ipn', ['as' => 'payments.ipn.get', 'uses' => 'Admin\PaymentController@post']);
    Route::post('/ipn', ['as' => 'payments.ipn.post', 'uses' => 'Admin\PaymentController@post']);
    Route::get('/ipn', ['as' => 'payments.ipn.index', 'uses' => 'Admin\PaymentController@get']);
});


Route::group(['prefix' => 'subscriptions'], function() {
    Route::get('/get/{reference}', ['as' => 'subscriptions.get', 'uses' => 'SubscriptionsController@get']);
    Route::get('/get', ['as' => 'subscriptions.get.ajax', 'uses' => 'SubscriptionsController@get']);
    Route::get('/enable/{reference}', ['as' => 'subscriptions.enable', 'uses' => 'SubscriptionsController@enable']);
    Route::get('/disable/{reference}', ['as' => 'subscriptions.disable', 'uses' => 'SubscriptionsController@disable']);
    Route::get('/enable-renew/{reference}', ['as' => 'subscriptions.enable.renew', 'uses' => 'SubscriptionsController@enableAutoRenew']);
    Route::get('/disable-renew/{reference}', ['as' => 'subscriptions.disable.renew', 'uses' => 'SubscriptionsController@disableAutoRenew']);
    Route::get('/enable-notifications/{reference}', ['as' => 'subscriptions.enable.notifications', 'uses' => 'SubscriptionsController@enableRenewNotifications']);
    Route::get('/disable-notifications/{reference}', ['as' => 'subscriptions.disable.notifications', 'uses' => 'SubscriptionsController@disableRenewNotifications']);
});

Route::get('/test-email',function(){ return view('emails.invitation');   });
Route::get('/test-invite',function(){ return view('emails.invite');   });
Route::get('/test-pass-reset',function(){ return view('emails.password-reset');   });
Route::get('/test-tasks-today',function(){ return view('emails.notifications.today-tasks'); });
Route::get('/test-reports-email',function(){ return view('emails.notifications.reports'); });
Route::get('/test-today-summary',function(){ return view('emails.notifications.tasks-report'); });
Route::get('/test-comment-email',function(){ return view('emails.notifications.comment-made'); });
Route::get('/test-tasks-done',function(){ return view('emails.notifications.todays-tasks-completed'); });
Route::get('/superstar',function(){ return view('emails.notifications.superstar'); });
Route::post('/store-inquiry','InquiryController@store');
Route::get('/test-report/{id}','Test\SendReportsTester@report');

Route::get('/oops',function(){
    if(isset($_GET['error'])){
        $error = $_GET['error'];
        return view('error',compact('error'));
    }
});

Route::get('test-push',function(){

    \Davibennun\LaravelPushNotification\Facades\PushNotification::app('appNameIOS')
        ->to('21037a792b9df3578ed8f6fb3edc69239aca77fc316af423e12070c5e60d59c4')
        ->send('BOOM');

});