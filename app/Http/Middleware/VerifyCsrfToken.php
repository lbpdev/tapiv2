<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'mobile-api/v1/*',
        'mobile-api/v2/*',
        'api/stages/update-order',
        'api/company/*',
        'payments/ipn',
        'store-inquiry',
//        'mobile-api/v1/tasks/store',
//        'mobile-api/v1/tasks/get-user-day',
//        'mobile-api/v1/tasks/get-user-week',
//        'mobile-api/v1/reset-email',
//        'mobile-api/v1/reset-password',
//        'mobile-api/v1/calendar/get-work-days'
    ];
}
