<?php

namespace App\Http\Middleware;

use App\Models\MobileApiToken;
use App\Repositories\Eloquent\CanCreateResponseCode;
use Closure;
use Illuminate\Support\Facades\Auth;

class AuthenticateApiRequest
{
    use CanCreateResponseCode;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $response = $next($request);

        $clientData = $request->input();

        if ($clientData) {

            $data = MobileApiToken::where('uuid',$clientData['uuid'])->first();

            if($data)
                if($data->token == $clientData['token'])
                    return $next($request);

        }

        return response($this->generateResponse('invalid-api-token'));
    }
}
