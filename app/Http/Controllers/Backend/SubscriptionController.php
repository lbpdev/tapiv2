<?php

namespace App\Http\Controllers\Backend;

use App\Models\Subscription;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SubscriptionController extends Controller
{

    public function __construct(Subscription $model){
        $this->model = $model;
    }

    public function index(){
        $data = $this->model->get();

        return view('backend.subscriptions.index', compact('data'));
    }

    public function create(){
        return view('backend.subscriptions.create');
    }

    public function edit($company_id){

        $userIcons = $this->getFileNames($this->filesystem->files($this->userIconDirectory));

        $company = Company::find($company_id);
        $options = $this->option->fromCompany($company_id)->lists('value','name');
        $options['name'] = $company->name;

        $roles = Role::get();
        $formats = Format::fromCompany($company_id)->orderBy('name','ASC')->get();
        $roleList = Role::orderBy('name','ASC')->lists('name','id');
        $departments = Department::fromCompany($company_id)->with('users')->orderBy('name','ASC')->get();
        $departmentList = Department::fromCompany($company_id)->orderBy('name','ASC')->lists('name','id');
        $stages = Stage::with('department')->fromCompany($company_id)->orderBy('order','ASC')->get();
        $colors = $this->getColors();
        $timezones = $this->getTimezones();
        $clients = Client::fromCompany($company_id)->orderBy('name','ASC')->get();
        $keys = Key::fromCompany($company_id)->orderBy('name','ASC')->get();

        $admins = User::whereHas('role', function($query){
            $query->where('role_id',1);
        })->get();

        $holidays = Holiday::where('company_id',$company_id)->orderBy('from')->get();

        return view('backend.subscriptions.view',compact('company','holidays','keys','options','userIcons','roles','roleList','departments','departmentList','stages','formats','timezones','colors','clients','data'));
    }

    public function store(Request $request){
        $article = $this->articles->store($request);

        if($article){
            Session::flash('success','Saved Successfully');
            return redirect(route('backend.subscriptions.edit',$article->id));
        }


        Session::flash('error','There was an error. Please try again.');
        return redirect()->back();
    }

    public function update(Request $request){
        $this->articles->update($request);

        Session::flash('success','Updated Successfully');
        return redirect()->back();
    }

    public function delete($article_id){

        if(Auth::user()->email == 'mars@leadingbrands.me' || Auth::user()->email == 'gene@leadingbrands.me' || Auth::user()->email == 'yesa@leadingbrands.me'){
            $company = $this->model->where('id',$article_id)->first();

            $subscriptions = $company->subscriptions()->get();

            if(count($subscriptions)){
                foreach ($subscriptions as $subscription)
                    $this->disableAutoRenew($subscription);
            }

    //        $this->model->where('id',$article_id)->delete();

            Session::flash('success','Deleted Successfully');
        }
        return redirect()->back();
    }

    private function getFileNames($files){
        $data = [];

        foreach($files as $key=>$file){
            $fileName = str_replace($this->userIconDirectory.'/',"",$file);
            $data[$fileName] = preg_replace('/\\.[^.\\s]{3,4}$/', '', $fileName);
        }

        return $data;
    }
}
