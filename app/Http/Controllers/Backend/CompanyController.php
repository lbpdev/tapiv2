<?php

namespace App\Http\Controllers\Backend;

use App\Models\Company;
use App\Models\Department;
use App\Models\Format;
use App\Models\Holiday;
use App\Models\Key;
use App\Models\Option;
use App\Models\Role;
use App\Models\Stage;
use App\Models\Subscription;
use App\Models\User;
use App\Services\Avengate\src\SoapClient;
use App\Services\ColorServer;
use App\Services\TimezoneService;
use App\Models\Client;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class CompanyController extends Controller
{

    use ColorServer;
    use TimezoneService;

    public function __construct(Company $model,Option $option, Filesystem $filesystem){
        $this->model = $model;
        $this->option = $option;
        $this->filesystem = $filesystem;
        $this->userIconDirectory = 'public/images/icons/user-icons';
    }


    public function index(){
        $data = $this->model->get();

        return view('backend.companies.index', compact('data'));
    }

    public function create(){
        return view('backend.companies.create');
    }

    public function edit($company_id){

        $userIcons = $this->getFileNames($this->filesystem->files($this->userIconDirectory));

        $company = Company::find($company_id);
        $options = $this->option->fromCompany($company_id)->lists('value','name');
        $options['name'] = $company->name;

        $roles = Role::get();
        $formats = Format::fromCompany($company_id)->orderBy('name','ASC')->get();
        $roleList = Role::orderBy('name','ASC')->lists('name','id');
        $departments = Department::fromCompany($company_id)->with('users')->orderBy('name','ASC')->get();
        $departmentList = Department::fromCompany($company_id)->orderBy('name','ASC')->lists('name','id');
        $stages = Stage::with('department')->fromCompany($company_id)->orderBy('order','ASC')->get();
        $colors = $this->getColors();
        $timezones = $this->getTimezones();
        $clients = Client::fromCompany($company_id)->orderBy('name','ASC')->get();
        $keys = Key::fromCompany($company_id)->orderBy('name','ASC')->get();

        $admins = User::whereHas('role', function($query){
            $query->where('role_id',1);
        })->get();

        $holidays = Holiday::where('company_id',$company_id)->orderBy('from')->get();

        $unassigned_users = User::whereDoesntHave('department')->where('company_id',Auth::user()->company->id)->get();

        $subscription = $company->subscription;

        return view('backend.companies.view',compact('subscription','unassigned_users','company','holidays','keys','options','userIcons','roles','roleList','departments','departmentList','stages','formats','timezones','colors','clients','data'));
    }

    public function store(Request $request){
        $article = $this->articles->store($request);

        if($article){
            Session::flash('success','Saved Successfully');
            return redirect(route('backend.companies.edit',$article->id));
        }
        
        Session::flash('error','There was an error. Please try again.');
        return redirect()->back();
    }

    public function update(Request $request){
        $this->articles->update($request);

        Session::flash('success','Updated Successfully');
        return redirect()->back();
    }

    public function delete($id){

        $company = $this->model->where('id',$id)->first();

//        if(count($subscriptions)){
//            foreach ($subscriptions as $subscription)
//                $this->disableAutoRenew($subscription);
//        }

        if(Auth::user()->email == 'mars@leadingbrands.me' || Auth::user()->email == 'gene@leadingbrands.me' || Auth::user()->email == 'yesa@leadingbrands.me'){
            if($company){
                $company->subscriptions()->delete();
                $company->users()->delete();

                $company->delete();
            }
            Session::flash('success','Deleted Successfully');
            return redirect()->back();
        }

        return redirect()->back();
    }

    private function getFileNames($files){
        $data = [];

        foreach($files as $key=>$file){
            $fileName = str_replace($this->userIconDirectory.'/',"",$file);
            $data[$fileName] = preg_replace('/\\.[^.\\s]{3,4}$/', '', $fileName);
        }

        return $data;
    }


    public function disableAutoRenew($subscription){

        SoapClient::setBaseUrl('https://api.avangate.com/soap/3.0/');
        SoapClient::setCredentials('LEADINGB', '@dJvyoEH3O1]w&|Aqk4!');

        try {
            Log::info('Current Status: '.$subscription->RecurringEnabled);

            try {
                Log::info('Disabling Recurring : '.$subscription->id);
                $response = SoapClient::disableRecurringBilling($subscription->SubscriptionReference);

                if($response)
                    Log::info('Recurring Payments disabled: '.$subscription->id);

                $subscription->RecurringEnabled = 0;

            } catch (\Exception $e) {

            }

            $subscription->save();

            Log::info('Database Updated RecurringEnabled: '.$subscription->RecurringEnabled);

        } catch (ClientException $e) {
        }
        return redirect()->back();
    }
}
