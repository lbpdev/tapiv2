<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Leave;
use App\Models\Task;
use App\Models\TaskChanges;
use App\Models\User;
use App\Models\UserTask;
use App\Repositories\Eloquent\CanCreateResponseCode;
use Carbon\Carbon;
use Davibennun\LaravelPushNotification\Facades\PushNotification;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

use App\Services\PushNotificationService;

class UserTasksController extends Controller
{
    use CanCreateResponseCode;

    public function __construct(UserTask $userTask, PushNotificationService $push){
        $this->model = $userTask;
        $this->push = $push;
    }


    public function get(){

        $data = [];

        if(strip_tags($_GET['id'])){
            $userTask = UserTask::with('task.project.client','task.project.brand','task.department','task.key')->where('id',$_GET['id'])->first();

            if($userTask){
                $userTask->toArray();
                $userTask['hours'] = $userTask['time'] / 60;
                $userTask['minutes'] = $userTask['time'] % 60;

                return response()->json($userTask);
            }
        }

        return response()->json(null);
    }


    public function update(Request $request)
    {
        $input = $request->input();
        $input['is_urgent'] = isset($input['is_urgent']) ? 1 : 0 ;


        $timeRequired = (intval($input['hours'])*60) + intval($input['minutes']);
        if($timeRequired<1)
            return response()->json($this->generateResponse('no-time-assigned'));

        $department = Department::where('id',$input['department_id'])->first();
        $workHours = $department->work_hours ? $department->work_hours : $this->currentCompany()->workHours;

        if($timeRequired > ($workHours)*60)
            return response()->json($this->generateResponseWithParameter('work-hour-exceeded-dept',$workHours));

        if($request->input('id')){
            $userTask = UserTask::with('task')->where('id',$request->input('id'))->first();

            if($userTask){
                $task = Task::where('id',$userTask->task->id)->first();

                $timeRequired = (intval($input['hours'])*60) + intval($input['minutes']);
                $timeRemaining = $task->time + $userTask->time;

                $timeRequired = (intval($input['hours'])*60) + intval($input['minutes']);

                $workHours = $department->work_hours ? $department->work_hours : $this->currentCompany()->workHours;

                $partial_leave = Leave::where('user_id',$userTask->user_id)->where('time','>',0)
                    ->whereDate('from','<=',$userTask->date_assigned)
                    ->whereDate('to','>=',$userTask->date_assigned)
                    ->orderBy('created_at','DESC')->first();

                $timeRequiredWithLeave = $timeRequired;

                if($partial_leave){
                    $partial_leave->time;
                    $timeRequiredWithLeave += $partial_leave->time;
                }

                if($timeRequiredWithLeave > ($workHours)*60)
                    return response()->json($this->generateResponseWithParameter('work-hour-exceeded-dept',$workHours));

                if($timeRequired > $timeRemaining)
                    return response()->json($this->generateResponse('not-enough-time'));

                $task->time += $userTask->time;
                $task->save();

                $input['time'] = $timeRequired;

                $newTask = $userTask->update($input);

                if($newTask){
                    $task->time -= $timeRequired;
                    $task->save();

                    if($userTask->date_assigned->format('Y-m-d') == Carbon::now()->setTimezone(Auth::user()->company->timezone)->format('Y-m-d')){
                        $user = User::where('id',$userTask->user_id)->first();

                        if($user){
                            if($user->id != Auth::user()->id){
                                TaskChanges::create([
                                    'user_id' => $user->id,
                                    'type'    => 'update'
                                ]);

//                                $message = PushNotification::Message('Hello. One of your tasks today has been updated.',array(
//                                    'custom' => array('intent' => 'task-updated')
//                                ));
//
//                                $this->push->send($user,$message);
                            }
                        }
                    }

                    return response()->json($this->generateResponseWithData('update-success',$userTask));

                }

            }
        }


        return response()->json($this->generateResponse('error'));
    }
    public function delete(){

        if(strip_tags($_GET['id'])){
            $userTask = UserTask::with('task')->where('id',$_GET['id'])->first();
            $task = $userTask;

            $userTask->task->time += $userTask->time;
            $userTask->task->save();

            if($userTask->delete()){

                if($task->date_assigned->format('Y-m-d') == Carbon::now()->setTimezone(Auth::user()->company->timezone)->format('Y-m-d')){
                    $user = User::where('id',$task->user_id)->first();

                    if($user){

                        if($user->id != Auth::user()->id){

                            TaskChanges::create([
                                'user_id' => $user->id,
                                'type'    => 'delete'
                            ]);

//                            $message = PushNotification::Message('Hello. One of your tasks today has been deleted.',array(
//                                'custom' => array('intent' => 'task-deleted')
//                            ));
//
//                            $this->push->send($user,$message);
                        }
                    }
                }

                return response()->json($userTask->task->id);
            }
        }

        return response()->json(false);
    }

    public function complete(){
        $utask = $this->model->where('id',$_GET['id'])->first();

        if($utask){
            $utask->date_completed = Carbon::now();

            return response()->json($utask->save() ? 1 : 3);
        }

        return response()->json(3);
    }

    public function incomplete(){
        $utask = $this->model->where('id',$_GET['id'])->first();

        if($utask){
            $utask->date_completed = null;
            return response()->json($utask->save() ? 0 : 3);
        }

        return response()->json(3);
    }

    public function updateOrder(){
        $utask = UserTask::find($_GET['id']);

        if($utask){

            $tasks = UserTask::where('id','!=',$_GET['id'])->where('user_id',$utask->user_id)->where('date_assigned',$utask->date_assigned)->orderBy('order','ASC')->get();
            $appended = false;

            foreach ($tasks as $index=>$task){
                $newOrder = intval($index)+1;

                if($appended){
                    $newOrder++;
                }

                if((intval($index)+1)==$_GET['order']){
                    $newOrder++;
                    $appended = true;
                }

                $task->order = $newOrder;
                $task->save();
            }

            $utask->order = $_GET['order'];
            $utask->save();
        }

        return response()->json(true);
    }

}
