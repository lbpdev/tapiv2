<?php

namespace App\Http\Controllers;

use App\Models\Subscription;
use App\Services\Avengate\src\AvengateClient;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Foundation\Testing\TestCase;
use Illuminate\Http\Request;

use App\Http\Requests;

class SubscriptionsController extends Controller
{
    public function get($SubscriptionReference = null){

        $reference = $SubscriptionReference ? $SubscriptionReference : ( isset($_GET['SubscriptionReference']) ? $_GET['SubscriptionReference'] : '');

        $client = new AvengateClient([
            'code' => 'LEADINGB',
            'key' => '@dJvyoEH3O1]w&|Aqk4!',
            'base_uri' => 'https://api.avangate.com/rest/3.0/'
        ]);

        // https://api.avangate.com/rest/3.0/orders/{OrderReference}/status/
        // https://api.avangate.com/rest/3.0/subscriptions/{SubscriptionReference}/

        try {
            $response = $client->get('subscriptions/'.$reference.'/');
            $data = json_decode($response->getBody()->getContents());

            $subscription = Subscription::where('SubscriptionReference',$data->SubscriptionReference)->first();

            if($subscription){
                $subscription->SubscriptionStartDate = $data->StartDate;
                $subscription->ExpirationDate = $data->ExpirationDate;
                $subscription->RecurringEnabled = $data->RecurringEnabled ? 1 : 0;
                $subscription->save();
            }

            return $response->getBody()->getContents();

        } catch (ClientException $e) {
            return 'Subscription not found';
        }
    }

    public function enableAutoRenew($SubscriptionReference){

        $client = new AvengateClient([
            'code' => 'LEADINGB',
            'key' => '@dJvyoEH3O1]w&|Aqk4!',
            'base_uri' => 'https://api.avangate.com/rest/3.0/'
        ]);

        // https://api.avangate.com/rest/3.0/subscriptions/{SubscriptionReference}/renewal/

        try {
            $response = $client->post('subscriptions/'.$SubscriptionReference.'/renewal/');
            $data = json_decode($response->getBody()->getContents());

        } catch (ClientException $e) {
            TestCase::assertEquals(401, $e->getResponse()->getStatusCode());
            $sentDetails = json_decode($e->getResponse()->getBody()->getContents());

            TestCase::assertEquals('AUTHENTICATION_ERROR', $sentDetails->error_code);
            TestCase::assertEquals('Authentication needed for this resource', $sentDetails->message);
        }
    }


    public function enable($SubscriptionReference){

        $client = new AvengateClient([
            'code' => 'LEADINGB',
            'key' => '@dJvyoEH3O1]w&|Aqk4!',
            'base_uri' => 'https://api.avangate.com/rest/3.0/'
        ]);

        // https://api.avangate.com/rest/3.0/subscriptions/SubscriptionReference/

        try {
            $response = $client->post('subscriptions/'.$SubscriptionReference.'/');
            $data = json_decode($response->getBody()->getContents());

        } catch (ClientException $e) {
            TestCase::assertEquals(401, $e->getResponse()->getStatusCode());
            $sentDetails = json_decode($e->getResponse()->getBody()->getContents());

            TestCase::assertEquals('AUTHENTICATION_ERROR', $sentDetails->error_code);
            TestCase::assertEquals('Authentication needed for this resource', $sentDetails->message);
        }
    }


    public function disable($SubscriptionReference){

        $client = new AvengateClient([
            'code' => 'LEADINGB',
            'key' => '@dJvyoEH3O1]w&|Aqk4!',
            'base_uri' => 'https://api.avangate.com/rest/3.0/'
        ]);

        // https://api.avangate.com/rest/3.0/subscriptions/SubscriptionReference/

        try {
            $response = $client->delete('subscriptions/'.$SubscriptionReference.'/');
            $data = json_decode($response->getBody()->getContents());

        } catch (ClientException $e) {
            TestCase::assertEquals(401, $e->getResponse()->getStatusCode());
            $sentDetails = json_decode($e->getResponse()->getBody()->getContents());

            TestCase::assertEquals('AUTHENTICATION_ERROR', $sentDetails->error_code);
            TestCase::assertEquals('Authentication needed for this resource', $sentDetails->message);
        }
    }

    public function disableAutoRenew($SubscriptionReference){

        $client = new AvengateClient([
            'code' => 'LEADINGB',
            'key' => '@dJvyoEH3O1]w&|Aqk4!',
            'base_uri' => 'https://api.avangate.com/rest/3.0/'
        ]);

        // https://api.avangate.com/rest/3.0/subscriptions/{SubscriptionReference}/renewal/

        try {
            $response = $client->delete('subscriptions/'.$SubscriptionReference.'/renewal/');
            $data = json_decode($response->getBody()->getContents());

            dd($data);
        } catch (ClientException $e) {
            TestCase::assertEquals(401, $e->getResponse()->getStatusCode());
            $sentDetails = json_decode($e->getResponse()->getBody()->getContents());

            TestCase::assertEquals('AUTHENTICATION_ERROR', $sentDetails->error_code);
            TestCase::assertEquals('Authentication needed for this resource', $sentDetails->message);
        }
    }

    public function enableRenewNotifications($SubscriptionReference){

        $client = new AvengateClient([
            'code' => 'LEADINGB',
            'key' => '@dJvyoEH3O1]w&|Aqk4!',
            'base_uri' => 'https://api.avangate.com/rest/3.0/'
        ]);

        // https://api.avangate.com/rest/3.0/subscriptions/{SUBSCR_REF}/renewal/notification/

        try {
            $response = $client->post('subscriptions/'.$SubscriptionReference.'/renewal/notification/');
            $data = json_decode($response->getBody()->getContents());

            dd($data);
        } catch (ClientException $e) {
            TestCase::assertEquals(401, $e->getResponse()->getStatusCode());
            $sentDetails = json_decode($e->getResponse()->getBody()->getContents());

            TestCase::assertEquals('AUTHENTICATION_ERROR', $sentDetails->error_code);
            TestCase::assertEquals('Authentication needed for this resource', $sentDetails->message);
        }
    }

    public function disableRenewNotifications($SubscriptionReference){

        $client = new AvengateClient([
            'code' => 'LEADINGB',
            'key' => '@dJvyoEH3O1]w&|Aqk4!',
            'base_uri' => 'https://api.avangate.com/rest/3.0/'
        ]);

        // https://api.avangate.com/rest/3.0/subscriptions/{SUBSCR_REF}/renewal/notification/

        try {
            $response = $client->delete('subscriptions/'.$SubscriptionReference.'/renewal/notification/');
            $data = json_decode($response->getBody()->getContents());

            dd($data);
        } catch (ClientException $e) {
            TestCase::assertEquals(401, $e->getResponse()->getStatusCode());
            $sentDetails = json_decode($e->getResponse()->getBody()->getContents());

            TestCase::assertEquals('AUTHENTICATION_ERROR', $sentDetails->error_code);
            TestCase::assertEquals('Authentication needed for this resource', $sentDetails->message);
        }
    }
}
