<?php

namespace App\Http\Controllers;

use App\Models\ClientBrand;
use App\Models\Client;
use App\Models\Format;
use App\Models\Language;
use App\Models\ProjectStage;
use App\Models\Stage;
use App\Models\Task;
use Carbon\Carbon;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\Repositories\Eloquent\CanCreateResponseCode;
use App\Models\Project;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class ProjectsController extends Controller
{
    use CanCreateResponseCode;

    public function __construct(Project $project,Stage $stages,Client $client,ClientBrand $brand){
        $this->model = $project;
        $this->brand = $brand;
        $this->client = $client;
        $this->stages = $stages;
    }

    /*
    * CHECK IF RECORD EXISTS AND STORE
    */

    public function store(Request $request){

        $data = $request->input('project');

        $client = $request->input('client');
        $client['company_id'] = $this->currentCompanyId();
        $brand['name'] = $request->input('brand');
        $brand['company_id'] = $this->currentCompanyId();

        $clientExists = $this->client->fromCurrentCompany()->where('name',$client['name'])->first();
        $client = $clientExists ? $clientExists : $this->client->create($client);

        $brandExists = $this->brand->fromCurrentCompany()->where('name',$brand['name'])->first();
        $brand = $brandExists ? $brandExists : ClientBrand::create($brand);

        if($data['name'] && $client && $brand){
            $data['client_id'] = $client->id;
            $data['client_brand_id'] = $brand->id;
            $data['company_id'] = $this->currentCompanyId();

            if($request->input('start_at') && $request->input('end_at')){
                $data['end_at'] = strtotime(Carbon::parse($request->input('end_at'))->format('Y-m-d'));
                $data['start_at'] = strtotime(Carbon::parse($request->input('start_at'))->format('Y-m-d'));
            }

            if(str_replace(' ', '', $request->input('format_name')) && $request->input('format_id') == 0) {
                $formatExists = Format::where('name',$request->input('format_name'))->where('company_id',Auth::user()->company->id)->first();

                if(count($formatExists)){
                    $data['format_id'] = $formatExists->id;
                } else {
                    $new_format = Format::create(['name'=>$request->input('format_name'),'company_id'=>Auth::user()->company->id]);
                    $data['format_id'] = $new_format->id;
                }
            }

            $newProject = $this->model->create($data);

            if(count($request->input('language_ids'))){
                foreach ($request->input('language_ids') as $language)
                    $newProject->languages()->create(['language_id'=>$language]);
            } else {
                $newProject->languages()->create(['language_id'=>3]);
            }

            $data['items'] = $this->getAll();
            $data['project'] = $newProject;

            $this->storeStages($newProject,$request);

            return redirect()->back();
        }

        return response()->json($this->generateResponse('error'));
    }


    /*
    * CHECK IF RECORD EXISTS AND STORE
    */

    public function storeStages($newProject,$request){
        $stages = $request->input('stage');
        $newProject->stages()->delete();

        if(count($stages)){
            foreach($stages as $stage){

                if($stage['start_at'] && $stage['end_at']){
                    $stage['end_at'] = Carbon::parse($stage['end_at'])->format('Y-m-d');
                    $stage['start_at'] = Carbon::parse($stage['start_at'])->format('Y-m-d');
                    $newProject->stages()->create($stage);
                }
            }
        }

        $projects = $this->getAll();

        return redirect()->back();
//        return response()->json($this->generateResponseWithData('create-success',$projects));
    }

    /*
    * GET IF RECORD EXISTS
    */

    public function get(){

        $data['project'] = $this->model->with('brand','client','format')->where('id',$_GET['id'])->first();
        $data['project']['language'] = $data['project']->languagesString;
        $data['project']['duration'] = $data['project']->duration;

        $data['stages'] = $this->stages->whereHas('project_stages',function($q){
            $q->where('project_id',$_GET['id']);
        })->get();


        $currentYear = isset($_GET['year']) ? Carbon::parse($_GET['year'].'-01-01') : new \DateTime();
        $currentYear = $currentYear->format('Y');

        $begin =  new \DateTime( Carbon::parse($currentYear.'-01-01')->subYear().'-01-01' );
        $end = new \DateTime( (intval($currentYear)+2).'-01-01' );

        $end = $end->modify( '+1 day' );

        $interval = new \DateInterval('P1D');

        $daterange = new \DatePeriod($begin, $interval ,$end);
        $dateRangeArray = [];

        foreach($daterange as $date)
            array_push($dateRangeArray, $date->format('Y-d-m'));

        $data['project']['range_start'] = array_search($data['project']->start_at->format('Y-d-m'), $dateRangeArray) + 1;
        $data['project']['range_end'] = array_search($data['project']->end_at->format('Y-d-m'), $dateRangeArray) + 1;
        $data['project']['range'] = ($data['project']['range_end']+1) - $data['project']['range_start'];
        $rangeDates = [];

        for($x=$data['project']['range_start']-1;$x<=$data['project']['range_end']-1;$x++)
            $rangeDates[] = $dateRangeArray[$x];

        $data['project']['range_dates'] = $rangeDates;

        foreach($data['stages'] as $index=>$stage){
            $data['stages'][$index]['project_stage'] = ProjectStage::with('stage')->where('stage_id',$stage->id)->where('project_id',$_GET['id'])->first();
            $data['stages'][$index]['project_stage']['start_index'] = array_search($data['stages'][$index]['project_stage']->start_at->format('Y-d-m'), $rangeDates) + 1;
            $data['stages'][$index]['project_stage']['end_index'] = array_search($data['stages'][$index]['project_stage']->end_at->format('Y-d-m'), $rangeDates) + 1;

            if(count($stage->department)){
                $tasks = Task::with('utasks')->where('project_id',$_GET['id'])->where('department_id',$stage->department['id'])->get();
                $data['stages'][$index]['tasks'] = $tasks;
                $data['stages'][$index]['hours'] = 0;

                foreach ($tasks as $task){
                    if(count($task->utasks)){
                        foreach ($task->utasks as $utask){
                            $data['stages'][$index]['hours'] += intval($utask->time);
                        }
                    }
                }
            }
        }

        return response()->json($data);
    }

    /*
    * GET IF RECORD EXISTS
    */

    public function getArray(){
        $data['project'] = $this->model->where('id',$_GET['id'])->first();
        $data['stages'] = $this->stages->whereHas('project_stages',function($q){
            $q->where('project_id',$_GET['id']);
        })->get();

        $currentYear = new \DateTime();
        $currentYear = $currentYear->format('Y');

        $begin = new \DateTime( $currentYear.'-01-01' );
        $end = new \DateTime( (intval($currentYear)+4).'-01-01' );

        $end = $end->modify( '+1 day' );

        $interval = new \DateInterval('P1D');
        $daterange = new \DatePeriod($begin, $interval ,$end);
        $dateRangeArray = [];

        foreach($daterange as $date)
            array_push($dateRangeArray, $date->format('Y-d-m'));

        foreach($data['stages'] as $index=>$stage){
            $data['stages'][$index]['project_stage'] = ProjectStage::with('stage')->where('stage_id',$stage->id)->where('project_id',$_GET['id'])->first();
            $data['stages'][$index]['project_stage']['start_index'] = array_search($data['stages'][$index]['project_stage']->start_at->format('Y-d-m'), $dateRangeArray) + 1;
            $data['stages'][$index]['project_stage']['end_index'] = array_search($data['stages'][$index]['project_stage']->end_at->format('Y-d-m'), $dateRangeArray) + 1;


            if(count($stage->department)){
                $tasks = Task::with('utasks')->where('project_id',$_GET['id'])->where('department_id',$stage->department['id'])->get();
                $data['stages'][$index]['tasks'] = $tasks;
                $data['stages'][$index]['hours'] = 0;
                foreach ($tasks as $task){
                    if(count($task->utasks)){
                        foreach ($task->utasks as $utask){
                            $data['stages'][$index]['hours'] += intval($utask->time);
                        }
                    }
                }
            }
        }

        return $data;
    }

    /*
    * GET DETAILS IF RECORD EXISTS
    */

    public function getDetails(){

        $project = $this->model->with('languages')->where('id',$_GET['id'])->first();

        $client = Client::where('id',$project->client_id)->first();

        $data['client[name]'] = $client ? $client->name : 'N/A';
        $data['client[color]'] =  $client ? $client->color : '#000';

        $brand = ClientBrand::where('id',$project->client_brand_id)->first();
            $data['brand'] = $brand->name;

        $data['project_id'] = $project->id;
        $data['project[name]'] = $project->name;
        $data['project[version]'] = $project->version;
        $data['project[completed_at]'] = $project->completed_at;

        $languages = array();

        foreach ($project->languages as $language)
            $languages[] = $language->language_id;

        $data['language_ids'] = $languages;
        $data['project[format_id]'] = $project->format_id;
        $data['project[start_at]'] = Carbon::parse($project->start_at)->setTimezone(Auth::user()->companyTimezone);
        $data['project[end_at]'] = Carbon::parse($project->end_at)->setTimezone(Auth::user()->companyTimezone);
        $data['stages'] = $project->load('stages.stage');

        return response()->json($data);
    }


    /*
    * CHECK IF RECORD EXISTS AND STORE
    */

    public function update(Request $request){

        $project_id = $request->input('project_id');
        $project = Project::where('id',$project_id)->first();

        if($project){

            $projectD = $request->input('project');
            $client = $request->input('client');
            $client['company_id'] = $this->currentCompanyId();
            $brand['name'] = $request->input('brand');

//            dd($this->checkRanges($request->input(),$request->input('stage')));

            $clientExists = $this->client->fromCurrentCompany()->where('name',$client['name'])->first();

            if($clientExists){
                if($clientExists->color != $client['color'])
                    $clientExists->color = $client['color'];

                $clientExists->save();

                $client = $clientExists;
            }
            else
                $client = $this->client->create($client);

            $brand['company_id'] = $this->currentCompanyId();

            $brandExists = $this->brand->fromCurrentCompany()->where('name',$brand['name'])->first();
            $brand = $brandExists ? $brandExists : ClientBrand::create($brand);

            if($projectD['name'] && $client && $brand){
                $projectD['client_id'] = $client->id;
                $projectD['client_brand_id'] = $brand->id;

                if($request->input('start_at') && $request->input('end_at')){
                    $projectD['end_at'] = strtotime(Carbon::parse($request->input('end_at'))->format('Y-m-d'));
                    $projectD['start_at'] = strtotime(Carbon::parse($request->input('start_at'))->format('Y-m-d'));
                }

                if(str_replace(' ', '', $request->input('format_name')) && $projectD['format_id'] == 0) {
                    $formatExists = Format::where('name',$request->input('format_name'))->where('company_id',Auth::user()->company->id)->first();

                    if(count($formatExists)){
                        $projectD['format_id'] = $formatExists->id;
                    } else {
                        $new_format = Format::create(['name'=>$request->input('format_name'),'company_id'=>Auth::user()->company->id]);
                        $projectD['format_id'] = $new_format->id;
                    }
                } elseif(str_replace(' ', '', $request->input('format_name'))=="" && $projectD['format_id'] == 0)
                    return response()->json($this->generateResponse('invalid-format-name'));

                $project->update($projectD);

                $newProject = $project;

                $project->languages()->delete();

                if(count($request->input('language_ids'))){
                    foreach ($request->input('language_ids') as $language)
                        $project->languages()->create(['language_id'=>$language]);
                } else {
                    $project->languages()->create(['language_id'=>3]);
                }

                $projects['items'] = $this->getAll();
                $projects['project'] = $project->load('stages.stage');

                $this->storeStages($newProject,$request);

                return redirect()->back();
            }
        }

        return response()->json($this->generateResponse('error'));
    }


    /*
    * CHECK IF RECORD EXISTS AND STORE
    */

    public function updateStages(Request $request){

        $stage = ProjectStage::with('project')->find($request->input('stage')[0]['stage_id']);
        $project = $stage->project;

        $currentYear = new \DateTime();
        $currentYear = $currentYear->format('Y');

        $begin = new \DateTime( $currentYear.'-01-01' );
        $end = new \DateTime( (intval($currentYear)+3).'-01-01' );
        $end = $end->modify( '+1 day' );

        $interval = new \DateInterval('P1D');
        $daterange = new \DatePeriod($begin, $interval ,$end);
        $dateRangeArray = [];

        foreach($daterange as $date)
            array_push($dateRangeArray, $date->format('Y-m-d'));

        $data['project']['range_start'] = array_search($project->start_at->format('Y-m-d'), $dateRangeArray) + 1;
        $data['project']['range_end'] = array_search($project->end_at->format('Y-m-d'), $dateRangeArray) + 1;
        $data['project']['range'] = $data['project']['range_end'] - $data['project']['range_start'];
        $rangeDates = [];

        for($x=$data['project']['range_start']-1;$x<=$data['project']['range_end']-1;$x++)
            $rangeDates[] = $dateRangeArray[$x];

        foreach($request->input('stage') as $stage){
            if($stage['start_at'] && $stage['end_at']){

                $start = new \DateTime($rangeDates[$stage['start_at'] - 1 ]);
                $end = new \DateTime($rangeDates[( $stage['start_at'] + $stage['end_at'] ) - 2]);

                $project_stage = ProjectStage::where('id',$stage['stage_id'])->first();
                $project_stage->start_at = strtotime($start->format('Y-m-d'));
                $project_stage->end_at = strtotime($end->format('Y-m-d'));
                $project_stage->save();
            }
        }

        return redirect()->back();
    }

    public function checkRanges($project,$stages){

        $start = $project['start_at'];
        $end = $project['end_at'];

        foreach($stages as $stage){
            if(!$this->check_in_range($start,$end,$stage['start_at']) || !$this->check_in_range($start,$end,$stage['end_at'])){
                return false;
            }
        }

        return true;
    }

    function check_in_range($start_date, $end_date, $date_from_user)
    {
        // Convert to timestamp
        $start_ts = Carbon::parse($start_date);
        $end_ts = Carbon::parse($end_date);
        $user_ts = Carbon::parse($date_from_user)->between($start_ts, $end_ts);

        return $user_ts;
    }

    /*
    * CHECK IF RECORD EXISTS AND STORE
    */

    public function updateStagesCalendar(Request $request){

        $projectDuration = $request->only(['end_at','start_at']);
        $stages = $request->input('stage');

        $project = Project::where('id',$request->input('project_id'))->first();

        if($project){

            $projectDuration['end_at'] = Carbon::parse($projectDuration['end_at'])->format('Y-m-d');
            $projectDuration['start_at'] = Carbon::parse($projectDuration['start_at'])->format('Y-m-d');

            $project->update($projectDuration);

            $project->stages()->delete();

            foreach($stages as $stage){

                if($stage['start_at'] && $stage['end_at']){
                    $stage['end_at'] = Carbon::parse($stage['end_at'])->format('Y-m-d');
                    $stage['start_at'] = Carbon::parse($stage['start_at'])->format('Y-m-d');

                    if($stage['end_at'] == $stage['start_at'])
                        $stage['end_at'] = Carbon::parse($stage['end_at'])->addDay()->format('Y-m-d');

                    $project->stages()->create($stage);
                }
            }
        }

        $projects = $this->getAll();

        return redirect()->back();
    }


    /*
    * DELETE A RECORD THROUGH ID
    */
    public function delete(Request $request){

        $id = $request->input('id');

        $item = $this->model->find(strip_tags($id));

        if($item)
            if($item->company_id != Auth::user()->company->id)
                return response()->json($this->generateResponse('record-not-exists'));

        $item->delete();

        return response()->json('');
    }

    /*
    * DELETE A RECORD THROUGH ID
    */
    public function complete(){

        $project = $this->model->where('id',$_GET['id'])->first();

        if($project){
            if($project->completed_at != null)
                $project->completed_at = null;
            else
                $project->completed_at = Carbon::now()->format('Y-m-d');

            $project->save();
        }

        return response()->json($project);
    }

    /*
    * DELETE A RECORD THROUGH ID
    */
    public function generateExcel(){

        $data = $this->model->where('id',1)->first()->toArray();

        $data['client_id'] = Client::where('id',$data['client_id'])->pluck('name')->first();
        $data['client_brand_id'] = ClientBrand::where('id',$data['client_brand_id'])->pluck('name')->first();
        $data['format_id'] = Format::where('id',$data['format_id'])->pluck('name')->first();
        $data['language_id'] = '';

        Excel::create('Sample', function($excel) use($data) {

            $excel->sheet('Sheet1', function($sheet) use($data) {

                $sheet->fromModel($data);

                $sheet->cells('A1:Z10', function($cells){
                    $cells->setAlignment('center');
                });

                $sheet->prependRow(1, array(
                    'ID', 'Name', 'Version', 'Client', 'Brand', 'Format', 'Language', 'Start', 'Deadline', 'Create at', 'Updated at'
                ));

            });

        })->download('xls');

    }

    /*
    * GET ALL RECORDS AND RETURN AS ARRAY
    */

    private function getAll(){
        return Format::with('projects')->get();
    }
}
