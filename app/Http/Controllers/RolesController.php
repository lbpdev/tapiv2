<?php

namespace App\Http\Controllers;

use App\Repositories\Eloquent\CanCreateResponseCode;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Role;
use App\Repositories\Eloquent\CanCreateSlug;

class RolesController extends Controller
{
    use CanCreateSlug , CanCreateResponseCode;

    public function __construct(Role $role){
        $this->model = $role;
    }

    /*
    * CHECK IF ROLE EXISTS AND STORE
    */

    public function store(Request $request){
        $input = $request->input();

        if(Role::where('name',$input['name'])->count())
            return response()->json($this->generateResponse('role-exists'));

        if($input['name']){
            $slug  = $this->generateSlug($input['name']);

            Role::create(array(
                'name'=>$input['name'],
                'slug'=>$slug,
                'icon'=>$input['icon']
            ));
        }

        $roles = $this->getAll();

        return response()->json($this->generateResponseWithData('create-success',$roles));
    }


    /*
    * DELETE A ROLE THROUGH ID
    */
    public function delete(Request $request){

        $role = Role::where('id',strip_tags($request->input('id')))->first();

        if($role)
            if($role->delete()) {
                $roles = $this->getAll();
                return response()->json($this->generateResponseWithData('delete-success',$roles));
        }

        return response()->json($this->generateResponse('role-not-exists'));
    }

    /*
    * GET ALL ROLES AND RETURN AS ARRAY
    */

    private function getAll(){
        return Role::get()->toArray();
    }

}
