<?PHP

namespace App\Http\Controllers\Customers;

use App\Models\Company;
use App\Models\CsrfToken;
use App\Models\Department;
use App\Models\Option;
use App\Models\Report;
use App\Models\Subscription;
use App\Models\User;
use App\Models\UserLog;
use App\Repositories\Eloquent\CanCreateResponseCode;
use App\Services\Avengate\src\AvengateClient;
use App\Services\Avengate\src\SoapClient;
use App\Services\ColorServer;
use App\Services\TimezoneService;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class CompanyController extends Controller
{
    use CanCreateResponseCode;
    use TimezoneService;
    use ColorServer;

    public function __construct(Company $company,Option $option, User $user, UserLog $userLog)
    {
        $this->model = $company;
        $this->option = $option;
        $this->user = $user;
        $this->userLog = $userLog;
    }

    public function index()
    {
        $timezones = $this->getTimezones();
        return view('pages.customers.index', compact('timezones'));
    }

    public function register(Request $request)
    {
        $subscription = $request->input('subscription');
        $redirect = route('settings.info');
        $option['option']['employees'] = '1-5';

        $options = $request->only('option');

        $user = $request->only('user');

        $company = $this->model->create(array('name'=>$options['option']['name']));

        if($company){
            foreach ($options['option'] as $key => $value) {
                $this->option->create(array('name'=>$key,'value'=>$value,'company_id'=>$company->id));
            }
        }

        $timezone = $company->timezone;
        $now = Carbon::now()->setTimezone($timezone)->format('Y-m-d');

        $this->option->create(array('name'=>'incomplete-tasks','value'=>Carbon::parse($now.' 17:30',$timezone)->setTimezone('UTC')->format('H:i'),'company_id'=>$company->id));
        $this->option->create(array('name'=>'today-tasks','value'=>Carbon::parse($now.' 8:30',$timezone)->setTimezone('UTC')->format('H:i'),'company_id'=>$company->id));
        $this->option->create(array('name'=>'tasks-report','value'=>Carbon::parse($now.' 18:00',$timezone)->setTimezone('UTC')->format('H:i'),'company_id'=>$company->id));

        Report::create([
            'company_id' => $company->id,
            'interval' => 'd',
            'type' => 'incomplete-tasks',
            'next_send' => Carbon::parse($now.' 17:30',$timezone)->addDay()->setTimezone('UTC'),
            'time' => Carbon::parse($now.' 17:30',$timezone)->addDay()->setTimezone('UTC')->format('H:i')
        ]);

        Report::create([
            'company_id' => $company->id,
            'interval' => 'd',
            'type' => 'today-tasks',
            'next_send' => Carbon::parse($now.' 8:30',$timezone)->addDay()->setTimezone('UTC'),
            'time' => Carbon::parse($now.' 8:30',$timezone)->addDay()->setTimezone('UTC')->format('H:i')
        ]);

        Report::create([
            'company_id' => $company->id,
            'interval' => 'd',
            'type' => 'tasks-report',
            'next_send' => Carbon::parse($now.' 18:00',$timezone)->addDay()->setTimezone('UTC'),
            'time' => Carbon::parse($now.' 18:00',$timezone)->addDay()->setTimezone('UTC')->format('H:i')
        ]);

        $workDays = $request->input('workdays');
        $this->option->create(array('name'=>'workdays','value'=> $workDays ? json_encode($workDays) : '["Sun","Mon","Tue","Wed","Thu"]','company_id'=>$company->id));

        $user['user']['password'] = Hash::make($user['user']['password']);
        $user['user']['company_id'] = $company->id;
        $user['user']['username'] = $user['user']['email'];

        $user = User::create($user['user']);

        $user->role()->sync([1]);

        $now = Carbon::now();
        $subscriptionData['SubscriptionStartDate'] = $now;
        $subscriptionData['ExpirationDate'] = $now->addDays(30);
        $subscriptionData['plan_id'] = 1;

        $department = Department::create(array(
            'name'=>'Management',
            'color'=>'#ffb943',
            'work_hours'=>8,
            'company_id'=>$company->id
        ));

        $user->department()->sync([$department->id]);

        $company->subscriptions()->create($subscriptionData);

        if($user){
            Auth::login($user);

            if($subscription){
                switch ($subscription) {
                    case 1:
                        $redirect = 'https://secure.avangate.com/order/checkout.php?PRODS=4698769&QTY=1&CART=1&CARD=2&DESIGN_TYPE=2&ADDITIONAL_USER_ID='.$user->id.'&BACK_REF=https%3A%2F%2Ftapiapp.com%2Fapp%2Fsettings%3Fm%3Dthank-you';
                        break;
                    case 2:
                        $redirect = 'https://secure.avangate.com/order/checkout.php?PRODS=4698853&QTY=1&CART=1&CARD=2&DESIGN_TYPE=2&ADDITIONAL_USER_ID='.$user->id.'BACK_REF=https%3A%2F%2Ftapiapp.com%2Fapp%2Fsettings%3Fm%3Dthank-you';
                        $option['option']['employees'] = '6-15';
                        break;
                    case 3:
                        $redirect = 'https://secure.avangate.com/order/checkout.php?PRODS=4698854&QTY=1&CART=1&CARD=2&DESIGN_TYPE=2&ADDITIONAL_USER_ID='.$user->id.'BACK_REF=https%3A%2F%2Ftapiapp.com%2Fapp%2Fsettings%3Fm%3Dthank-you';
                        $option['option']['employees'] = '16-30';
                        break;
                    case 4:
                        $redirect = 'https://secure.avangate.com/order/checkout.php?PRODS=4698855&QTY=1&CART=1&CARD=2&DESIGN_TYPE=2&ADDITIONAL_USER_ID='.$user->id.'BACK_REF=https%3A%2F%2Ftapiapp.com%2Fapp%2Fsettings%3Fm%3Dthank-you';
                        $option['option']['employees'] = '31-50';
                        break;
                }
            }

//            $user->logs()->create(array('activity'=>'login'));

//            $admins = explode(",",env('ADMIN_EMAIL'));
//
//            foreach ($admins as $admin){
//                Mail::send('emails.new-company', ['company'=>$company], function($message) use($admin,$user)
//                {
//                    $message->to($admin, 'TapiApp Admin')->subject('New Company Registration');
//                });
//            }

            if($request->has('csrf_token'))
                return $redirect;
            else
                return redirect($redirect);
        }
    }

    public function getUserLimit(){

        if(!isset($_GET['id']))
            return null;

        $company = Company::with('subscriptions.plan')->where('id',$_GET['id'])->first();

        $data['current_users'] = count($company->users()->where('is_active',1)->get());
        $data['max_users'] = null;

        if($company->subscription)
            if($company->subscription->plan)
                $data['max_users'] = $company->subscription->plan->max_users;

        return response()->json($data);
    }

    public function toggleAutoRenew($SubscriptionReference){

        $subscription = Subscription::where('SubscriptionReference',$SubscriptionReference)->where('company_id',Auth::user()->company->id)->first();

        SoapClient::setBaseUrl('https://api.avangate.com/soap/3.0/');
        SoapClient::setCredentials('LEADINGB', '@dJvyoEH3O1]w&|Aqk4!');

        try {
            Log::info('Current Status: '.$subscription->RecurringEnabled);


            try {
                Log::info('Disabling Recurring : '.$subscription->id);
                $response = SoapClient::disableRecurringBilling($SubscriptionReference);

                if($response)
                    Log::info('Recurring Payments disabled: '.$subscription->id);

                $subscription->RecurringEnabled = 0;

            } catch (\Exception $e) {

                Log::info('Enabling Recurring : '.$subscription->id);
                $response = SoapClient::enableRecurringBilling($SubscriptionReference);

                if($response)
                    Log::info('Recurring Payments enabled: '.$subscription->id);

                $subscription->RecurringEnabled = 1;
            }

            $subscription->save();

            Log::info('Database Updated RecurringEnabled: '.$subscription->RecurringEnabled);

        } catch (ClientException $e) {
        }
        return redirect()->back();
    }

    public function disableAutoRenew($SubscriptionReference){

        $subscription = Subscription::where('SubscriptionReference',$SubscriptionReference)->where('company_id',Auth::user()->company->id)->first();

        SoapClient::setBaseUrl('https://api.avangate.com/soap/3.0/');
        SoapClient::setCredentials('LEADINGB', '@dJvyoEH3O1]w&|Aqk4!');

        try {
            Log::info('Current Status: '.$subscription->RecurringEnabled);


            try {
                Log::info('Disabling Recurring : '.$subscription->id);
                $response = SoapClient::disableRecurringBilling($SubscriptionReference);

                if($response)
                    Log::info('Recurring Payments disabled: '.$subscription->id);

                $subscription->RecurringEnabled = 0;

            } catch (\Exception $e) {

            }

            $subscription->save();

            Log::info('Database Updated RecurringEnabled: '.$subscription->RecurringEnabled);

        } catch (ClientException $e) {
        }
        return redirect()->back();
    }

    public function generateCSRF(Request $request)
    {
        if($_GET['session_id']){
            $token = str_random(32);

            CsrfToken::where('session_id',$_GET['session_id'])->delete();

            if(CsrfToken::create([ 'token' => $token, 'session_id' => $_GET['session_id']])){
                return $token;
            }
        }

        return [];
    }
}
