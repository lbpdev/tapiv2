<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\ProjectStage;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Stage;

use App\Repositories\Eloquent\CanCreateResponseCode;
use App\Repositories\Eloquent\CannotAcceptWhiteSpace;
use Illuminate\Support\Facades\Auth;

class StagesController extends Controller
{
    use CanCreateResponseCode, CannotAcceptWhiteSpace;

    public function __construct(Stage $stage){
        $this->model = $stage;
    }

    /*
    * GET RECORD IF EXISTS
    */

    public function get(){

        $stage = $this->model->with('department')->where('id',$_GET['id'])->first();

        if(!$stage)
            return response()->json($this->generateResponse('stage-not-exists'));


        $data['projectsCount'] = 0;
        $data['tasksCount'] = 0;

        $projects = Project::fromCurrentCompany()->whereHas('stages',function($query) use($stage){
            $query->where('stage_id',$stage->id);
        })->get();

        foreach ($projects as $project){
            foreach ($project->tasks as $task){
                $data['tasksCount'] += count($task->utasks);
            }
        }

        $data['projectsCount'] = count($projects);
        $data['stage'] = $stage;

        return response()->json($this->generateResponseWithData('create-success',$data));
    }

    /*
    * CHECK IF RECORD EXISTS AND STORE
    */

    public function store(Request $request){
        $input = $request->input();
        $company_id = $this->currentCompanyId();

        if(isset($input['company_id']))
            $company_id = $input['company_id'];

        if(!$this->checkWhiteSpaces($input['name']))
            return response()->json($this->generateResponse('white-spaces'));

        if($this->model->fromCompany($company_id)->where('name',$input['name'])->count())
            return response()->json($this->generateResponse('stage-exists'));

        if($input['name']){
            $stages_count = $this->model->where('company_id',$company_id)->count();

            $stage = $this->model->create(array(
                'name'=>$input['name'],
                'color'=>$input['color'],
                'internal'=>$input['internal'],
                'hasTime'=> $input['hasTime'] ? 1 : null,
                'company_id'=> $company_id,
                'order' => $stages_count+1
            ));

            if($stage && $input['department_id'])
                $stage->department()->sync([$input['department_id']]);
        }

        $stages = $this->getAll($company_id);

        return response()->json($this->generateResponseWithData('create-stage-success',$stages));
    }


    /*
    * DELETE A RECORD THROUGH ID
    */
    public function delete(Request $request){

        $data = $this->model->where('id',strip_tags($request->input('id')))->first();

        if($data){

            if($data->company_id != Auth::user()->company->id)
                return response()->json($this->generateResponse('record-not-exists'));

            $company_id = $data->company_id;
            if($data->delete()) {
                ProjectStage::where('stage_id',$request->input('id'))->delete();
                $data = $this->getAll($company_id);
                return response()->json($this->generateResponseWithData('delete-stage-success',$data));
            }
        }

        return response()->json($this->generateResponse('record-not-exists'));
    }

    /*
    * UPDATE EXISTING AND RECORD
    */

    public function update(Request $request){
        $input = $request->input();
        $company_id = $this->currentCompanyId();

        if(isset($input['company_id']))
            $company_id = $input['company_id'];

        $stage = $this->model->where('id',$input['id'])->first();

        if(!$stage)
            return response()->json($this->generateResponse('stage-not-exist'));

        if(!$this->checkWhiteSpaces($input['name']))
            return response()->json($this->generateResponse('white-spaces'));

        if($input['name']){
            if(strtolower($input['name'])==strtolower($stage->name))
                $stage->update(array(
                    'color'=>$input['color'],
                    'internal'=>$input['internal'],
                    'hasTime'=> $input['hasTime'] ? 1 : null,
                ));

            elseif($this->model->fromCompany($company_id)->where('name',$input['name'])->first())
                return response()->json($this->generateResponse('stage-exists'));

            else {
                $stage->update(array(
                    'name'=>$input['name'],
                    'hasTime'=> $input['hasTime'] ? 1 : null,
                    'color'=>$input['color'],
                    'internal'=>$input['internal']
                ));

            }

            if($input['department_id'])
                $stage->department()->sync([$input['department_id']]);
            else
                $stage->department()->sync([]);
        }

        $stages = $this->getAll($company_id);

        return response()->json($this->generateResponseWithData('update-stage-success',$stages));
    }


    /*
    * UPDATE EXISTING RECORD ORDER
    */

    public function updateOrder(Request $request){
        $input = $request->input();

        foreach($input['order'] as $index=>$stage_id){
            $stage = $this->model->where('id',$stage_id)->first();

            if($stage){
                $stage->order = $index + 1;
                $stage->save();
            }

        }

        return response()->json($input);
    }

    /*
    * GET ALL RECORDS AND RETURN AS ARRAY
    */

    private function getAll($company_id = null){

        if(!$company_id)
            return $this->model->with('department')->orderBy('order','ASC')->fromCurrentCompany()->get()->toArray();

        return $this->model->with('department')->orderBy('order','ASC')->fromCompany($company_id)->get()->toArray();
    }
}
