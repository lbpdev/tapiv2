<?php

namespace App\Http\Controllers;

use App\Models\Key;
use App\Models\Project;
use App\Models\Task;
use App\Models\UserTask;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;

use App\Repositories\Eloquent\CanCreateResponseCode;
use App\Repositories\Eloquent\CannotAcceptWhiteSpace;
use Illuminate\Support\Facades\Auth;

class KeysController extends Controller
{
    use CanCreateResponseCode,CannotAcceptWhiteSpace;

    public function __construct(Key $key)
    {
        $this->model = $key;
    }

    public function store(Request $request){
        $input = $request->input();

        if(!$this->checkWhiteSpaces($input['name']))
            return response()->json($this->generateResponse('white-spaces'));

        if(Key::fromCurrentCompany()->where('name',$input['name'])->where('department_id',$input['department_id'])->count())
            return response()->json($this->generateResponse('key-name-exists'));

        if($input['name']){

            if($input['department_id']=='general')
                $input['department_id'] = null;

            $input['company_id'] = $this->currentCompanyId();

            $input['name'] = strip_tags(trim($input['name']));
            $key = Key::create($input);

            $keys = $this->getAll();

            return response()->json($this->generateResponseWithData('create-key-success',$keys));

        }

        return response()->json($this->generateResponse('error'));

    }

    /*
   * GET RECORD IF EXISTS
   */

    public function get(){

        $key = $this->model->with('department','tasks.project')->where('id',$_GET['id'])->first();

        if(!$key)
            return response()->json($this->generateResponse('key-not-exists'));

        $data['projectsCount'] = Project::fromCurrentCompany()->whereHas('tasks', function($query) use($key){
            $query->where('key_id',$key->id);
        })->count();

        $data['tasksCount'] = UserTask::whereHas('task', function($query) use($key){
            $query->where('key_id',$key->id);
        })->count();

        $data['key'] = $key;
        
        return response()->json($this->generateResponseWithData('create-success',$data));
    }

    /*
    * UPDATE EXISTING AND RECORD
    */

    public function update(Request $request){
        $input = $request->input();

        $data = $this->model->with('department')->where('id',$input['id'])->first();

        if(!$data)
            return response()->json($this->generateResponse('key-not-exist'));

        if(!$this->checkWhiteSpaces($input['name']))
            return response()->json($this->generateResponse('white-spaces'));

        if($input['name']){

            if(( strtolower($data->name) != strtolower($input['name']) ) && $this->model->fromCurrentCompany()->where('name',$input['name'])->first())
                return response()->json($this->generateResponse('key-exists'));

            else {
                $input['name'] = strip_tags(trim($input['name']));
                $data->update(array(
                    'department_id'=>$input['department_id'],
                    'name'=>$input['name'],
                ));
            }
        }

        $keys = $this->getAll();

        return response()->json($this->generateResponseWithData('update-key-success',$keys));
    }


    /*
    * DELETE A RECORD THROUGH ID
    */
    public function delete(Request $request){

        $data = Key::where('id',strip_tags($request->input('id')))->first();

        if($data)

            if($data->company_id != Auth::user()->company->id)
                return response()->json($this->generateResponse('record-not-exists'));

            Task::where('key_id',$request->input('id'))->delete();

            if($data->delete()) {
                $data = $this->getAll();
                return response()->json($this->generateResponseWithData('delete-key-success',$data));
            }

        return response()->json($this->generateResponse('record-not-exists'));
    }

    /*
    * GET ALL RECORDS AND RETURN AS ARRAY
    */

    private function getAll(){
        return $this->model->with('department')->fromCurrentCompany()->orderBy('name','asc')->get()->toArray();
    }


    private function getAllByDepartment($department_id){
        return Key::fromCurrentCompany()->where('department_id',$department_id)->get()->toArray();
    }
}
