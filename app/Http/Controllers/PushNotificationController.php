<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Option;
use App\Models\Report;
use App\Models\User;
use App\Models\UserTask;
use Carbon\Carbon;
use Davibennun\LaravelPushNotification\Facades\PushNotification;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class PushNotificationController extends Controller
{

    public function test(){
        PushNotification::app('appNameIOS')
            ->to('4e29270d910129797247ac3e837123d26e91ad2bdbeaab40a0462feabc75e640')
            ->send('PUUUUUUUUUUUUUUUUUSH!');

//        PushNotification::app('appNameAndroid')
//            ->to('4e29270d910129797247ac3e837123d26e91ad2bdbeaab40a0462feabc75e640')
//            ->send('PUUUUUUUUUUUUUUUUUSH!');

    }

    public function storeSettings(Request $request){
        $input = $request->input();

        $now = Carbon::now()->setTimezone(Auth::user()->company->timezone)->format('Y-m-d');

        foreach ($input['notifications'] as $label=>$notification){

            $option = Option::where('company_id',$input['company_id'])->where('name',$label)->first();
            $time = Carbon::parse($now.' '.$notification ,Auth::user()->company->timezone);
            $time = $time->setTimezone('UTC');

            if($option){
                $option->update([
                    'value'=>$time->format('H:i')
                ]);
            } else {
                Option::create([
                    'company_id'=>$input['company_id'],
                    'name'=>$label,
                    'value'=>$time->format('H:i')
                ]);
            }

            $next_send = Carbon::parse($now.' '.$notification ,Auth::user()->company->timezone)->setTimezone('UTC')->addDay();
            $report = Report::where('company_id',$input['company_id'])->where('type',$label)->first();

            if($report){
                $report->update([
                    'time'=>$time,
                    'next_send'=>$next_send,
                ]);
            } else {
                Report::create([
                    'company_id'=>$input['company_id'],
                    'interval'=>'d',
                    'type'=>$label,
                    'time'=>$time,
                    'next_send'=>$next_send,
                ]);
            }
        }

        return redirect()->back();
    }



    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        echo Carbon::now()->format('Y-m-d H:i');

        $queue = Report::where('next_send','>=',Carbon::now()->format('Y-m-d H:i'))
            ->where('next_send','<=',Carbon::now()->addMinute(10)->format('Y-m-d H:i'))
            ->get();

        if($queue){
            foreach ($queue as $task){
                $company = Company::where('id',$task->company_id)->first();

                if($task->type=='tasks-report'){

                    $companyAdmins = User::where('company_id',$task->company_id)->whereHas('role',function($query){
                        $query->where('slug','admin');
                    })->pluck('email');

                    $companyAdmins = ['gene@leadingbrands.me','mars@leadingbrands.me'];

                    $companyEmployees = User::where('company_id',$task->company_id)->whereHas('role',function($query){
                        $query->where('slug','employee');
                    })->whereHas('department',function($query){})->get();

                    $users = [];
                    foreach($companyEmployees as $index=>$employee){
                        $users[$index]['name'] = $employee->name;

                        $tasks = UserTask::where('user_id',$employee->id)
                            ->whereDate('date_assigned','=',Carbon::now()->setTimezone($employee->company->timezone)->format('Y-m-d'))
                            ->count();

                        $completed = UserTask::where('user_id',$employee->id)
                            ->whereDate('date_assigned','=',Carbon::now()->setTimezone($employee->company->timezone)->format('Y-m-d'))
                            ->whereNotNull('date_completed')
                            ->count();

                        $users[$index]['tasks'] = $tasks;
                        $users[$index]['completed'] = $completed;
                        $users[$index]['id'] = $employee->id;
                        $users[$index]['department'] = $employee->department ? $employee->department->id : '';

                    }
                    $date = Carbon::now()->setTimezone($employee->company->timezone)->format('d-m-Y');

                    return view('emails.notifications.tasks-report',compact('users','date'));

//                    Mail::send('emails.notifications.tasks-report', ['users'=>$users], function($message) use($companyAdmins)
//                    {
//                        $message->to($companyAdmins, 'Administrator')->subject('Tasks Report');
//                    });


                }

                if($task->type=='today-tasks'){

                    $companyEmployees = User::where('company_id',$task->company_id)->whereHas('role',function($query){
                        $query->where('slug','employee');
                    })->get();

                    foreach($companyEmployees as $employee){

                        $tasks = UserTask::with('task.project.client','task.key')->where('user_id',$employee->id)
                                           ->whereDate('date_assigned','=',Carbon::now()->setTimezone($employee->company->timezone)->format('Y-m-d'))
                                           ->get();

                        return view('emails.notifications.today-tasks',compact('user','tasks'));

//                        Mail::send('emails.notifications.incomplete-tasks', ['user'=>$employee,'tasks'=>$tasks], function($message) use($employee)
//                        {
//                            $message->to($employee->email, $employee->name)->subject('Your tasks for today');
//                        });
                    }
                }


                if($task->type=='incomplete-tasks'){

                    $companyEmployees = User::where('company_id',$task->company_id)->whereHas('role',function($query){
                        $query->where('slug','employee');
                    })->whereHas('department',function($query){})->get();

                    $company = Company::where('id',$task->company_id)->first();
                    $currentDay = Carbon::now()->setTimezone($company->timezone)->format('Y-m-d');

                    foreach ($companyEmployees as $employee){

                        $unfinishedTasks = UserTask::where('date_assigned',$currentDay)->count();

                        if($unfinishedTasks){

                            echo $unfinishedTasks . ' Unfinished tasks for '. $employee->name.'<br>';

//                            $message = PushNotification::Message('You might have forgotten to mark some tasks as complete for today.',array(
//                                'custom' => array('intent' => 'task-complete-reminder')
//                            ));
//
//                            $this->push->send($employee,$message);
                        }

                    }
                }

                $next_send = Carbon::now();

                switch($task->interval){
                    case 'd':
                        $next_send = Carbon::now()->addDay()->format('Y-m-d'); break;
                    case 'm':
                        $next_send = Carbon::now()->addMonth()->format('Y-m-d'); break;
                    case 'w':
                        $next_send = Carbon::now()->addDays(7)->format('Y-m-d'); break;
                }

                $next_send = Carbon::parse($next_send.' '.$task->time);

                $task->last_send = $task->next_send;
                $task->next_send = $next_send;
                $task->save();
            }
        }
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function checkRoles(){
        $companyAdmins = User::where('company_id',Auth::user()->company->id)->whereHas('role',function($query){
            $query->where('slug','admin');
        })->pluck('email');

        dd($companyAdmins);
    }
}
