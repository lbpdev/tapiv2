<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Holiday;
use App\Models\Leave;
use App\Models\Option;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Repositories\Eloquent\CanCreateResponseCode;
use Illuminate\Support\Facades\Auth;

class HolidayController extends Controller
{
    use CanCreateResponseCode;

    public function __construct(Holiday $model)
    {
        $this->model = $model;
    }

    /*
    * GET RECORD IF EXISTS
    */

    public function get(){

        $data = $this->model->where('id',$_GET['id'])->first()->toArray();

        if(!$data)
            return response()->json($this->generateResponse('error'));

        $data['from'] = Carbon::parse($data['from'])->setTimezone(Auth::user()->companyTimezone)->format('m/d/Y');
        $data['to'] = Carbon::parse($data['to'])->setTimezone(Auth::user()->companyTimezone)->format('m/d/Y');

        return response()->json($this->generateResponseWithData('create-success',$data));
    }

    public function store(Request $request){
        $input = $request->input();

        if(str_replace(' ', '', $input['name'])=="")
            return response()->json($this->generateResponse('invalid-name'));

        if(str_replace(' ', '', $input['date'])=="")
            return response()->json($this->generateResponse('invalid-date-range'));

        if(Holiday::where('company_id',$this->currentCompanyId())->where('name',$input['name'])->count())
            return response()->json($this->generateResponse('name-exists'));


        $company_id = $this->currentCompanyId();

        if(isset($input['company_id']))
            $company_id = $input['company_id'];

        $date = json_decode($input['date']);

        $holiday = $this->model->create([
            'from' => strtotime($date->start),
            'to' => strtotime($date->end),
            'name' => $input['name'],
            'is_annual' => $input['is_annual'],
            'company_id' => $company_id,
        ]);


        if(isset($input['company_id']))
            $employees = User::where('company_id',$company_id)->get();
        else
            $employees = $this->currentCompany()->users()->get();

        foreach ($employees as $user)
            $user->leaves()->create([
                'from' => strtotime($date->start),
                'to' => strtotime($date->end),
                'type' => 'holiday',
                'holiday_id' => $holiday->id,
            ]);

        if($holiday){
            $data = $this->getAll($company_id);
            $this->generateAnnualHolidaysMeta($company_id);

            return response()->json($this->generateResponseWithData('create-holiday-success',$data));
        }

        return response()->json($this->generateResponse('error'));
    }

    public function update(Request $request){
        $input = $request->input();

        $company_id = $this->currentCompanyId();

        if(isset($input['company_id']))
            $company_id = $input['company_id'];

        $holiday = $this->model->where('id',$input['id'])->first();

        if(!$holiday)
            return response()->json($this->generateResponse('error'));

        if(str_replace(' ', '', $input['name'])=="")
            return response()->json($this->generateResponse('invalid-name'));

        if(str_replace(' ', '', $input['date'])=="")
            return response()->json($this->generateResponse('invalid-date-range'));

        if($holiday->name != $input['name'] && Holiday::where('company_id',$company_id)->where('name',$input['name'])->count())
            return response()->json($this->generateResponse('name-exists'));


        $date = json_decode($input['date']);

        $holiday->update([
            'from' => strtotime($date->start),
            'to' => strtotime($date->end),
            'name' => $input['name'],
            'is_annual' => $input['is_annual'],
            'company_id' => $company_id,
        ]);

        $leaves = Leave::where('holiday_id',$holiday->id)->get();

        foreach ($leaves as $leave)
            $leave->update([
                'from' => strtotime($date->start),
                'to' => strtotime($date->end)
            ]);

        if($holiday){
            $data = $this->getAll($company_id);
            $this->generateAnnualHolidaysMeta($company_id);

            return response()->json($this->generateResponseWithData('update-holiday-success',$data));
        }

        return response()->json($this->generateResponse('error'));
    }

    /*
    * DELETE A RECORD THROUGH ID
    */
    public function delete(Request $request){

        $data = $this->model->where('id',strip_tags($request->input('id')))->first();

        if($data)
            $company_id = $data->company_id;
            if($data->delete()) {
                Leave::where('holiday_id',$data->id)->delete();

                $data = $this->getAll($company_id);

                $this->generateAnnualHolidaysMeta($company_id);

                return response()->json($this->generateResponseWithData('delete-holiday-success',$data));
            }

        return response()->json($this->generateResponse('record-not-exists'));
    }

    public function generateAnnualHolidaysMeta($company_id = null){

        if(!$company_id)
            $company_id = $this->currentCompanyId();

        $holidays = Holiday::where('company_id',$company_id)->where('is_annual',1)->get();

        foreach ($holidays as $holiday){
            $begin = new \DateTime($holiday->from);
            $end = new \DateTime($holiday->to);

            $end = $end->modify( '+1 day' );

            $interval = new \DateInterval('P1D');
            $daterange = new \DatePeriod($begin, $interval ,$end);

            foreach($daterange as $date){
                $dates[$date->format("Y-m-d")] = $date->format("Y-m-d");
            }
        }

        $this->getCompany($company_id)->options()->where('name','annual-holidays')->delete();

        if($dates){
            if($this->getCompany($company_id)->options()->create([
                'name' => 'annual-holidays',
                'value' => json_encode($dates),
            ]))
                return true;
        }
        return false;
    }

    public function getAll($company_id = null){

        if(!$company_id)
            return $this->model->fromCurrentCompany()->orderBy('from')->get();

        return $this->model->fromCompany($company_id)->orderBy('from')->get();
    }
}
