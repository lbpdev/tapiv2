<?php

namespace App\Http\Controllers\Auth;

use App\Models\MobilePasswordReset;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use Illuminate\Support\Facades\Request;

class AuthController extends Controller
{
//    protected $username = 'username';
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

//    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */

    protected $redirectTo = '/calendar/year';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    protected function login(Request $request)
    {
        if(!Auth::attempt($request->only('email', 'password'))){

            $passReset = MobilePasswordReset::where('email',$request->input('email'))
                ->where('status',0)
                ->where('temp_password',$request->input('password'))
                ->first();

            if($passReset){
                $user = User::with('role','department')->where('email', $request->input('email'))->first();
                return response()->json([
                    'status'=>'',
                    'code'=>200,
                    'fromReset'=>1,
                    'data'=>$user
                ]);
            }

            return response()->json([
                'status'=>'Invalid Username or Password',
                'code'=>400,
            ]);
        }

        $user = User::with('role','department')->where('email', $request->input('email'))->first();

        return response()->json([
            'status'=>'',
            'code'=>200,
            'fromReset'=>0,
            'data'=>$user
        ]);

    }

    public function inviteLogin($token)
    {
        $user = User::where('token',$token)->first();

        return view('auth.login',compact('user'));
    }

    public function logout(){
        $user = Auth::user();

        if($user)
            $user->logs()->create(array('activity'=>'logout'));

        Auth::logout();
        return redirect(url('/login'));
    }
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    protected function authenticated( $user)
    {
        
        if(Auth::user()->role->slug == 'admin')
            return redirect(route('calendar.year'));
        else
            return redirect(route('user.calendar.week'));
    }

}
