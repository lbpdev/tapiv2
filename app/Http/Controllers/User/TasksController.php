<?php

namespace App\Http\Controllers\User;

use App\Models\Task;
use App\Models\UserTask;
use App\Repositories\Eloquent\CanCreateResponseCode;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TasksController extends Controller
{

    use CanCreateResponseCode;

    public function __construct(Task $task,UserTask $userTask){
        $this->model = $task;
        $this->userTask = $userTask;
    }

    public function store(Request $request)
    {
        $input = $request->except('project');

        $input['date'] = strtotime($input['date']);
        $input['time'] = 0;

        if($input['department_id']=='general')
            $input['department_id'] = null;

        $task = $this->model->create($input);
        $task->load('project.client','key');

        if($task){
            $input = $request->input();
            $input['date_assigned'] = strtotime($input['date']);
            $input['is_urgent'] = isset($input['is_urgent']) ? 1 : 0 ;
            $date_completed = null;

            $timeRequired = (intval($input['hours'])*60) + intval($input['minutes']);

            $input['time'] = $timeRequired;
            $input['task_id'] = $task->id;
            $input['user_id'] = Auth::user()->id;
            $input['date_completed'] = $date_completed ? strtotime($date_completed) : null;

            $newTask = $this->userTask->create($input);

            if($newTask){

                $data['item'] = $this->generateDropItem($newTask->id);
                $data['column'] = strtotime($input['date']);

                return response()->json($this->generateResponseWithData('create-success',$data));

            }

        }

        return response()->json($this->generateResponse('error'));
    }

    public function assign(Request $request)
    {

    }

    public function generateDropItem($userTask_id){
        $userTask = UserTask::with('task.project.client','task.key')->where('id',$userTask_id)->first();

        $userTask['hours'] = intval($userTask->time/60);
        $userTask['minutes']  = $userTask->time%60;

        $item  = '<li id="'.$userTask_id.'" class="closed ui-state-default clearfix ui-sortable-handle" data-user-task-id="'. $userTask->id .'" data-task-id="'. $userTask->task->id .'" style="background-color:#fff !important;">';
        $item  .= '<div class="">';

        if(isset($userTask->date_completed)){
            if($userTask->date_completed->format('Y')>0){
                $item  .= '<a datacolor="'. $userTask->task->project->client->color .'" data-id="'. $userTask->id .'" data-time="'. $userTask->time .'" class="user-task-icon complete" href="#">';

                $item .= '<div class="icon" style="border:0; background-color: '. $userTask->task->project->client->color .';color: #fff">';

                $item .= $userTask->task->key->code;
                $item .= '</div>';
                $item .= '</a>';
            }
        }
        else {
            $item .= '<a data-color="'. $userTask->task->project->client->color .'" data-id="'. $userTask->id .'" data-time="'. $userTask->time .'" class="user-task-icon"  href="#">';
            $item .= '<div class="icon" style="border-color: '. $userTask->task->project->client->color .';color: '. $userTask->task->project->client->color .'">';
            $item .= $userTask->task->key->code;
            $item .= '</div>';
            $item .= '</a>';
        }
        $item .= '<div class="details" style="color: '. $userTask->task->project->client->color .'">';
        $item .= '<span class="title edit-utask">'. $userTask->task->project->name .'</span>';
        $item .= '<span class="collapse-icon"></span>';
        $item .= '</div>';
        $item .= '</div>';
        $item .= '<div class="">';
        $item .= '<div class="">';
        $item .= '<img class="time-icon" src="'. asset('public/images/icons/task-icons/Clock-Icon.png') .'" height="28">';
        $item .= '</div>';
        $item .= '<div style="width: 33px;"></div>';
        $item .= '<div class="details" style="color: '. $userTask->task->project->client->color .'">';
        $item .= '<span class="time">';
        $item .= $userTask['hours']  ? $userTask['hours'].'hr' : '';
        $item .=  $userTask['minutes']  ? $userTask['minutes'].'min' : '';
        $item .= '</span>';
        $item .= '</div>';
        $item .= '</div>';
        $item .= '<div class="details notes">';
        $item .= '<span class="">'. $userTask->task->note .'</span>';
        $item .= '</div>';
        $item .= '<div class="details actions">';
        $item .= '<span class="icons pull-right" style="border: 0; padding:0">';

        $item .= '<img src="'. asset('public/images/icons/task-icons/Important.png') .'" class="urgent_icon '. ( $userTask->is_urgent ? '' : 'hidden' ) .'" height="22">';

        $item .= '<a target="_blank" href="https://podio.com/login?force_locale=en_US"><img src="'. asset('public/images/icons/task-icons/Podio-Icon.png') .'" height="22"></a>';
        $item .= '<a data-utask-id="'. $userTask->id .'" class="delete-utask" href="#"><img src="'. asset('public/images/icons/task-icons/Forward-Icon.png') .'" height="22"></a>';
        $item .= '</span>';
        $item .= '</div>';
        $item .= '</li>';

        return $item;

    }
}
