<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\ClientBrand;
use App\Models\Department;
use App\Models\Format;
use App\Models\Language;
use App\Models\Leave;
use App\Models\Option;
use App\Models\Project;
use App\Models\Stage;
use App\Models\Task;
use App\Models\User;
use App\Models\UserTask;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Key;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

use App\Repositories\Eloquent\WeekLibrary;

class WeekViewController extends Controller
{

    use WeekLibrary;

    public function __construct(
        Option $option,
        Format $format,
        Client $client,
        Language $language,
        Stage $stage,
        Department $department
    ){
        $this->department = $department;
        $this->option = $option;
        $this->format = $format;
        $this->client = $client;
        $this->stage = $stage;
        $this->language = $language;
    }

    public function index(){

        $formats = $this->format->fromCurrentCompany()->lists('name','id');
        $clients = $this->client->fromCurrentCompany()->select('name','color')->get();

        $weeks = $this->getWeeks();

        if(!Auth::user()->department){
            return view('errors.unassigned');
        }

        $departments = Department::where('id',Auth::user()->department->id)->with('users.department','keys')->get();

        if(!$departments)
            return "Please setup departments";

        if(isset($_GET['day']) && isset($_GET['user']) && isset($_GET['department']) ){
            try {
                Carbon::parse($_GET['day']);
            } catch( \Exception $e){
                return redirect(route('user.calendar.week'));
            }
        }

        $departmentsList = Department::fromCurrentCompany()->withEmployeesOnly()->lists('name','id');
        $keyList = Key::fromCurrentCompany()->lists('name','id');
        $generalKeys = Key::fromCurrentCompany()->where('department_id',null)->get();

        $projectList = $this->generateProjectList();
        $tasks = Task::with('department','project.client','key')->get();

        $workDaysInWeek = $this->getWorkDaysInWeeks();
        $workDays = $this->getWorkDays($this->currentCompanyId());
        $workDaysName = $this->getWorkDaysInt();

        $workHours = $this->option->fromCurrentCompany()->where('name','workhours')->first();
        $workHours = $workHours ? $workHours->value : 8;

        $currentUser = isset($_GET['user']) ? User::where('id',$_GET['user'])->first() : Auth::user();

        if(!$currentUser)
            return redirect(route('user.calendar.week'));

        $currentDepartment = isset($_GET['department']) ? Department::fromCurrentCompany()->where('name',$_GET['department'])->first() : Auth::user()->department;

        if(!$currentDepartment)
            return redirect(route('user.calendar.week'));

        $currentDay = isset($_GET['day']) ? Carbon::parse($_GET['day']) : new \DateTime();

        $currentDay = $currentDay->format('d-m-Y');
        $key = array_search($currentDay, $workDays); // $key = 2;

        $key = -1;
        $key = array_search($currentDay, $workDays); // $key = 2;

        if(!$key){
            $currentDay = Carbon::parse($currentDay)->addDay(1)->format('d-m-Y');
            return redirect(route('user.calendar.week').'?user='.$currentUser['id'].'&department='.$currentDepartment->name.'&day='.$currentDay);
        }

        $currentWeek = $this->getCurrentWeekFull($currentDay, $weeks, $currentUser);

        if(!isset($_GET['day']))
            return redirect(route('user.calendar.week').'?user='.$currentUser['id'].'&department='.$currentDepartment->name.'&day='.$currentWeek[1]['day']);

        $currentTasks = $this->getCurrentWeekTasks($currentWeek,$departments,$currentDepartment);


        if(Auth::user()->department->name != $_GET['department'] || $currentUser->department->id != Auth::user()->department->id)
            return redirect(route('user.calendar.week'));


        return view('pages.user.calendar.week.index',compact(
            'formats','clients','departments','generalKeys','generalKeysArray','departmentsList','projectList','keyList','tasks',
            'workDays','workHours','currentDay','currentWeek','workDaysName','currentTasks','currentUser','currentDepartment'
        ));
    }
//
//    public function generateProjectList(){
//        $projects = Project::with('client','brand')->get();
//        $data = [];
//
//        foreach($projects as $index=>$project){
//            $data[$index]['label'] = $project->name . ' ( ' . $project->client->name . ' ' . $project->brand->name . ' )';
//            $data[$index]['value'] = $project->id;
//            $data[$index]['brand'] =  $project->brand->name;
//            $data[$index]['client'] = $project->client->name;
//            $data[$index]['deadline'] = $project->end_at ? $project->end_at->format('m/d/Y') : "N/A";
//        }
//
//        return json_encode($data);
//
//    }
//
//    private function getWorkDays(){
//
//        $workDays = Option::where('name','work-days')->first();
//
//        if(!$workDays){
//            $weekends = ['Fri','Sat'];
//            $data = "";
//            $begin = new \DateTime( '2016-01-01' );
//            $end = new \DateTime( '2017-01-01' );
//            $end = $end->modify( '+1 day' );
//
//            $interval = new \DateInterval('P1D');
//            $daterange = new \DatePeriod($begin, $interval ,$end);
//
//            $daysInWeek = [];
//            $end = null;
//            $start = null;
//            $lastDate = null;
//            $workDays = array();
//            $weekIndex = 0;
//
//            foreach($daterange as $date){
//                if(!in_array($date->format("D"),$weekends)){
//                    if($date->format("D")=='Sun')
//                        $weekIndex++;
//
//                    $workDays[$weekIndex][] = $date->format('d-m-Y');
//                }
//            }
//
//            Option::create(array('name'=>'work-days','value'=>json_encode($workDays)));
//            return $workDays;
//        }
//
//        return json_decode($workDays->value);
//    }
//
//    private function getWorkDaysForSelect($workDays){
//
//        $weekends = ['Fri','Sat'];
//        $weekIndex = 0;
//        $lastDay = 1;
//
//        $workDaysSelect = Option::where('name','work-days-select')->first();
//
//        $workDays = json_decode(json_encode($workDays), true);
//
//        if(!$workDaysSelect){
//            foreach($workDays as $mIndex=>$week){
//
//                $daysinWeek = count($week) - 1;
//                foreach($week as $wIndex=>$date){
//                    $date = \Nesbot\Carbon::parse($date);
//                    $month = $date->format('M');
//
//                    if(!in_array($date->format("D"),$weekends)){
//
//                        if($date->format("D")=='Sun'){
//                            $lastDay = Carbon::parse($workDays[$mIndex][$daysinWeek])->format('d');
//                            $data[$date->format('d').'-'.$date->format('m')] = $month . ' ' . $date->format('d') . ' - ' . $lastDay;
//                        }
//                    }
//
//                }
//            }
//
//            Option::create(array('name'=>'work-days-select','value'=>json_encode($data)));
//            return $data;
//        }
//
//        foreach (json_decode($workDaysSelect->value) as $key=>$day)
//            $data[$key] = $day;
//
//        return $data;
//    }
//
//    private function getCurrentWeek($currentDay, $workWeeks , $currentUser){
//        $data= [];
//        $addCount = 0;
//        $userLeaves = $this->getUserLeaves($currentUser->id);
//
//        while(count($data)<1){
//            foreach($workWeeks as $index=>$week){
//                if(in_array($currentDay,$week)){
//                    foreach($week as $day){
//                        $d = strtotime($day);
//                        array_push($data , ['day'=>$day,'daym'=>date("d", $d).'-'.date("m", $d), 'year'=> Carbon::parse($day)->format('Y')]);
//                    }
//                    break;
//                }
//            }
//
//            $currentDay = Carbon::parse($currentDay)->addDay()->format('d-m-Y');
//            $addCount++;
//        }
//
//        dd('kalas');
//        foreach($data as $index=>$week){
//            $data[$index]['tasks'] = UserTask::with('task.project.client','task.key')->where('user_id',$currentUser->id)->where('date_assigned',Carbon::parse($week['day'])->format('Y-m-d'))->get();
//
//
//            if(in_array(Carbon::parse($week['day'])->format('Y-m-d'),$userLeaves))
//                $data[$index]['on_leave'] = true;
//            else
//                $data[$index]['on_leave'] = false;
//
//            if(count($data[$index]['tasks'])){
//                foreach($data[$index]['tasks'] as $i=>$task){
//                    $data[$index]['tasks'][$i]->hours = intval($task->time/60);
//                    $data[$index]['tasks'][$i]->minutes  = $task->time%60;
//                }
//            }
//        }
//
//        return $data;
//    }
//
//
//    private function getUserLeaves($user_id)
//    {
//        $leaves = Leave::where('user_id',$user_id)->get();
//
//        $dates = [];
//
//        foreach ($leaves  as $key => $leave) {
//            $begin = new \DateTime($leave->from);
//            $end = new \DateTime($leave->to);
//
//            $end = $end->modify( '+1 day' );
//
//            $interval = new \DateInterval('P1D');
//            $daterange = new \DatePeriod($begin, $interval ,$end);
//
//            foreach($daterange as $date){
//                $dates[] = $date->format("Y-m-d");
//            }
//
//        }
//
//        return $dates;
//    }
//
//    private function getCurrentWeekTasks($currentWeek,$departments,$currentDepartment){
//        $tasks = [];
//
//        $uncompleted = Task::with('department','project.client','key')
//            ->where('department_id',$currentDepartment->id)
//            ->where('date','<',Carbon::parse($currentWeek[0]['day'])->format('Y-m-d'))
//            ->where('time','>',0)
//            ->get()->toArray();
//
//        foreach ($uncompleted as $u){
//            $u['hours'] = intval($u['time']/60);
//            $u['minutes'] = $u['time']%60;
//            array_push($tasks,$u);
//        }
//
//        $generalTasks = Task::with('department','project.client','key')
//            ->whereNull('department_id')
//            ->where('date','<',Carbon::parse($currentWeek[count($currentWeek)-1]['day'])->format('Y-m-d'))
//            ->where('time','>',0)
//            ->get()->toArray();
//
//        foreach ($generalTasks as $u){
//            $u['hours'] = intval($u['time']/60);
//            $u['minutes'] = $u['time']%60;
//            array_push($tasks,$u);
//        }
//
//        foreach($currentWeek as $index=>$week){
//            $task = Task::with('department','project.client','key')
//                ->where('department_id',$currentDepartment->id)
//                ->where('time','>',0)
//                ->where('date',Carbon::parse($week['day'])->format('Y-m-d'))->get()->toArray();
//
//            if($task){
//                foreach ($task as $index=>$t){
//                    if(!in_array($t, $tasks, true)){
//                        $t['hours'] = intval($t['time']/60);
//                        $t['minutes'] = $t['time']%60;
//                        array_push($tasks,$t);
//                    }
//                }
//            }
//        }
//        return $tasks;
//    }


}