<?php

namespace App\Http\Controllers;

use App\Models\Format;
use App\Models\Language;
use App\Models\Option;
use App\Models\Project;
use App\Models\Stage;
use App\Models\Client;
use App\Services\ColorServer;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

class PrintController extends Controller
{

    use ColorServer;

    public function __construct(
        Option $option,
        Format $format,
        Client $client,
        Language $language,
        Stage $stage
    ){
        $this->option = $option;
        $this->format = $format;
        $this->client = $client;
        $this->stage = $stage;
        $this->language = $language;
    }

    public function generateYearPrint(Request $request){

        $currentYear = Carbon::now()->setTimezone(Auth::user()->company->timezone)->format('Y');

        if(isset($_POST['year'])){
            $checkYear = date_parse($_POST['year']);

            if(!$checkYear['error_count']){
                $currentYear = $_POST['year'];
            } else {
                return Redirect::to(route('calendar.year'));
            }
        }

        $workDayStart = $this->option->fromCurrentCompany()->where('name','workdays_from')->first();
        $workDayEnd = $this->option->fromCurrentCompany()->where('name','workdays_to')->first();

        $formats = $this->format->fromCurrentCompany()->lists('name','id');
        $stagesIn = $this->stage->fromCurrentCompany()->where('internal',1)->orderBy('order','ASC')->get();
        $stagesEx = $this->stage->fromCurrentCompany()->where('internal',0)->orderBy('order','ASC')->get();
        $stagesList = $this->stage->fromCurrentCompany()->orderBy('name','ASC')->get();

        $languages = $this->language->lists('name','id');
        $clientsAll = $this->client->fromCurrentCompany()->get();

        $colors = $this->getColors();
        $clients = [];
        foreach($clientsAll as $index=>$client){
            $clients[$index]['label'] = $client->name;
            $clients[$index]['value'] = $client->color;
        }
        $clients = json_encode($clients);

        $weekends = $this->getWeekends(
            $workDayStart ? $workDayStart->value : 1,
            $workDayEnd ? $workDayEnd->value : 5
        );

        $weeksPerMonth = $this->getWeeksPerMonth($currentYear,true);
        $weekDivisions = $this->generateWeekDiv($weekends);

        $weekDivisionsLeft = $weekDivisions['left'];
        $weekDivisionsWidth = $weekDivisions['width'];

        $dateHeadersModal = $this->generateDateHeaderFull();

        $projectsQuery = Project::query();

        if(isset($_POST['filter'])){

            if($request->input('filter')=='client'){
                $projectsQuery = Client::query();
                $projectsQuery->with([
                    'projects' => function ($query) use($currentYear) {
                        $query->fromYear($currentYear)->whereNull('completed_at')->orderBy('name', 'asc');
                    }
                    , 'projects.stages.stage'
                ])->fromCurrentCompany()->orderBy('name','asc');
            }

            elseif($request->input('filter')=='format'){
                $projectsQuery = Format::query();
                $projectsQuery->with([
                    'projects' => function ($query) use($currentYear) {
                        $query->fromYear($currentYear)->whereNull('completed_at')->orderBy('name', 'asc');
                    }
                    , 'projects.stages.stage'
                ])->fromCurrentCompany()->orderBy('name','asc');

            }

            elseif($request->input('filter')=='archive'){
                $projectsQuery = Project::query();
                $projectsQuery->with('stages.stage')->fromCurrentCompany()->whereNotNull('completed_at')->orderBy('name','asc');
            }

            elseif($request->input('filter')=='project'){
                $projectsQuery = Project::query();
                $projectsQuery->with('stages.stage')->fromCurrentCompany()->whereNull('completed_at')->fromYear($currentYear)->orderBy('name','asc');
            }

        } else {
            $projectsQuery->fromCurrentCompany()->whereNull('completed_at')->fromYear($currentYear)->orderBy('name', 'asc');
        }

//        $projectsQuery = $projectsQuery->fromYear($currentYear);
        $projectsInFormat = $projectsQuery->get();
        $projects = [];
        $index = 0;
        $counter = 0;

        foreach ($projectsInFormat as $project){

            if($counter<5)
            {
                $projects[$index][] = $project;
            }
            else
            {
                $counter = 0;
                $index++;
                $projects[$index][] = $project;
            }
            $counter++;
        }

        if(isset($_POST['filter'])){
            if($request->input('filter')=='project'||$request->input('filter')=='archive')
                $projectsInFormatHtml = $this->generateProjectStages($projectsInFormat,$weekDivisionsLeft,$weekDivisionsWidth,20);
            else
                $projectsInFormatHtml = $this->generateProjects($projectsInFormat,$weekDivisionsLeft,$weekDivisionsWidth,20);
        }
        else
            $projectsInFormatHtml = $this->generateProjects($projectsInFormat,$weekDivisionsLeft,$weekDivisionsWidth,20);

//        $projectsQuery->with('stages.stage')->fromCurrentCompany()->fromYear($currentYear)->whereNull('completed_at')->orderBy('name','asc');

//        $projectsInFormat = $projectsQuery->fromCurrentCompany()->fromYear($currentYear)->get();
//
//        $projectsInFormatHtml = $this->generateProjectStages($projectsInFormat,$weekDivisionsLeft,$weekDivisionsWidth,20);

        $firstWorkDay = $this->currentCompany()->firstDay;
        $company = $this->currentCompany();

        $projectsInFormat = $this->generateProjectList($projectsInFormat);

//        if(env('APP_PROTOCOL')=="HTTPS")
//        {

        $pdf = App::make('snappy.pdf.wrapper');
        $pdf->loadView(
            'pages.admin.calendar.year.print',compact(
            'weekends','formats','clients','languages','firstWorkDay',
            'projectsInFormat','projectsInFormatHtml','stagesIn','stagesEx',
            'dateHeaders','dateHeadersModal','dateDivs','weekDivisionsLeft',
            'weekDivisionsWidth','colors','weeksPerMonth','subscription','stagesList','company')
        )->setPaper('a4')->setOrientation('landscape');

        return $pdf->inline();
//        }
//        else
//        {

            return view('pages.admin.calendar.year.print',compact(
                'weekends','formats','clients','languages','firstWorkDay',
                'projectsInFormat','projectsInFormatHtml','stagesIn','stagesEx',
                'dateHeaders','dateHeadersModal','dateDivs','weekDivisionsLeft',
                'weekDivisionsWidth','colors','weeksPerMonth','subscription','stagesList','company'));
//        }
//        return $pdf->download(Auth::user()->company->name.' Schedule - '.Carbon::now()->setTimezone(Auth::user()->company->timezone)->format('d-m-Y'));

    }


    public function generateProjectList($projectsInFormat){
        $html = "";

        foreach($projectsInFormat as $format){
            if(count($format->projects)){
                $html .= '<li class="category"><a href="#">'.$format->name.'</a></li>';

                foreach($format->projects as $project)
                    $html .= '<li><a href="#">'.$project->name.'</a></li>';
            }
            else{
                if(isset($_POST['filter'])){
                    if($_POST['filter']=='project'||$_POST['filter']=='archive'){
                        $html .= '<li><a href="#">'.$format->name.'</a></li>';
                    }
                }
                elseif(isset($_POST['keyword']))
                    $html .= '<li><a href="#">'.$format->name.'</a></li>';
            }
        }

        return $html;
    }

    public function generateProjectStages($projects,$weekDivisionsLeft,$weekDivisionsWidth,$colWidth){

        $html = "";

        foreach($projects as $id=>$project){

            $html .= '<li class="row-item"><a href="#'.$project->id.'"><ul class="stages">';

            if(count($project->stages)){
                foreach($project->stages as $stage){
                    $left = 0;
                    $width = $colWidth;

                    if($stage->start_at){

                        $color = $stage->color;
                        $start_month = $stage->start_at->format('m-Y');

                        $start_day = (int)$stage->start_at->format('d');
                        $end_day = (int)$stage->end_at->format('d');

                        $end_month = (int)$stage->end_at->format('m');
                        $start_year = (int)$stage->end_at->format('Y');


                        if(isset($weekDivisionsLeft[$start_month])){
                            /**
                             *  START OF LEFT POSITIONING
                             */


                            foreach($weekDivisionsLeft as $key=>$monthData){
                                if($key != $stage->start_at->format('m-Y'))
                                    $left += count($monthData) * $colWidth;
                                else
                                    break;
                            }

                            foreach($weekDivisionsLeft[$start_month] as $key=>$weekDatas){
                                if(isset($weekDivisionsLeft[$start_month][$key])){
                                    if(in_array($start_day,$weekDivisionsLeft[$start_month][$key])){
                                        $left += ($key)  * $colWidth;
                                        break;
                                    }
                                }
                            }

                            /**
                             *  START OF WIDTH COMPUTATION
                             */

                            $monthToSearch = $weekDivisionsWidth[$start_month];
                            $startAdding = false;
                            $run = true;

                            while($run){

                                foreach($monthToSearch as $key=>$weekDatas){
                                    if(in_array($stage->start_at->format('d-m-Y'),$monthToSearch[$key]))
                                        $startAdding = true;

                                    if(in_array($stage->end_at->format('d-m-Y'),$monthToSearch[$key])){
                                        $run = false;
                                        break;
                                    }

                                    if($startAdding)
                                        $width += $colWidth;
                                }

                                $start_month = Carbon::parse('1-'.$start_month)->addMonth()->format('m-Y');

                                if(isset($weekDivisionsWidth[$start_month]))
                                    $monthToSearch = $weekDivisionsWidth[$start_month];
                                else
                                    $run = false;

                            }
                        } else {
                            $left = 0;
                            $monthCount = 0;
                            $weekCount = 0;
                            $done = false;

                            if(isset($weekDivisionsWidth[$stage->end_at->format('m-Y')])){
                                foreach($weekDivisionsWidth as $monthIndex=>$month){
                                    if(!$done){
                                        foreach($month as $weekIndex=>$week){
                                            if(!$done){
                                                foreach($week as $dayIndex=>$day){
                                                    if(!$done){
                                                        if($day==$stage->end_at->format('d-m-Y')){
                                                            $done = true;
                                                        }
                                                    }
                                                }
                                                $width += $colWidth;
                                            }
                                        }
                                    }
                                }
                            } else {
                                $width = 0;
                            }

                        }

                        $html .= '<li class="viewStagesLink" style="background-color: '.$color.'; width: '.$width.'px; left: '.$left.'px;"> </li>';

                    }
                }
            }

            $html .= '</ul></a></li>';
        }


        return $html;
    }





    public function generateProjectStagesPaginated($projects,$weekDivisionsLeft,$weekDivisionsWidth,$colWidth){

        $data = [];
        $counter = 0;
        $index = 0;


        foreach($projects as $index=>$projs){
            $html = "";
            foreach($projs as $id=>$project){

                $html .= '<li class="row-item"><a href="#'.$project->id.'"><ul class="stages">';

                if(count($project->stages)){
                    foreach($project->stages as $stage){
                        $left = 0;
                        $width = $colWidth;

                        if($stage->start_at){

                            $color = $stage->color;
                            $start_month = $stage->start_at->format('m-Y');

                            $start_day = (int)$stage->start_at->format('d');
                            $end_day = (int)$stage->end_at->format('d');

                            $end_month = (int)$stage->end_at->format('m');
                            $start_year = (int)$stage->end_at->format('Y');


                            if(!isset($weekDivisionsLeft[$start_month]))
                                continue;

                            /**
                             *  START OF LEFT POSITIONING
                             */


                            foreach($weekDivisionsLeft as $key=>$monthData){
                                if($key != $stage->start_at->format('m-Y'))
                                    $left += count($monthData) * $colWidth;
                                else
                                    break;
                            }

                            foreach($weekDivisionsLeft[$start_month] as $key=>$weekDatas){
                                if(isset($weekDivisionsLeft[$start_month][$key])){
                                    if(in_array($start_day,$weekDivisionsLeft[$start_month][$key])){
                                        $left += ($key)  * $colWidth;
                                        break;
                                    }
                                }
                            }

                            /**
                             *  START OF WIDTH COMPUTATION
                             */

                            $monthToSearch = $weekDivisionsWidth[$start_month];
                            $startAdding = false;
                            $run = true;

                            while($run){

                                foreach($monthToSearch as $key=>$weekDatas){

                                    if(in_array($stage->start_at->format('d-m-Y'),$monthToSearch[$key]))
                                        $startAdding = true;

                                    if(in_array($stage->end_at->format('d-m-Y'),$monthToSearch[$key])){
                                        $run = false;
                                        break;
                                    }

                                    if($startAdding)
                                        $width += $colWidth;
                                }

                                $start_month = Carbon::parse('1-'.$start_month)->addMonth()->format('m-Y');

                                if(isset($weekDivisionsWidth[$start_month]))
                                    $monthToSearch = $weekDivisionsWidth[$start_month];
                                else
                                    $run = false;

                            }

                            $html .= '<li class="viewStagesLink" style="background-color: '.$color.'; width: '.$width.'px; left: '.$left.'px;"> </li>';

                        }
                    }
                }

                $html .= '</ul></a></li>';
            }
            $data[$index][] = $html;
        }

        return $data;
    }

    public function generateProjects($projectsInFormat,$weekDivisionsLeft,$weekDivisionsWidth,$colWid = 50){
        $html = "";
        foreach($projectsInFormat as $format){
            if(count($format->projects)){
                $html .= '<li class="row-item">';
                $html .= '<ul class="stages blank"></ul>';
                $html .= $this->generateProjectStages($format->projects,$weekDivisionsLeft,$weekDivisionsWidth,$colWid);
                $html .= '</li>';
            }
        }

        return $html;
    }



    public function getWeekends($start,$end) {
        $days = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'];
        $start = $days[$start];
        $end = $days[$end];
        $weekends = [];
        $stop = false;

        $loop = 0;

        $weekendStarted = false;

        while(!$stop) {
            if($loop<7) {

                if($days[$loop]==$end && !$weekendStarted){
                    $weekendStarted = true;
                }

                elseif($days[$loop]!=$start && $weekendStarted) {
                    array_push($weekends,$days[$loop]);
                }

                elseif( count($weekends) > 0 && $days[$loop]==$start )
                    $stop = true;

                $loop++;
            } else
                $loop = 0;
        }

        return $weekends;
    }

    function getWeeksPerMonth($year,$single = null)
    {

        $months = [];
        $year = intval($year);


        if($single){
            for ($m=1; $m<=12; $m++) {
                $months[$year][$m]['name'] = date('F', mktime(0,0,0,$m, 1, $year));

                $weeks = cal_days_in_month(CAL_GREGORIAN, $m, $year) / 7;
                $months[$year][$m]['weeks'] = $weeks > 4 ? 5 : 4;

            }
        } else {
            for ($y=($year-1); intval($y)<=($year+1); $y++) {
                for ($m=1; $m<=12; $m++) {
                    $months[$y][$m]['name'] = date('F', mktime(0,0,0,$m, 1, $y));

                    $weeks = cal_days_in_month(CAL_GREGORIAN, $m, $y) / 7;
                    $months[$y][$m]['weeks'] = $weeks > 4 ? 5 : 4;

                }
            }
        }

        return $months;
    }


    public function generateWeekDiv($weekends){
        $data = "";

        $year = Carbon::now()->setTimezone(Auth::user()->company->timezone);

        if(isset($_POST['year'])){
            $checkYear = date_parse($_POST['year']);

            if($checkYear['error_count']==0){
                $year = Carbon::parse($_POST['year'].'-01-01');
            }
        }

        $begin = new \DateTime( $year->format('Y').'-01-01' );
        $end = new \DateTime( $year->addYear()->format('Y').'-01-01' );

        $end = $end->modify( '+1 day' );

        $interval = new \DateInterval('P1D');
        $daterange = new \DatePeriod($begin, $interval ,$end);
        $dateRangeArray = [];

        foreach($daterange as $date)
            array_push($dateRangeArray, $date);

        $end = null;
        $start = null;
        $lastDate = null;
        $weekResetCounter = 0;

        $data = [];
        $dateDivsIndex = 0;
        $currentMonth = '01';
        $mon = 1;

        foreach($dateRangeArray as $index=>$day){

            if($currentMonth!=$day->format('m'))   {
                $dateDivsIndex = 0;
                $weekResetCounter = 0;
                $mon++;
            }

            if(isset($dateRangeArray[$index+1])) {

                if ($weekResetCounter > 6) {
                    $dateDivsIndex++;
                    $weekResetCounter = 0;
                }

                $data['width'][$day->format('m-Y')][$dateDivsIndex][] = $day->format('d-m-Y');
                $data['left'][$day->format('m-Y')][$dateDivsIndex][] = $day->format('d');

                $weekResetCounter++;
            }

            $currentMonth = $day->format('m');
        }

        return $data;
    }


    public function generateDateHeaderFull(){
        $data = "";

        $year = Carbon::now()->setTimezone(Auth::user()->company->timezone);

        if(isset($_POST['year'])){
            $checkYear = date_parse($_POST['year']);

            if($checkYear['error_count']==0){
                $year = Carbon::parse($_POST['year']);
            }
        }

        $begin = new \DateTime( $year->subYear(1)->format('Y').'-01-01' );
        $end = new \DateTime( $year->addYear(3)->format('Y').'-01-01' );
        $end = $end->modify( '+1 day' );

        $interval = new \DateInterval('P1D');
        $daterange = new \DatePeriod($begin, $interval ,$end);

        $end = null;
        $start = null;
        $lastDate = null;
        $workDays = array();

        foreach($daterange as $date)
            array_push($workDays, $date);

        $month = 0;
        $currentMonth = Carbon::now()->setTimezone(Auth::user()->company->timezone)->format('F');
        $currentMonthYear = Carbon::now()->setTimezone(Auth::user()->company->timezone)->format('m-Y');
        $data = "";

        foreach($workDays as $index=>$day){

            if(isset($workDays[$index+1])){

                if($month!=idate('m',strtotime($day->format('d-m-Y')))){

                    $daysInMonth = cal_days_in_month(CAL_GREGORIAN, $day->format('m'), $day->format('Y'));
                    $daysInMonth *= 20;

                    if($month != 0 ){
                        $data .='</ul></li>';
                    }

                    $data .='<li class="clearfix">'.
                        '<div style="width:'.$daysInMonth.'px" class="month '. ($day->format('m-Y') == $currentMonthYear ? 'activeMonth' : ' ' ) .'">'.$day->format('F').'</div>'.
                        '<ul class="days" style="width:'.$daysInMonth.'px">';

                    $month = $day->format('m');
                }

                $dayInt = idate('d',strtotime($day->format('d-m-Y')));

                $data .='<li data-date="'.$day->format('d-m-Y').'">'. $dayInt . '</li>';
            }
        }

        return $data;
    }


}
