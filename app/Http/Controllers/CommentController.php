<?php

namespace App\Http\Controllers;

use App\Models\TaskComment;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use App\Repositories\Eloquent\CanCreateResponseCode;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Logging\Log;

class CommentController extends Controller
{
    use CanCreateResponseCode;

    public function __construct(Log $log)
    {
        $this->log = $log;

        $this->log->useDailyFiles(storage_path().'/logs/notifications.log');
        return $this->validatesRequestErrorBag;
    }

    public function store(Request $request){

        $comment = TaskComment::where('date',$request->input('date'))->
                                where('user_id',Auth::user()->id)->
                                where('author_id',Auth::user()->id)->
                                first();
        $user = Auth::user();

        $new = false;

        $companyAdmins = User::where('company_id',Auth::user()->company->id)->whereHas('role',function($query){
            $query->where('slug','admin');
        })->lists('email');

        if(!$comment){
            $comment = TaskComment::create([
                'date'=>$request->input('date'),
                'user_id'=>Auth::user()->id,
                'author_id'=>Auth::user()->id,
                'comment'=>$request->input('comment')
            ]);
            $new = true;
        }
        else
            $comment->update([
                'comment'=>$request->input('comment')
            ]);

        if($comment){
            $data = $comment;

            if($new){
                foreach($companyAdmins as $admin){

                    if(Auth::user()->email != $admin){

                        $this->log->info('Sending Comment Email to:'. $admin);

                        Mail::send('emails.notifications.comment-made', ['date'=>$comment->date,'user'=>$user,'department'=>$user->department,'comment'=>$comment->comment], function($message) use($admin,$user)
                        {
                            $message->from('no-reply@tapiapp.com', $user->name);
                            $message->to($admin, 'Administrator')->subject($user->name. ' commented on today’s schedule.');
                        });
                    }
                }
            }

            if(str_replace(' ', '', $comment->comment)=="")
                $comment->delete();

            return response()->json($this->generateResponseWithData('create-success',$data));
        }

        return response()->json($this->generateResponse('error'));
    }

    public function get(){
        $comments = TaskComment::where('date',$_GET['date'])->where('user_id',$_GET['user_id'])->get();
        return response()->json($comments);
    }

}
