<?php

namespace App\Http\Controllers\Mobile;

use App\Models\MobileApiToken;
use App\Models\Option;
use App\Models\User;
use App\Models\LoginAttempt;
use App\Repositories\Eloquent\CanCreateResponseCode;
use App\Repositories\Eloquent\MobileApiTokenLibrary;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Models\MobilePasswordReset;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    use CanCreateResponseCode;

//    protected $username = 'username';
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */


    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(MobileApiTokenLibrary $tokenLibrary)
    {
        $this->tokenLibrary = $tokenLibrary;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    protected function authenticated( $user)
    {
        if(Auth::user()->role->slug == 'admin')
            return redirect(route('calendar.year'));
        else
            return redirect(route('user.calendar.week'));
    }

    protected function login(Request $request)
    {
        $token = Session::get('_token');

        if($request->has('uuid'))
            $token = $request->input('uuid');

        $lockOut = $this->checkAttempts($token);

        $passReset = MobilePasswordReset::where('email',$request->input('email'))
            ->where('status',0)
            ->where('temp_password',$request->input('password'))
            ->first();

        $field = filter_var($request->input('email'), FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        $request->merge([$field => $request->input('email')]);

        if($passReset){

            $user = User::with('role','department')->where($field, $request->input('email'))->first();

            return response()->json([
                'message'=>'Password reset detected.',
                'status'=>200,
                'fromReset'=>1,
                'data'=>$user
            ]);
        }

        $userNotActive = User::with('role','department')->where($field, $request->input('email'))->where('is_active', 0)->first();

        if($userNotActive)
            return response()->json([
                'message'=>'This user account has been deactivated by your company administrator.',
                'status'=>400,
            ]);

        if($lockOut)
            return response()->json([
                'message'=>'Too many login attempts.<br> Please try again after '.$lockOut.' minutes',
                'status'=>400,
            ]);

        if(!Auth::attempt($request->only($field, 'password'))){

            $user = User::with('role','department')->where($field, $request->input('email'))->first();

            if(!$user)
                return response()->json([
                    'message'=>'Invalid Username or Password',
                    'status'=>400,
                ]);

            LoginAttempt::create(array(
                    'email'=>$request->input('email'),
                    'password'=>$request->input('password'),
                    'ip'=>$request->ip(),
                    'token'=>$token
                ));

            return response()->json([
                'message'=>'Invalid Username or Password',
                'status'=>400,
            ]);
        }

        $user = User::with('role','department','company.options')->where($field, $request->input('email'))->first();

        if($request->has('uuid'))
            $token = $this->tokenLibrary->generateToken($user,$request->input('uuid'));

        if($request->has('uuid'))
            $user->logs()->create(array('activity'=>'login'));
        else
            $user->logs()->create(array('activity'=>'login','device'=>'mobile'));

        if(isset($token)){
            $user->token = $token;
            $companyLogo = Option::where('name','logo')->where('company_id',Auth::user()->company_id)->first();

            $user->company_logo = $companyLogo ? $companyLogo->value : null;
        }


        return response()->json([
            'message'=>'',
            'status'=>200,
            'fromReset'=>0, 
            'data'=>$user
        ]);

    }

    protected function resetEmail(Request $request)
    {
        $input = $request->input();

        if(!isset($input['email']))
            return response()->json([
                'message'=>'Invalid Token Email',
                'status'=>400,
            ]);

        $field = filter_var($request->input('email'), FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        $request->merge([$field => $request->input('email')]);

        $user = User::where($field,$input['email'])->first();

        if(!$user)
            return response()->json([
                'message'=>'Sorry, this email address does not appear to be registered on TapiApp. Please try again. ',
                'status'=>400,
            ]);


        MobilePasswordReset::where('email',$user['email'])->where('status',0)->delete();

        $newPass = Str::random(6);

        $newPassword = MobilePasswordReset::create([
            'email' => $user['email'],
            'temp_password' => $newPass,
            'status' => 0
        ]);

        Mail::send('emails.password-reset', ['temp_password' => $newPass,'email' => $user['email']], function($message) use($user)
        {
            $message->to($user['email'], $user['name'])->subject('Password reset for TapiApp');
        });

        if($newPassword)
            return response()->json([
                'status' => 200,
                'message' => "",
            ]);

        return response()->json([
                'message'=>'Invalid Token Email',
                'status'=>400,
            ]);

    }

    protected function resetPassword(Request $request)
    {
        $input = $request->input();

        if(!isset($input['email']))
            return response()->json([
                'message'=>'Invalid Token Email',
                'status'=>400,
            ]);

        if($input['password']==$input['password_confirm'] && strlen($input['password'])<6)
            return response()->json([
                'message'=>'Password is too short. Please enter atleast 6 characters',
                'status'=>400,
            ]);

        if($input['password']!=$input['password_confirm'])
            return response()->json([
                'message'=>'Passwords did not match.',
                'status'=>400,
            ]);

        $field = filter_var($request->input('email'), FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        $request->merge([$field => $request->input('email')]);

        $user = User::where($field,$input['email'])->first();
        $passData = MobilePasswordReset::where('email',$input['email'])->where('temp_password',$input['temp_password'])->where('status',0)->first();

        if($passData && $user){

            $user->password = Hash::make($input['password']);
            $user->save();

            $passData->status = 1;
            $passData->save();

            return response()->json([
                'status' => 200,
                'message' => "",
                'data' => $user
            ]);
        }

        return response()->json([
                'message'=>'An error has occurred. Please try again.',
                'status'=>400,
            ]);

    }


    public function logout(Request $request){

        $loggedOut = MobileApiToken::where('uuid',$request->input('uuid'))->where('token',$request->input('token'))->delete();

        if($loggedOut)
            return response()->json($this->generateResponse('success'));

        return response()->json($this->generateResponse('error'));
    }


    public function checkAttempts($token){

        $attempt_range = Carbon::now()->subMinutes(15);

        $attempts = LoginAttempt::where('token',$token)->where('created_at','>',$attempt_range)->count();

        if($attempts>4){
            $last_attempt = LoginAttempt::where('token',$token)->orderBy('id','DESC')->first();
            $last_attempt = Carbon::parse($last_attempt->created_at);

            return 15 - $last_attempt->diffInMinutes(Carbon::now());
        }

        return false;
    }
}
