<?php

namespace App\Http\Controllers\Mobile;

use App\Models\DeviceToken;
use App\Models\MobileApiToken;
use App\Models\Option;
use App\Models\User;
use App\Models\UserLog;
use App\Repositories\Eloquent\MobileApiTokenLibrary;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\Eloquent\CanCreateResponseCode;
use Illuminate\Support\Facades\Session;

class TokenController extends Controller
{
    use CanCreateResponseCode;

    public function __construct(MobileApiToken $model, MobileApiTokenLibrary $tokenLibrary)
    {
        $this->model = $model;
        $this->tokenLibrary = $tokenLibrary;
    }

    public function get(Request $request){
        $token = $this->tokenLibrary->getToken($request);

        if($token){
            $user = User::with('role','department','company.options')->where('id', $token->user_id)->first();

            if($user){
                $user->token = $token;

                $companyLogo = Option::where('name','logo')->where('company_id',$user->company_id)->first();

                $user->company_logo = $companyLogo ? $companyLogo->value : null;

                if( UserLog::where('user_id',$token->user_id)->
                    where('activity','login')->
                    where('device','mobile')->
                    whereDate('created_at','=',Carbon::now()->format('Y-m-d'))->
                    count() < 1){
                    UserLog::create(['user_id' => $token->user_id,'activity' => 'login','device'=>'mobile']);
                }

//                if($request->has('push_token')){
//                    $existitng = DeviceToken::where('push_token',$request->input('push_token'))->first();
//
//                    if(!$existitng){
//                        $user->devices()->create([
//                            'push_token' => $request->input('push_token'),
//                            'os' => $request->input('os')
//                        ]);
//                    }
//                }

                return response()->json($this->generateResponseWithData('token-found',$user));
            }
        }

        return response()->json($this->generateResponse('invalid-token'));

    }
}
