<?php

namespace App\Http\Controllers\Mobile;

use App\Models\MobileApiToken;
use App\Models\User;
use App\Repositories\Eloquent\CanCreateResponseCode;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class PasswordController extends Controller
{
    use CanCreateResponseCode;

    public function updatePw(Request $request)
    {
        $input = $request->input();

        $token = MobileApiToken::where('token',$input['token'])->where('uuid',$input['uuid'])->first();

        if($token){
            $user = User::find($token->user_id);

            if($user){

                if(!Hash::check($input['current_password'],$user->password))
                    return response()->json($this->generateResponse('wrong-current-password'));

                if($user->update(['password'=>Hash::make($input['new_password'])]))
                    return response()->json($this->generateResponse('user-updated-success'));

            }
        } else {
            return response()->json($this->generateResponse('invalid-api-token'));
        }

        return response()->json($this->generateResponse('error'));
    }
}
