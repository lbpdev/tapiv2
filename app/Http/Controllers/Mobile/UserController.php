<?php

namespace App\Http\Controllers\Mobile;

use App\Models\Company;
use App\Models\Department;
use App\Models\MobileApiToken;
use App\Models\User;
use App\Repositories\Eloquent\CanCreateResponseCode;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    use CanCreateResponseCode;

    public function __construct(User $user)
    {
        $this->model = $user;
    }

    public function updateName(Request $request)
    {
        $input = $request->input();

        $user = $this->model->find($input['user_id']);

        if($user){
            $user->name = $input['name'];
            $user->save();

            return response()->json($this->generateResponse('user-updated-success'));
        }

        return response()->json($this->generateResponse('error'));
    }

    public function updatePw(Request $request)
    {
        $input = $request->input();

        $token = MobileApiToken::where('token',$input['token'])->where('uuid',$input['uuid'])->first();

        if($token){
            $user = User::find($token->user_id);

            if($user){

                if(!Hash::check($input['current_password'],$user->password))
                    return response()->json($this->generateResponse('wrong-current-password'));

                if($user->update(['password'=>Hash::make($input['new_password'])]))
                    return response()->json($this->generateResponse('user-updated-success'));

            }
        } else {
            return response()->json($this->generateResponse('invalid-api-token'));
        }

        return response()->json($this->generateResponse('error'));
    }

    public function get(Request $request)
    {
        $input = $request->input();

        $token = MobileApiToken::where('token',$input['token'])->where('uuid',$input['uuid'])->first();

        if($token){
            $user = User::find($token->user_id);
            $company = Company::find($input['company_id']);

            if(!$company)
                return response()->json([]);

            if($user->company->id == $company->id){
                if($user->scheduleDepartment){
                    $schedDept = $user->scheduleDepartment()->with('users.department')->where('department_id','!=',$user->department->id)->withMembersOnly($company->id)->get();

                    foreach($schedDept as $dp)
                        $departments[] = $dp;

                    $userDept = $user->department()->with('users.department')->first();

                    if($userDept)
                        $departments[] = $userDept;

                }
                elseif($user->isEmployee) {
                    $departments = $user->department()->with('users.department')->withMembersOnly($company->id)->get();
                } else {
                    $departments = Department::where('company_id',$company->id)->with('users.department')->withMembersOnly($company->id)->get();
                }

                return response()->json($departments);

            } else {
                return response()->json([]);
            }
        }
        else {
            return response()->json($this->generateResponse('invalid-api-token'));
        }

    }


}
