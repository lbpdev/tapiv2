<?php

namespace App\Http\Controllers\Mobile;

use App\Models\TaskComment;
use App\Repositories\Eloquent\CanCreateResponseCode;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{
    use CanCreateResponseCode;

    public function store(Request $request){

        $comment = TaskComment::where('date',$request->input('date'))->
        where('user_id',$request->input('user_id'))->
        where('author_id',$request->input('user_id'))->
        first();

        if(!$comment)
            $comment = TaskComment::create([
                'date'=>$request->input('date'),
                'user_id'=>$request->input('user_id'),
                'author_id'=>$request->input('user_id'),
                'comment'=>$request->input('comment')
            ]);
        else
            $comment->update([
                'comment'=>$request->input('comment')
            ]);


        if($comment){
            $data = $comment;

            if(str_replace(' ', '', $comment->comment)=="")
                $comment->delete();

            return response()->json($this->generateResponseWithData('create-success',$data));
        }

        return response()->json($this->generateResponse('error'));
    }

    public function get(Request $request){
        $comments = TaskComment::where('date',$request->input('date'))->where('user_id',$request->input('user_id'))->first();

        return response()->json($comments);
    }
}
