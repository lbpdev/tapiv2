<?php

namespace App\Http\Controllers\Mobile;

use App\Models\Option;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CalendarController extends Controller
{

    public function getWorkDays(){

        $workDays = Option::where('name','work-days')->first();

        $year = Carbon::now()->format('Y');

        // if(!$workDays){
        $weekends = ['Fri','Sat'];
        $data = "";
        $begin = new \DateTime( $year.'-01-01' );
        $end = new \DateTime( (intval($year)+1).'-01-01' );
        $end = $end->modify( '+1 day' );

        $interval = new \DateInterval('P1D');
        $daterange = new \DatePeriod($begin, $interval ,$end);

        $daysInWeek = [];
        $end = null;
        $start = null;
        $lastDate = null;
        $workDays = array();
        $dayIndex = 0;

        foreach($daterange as $date){
            if(!in_array($date->format("D"),$weekends)){
                $workDays[$dayIndex]['value'] = $date->format('Y-m-d');
                $workDays[$dayIndex]['text'] = $date->format('M d Y');
                $dayIndex++;
            }
        }

        // Option::create(array('name'=>'work-days','value'=>json_encode($workDays)));
        return response()->json($workDays);
        // }

        // return json_decode($workDays->value);
    }

}
