<?php

namespace App\Http\Controllers\Mobile;

use App\Models\DeviceToken;
use App\Models\MobileApiToken;
use App\Models\User;
use App\Repositories\Eloquent\CanCreateResponseCode;
use App\Repositories\Eloquent\MobileApiTokenLibrary;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DeviceTokenController extends Controller
{
    use CanCreateResponseCode;
    public function __construct(DeviceToken $model, MobileApiTokenLibrary $tokenLibrary)
    {
        $this->model = $model;
        $this->tokenLibrary = $tokenLibrary;
    }

    public function store(Request $request){

        $token = MobileApiToken::where('token',$request->input('token'))->where('uuid',$request->input('uuid'))->first();

        if($token){
            if($request->has('push_token')){

                $existitng = DeviceToken::where('push_token',$request->input('push_token'))->first();

                if(!$existitng){
                    $user = User::where('id',$request->input('user_id'))->first();

                    if($user){
                        $user->devices()->create([
                            'push_token' => $request->input('push_token'),
                            'os' => $request->input('os')
                        ]);
                        return response()->json($this->generateResponse('store-device-token-success'));
                    }
                } else {
                    return response()->json($this->generateResponse('existing-token'));
                }
            }
        } else {
            return response()->json($this->generateResponse('invalid-api-token'));
        }

        return response()->json($this->generateResponse('error'));
    }

    public function destroy(Request $request){

        $token = MobileApiToken::where('token',$request->input('token'))->where('uuid',$request->input('uuid'))->first();

        if($token){

            if($request->has('push_token')){
                $token = DeviceToken::where('push_token',$request->input('push_token'))->first();

                if($token){
                    if($token->delete()){
                        return response()->json($this->generateResponse('delete-device-token-success'));
                    }
                }
            }
        } else {
            return response()->json($this->generateResponse('invalid-api-token'));
        }

        return response()->json($this->generateResponse('error'));
    }
}
