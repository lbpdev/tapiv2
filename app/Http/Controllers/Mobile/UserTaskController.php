<?php

namespace App\Http\Controllers\Mobile;

use App\Models\Leave;
use App\Models\Option;
use App\Models\Task;
use App\Models\TaskComment;
use App\Models\UserTask;
use App\Models\User;

use App\Repositories\Eloquent\CanCreateResponseCode;
use App\Repositories\Eloquent\WeekLibrary;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;

class UserTaskController extends Controller
{
    use CanCreateResponseCode, WeekLibrary;

    public function __construct(UserTask $userTask, Task $task){
        $this->model = $userTask;
        $this->tasks= $task;
    }

    // MOBILE METHODS

    public function store(Request $request) {
        $input = $request->input();

        $input['date'] = new \DateTime;
        $input['time'] = 0;

        if($input['department_id']=='general')
            $input['department_id'] = null;

        $task = $this->tasks->create($input);

        $task->load('project.client','key');

        if($task){
            $input = $request->input();
            $input['date_assigned'] = strtotime($input['date']);
            $input['is_urgent'] = isset($input['is_urgent']) ? 1 : 0 ;

            $timeRequired = (intval($input['hours'])*60) + intval($input['minutes']);

            $input['time'] = $timeRequired;
            $input['task_id'] = $task->id;
            $input['user_id'] = $input['user_id'];
            $input['date_completed'] = null;

            $this->model->create($input);

            return response()->json('Task Added', 200);
        }

        return response()->json('Error', 201);
    }

    public function getByUserDay(Request $request){

        $data = [];
        $returnData = [];

        $input = $request->input();

        if(strip_tags($input['user_id'])){
            $query = UserTask::query();

            $query = $query->with('task.project.client','task.project.brand','task.department','task.key')->where('user_id',$input['user_id']);

            if(isset($input['date']))
                $query = $query->where('date_assigned',Carbon::parse($input['date'])->format('Y-m-d'));

//            if(isset($input['sort'])){
//                switch($input['sort']){
//                    case 'az':
//                        $query = $query->with(['task.project' => function ($q) {
//                            $q->orderBy('name','desc');
//                        }]);
//                        break;
//                    case 'pr':
//                        $query = $query->orderBy('is_urgent','desc');
//                        break;
//                    case 'dl':
//                        $query = $query->with(['task.project' => function ($q) {
//                            $q->orderBy('end_at','desc');
//                        }]);
//                        break;
//                }
//            }

            $userTasks = $query->has('task.project')->orderBy('order','ASC')->get();
            $onLeave = $this->checkLeave($input['user_id'],$input['date']);
            $leaveType = $this->checkLeaveType($input['user_id'],$input['date']);
            $leave_hours = $this->checkLeaveHours($input['user_id'],$input['date']);

            if(count($userTasks)){
                foreach ($userTasks as $index=>$userTask) {

                    $loop = [];

                    $task = $userTask->task;
                    $project = $userTask->task->project;

                    $loop['id'] = $userTask->id;
                    $loop['task_id'] = $userTask->task->id;
                    $loop['user_id'] = $userTask->user_id;
                    $loop['time'] = (intval($userTask->time/60)).'hr';
                    $loop['time_val'] = ($userTask->time/60);

                    if($userTask->time%60)
                        $loop['time'] .= ' '.($userTask->time%60).'min';

                    $loop['is_urgent'] = $task->is_urgent;
                    $loop['date_assigned'] = $userTask->date_assigned->format('d-m-Y');
                    $loop['date_completed'] = $userTask->date_completed ? $userTask->date_completed->format('d-m-Y') : null;
                    $loop['complete'] = $userTask->date_completed ? 'complete' : '';
                    $loop['created_at'] = $userTask->created_at->format('d-m-Y');
                    $loop['updated_at'] = $userTask->updated_at->format('d-m-Y');
                    $loop['is_extra'] = $userTask->is_extra;
                    $loop['key'] = $task->key->code;
                    $loop['note'] = $task->note ? $task->note : '';
                    $loop['project'] = $project->name;
                    $loop['color'] = $project->client->color;
                    $loop['code'] = $task->key->code;
                    $loop['order'] = $userTask->order;

                    $usertask = UserTask::where('id',$userTask->id)->first();

                    if($usertask){
                        $usertask->load('task.project.client','task.project.brand','task.project.format','task.key');
                        $loop['details'] = $usertask;
                        $loop['details']['language'] = $project->languagesString;
                        $loop['details']['duration'] = $project->duration;
                    }

                    $returnData['tasks'][$index] = $loop;
                }
            }

            $returnData['on_leave'] = $onLeave;
            $returnData['leave_type'] = $leaveType;
            $returnData['leave_hours'] = $leave_hours;
            $returnData['comment'] = TaskComment::where('date',Carbon::parse($input['date'])->format('Y-m-d'))->where('user_id',$input['user_id'])->where('author_id',$input['user_id'])->first();

            return response()->json($returnData);
        }

        return response()->json(null);
    }

    public function getByUserWeek(Request $request){

        $returnData = [];

        $input = $request->input();
        $workDays = $this->getWorkDays($input['company_id']);

        $currentDay = isset($input['date']) ? Carbon::parse($input['date']) : new \DateTime();
        $currentDay = $currentDay->format('d-m-Y');

        $currentUser = isset($input['user_id']) ? User::with('leaves')->where('id',$input['user_id'])->first() : null;

        if(!$currentUser)
            return "User does not exist";

        $currentWeek = $this->getCurrentWeek($currentDay, $workDays, $currentUser);

        foreach($currentWeek as $index=>$week){
            $day = Carbon::parse($week['day'])->format('Y-m-d');

            $currentWeek[$index]['leave_type'] = $this->checkLeaveType($input['user_id'],Carbon::parse($week['day'])->format('d-m-Y'));
            $currentWeek[$index]['on_leave'] = $this->checkLeave($input['user_id'],Carbon::parse($week['day'])->format('d-m-Y'));
            $currentWeek[$index]['leave_hours'] = $this->checkLeaveHours($input['user_id'],Carbon::parse($week['day'])->format('d-m-Y'));
            $currentWeek[$index]['comment'] = TaskComment::where('date',$day)->where('user_id',$input['user_id'])->where('author_id',$input['user_id'])->first();

            $query = UserTask::query();

            $query = $query->where('user_id',$input['user_id']);

            $query = $query->where('date_assigned',$day);

            if(isset($input['sort'])){
                switch($input['sort']){
                    case 'az':
                        $query = $query->with(['task.project' => function ($q) {
                            $q->orderBy('name','desc');
                        }]);
                        break;
                    case 'pr':
                        $query = $query->orderBy('is_urgent','desc');
                        break;
                    case 'dl':
                        $query = $query->with(['task.project' => function ($q) {
                            $q->orderBy('end_at','desc');
                        }]);
                        break;
                }
            }

            $userTasks = $query->has('task.project')->orderBy('order','ASC')->get();

            if(count($userTasks)){

                $currentWeek[$index]['tasks'] = [];

                foreach ($userTasks as $tIndex=>$userTask) {

                    $task = $userTask->task;
                    $project = $userTask->task->project;

                    $loop['id'] = $userTask->id;
                    $loop['task_id'] = $task->id;
                    $loop['user_id'] = $userTask->user_id;
                    $loop['time'] = (intval($userTask->time/60)).'hr';
                    $loop['time_val'] = $userTask->time/60;
                    $loop['deadline'] = $project ? ( $project->end_at ? $project->end_at->format('d-m-Y') : false )  : null;

                    if($userTask->time%60)
                        $loop['time'] .= ' '.($userTask->time%60).'min';

                    $loop['is_urgent'] = $task->is_urgent;
                    $loop['date_assigned'] = $userTask->date_assigned->format('d-m-Y');

                    $loop['date_completed'] = $userTask->date_completed ? $userTask->date_completed->format('d-m-Y') : null;
                    $loop['complete'] = $userTask->date_completed ? 'complete' : '';
                    $loop['created_at'] = $userTask->created_at->format('d-m-Y');
                    $loop['updated_at'] = $userTask->updated_at->format('d-m-Y');
                    $loop['is_extra'] = $userTask->is_extra;
                    $loop['key'] = $task->key->code;
                    $loop['note'] = $task->note ? $task->note : '';
                    $loop['project'] = $project ? $project->name : 'N/A';
                    $loop['color'] = $project ? ( $project->client ? $project->client->color : false ) : '';
                    $loop['code'] = $task->key->code;
                    $loop['order'] = $userTask->order;

                    $usertask = UserTask::where('id',$userTask->id)->first();

                    if($usertask){
                        $usertask->load('task.project.client','task.project.brand','task.project.format','task.key');
                        $loop['details'] = $usertask;
                        $loop['details']['language'] = $project->languagesString;
                        $loop['details']['duration'] = $project->duration;
                    }

                    $currentWeek[$index]['tasks'][$tIndex] = $loop;
                }
            }
        }

        return response()->json($currentWeek);
    }

    public function toggleStatus(Request $request){

        $input = $request->input();

        $data = [];
        $returnData = [];

        if(strip_tags($input['user_task_id'])){

            $userTask = UserTask::where('id',$input['user_task_id'])->first();

            $dateCompleted = $userTask->date_completed ? null : Carbon::now()->format('Y-m-d');

            if($userTask)
                $userTask->update(['date_completed' => $dateCompleted ]);

            return response()->json($dateCompleted ? Carbon::now()->format('d-m-Y') : null);
        }

        return response()->json(null);
    }

    public function view(){

        $usertask = UserTask::where('id',$_GET['id'])->first();

        if($usertask){
            $usertask->load('task.project.client','task.project.brand','task.project.format','task.key');
            $usertask->language = $usertask->task->project->languagesString;
            $usertask->duration = $usertask->task->project->duration;

            return response()->json($usertask);
        }

        return response()->json(null);
    }


    public function get(Request $request){

        $usertask = UserTask::where('id',$request->input('id'))->first();

        if($usertask){
            $usertask->load('task.project.client','task.project.brand','task.project.format','task.key');
            $usertask->language = $usertask->task->project->languagesString;
            $usertask->duration = $usertask->task->project->duration;

            return response()->json($usertask);
        }

        return response()->json(null);
    }

    public function checkLeave($user_id,$date){
        $leaves = $this->getUserLeaves($user_id,$date);

        if(in_array($date,$leaves))
            return 1;

        return 0;
    }

    public function checkLeaveHours($user_id,$date){
        $leave= Leave::where('user_id',$user_id)->whereDate('from','<=',Carbon::parse($date)->format('Y-m-d'))->whereDate('to','>=',Carbon::parse($date)->format('Y-m-d'))->first();

        if($leave){
            if($leave->time>0)
                return $leave->time/60;
            else
                return 0;
        }

        return null;
    }

    public function checkLeaveType($user_id,$date){

        $leave= Leave::where('user_id',$user_id)->whereDate('from','<=',Carbon::parse($date)->format('Y-m-d'))->whereDate('to','>=',Carbon::parse($date)->format('Y-m-d'))->first();

        if($leave)
            return $leave->type;

        return null;
    }

    private function getUserLeaves($user_id)
    {
        $leaves = Leave::where('user_id',$user_id)->get();

        $dates = [];

        foreach ($leaves  as $key => $leave) {
            $begin = new \DateTime($leave->from);
            $end = new \DateTime($leave->to);

            $end = $end->modify( '+1 day' );

            $interval = new \DateInterval('P1D');
            $daterange = new \DatePeriod($begin, $interval ,$end);

            foreach($daterange as $date){
                $dates[] = $date->format("d-m-Y");
            }

        }

        return $dates;
    }

    public function complete(Request $request){

        $utask = $this->model->where('id',$request->input('user_task_id'))->first();

        if($utask){
            $utask->date_completed = Carbon::now();

            return response()->json($utask->save() ? 1 : 3);
        }

        return response()->json(3);
    }

    public function incomplete(Request $request){
        $utask = $this->model->where('id',$request->input('user_task_id'))->first();

        if($utask){
            $utask->date_completed = null;
            return response()->json($utask->save() ? 2 : 3);
        }

        return response()->json(3);
    }

}

