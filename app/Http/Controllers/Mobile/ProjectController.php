<?php

namespace App\Http\Controllers\Mobile;

use App\Models\Project;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{
    public function __construct(Project $project){
        $this->model = $project;
    }

    public function getAll(Request $request){
        $input = $request->input();

        return response()->json($this->model->with('client','brand')->whereNull('completed_at')->fromCompany($input['company_id'])->get()->toArray());
        // return response()->json($this->model->select('name as text','id as value')->orderBy('name')->get()->toArray());
    }

    public function get(){
        return response()->json($this->model->with('client','brand')->where('id',$_GET['id'])->first()->toArray());
    }
}
