<?php

namespace App\Http\Controllers\Mobile;

use App\Models\Key;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class KeyController extends Controller
{
    public function __construct(Key $key){
        $this->model = $key;
    }    

    public function getAll(Request $request){
        $input = $request->input();

        if($request->has('department_id'))
            return response()->json($this->model->fromCompany($input['company_id'])->where('department_id',$input['department_id'])->get()->toArray());

        return response()->json($this->model->get()->toArray());
//        return response()->json($this->model->select('name as text','id as value')->orderBy('name')->get()->toArray());
    }
}
