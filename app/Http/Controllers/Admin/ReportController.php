<?php

namespace App\Http\Controllers\Admin;

use App\Models\Color;
use App\Models\Company;
use App\Models\Department;
use App\Models\Format;
use App\Models\Option;
use App\Models\Project;
use App\Models\Report;
use App\Models\User;
use App\Services\ReportService;
use Carbon\Carbon;
use Davibennun\LaravelPushNotification\Facades\PushNotification;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;

class ReportController extends Controller
{

    use ReportService;

    public function index(){
        $year = Carbon::now()->format('Y');
        $month = Carbon::now()->format('m');
        $start = Carbon::parse('01-'.$month.'-'.$year);
        $end = Carbon::parse('01-'.$month.'-'.$year)->addYear()->subDay();
        $projects = [];
        $ids = [];

        $queueStart = Project::with('stages','client','brand','languages','format')->fromCurrentCompany()->whereBetween('start_at', [$start, $end])->get();

        foreach ($queueStart as $project){
            $ids[] = $project->id;
            $projects[] = $project;
        }

        $queueEnd = Project::with('stages','client','brand','languages','format')->fromCurrentCompany()->whereBetween('end_at', [$start, $end])->whereNotIn('id', $ids)->get();

        foreach ($queueEnd as $project){
            $projects[] = $project;
        }

        $userRanges = [];
        $userRanges[0] = 'Whole company';

        $departments = Department::fromCurrentCompany()->lists('name','id');
        $users = User::fromCurrentCompany()->lists('name','id');

        foreach ($departments as $id=>$department)
            $userRanges['department['.$id.']'] = $department;

        foreach ($users as $id=>$user)
            $userRanges['user['.$id.']'] = $user;

        $deliverables = Format::fromCurrentCompany()->orderBy('name','ASC')->lists('name');

        $colors = Color::lists('value');
        $options = Option::fromCurrentCompany()->lists('value','name');
        $reports = Report::fromCurrentCompany()->where('type','email-reports')->first();

        return view('pages.reports.index', compact('projects','userRanges','deliverables','colors','options','reports'));
    }

    public function user($id){
        $user = User::with('department','role','logs')->find($id);

        return view('pages.reports.users.index',compact('user'));
    }

    public function project($id){
        $data = Project::with('client','brand','languages')->find($id);

        return view('pages.reports.projects.index',compact('data'));
    }

    public function generateReport($company_id)
    {
        $range = null;
        $department = 0;
        $user = null;

        $year = Carbon::now()->format('Y');
        $month = Carbon::now()->format('m');
        $start = Carbon::parse('01-'.$month.'-'.$year);
        $end = Carbon::parse('01-'.$month.'-'.$year)->addYear()->subDay();
        $projects = [];
        $ids = [];

        $queueStart = Project::with('stages','client','brand','languages','format')->where('company_id',$company_id)->whereBetween('start_at', [$start, $end])->get();

        foreach ($queueStart as $project){
            $ids[] = $project->id;
            $projects[] = $project;
        }

        $queueEnd = Project::with('stages','client','brand','languages','format')->where('company_id',$company_id)->whereBetween('end_at', [$start, $end])->whereNotIn('id', $ids)->get();

        foreach ($queueEnd as $project){
            $projects[] = $project;
        }

        $userRanges = [];
        $userRanges[0] = 'Whole company';

        $departments = Department::where('company_id',$company_id)->lists('name','id');
        $users = User::where('company_id',$company_id)->lists('name','id');

        foreach ($departments as $id=>$department)
            $userRanges['department['.$id.']'] = $department;

        foreach ($users as $id=>$user)
            $userRanges['user['.$id.']'] = $user;

        $deliverables = Format::where('company_id',$company_id)->orderBy('name','ASC')->lists('name');

        $colors = Color::lists('value');

        $range = isset($_GET['prodDateRange']) ? $_GET['prodDateRange'] : null;
        $department = isset($_GET['prodDepRange']) ? $_GET['prodDepRange'] : null;
        $user = isset($_GET['prodUserRange']) ? $_GET['prodUserRange'] : null;

        $productivityData = json_encode($this->productivity(true,$company_id,$range,$department,$user));


        $range = isset($_GET['deadDateRange']) ? $_GET['deadDateRange'] : null;

        $deadline = $this->deadline(true,$company_id,$range);

        $deadlineData = json_encode($deadline['values']);
        $deadlineLabels = json_encode($deadline['labels']);

        //productivityDateRange=today&productivityUserRange=0&deadlineDateRange=this-quarter&timeDateRange=today&timeUserRange=0&deliverableDateRange=this-month&deliverableUserRange=0&

        $range = isset($_GET['timeDateRange']) ? $_GET['timeDateRange'] : null;
        $department = isset($_GET['timeDepRange']) ? $_GET['timeDepRange'] : null;
        $user = isset($_GET['timeUserRange']) ? $_GET['timeUserRange'] : null;

        $timeData = json_encode($this->booked(true,$company_id,$range,$department,$user));



        $range = isset($_GET['delDateRange']) ? $_GET['delDateRange'] : null;
        $department = isset($_GET['delDepRange']) ? $_GET['delDepRange'] : null;
        $user = isset($_GET['delUserRange']) ? $_GET['delUserRange'] : null;

        $deliverable = $this->deliverables(true,$company_id,$range,$department,$user);
        $deliverableData = json_encode($deliverable['values']);
        $deliverableLabels = json_encode($deliverable['labels']);

        return view('pages.reports.print', compact('projects','userRanges','deliverables','colors','productivityData','deadlineData','deadlineLabels','timeData','timeLabels','deliverableData','deliverableLabels'));

        $pdf = App::make('snappy.pdf.wrapper');
        $pdf->loadView('pages.reports.print', compact('projects','userRanges','deliverables','colors','productivityData','deadlineData','deadlineLabels','timeData','timeLabels','deliverableData','deliverableLabels'))->setPaper('a4')->setOrientation('portrait')->setOption('margin-bottom', 0);

        return $pdf;
    }
}
