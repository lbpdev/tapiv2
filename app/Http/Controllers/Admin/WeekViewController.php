<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\ClientBrand;
use App\Models\Department;
use App\Models\Format;
use App\Models\Language;
use App\Models\Leave;
use App\Models\Option;
use App\Models\Project;
use App\Models\Stage;
use App\Models\Task;
use App\Models\TaskChanges;
use App\Models\TaskComment;
use App\Models\User;
use App\Models\UserTask;
use App\Services\PushNotificationService;
use Carbon\Carbon;
use Davibennun\LaravelPushNotification\Facades\PushNotification;
use Illuminate\Http\Request;
use App\Models\Key;

use App\Http\Requests;

use App\Repositories\Eloquent\WeekLibrary;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class WeekViewController extends Controller
{

    use WeekLibrary;

    public function __construct(
            Option $option,
            Format $format,
            Client $client,
            Language $language,
            Stage $stage,
            Department $department,
            PushNotificationService $push
    ){
        $this->department = $department;
        $this->option = $option;
        $this->format = $format;
        $this->client = $client;
        $this->stage = $stage;
        $this->language = $language;
        $this->push = $push;
    }

    public function index(){

        $weeks = $this->getWeeks();
        $formats = $this->format->fromCurrentCompany()->lists('name','id');
        $clients = $this->client->fromCurrentCompany()->select('name','color')->orderBy('name','ASC')->get();

        /** Jeffrey Update 2 */
        // $departments = Department::with('users.department','keys')->withEmployeesOnly()->get();

        if(Auth::user()->scheduleDepartment){
            $schedDept = Auth::user()->scheduleDepartment()->with('users.department','keys')->where('department_id','!=',Auth::user()->department->id)->withMembersOnly()->get();

            foreach($schedDept as $dp)
                $departments[] = $dp;

            $userDept = Auth::user()->department()->with('users.department','keys')->first();

            if($userDept)
                $departments[] = $userDept;

        }
        else
            $departments = Department::with('users.department','keys')->withMembersOnly()->get();

        $workHours = $this->currentCompany()->workHours;

        if(!$departments)
            return redirect(url('/oops').'?error=Company setup is incomplete. <br>Please contact you site administrator regarding this issue.');

        if(isset($_GET['day']) && isset($_GET['user']) && isset($_GET['department']) ){
            try {
                Carbon::parse($_GET['day']);
            } catch( \Exception $e){
                return redirect(route('calendar.week'));
            }
        }
        
        /** Jeffrey Update 2 */
        // $departmentsList = Department::fromCurrentCompany()->withEmployeesOnly()->lists('name','id');

        if(Auth::user()->scheduleDepartment){
            $departmentsList[0][Auth::user()->department->id] = Auth::user()->department->name;
            $departmentsList[1][Auth::user()->scheduleDepartment->id] = Auth::user()->scheduleDepartment->name;
        }
        else
            $departmentsList = Department::fromCurrentCompany()->withMembersOnly()->lists('name','id');

        $keyList = Key::fromCurrentCompany()->orderBy('name','ASC')->lists('name','id');
        $generalKeys = Key::fromCurrentCompany()->where('department_id',null)->get();

        $projectList = $this->generateProjectList();
        $tasks = Task::with('department','project.client','key')->fromCurrentCompany()->get();

        $workDaysInWeek = $this->getWorkDaysInWeeks();
        $workDays = $this->getWorkDays($this->currentCompanyId());

        $workDaysName = $this->getWorkDaysInt();

        $currentDay = isset($_GET['day']) ? Carbon::parse($_GET['day']) : Carbon::now()->setTimezone(Auth::user()->company->timezone);
        $currentDay = $currentDay->format('d-m-Y');

        if(isset($_GET['user']))
            $currentUser = User::fromCurrentCompany()->with('leaves')->where('id',$_GET['user'])->first();
        else {
            $currentUser = Auth::user();

        //            Get first user of department as current user.
        //            foreach ($departments as $department){
        //                if($department){
        //                    $user = User::fromCurrentCompany()->with('leaves')->whereHas('department',function($q) use($department){
        //                        $q->where('department_id',$department->id);
        //                    })
        //                    ->whereHas('role',function($q) use($department){
        //                        /** Jeffrey Update */
        //                        // $q->where('role_id',3);
        //                        $q->where('role_id',3)
        //                          ->orWhere('role_id',1);
        //                    })
        //                    ->where('is_active',1)->first();
        //
        //                    if($user){
        //                        $currentUser = $user;
        //                        break;
        //                    }
        //                }
        //            }
        }

        if(!isset($currentUser))
            return redirect(url('/oops').'?error=There are currently no active users for the company. <br>Please contact your site administrator.');
        /** Jeffrey Update */
        // if(!$currentUser->isEmployee || !$currentUser->is_active)

        if(!$currentUser->is_active)
            return redirect(route('calendar.week'));

        if(isset($_GET['department']))
            $currentDepartment = Department::fromCurrentCompany()->where('name',$_GET['department'])->first();
        else
            $currentDepartment = Auth::user()->department;

//        if(Auth::user()->scheduleDepartment->id != $currentDepartment->id || $currentDepartment->id != Auth::user()->department->id)
//            return redirect(route('calendar.week').'?user='.Auth::user()->id.'&department='.Auth::user()->department->name.'&day='.$currentDay);

        if(!$departmentsList)
            return "Company currently has no departments. Please complete the setup from the settings area first.";

        if(!$currentDepartment)
            return redirect(route('calendar.week'));

        $key = -1;
        $key = array_search($currentDay, $workDays); // $key = 2;

        if($key<0){
            $currentDay = Carbon::parse($currentDay)->addDay(1)->format('d-m-Y');
            return redirect(route('calendar.week').'?user='.$currentUser['id'].'&department='.$currentDepartment->id.'&day='.$currentDay);
        }

        $currentWeek = $this->getCurrentWeekFull($currentDay, $weeks, $currentUser);

        if(!isset($_GET['day']))
            return redirect(route('calendar.week').'?user='.$currentUser['id'].'&department='. ( isset($_GET['department']) ? $_GET['department'] : $currentDepartment->name ).'&day='.$currentDay);

        $currentTasks = count($currentWeek) ? $this->getCurrentWeekTasks($currentWeek,$departments,$currentDepartment) : [];

        $workHours = $currentDepartment->work_hours ? $currentDepartment->work_hours : $workHours;

        return view('pages.admin.calendar.week.index',compact(
            'formats','clients','departments','generalKeys','generalKeysArray','departmentsList','projectList','keyList','tasks',
            'workDays','workHours','currentDay','currentWeek','workDaysName','currentTasks','currentUser','currentDepartment'
        ));
    }

    public function getWeekends($start,$end) {
        $days = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'];
        $start = $days[$start];
        $end = $days[$end];
        $weekends = [];
        $stop = false;

        $loop = 0;

        $weekendStarted = false;

        while(!$stop) {
            if($loop<7) {

                if($days[$loop]==$end && !$weekendStarted){
                    $weekendStarted = true;
                }

                elseif($days[$loop]!=$start && $weekendStarted) {
                    array_push($weekends,$days[$loop]);
                }

                elseif( count($weekends) > 0 && $days[$loop]==$start )
                    $stop = true;

                $loop++;
            } else
                $loop = 0;
        }

        return $weekends;
    }
    public function getWorkDaysJson(){
        return $this->getWorkDaysInt();
    }

    public function getAvailableProjectsJson(){
        return $this->generateProjectList();
    }
}