<?php

namespace App\Http\Controllers\Admin;

use App\Models\Client;
use App\Models\Color;
use App\Models\Department;
use App\Models\Key;
use App\Models\Language;
use App\Models\Option;
use App\Models\Format;
use App\Models\Project;
use App\Models\ProjectStage;
use App\Models\Stage;
use App\Models\Subscription;
use App\Services\ColorServer;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class CalendarController extends Controller
{
    use ColorServer;

    public function __construct(
        Option $option,
        Format $format,
        Client $client,
        Language $language,
        Stage $stage
    ){
        $this->option = $option;
        $this->format = $format;
        $this->client = $client;
        $this->stage = $stage;
        $this->language = $language;
    }

    public function index(){

        $currentYear = Carbon::now()->setTimezone(Auth::user()->company->timezone)->format('Y');

        if(isset($_GET['year'])){
            $checkYear = date_parse($_GET['year']);

            if(!$checkYear['error_count']){
                $currentYear = $_GET['year'];
            } else {
                return Redirect::to(route('calendar.year'));
            }
        }

        $workDayStart = $this->option->fromCurrentCompany()->where('name','workdays_from')->first();
        $workDayEnd = $this->option->fromCurrentCompany()->where('name','workdays_to')->first();

        $formats = $this->format->fromCurrentCompany()->lists('name','id');
        $stagesIn = $this->stage->fromCurrentCompany()->where('internal',1)->orderBy('order','ASC')->get();
        $stagesEx = $this->stage->fromCurrentCompany()->where('internal',0)->orderBy('order','ASC')->get();
        $stagesList = $this->stage->fromCurrentCompany()->orderBy('name','ASC')->get();

        $languages = $this->language->lists('name','id');
        $clientsAll = $this->client->fromCurrentCompany()->get();
        $colors = $this->getColors();
        $clients = [];

        foreach($clientsAll as $index=>$client){
            $clients[$index]['label'] = $client->name;
            $clients[$index]['value'] = $client->color;
        }
        $clients = json_encode($clients);

        $weekends = $this->getWeekends(
            $workDayStart ? $workDayStart->value : 1,
            $workDayEnd ? $workDayEnd->value : 5
        );

        $weeksPerMonth = $this->getWeeksPerMonth($currentYear);
        $weekDivisions = $this->generateWeekDiv($weekends);

        $weekDivisionsLeft = $weekDivisions['left'];
        $weekDivisionsWidth = $weekDivisions['width'];

        $dateHeadersModal = $this->generateDateHeaderFull();

        $projectsQuery = Format::query();

        if(isset($_GET['keyword'])){
            $projectsQuery = Project::query();
            $projectsQuery->with('stages.stage')->fromCurrentCompany()->fromYear($currentYear)->whereNull('completed_at')->where('name', 'like','%'.$_GET['keyword'].'%');
        }
        elseif(isset($_GET['filter'])){

            if($_GET['filter']=='client'){
                $projectsQuery = Client::query();
                $projectsQuery->with([
                    'projects' => function ($query) use($currentYear)  {
                        $query->fromCurrentCompany()->fromYear($currentYear)->whereNull('completed_at')->orderBy('name', 'asc');
                    }
                    , 'projects.stages.stage'
                ])->orderBy('name','asc');
            }

            elseif($_GET['filter']=='format'){
                $projectsQuery->with([
                    'projects' => function ($query) use($currentYear) {
                        $query->fromCurrentCompany()->fromYear($currentYear)->whereNull('completed_at')->orderBy('name', 'asc');
                    }
                    , 'projects.stages.stage'
                ])->orderBy('name','asc');
            }

            elseif($_GET['filter']=='archive'){
                $projectsQuery = Project::query();
                $projectsQuery->with('stages.stage')->fromCurrentCompany()->whereNotNull('completed_at')->fromYear($currentYear)->orderBy('name','asc');
            }

            elseif($_GET['filter']=='project'){
                $projectsQuery = Project::query();
                $projectsQuery->with('stages.stage')->fromCurrentCompany()->whereNull('completed_at')->fromYear($currentYear)->orderBy('name','asc');
            }

        } else {
            $projectsQuery->with([
                'projects' => function ($query) use($currentYear) {
                    $query->fromCurrentCompany()->whereNull('completed_at')->fromYear($currentYear)->orderBy('name', 'asc');
                }
                , 'projects.stages.stage'
            ])->orderBy('name','asc');
        }

        $projectsInFormat = $projectsQuery->get();

        if(isset($_GET['filter'])){
            if($_GET['filter']=='project'||$_GET['filter']=='archive')
                $projectsInFormatHtml = $this->generateProjectStages($projectsInFormat,$weekDivisionsLeft,$weekDivisionsWidth,50);
            else
                $projectsInFormatHtml = $this->generateProjects($projectsInFormat,$weekDivisionsLeft,$weekDivisionsWidth);
        }
        elseif(isset($_GET['keyword']))
            $projectsInFormatHtml = $this->generateProjectStages($projectsInFormat,$weekDivisionsLeft,$weekDivisionsWidth,50);
        else
            $projectsInFormatHtml = $this->generateProjects($projectsInFormat,$weekDivisionsLeft,$weekDivisionsWidth);

        $firstWorkDay = $this->currentCompany()->firstDay;

        return view('pages.admin.calendar.year.index',compact(
            'weekends','formats','clients','languages','firstWorkDay',
            'projectsInFormat','projectsInFormatHtml','stagesIn','stagesEx',
            'dateHeaders','dateHeadersModal','dateDivs','weekDivisionsLeft',
            'weekDivisionsWidth','colors','weeksPerMonth','subscription','stagesList'));
    }

    public function search(Request $request) {
        $keyword = filter_var($request->input('keyword'), FILTER_SANITIZE_STRING);

        return redirect(route('calendar.year').( $keyword!="" ? '?keyword='.$keyword : ''));
    }

    public function getWeeks(Request $request) {
        $keyword = filter_var($request->input('keyword'), FILTER_SANITIZE_STRING);

        return redirect(route('calendar.year').( $keyword!="" ? '?keyword='.$keyword : ''));
    }

    /*
    * DELETE A RECORD THROUGH ID
    */
    public function generatePdf(){

        $stagesIn = $this->stage->fromCurrentCompany()->where('internal',1)->orderBy('order','ASC')->get();
        $stagesEx = $this->stage->fromCurrentCompany()->where('internal',0)->orderBy('order','ASC')->get();
        $project = Project::with('stages.stage')->where('id',$_GET['id'])->first();

        $weeksPerMonth = $this->getWeeksPerMonth(Carbon::parse($project->start_at)->format('Y'));

        $workDayStart = $this->option->fromCurrentCompany()->where('name','workdays_from')->first();
        $workDayEnd = $this->option->fromCurrentCompany()->where('name','workdays_to')->first();

        $weekends = $this->getWeekends(
            $workDayStart ? $workDayStart->value : 1,
            $workDayEnd ? $workDayEnd->value : 5
        );

        $projectsInFormat = Project::with('stages')->where('id',$_GET['id'])->first();

        $projectDuration = $this->getProjectDuration($projectsInFormat->stages);

        for($year=$projectDuration['bottom'];$year<=$projectDuration['top'];$year++){
            $weekDivisionsLeft = $this->generateWeekDivOnce($year)['left'];
            $weekDivisionsWidth = $this->generateWeekDivOnce($year)['width'];

            $projectsInFormatHtml[$year] = $this->generateProjectStagesPDF($projectsInFormat,$weekDivisionsLeft,$weekDivisionsWidth,20,$year);
        }

//        return view('pages.admin.calendar.year.pdf.project', compact('clients','stagesIn','stagesEx','project','weeksPerMonth','projectsInFormatHtml'));

        $pdf = App::make('snappy.pdf.wrapper');
        $pdf->loadView('pages.admin.calendar.year.pdf.project', compact('clients','stagesIn','stagesEx','project','weeksPerMonth','projectsInFormatHtml'))->setPaper('a4')->setOrientation('landscape')->setOption('margin-bottom', 0);
        return $pdf->download($project->name.'pdf');

//        return view('pages.admin.calendar.year.pdf.project',compact(
//            'clients','stagesIn','stagesEx','project','weeksPerMonth','projectsInFormatHtml'));

    }

    public function getProjectDuration($stages){

        foreach ($stages as $stage){
            $yearStart = (int)$stage->start_at->format('Y');
            $yearEnd = (int)$stage->end_at->format('Y');

            $bottom[] = $yearStart;
            $top[] = $yearEnd;
        }


        $data['bottom'] = min($bottom);
        $data['top'] = max($top);

        return $data;
    }


    public function getWeekends($start,$end) {
        $days = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'];
        $start = $days[$start];
        $end = $days[$end];
        $weekends = [];
        $stop = false;

        $loop = 0;

        $weekendStarted = false;

        while(!$stop) {
            if($loop<7) {

                if($days[$loop]==$end && !$weekendStarted){
                    $weekendStarted = true;
                }

                elseif($days[$loop]!=$start && $weekendStarted) {
                    array_push($weekends,$days[$loop]);
                }

                elseif( count($weekends) > 0 && $days[$loop]==$start )
                    $stop = true;

                $loop++;
            } else
                $loop = 0;
        }

        return $weekends;
    }

    function getWeeksPerMonth($year,$single = null)
    {

        $months = [];
        $year = intval($year);


        if($single){
            for ($m=1; $m<=12; $m++) {
                $months[$year][$m]['name'] = date('F', mktime(0,0,0,$m, 1, $year));

                $weeks = cal_days_in_month(CAL_GREGORIAN, $m, $year) / 7;
                $months[$year][$m]['weeks'] = $weeks > 4 ? 5 : 4;

            }
        } else {
            for ($y=($year-1); intval($y)<=($year+1); $y++) {
                for ($m=1; $m<=12; $m++) {
                    $months[$y][$m]['name'] = date('F', mktime(0,0,0,$m, 1, $y));

                    $weeks = cal_days_in_month(CAL_GREGORIAN, $m, $y) / 7;
                    $months[$y][$m]['weeks'] = $weeks > 4 ? 5 : 4;

                }
            }
        }

        return $months;
    }

    public function generateDateHeaderFull(){
        $data = "";

        $year = Carbon::now()->setTimezone(Auth::user()->company->timezone);

        if(isset($_GET['year'])){
            $checkYear = date_parse($_GET['year']);

            if($checkYear['error_count']==0){
                $year = Carbon::parse($_GET['year']);
            }
        }

        $begin = new \DateTime( $year->subYear(1)->format('Y').'-01-01' );
        $end = new \DateTime( $year->addYear(3)->format('Y').'-01-01' );
        $end = $end->modify( '+1 day' );

        $interval = new \DateInterval('P1D');
        $daterange = new \DatePeriod($begin, $interval ,$end);

        $end = null;
        $start = null;
        $lastDate = null;
        $workDays = array();

        foreach($daterange as $date)
            array_push($workDays, $date);

        $month = 0;
        $currentMonth = Carbon::now()->setTimezone(Auth::user()->company->timezone)->format('F');
        $currentMonthYear = Carbon::now()->setTimezone(Auth::user()->company->timezone)->format('m-Y');
        $data = "";

        foreach($workDays as $index=>$day){

            if(isset($workDays[$index+1])){

                if($month!=idate('m',strtotime($day->format('d-m-Y')))){

                    $daysInMonth = cal_days_in_month(CAL_GREGORIAN, $day->format('m'), $day->format('Y'));
                    $daysInMonth *= 20;

                    if($month != 0 ){
                        $data .='</ul></li>';
                    }

                    $data .='<li class="clearfix">'.
                        '<div style="width:'.$daysInMonth.'px" class="month '. ($day->format('m-Y') == $currentMonthYear ? 'activeMonth' : ' ' ) .'">'.$day->format('F').'</div>'.
                        '<ul class="days" style="width:'.$daysInMonth.'px">';

                    $month = $day->format('m');
                }

                $dayInt = idate('d',strtotime($day->format('d-m-Y')));

                $data .='<li data-date="'.$day->format('d-m-Y').'">'. $dayInt . '</li>';
            }
        }

        return $data;
    }


    public function generateProjects($projectsInFormat,$weekDivisionsLeft,$weekDivisionsWidth,$colWid = 50){
        $html = "";
        foreach($projectsInFormat as $format){
            if(count($format->projects)){
                $html .= '<ul class="stages blank"></ul>';
                $html .= $this->generateProjectStages($format->projects,$weekDivisionsLeft,$weekDivisionsWidth,$colWid);
            }
        }
        return $html;
    }


    public function generateProjectStagesPDF($project,$weekDivisionsLeft,$weekDivisionsWidth,$colWidth,$index){
        $html = "";
        if(count($project->stages)){
            foreach($project->stages as $stage){
                $html .= '<ul class="stages">';
                $left = 0;
                $width = $colWidth;

                if($stage->start_at){

                    $color = $stage->color;
                    $start_month = $stage->start_at->format('m-Y');

                    $start_day = (int)$stage->start_at->format('d');

                    $end_day = (int)$stage->end_at->format('d');

                    $end_month = (int)$stage->end_at->format('m');
                    $end_month_y = $stage->end_at->format('m-Y');

                    /**
                     *  START OF LEFT POSITIONING
                     */

                    foreach($weekDivisionsLeft as $key=>$monthData){
                        if($key != $stage->start_at->format('m-Y'))
                            $left += count($monthData) * $colWidth;
                        else
                            break;
                    }

                    if(!isset($weekDivisionsLeft[$start_month]))
                        $left = 0;
                    else {
                        foreach($weekDivisionsLeft[$start_month] as $key=>$weekDatas){
                            if(isset($weekDivisionsLeft[$start_month][$key])){
                                if(in_array($start_day,$weekDivisionsLeft[$start_month][$key])){
                                    $left += ($key)  * $colWidth;
                                    break;
                                }
                            }
                        }
                    }

                    /**
                     *  START OF WIDTH COMPUTATION
                     */

                    $continuation = false;

                    if(!isset($weekDivisionsWidth[$start_month]))
                        $continuation = true;

                    if(isset($weekDivisionsWidth[$start_month]) || isset($weekDivisionsWidth[$end_month_y])){
                        $monthToSearch = $weekDivisionsWidth[$continuation ? ('01-'.$index) :$start_month];
                        $startAdding = false;
                        $run = true;


                        while($run){

                            foreach($monthToSearch as $key=>$weekDatas){

                                if($continuation || in_array($stage->start_at->format('d-m-Y'),$monthToSearch[$key]))
                                    $startAdding = true;

                                if(in_array($stage->end_at->format('d-m-Y'),$monthToSearch[$key])){
                                    $run = false;
                                    break;
                                }

                                if($startAdding){
                                    $width += $colWidth;
                                }
                            }

                            $start_month = Carbon::parse('01-'.($start_month))->addMonth()->format('m-Y');

                            if(isset($weekDivisionsWidth[$start_month]))
                                $monthToSearch = $weekDivisionsWidth[$start_month];
                            else
                                $run = false;
                        }

                    } else {

                        if(((int)$stage->end_at->format('Y')>(int)$index)&&((int)$stage->start_at->format('Y')<(int)$index)&&$continuation){
                            $width = $colWidth * 12 * 5;
                        }
                        else
                            $width = 0;
                    }

                    $html .= '<li class="viewStagesLink" title="Click to view more details." data-project-id="'.$project->id.'" style="background-color: '.$color.'; width: '.$width.'px; left: '.$left.'px;"> </li>';
                }
                $html .= '</ul>';
            }
        }
        return $html;
    }

    public function generateProjectStages($projects,$weekDivisionsLeft,$weekDivisionsWidth,$colWidth){
        $html = "";

        foreach($projects as $id=>$project){
            $html .= '<a href="#'.$project->id.'"><ul class="stages">';

            if(count($project->stages)){
                foreach($project->stages as $stage){
                    $left = 0;
                    $width = $colWidth;

                    if($stage->start_at){

                        $color = $stage->color;
                        $start_month = $stage->start_at->format('m-Y');

                        $start_day = (int)$stage->start_at->format('d');
                        $end_day = (int)$stage->end_at->format('d');

                        $end_month = (int)$stage->end_at->format('m');
                        $start_year = (int)$stage->end_at->format('Y');


                        if(!isset($weekDivisionsLeft[$start_month]))
                            continue;

                        /**
                         *  START OF LEFT POSITIONING
                         */


                        foreach($weekDivisionsLeft as $key=>$monthData){
                            if($key != $stage->start_at->format('m-Y'))
                                $left += count($monthData) * $colWidth;
                            else
                                break;
                        }

                        foreach($weekDivisionsLeft[$start_month] as $key=>$weekDatas){
                            if(isset($weekDivisionsLeft[$start_month][$key])){
                                if(in_array($start_day,$weekDivisionsLeft[$start_month][$key])){
                                    $left += ($key)  * $colWidth;
                                    break;
                                }
                            }
                        }

                        /**
                         *  START OF WIDTH COMPUTATION
                         */

                        $monthToSearch = $weekDivisionsWidth[$start_month];
                        $startAdding = false;
                        $run = true;

                        while($run){

                            foreach($monthToSearch as $key=>$weekDatas){

                                if(in_array($stage->start_at->format('d-m-Y'),$monthToSearch[$key]))
                                    $startAdding = true;

                                if(in_array($stage->end_at->format('d-m-Y'),$monthToSearch[$key])){
                                    $run = false;
                                    break;
                                }

                                if($startAdding)
                                    $width += $colWidth;
                            }

                            $start_month = Carbon::parse('1-'.$start_month)->addMonth()->format('m-Y');

                            if(isset($weekDivisionsWidth[$start_month]))
                                $monthToSearch = $weekDivisionsWidth[$start_month];
                            else
                                $run = false;

                        }

                        $html .= '<li class="viewStagesLink" title="Click to view more details." data-project-id="'.$project->id.'" style="background-color: '.$color.'; width: '.$width.'px; left: '.$left.'px;"> </li>';
                    }
                }
            }
            $html .= '</ul></a>';
        }

        return $html;
    }

    public function generateWeekDiv($weekends){
        $data = "";

        $year = Carbon::now()->setTimezone(Auth::user()->company->timezone);

        if(isset($_GET['year'])){
            $checkYear = date_parse($_GET['year']);

            if($checkYear['error_count']==0){
                $year = Carbon::parse($_GET['year'].'-01-01');
            }
        }

        $begin = new \DateTime( $year->subYear(1)->format('Y').'-01-01' );
        $end = new \DateTime( $year->addYear(3)->format('Y').'-01-01' );

        $end = $end->modify( '+1 day' );

        $interval = new \DateInterval('P1D');
        $daterange = new \DatePeriod($begin, $interval ,$end);
        $dateRangeArray = [];

        foreach($daterange as $date)
            array_push($dateRangeArray, $date);

        $end = null;
        $start = null;
        $lastDate = null;
        $weekResetCounter = 0;

        $data = [];
        $dateDivsIndex = 0;
        $currentMonth = '01';
        $mon = 1;

        foreach($dateRangeArray as $index=>$day){

            if($currentMonth!=$day->format('m'))   {
                $dateDivsIndex = 0;
                $weekResetCounter = 0;
                $mon++;
            }

            if(isset($dateRangeArray[$index+1])) {

                if ($weekResetCounter > 6) {
                    $dateDivsIndex++;
                    $weekResetCounter = 0;
                }

                $data['width'][$day->format('m-Y')][$dateDivsIndex][] = $day->format('d-m-Y');
                $data['left'][$day->format('m-Y')][$dateDivsIndex][] = $day->format('d');

                $weekResetCounter++;
            }

            $currentMonth = $day->format('m');
        }

        return $data;
    }

    public function generateWeekDivOnce($year){
        $data = "";
        $year = Carbon::parse('01-01-'.$year);

        $begin = new \DateTime( $year->format('Y').'-01-01' );
        $end = new \DateTime( $year->addYear()->format('Y').'-01-01' );

        $end = $end->modify( '+1 day' );

        $interval = new \DateInterval('P1D');
        $daterange = new \DatePeriod($begin, $interval ,$end);
        $dateRangeArray = [];

        foreach($daterange as $date)
            array_push($dateRangeArray, $date);

        $end = null;
        $start = null;
        $lastDate = null;
        $weekResetCounter = 0;

        $data = [];
        $dateDivsIndex = 0;
        $currentMonth = '01';
        $mon = 1;

        foreach($dateRangeArray as $index=>$day){

            if($currentMonth!=$day->format('m'))   {
                $dateDivsIndex = 0;
                $weekResetCounter = 0;
                $mon++;
            }

            if(isset($dateRangeArray[$index+1])) {

                if ($weekResetCounter > 6) {
                    $dateDivsIndex++;
                    $weekResetCounter = 0;
                }

                $data['width'][$day->format('m-Y')][$dateDivsIndex][] = $day->format('d-m-Y');
                $data['left'][$day->format('m-Y')][$dateDivsIndex][] = $day->format('d');

                $weekResetCounter++;
            }

            $currentMonth = $day->format('m');
        }

        return $data;
    }

    public function getColorPalette(){
        $colors = $this->getColors();
        return response()->json($colors);
    }
}