<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    public function currentUser(){
        return Auth::user();
    }

    public function currentCompanyId(){
        if(Auth::user()){
            return isset(Auth::user()->company_id) ? Auth::user()->company_id : null;
        }

        return null;
    }

    public function currentCompany(){

        if(Auth::user()){
            return Auth::user()->company ? Auth::user()->company : null;
        }

        return null;
    }

    public function getCompany($company_id){

        $company = Company::where('id',$company_id)->first();

        return $company;
    }
}
