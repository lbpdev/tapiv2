<?php

namespace App\Http\Controllers;

use App\Models\Leave;
use App\Repositories\Eloquent\CanCreateResponseCode;
use Illuminate\Http\Request;

use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class LeavesController extends Controller
{
    use CanCreateResponseCode;
    
    public function __construct(Leave $leave) {
        $this->model = $leave;
    }

    public function store(Request $request) {

        $input = $request->only('user_id','type');

        $data['user_id'] = $request->input('user_id');
        $data['type'] = $request->input('type');
        $data['from'] = strtotime($request->input('start'));
        $data['to'] = strtotime($request->input('end'));
        $data['time'] = $request->input('time') * 60;

        if($data['time']>Auth::user()->company->workHours*60)
            return response()->json($this->generateResponse('error'));

        $daterange = $this->getDateRange($request->input('start'),$request->input('end'));

        $returnData = [];
        foreach($daterange as $date){
            $returnData['dates'][] = strtotime($date->format("Y-m-d"));
            $returnData['time'][] = $data['time'];
            $returnData['type'][] = $data['type'];
        }

        Leave::create($data);

        $returnData['leaves'] = $this->getByUserId($request->input('user_id'));

        return response()->json($this->generateResponseWithData('create-success',$returnData));
    }

    public function delete(Request $request) {

        $leaveDates = $this->model->where('id',strip_tags($request->input('id')))->first();
        $user_id = $leaveDates->user_id;

        if($leaveDates)
            if($leaveDates->delete()) {
                $data = [];

                $data['open'][] = [];
                $data['closed'][] = [];

                $daterange = $this->getDateRange($leaveDates->from,$leaveDates->to);

                foreach($daterange as $date){
                    $leaves = Leave::where('from','<=',$date->format('Y-m-d'))->
                    where('to','>=',$date->format('Y-m-d'))->
                    where('user_id',$user_id)->
                    get();

                    if(count($leaves)<1){
                        $data['open'][] = strtotime($date->format("Y-m-d"));
                    }
                    else{
                        $data['closed'][$date->format("Y-m-d")]['date'] = strtotime($date->format("Y-m-d"));
                        $data['closed'][$date->format("Y-m-d")]['type'] = 'leave';

                        foreach($leaves as $leave){
                            if($leave->holiday_id > 0)
                                $data['closed'][$date->format("Y-m-d")]['type'] = 'holiday';
                        }
                    }

                }

                return response()->json($this->generateResponseWithData('delete-success',$data));
            }

        return response()->json($this->generateResponse('record-not-exists'));
    }

    public function get() {
        $data = $this->model->where('user_id',strip_tags($_GET['id']))->get()->toArray();
        return response()->json($data);
    }

    public function getByUserId($user_id) {
        $data = $this->model->where('user_id',$user_id)->get()->toArray();
        return $data ? json_encode($data) : null;
    }

    public function getDateRange($start,$end) {

        $begin = new \DateTime($start);
        $end = new \DateTime($end);

        $end = $end->modify( '+1 day' );
        $interval = new \DateInterval('P1D');
        $daterange = new \DatePeriod($begin, $interval ,$end);

        return $daterange;

    }


    /*
    * GET ALL ROLES AND RETURN AS ARRAY
    */

    private function getAll(){
        return $this->model->get()->toArray();
    }

}
