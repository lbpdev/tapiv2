<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Format;
use App\Models\Key;
use App\Models\Task;
use App\Models\TaskChanges;
use App\Models\User;
use App\Models\UserLog;
use App\Models\UserTask;
use App\Repositories\Eloquent\CanCreateResponseCode;
use App\Services\PushNotificationService;
use Carbon\Carbon;
use Davibennun\LaravelPushNotification\Facades\PushNotification;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class TasksController extends Controller
{

    use CanCreateResponseCode;

    public function __construct(Task $task,UserTask $userTask, PushNotificationService $push){
        $this->model = $task;
        $this->userTask = $userTask;
        $this->push = $push;
    }

    public function get()
    {
        $task = Task::with('department','project.client','project.brand','project.languages','project.format','key')->where('id',strip_tags($_GET['id']))->first();

        if(!$task)
            return response()->json($this->generateResponse('error'));

        $task->langs = $task->project->languagesString;
        $task->formatName = Format::where('id',$task->project->format_id)->first()->name;

        return response()->json($task);
    }

    public function store(Request $request)
    {
        $input = $request->except('project');
        $is_merged = false;

        $input['date'] = strtotime($input['date']);
        $input['is_extra'] = 0;
        $input['time'] = (intval($input['hours']) * 60 ) + $input['minutes'];

        $timeRequired = (intval($input['hours'])*60) + intval($input['minutes']);

        if($timeRequired<1)
            return response()->json($this->generateResponse('create-no-time-assigned'));

        if(!$input['project_id'])
            return response()->json($this->generateResponse('invalid-project-selected'));

        if(!isset($input['key_id']) && !$input['new_key_name'])
            return response()->json($this->generateResponse('no-key-selected'));

        if($input['department_id']=='general')
            $input['department_id'] = null;

        $task = Task::where('project_id',$input['project_id'])->
                              where('key_id',isset($input['key_id']) ? $input['key_id'] : null)->
                              where('department_id',$input['department_id'])->
                              where('note',$input['note'])->
                              where('is_extra',$input['is_extra'])->
                              first();

        if($input['new_key_name']){
            $keyExists = Key::where('name',$input['new_key_name'])->where('company_id',Auth::user()->company->id)->where('department_id',$input['department_id'])->first();

            if(count($keyExists)){
                $input['key_id'] = $keyExists->id;
            } else {
                $department = Department::where('id',$input['department_id'])->first();

                if($department){
                    $new_key = $department->keys()->create([
                            'name'=>$input['new_key_name'],
                            'company_id'=>$department->company_id]
                    );
                    $input['key_id'] = $new_key->id;
                }
            }
        }

        if($task){
            $task->time += $input['time'];
            $task->save();

            $is_merged = true;
        }
        else
            $task = $this->model->create($input);

        $task->load('project.client','key');

        if($task){

            if($is_merged)
                $data = $task;
            else
                $data = $this->generateTaskItem($task->toArray());

            return response()->json($this->generateResponseWithData('create-success',$data));
        }

        return response()->json($this->generateResponse('error'));
    }

    public function storeExtra(Request $request)
    {
        $input = $request->except('project');

        $input['date'] = strtotime($input['date']);
        $input['time'] = (intval($input['hours']) * 60 ) + $input['minutes'];

        if($input['department_id']=='general')
            $input['department_id'] = null;

        $task = $this->model->create($input);
        $task->load('project.client','key');

        if($task){
            $data = $this->generateTaskItem($task->toArray());
            return response()->json($this->generateResponseWithData('create-success',$data));
        }

        return response()->json($this->generateResponse('error'));
    }


    public function update(Request $request)
    {
        $input = $request->except('project','task_id');

        $input['date'] = strtotime($input['date']);
        $input['is_extra'] = 0;
        $input['time'] = (intval($input['hours']) * 60 ) + $input['minutes'];

        $timeRequired = (intval($input['hours'])*60) + intval($input['minutes']);
        if($timeRequired<1)
            return response()->json($this->generateResponse('create-no-time-assigned'));

        if($input['department_id']=='general')
            $input['department_id'] = null;

        $task = Task::where('id',$request->input('task_id'))->first();

        if(!$input['project_id'])
            return response()->json($this->generateResponse('invalid-project-selected'));

        if($input['new_key_name']){
            $keyExists = Key::where('name',$input['new_key_name'])->where('company_id',Auth::user()->company->id)->first();

            if(count($keyExists)){
                $input['key_id'] = $keyExists->id;
            } else {
                $department = Department::where('id',$input['department_id'])->first();

                if($department){
                    $new_key = $department->keys()->create(['name'=>$input['new_key_name'],'company_id'=>$department->company_id]);
                    $input['key_id'] = $new_key->id;
                }
            }
        }

        if($task){
            $task->update($input);

            $identicalTask = Task::where('project_id',$input['project_id'])->
            where('id','!=',$task->id)->
            where('key_id',$input['key_id'])->
            where('department_id',$input['department_id'])->
            where('is_extra',$input['is_extra'])->
            get();

            $duplicates = [];

            if(count($identicalTask)){

                foreach ($identicalTask as $itask){

                    // MOVE TASK TIMES
                    $task->time += (int)$itask->time;
                    $duplicates[] = $itask->id;


                    // MERGE DUPLICATE USER TASKS
                    $usertasks = UserTask::where('task_id',$itask->id)->get();

                    foreach ($usertasks as $utask){
                        $utask->task_id = $task->id;
                        $utask->save();
                    }
                }

                $task->save();
                $itask->delete();
            }

            $data = $task->load('key');

            if($duplicates)
                $data['merged'] = $duplicates;

            return response()->json($this->generateResponseWithData('update-success',$data));
        }

        return response()->json($this->generateResponse('error'));
    }

    public function assign(Request $request)
    {
        $input = $request->input();
        $input['is_urgent'] = isset($input['is_urgent']) ? 1 : 0 ;
        $date_completed = null;

        $timeRequired = (intval($input['hours'])*60) + intval($input['minutes']);

        if($timeRequired<1)
            return response()->json($this->generateResponse('no-time-assigned'));

        $department = Department::where('id',$input['department_id'])->first();
        $workHours = $department->work_hours ? $department->work_hours : $this->currentCompany()->workHours;

        $user = User::find($input['user_id']);

        $partial_leave = $user->leaves()->where('time','>',0)
            ->whereDate('from','<=',Carbon::parse($input['date_assigned'])->format('Y-m-d'))
            ->whereDate('to','>=',Carbon::parse($input['date_assigned'])->format('Y-m-d'))
            ->orderBy('created_at','DESC')->first();

        $timeRequiredWithLeave = $timeRequired;

        if($partial_leave){
            $partial_leave->time;
            $timeRequiredWithLeave += $partial_leave->time;
        }

        if($timeRequiredWithLeave > ($workHours)*60)
            return response()->json($this->generateResponseWithParameter('work-hour-exceeded-dept',$workHours));

        $existingTask = UserTask::where('user_id',$input['user_id'])->
                                  where('task_id',$input['task_id'])->
                                  where('date_assigned',Carbon::parse($input['date_assigned'])->format('Y-m-d'))->
                                  first();

        if($request->input('user_task_id')){
            $userTask = UserTask::with('task')->where('id',$request->input('user_task_id'))->first();

            if($userTask){
                $task = Task::where('id',$userTask->task->id)->first();
                $task->time += $userTask->time;
                $task->save();

                $date_completed = $userTask->date_completed;

                $userTask->delete();
            }
        }

        if($existingTask) {
            $task = Task::where('id',$existingTask->task->id)->first();
            $task->time += $existingTask->time;
            $task->save();
        }

        $task = $this->model->where('id',$input['task_id'])->first();

        $timeRemaining = $task->time;

        if($timeRequired > $timeRemaining)
            return response()->json($this->generateResponse('not-enough-time'));

        $input['time'] = $timeRequired;
        $input['date_completed'] = $date_completed == null ? null : strtotime($date_completed);
        $input['date_assigned'] = strtotime($input['date_assigned']);

        if($existingTask){

            $existingTask->time = $timeRequired;
            $existingTask->note = $input['note'];
            $existingTask->is_urgent = $input['is_urgent'];
            $existingTask->save();

            $task->time -= $timeRequired;
            $task->save();

            $data['element'] = $this->generateDropItem($existingTask->id);
            $data['user_task_id'] = $existingTask->id;
            $data['merged_user_task'] = $existingTask;

            $this->updateOrder($existingTask);

            if($existingTask->date_assigned->format('Y-m-d') == Carbon::now()->setTimezone(Auth::user()->company->timezone)->format('Y-m-d')){
                $user = User::where('id',$existingTask->user_id)->first();

                if($user){

                    TaskChanges::create([
                        'user_id' => $user->id,
                        'type'    => 'added'
                    ]);

//                    $message = PushNotification::Message('Hello. A task has been added to your schedule today.',array(
//                        'intent' => 'task-added'
//                    ));
//
//                    $this->push->send($user,$message);
                }
            }

            return response()->json($this->generateResponseWithData('create-success',$data));
        }
        else {
            $newTask = $this->userTask->create($input);

            if($newTask){
                $task->time -= $timeRequired;
                $task->save();

                $data['element'] = $this->generateDropItem($newTask->id);
                $data['user_task_id'] = $newTask->id;

                $this->updateOrder($newTask);

                if($newTask->date_assigned->format('Y-m-d') == Carbon::now()->setTimezone(Auth::user()->company->timezone)->format('Y-m-d')){
                    $user = User::where('id',$newTask->user_id)->first();

                    if($user){
                        if($user->id != Auth::user()->id){

                            TaskChanges::create([
                                'user_id' => $user->id,
                                'type'    => 'added'
                            ]);

//                            $message = PushNotification::Message('A task has been added to your schedule today.',array(
//                                'custom' => array('intent' => 'task-added')
//                            ));
//
//                            $this->push->send($user,$message);
                        }
                    }
                }
                return response()->json($this->generateResponseWithData('create-success',$data));
            }
        }


        return response()->json($this->generateResponse('error'));
    }


    public function updateOrder($userTask){

        $tasks = UserTask::where('id','!=',$userTask->id)->where('user_id',$userTask->user_id)->where('date_assigned',$userTask->date_assigned)->orderBy('order','ASC')->get();
        $appended = false;

        foreach ($tasks as $index=>$task){

            if($appended){
                $task->order += 1;
                $task->save();
            }

            if($task->order==$userTask->order){
                $task->order += 1;
                $task->save();
                $appended = true;
            }
        }

        return true;
    }


    public function delete(){

        if(strip_tags($_GET['id'])){
            $task = Task::where('id',$_GET['id'])->first();

            if($task){
                $task->time = 0;
                $task->save();
            }

            $data['id'] = $_GET['id'];
            return response()->json($this->generateResponseWithData('delete-success',$data));
        }

        return response()->json($this->generateResponse('error'));
    }


    public function getTaskItem(){

        $task = Task::with('project.client','key','department')->where('id',$_GET['id'])->first()->toArray();

        $item = $this->generateTaskItem($task);

        return $item;

    }


    public function generateTaskItem($task){

        if($task['time']<=0)
            return null;

        $task['hours'] = intval($task['time']/60);
        $task['minutes']  = $task['time']%60;

        $item  = '<li class="ui-state-default clearfix"  data-name="'. $task['project']['name'] .'" data-deadline="'. $task['project']['end_at'] .'"  id="task-'. $task['id'] .'" data-color="'. $task['project']['client']['color'] .'" data-task-id="'. $task['id'] .'">';
        $item  .= '<div class="color-bg" style="background-color: '. $task['project']['client']['color'] .' !important;"></div>';
        $item  .= '<div class="details">';
        $item  .= '<span class="key">'. $task['project']['client']['name'] .'</span>';
        $item  .= '<span class="title">'. $task['project']['name'] .'</span>';
        $item  .= '<span class="title">'. $task['key']['name'] .'</span>';
        $item  .= '</div>';
        $item  .= '<div class="action" style="border-top: 1px solid '. $task['project']['client']['color'] .';">';
        $item  .= '<span class="time">'. $task['hours'] .'hr '. $task['minutes'] .'mins</span>';
        $item  .= '<a data-task-id="'. $task['id'] .'" class="delete" href="#"><img src="'. asset('public/images/icons/Delete-Icon-X.png') .'" height="12" title="remove"></a>';
        $item  .= '<a href="#" class="edit" data-task-id="'. $task['id'] .'"><img src="'. asset('public/images/icons/Edit-Icon-Gray.png') .'" height="12" class="mCS_img_loaded"></a>';
        $item  .= '<img src="'. asset('public/images/icons/Star-Icon-Gray'. ( $task['is_urgent'] ? '-Important' : '' ) .'.png') .'" height="12" class="mCS_img_loaded">';
        $item  .= '</div>';
        $item  .= '</li>';

        return $item;
    }

    public function generateDropItem($userTask_id){

        $userTask = UserTask::with('task.project.client','task.key')->where('id',$userTask_id)->first();

        $userTask['hours'] = intval($userTask->time/60);
        $userTask['minutes']  = $userTask->time%60;


        $item  = '<li id="'.$userTask_id.'" class="closed ui-state-default clearfix ui-sortable-handle" data-user-task-id="'. $userTask->id .'" data-task-id="'. $userTask->task->id .'" style="background-color:#fff !important;">';
        $item  .= '<div class="color-bg" style="background-color: '. $userTask->task->project->client->color .' !important;"></div>';
        $item  .= '<div class="">';

        if($userTask->date_completed){
                $item  .= '<a data-color="'. $userTask->task->project->client->color  .'" data-id="'. $userTask->id .'" data-time="'. $userTask->time .'" class="user-task-icon complete" href="#">';
                $item  .= '<div class="icon" style="border:0; background-color:'. $userTask->task->project->client->color .';"></div>';
                $item  .= '<div class="title">'.$userTask->task->project->client->name . '</div>';
                $item  .= '</a>';
        }
        else {
                $item  .= '<a data-color="'. $userTask->task->project->client->color .'" data-id="'. $userTask->id .'" data-time="'. $userTask->time .'" class="user-task-icon"  href="#">';
                $item  .= '<div class="icon" style="border-color:'. $userTask->task->project->client->color .';"></div>';
                $item  .= '<div class="title">'.$userTask->task->project->client->name . '</div>';
                $item  .= '</a>';
        }

        $item  .= '<div class="details">';
        $item  .= '<span class="edit-utask">'. $userTask->task->project->name .'</span>';
        $item  .= '<span class="edit-utask">'.$userTask->task->key->name . '</span>';
        $item  .= '</div></div>';
        $item  .= '<div class="details notes"><span class="" >'.$userTask->note.'</span></div>';
        $item  .= '<div class="details" style="border-top: 1px solid '. $userTask->task->project->client->color .';">';
        $item .= '<span class="time">';
        $item .= $userTask['hours']  ? $userTask['hours'].'hr ' : '';
        $item .=  $userTask['minutes'] > 0 ? $userTask['minutes'].'min' : '';
        $item .= '</span>';

        $item  .= '<div class="details actions">';
            $item  .= '<span class="icons pull-right text-right" style="border: 0; padding:0">';
            $item  .= '<img src="'. asset('public/images/icons/Star-Icon-Gray'. ( $userTask->task->is_urgent ? '-Important' : '' ).'.png' ) .'" height="12" class="urgent_icon">';
            $item  .= '<a href="#" class="edit edit-utask" data-utask-id="'.$userTask->id.'"><img src="'.asset("public/images/icons/Edit-Icon-Gray.png").'" height="12"></a>';
            $item  .= '<a data-utask-id="'. $userTask->id .'" class="delete-utask" href="#"><img src="'. asset('public/images/icons/Delete-Icon-X.png') .'" height="12" title="Remove"></a>';
            $item  .= '</span>';
        $item  .= '</div>';

        $item  .= '</div>';
        $item  .= '</li>';

//
//
//        <!--  OLD -->
//        $item  = '<li id="'.$userTask_id.'" class="closed ui-state-default clearfix ui-sortable-handle" data-user-task-id="'. $userTask->id .'" data-task-id="'. $userTask->task->id .'" style="background-color:#fff !important;">';
//        $item  .= '<div class="">';
//
//        if($userTask->date_completed){
//            $item  .= '<a datacolor="'. $userTask->task->project->client->color .'" data-id="'. $userTask->id .'" data-time="'. $userTask->time .'" class="user-task-icon complete" href="#">';
//
//            $item .= '<div class="icon" style="border:0; background-color: '. $userTask->task->project->client->color .';color: #fff">';
//
//            $item .= $userTask->task->key->code;
//            $item .= '</div>';
//            $item .= '</a>';
//        }
//        else {
//            $item .= '<a data-color="'. $userTask->task->project->client->color .'" data-id="'. $userTask->id .'" data-time="'. $userTask->time .'" class="user-task-icon"  href="#">';
//            $item .= '<div class="icon" style="border-color: '. $userTask->task->project->client->color .';color: '. $userTask->task->project->client->color .'">';
//            $item .= $userTask->task->key->code;
//            $item .= '</div>';
//            $item .= '</a>';
//        }
//        $item .= '<div class="details" style="color: '. $userTask->task->project->client->color .'">';
//        $item .= '<span class="title edit-utask">'. $userTask->task->project->name .'</span>';
//        $item .= '<span class="collapse-icon"></span>';
//        $item .= '</div>';
//        $item .= '</div>';
//        $item .= '<div class="">';
//        $item .= '<div style="width:33px">';
//        $item .= '</div>';
//        $item .= '<div class="details" style="color: '. $userTask->task->project->client->color .'">';
//        $item .= '<span class="time">';
//        $item .= $userTask['hours']  ? $userTask['hours'].'hr ' : '';
//        $item .=  $userTask['minutes'] > 0 ? $userTask['minutes'].'min' : '';
//        $item .= '</span>';
//        $item .= '</div>';
//        $item .= '</div>';
//        $item .= '<div class="details notes">';
//        $item .= '<span style="color:'.$userTask->task->project->client->color.'">'. $userTask->note .'</span>';
//        $item .= '</div>';
//        $item .= '<div class="details actions">';
//        $item .= '<span class="icons pull-right" style="border: 0; padding:0">';
//
//        $item .= '<img src="'. asset('public/images/icons/task-icons/Important'.( $userTask->is_urgent ? '-active' : '' ).'.png' ) .'" class="urgent_icon" height="22">';
//
//        $item .= '<a target="_blank" href="https://podio.com/login?force_locale=en_US"><img src="'. asset('public/images/icons/task-icons/Podio-Icon.png') .'" height="22"></a>';
//        $item .= '<a data-utask-id="'. $userTask->id .'" class="delete-utask" href="#"><img src="'. asset('public/images/icons/task-icons/Forward-Icon.png') .'" height="22"></a>';
//        $item .= '</span>';
//        $item .= '</div>';
//        $item .= '</li>';

        return $item;

    }
}
