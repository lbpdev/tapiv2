<?php

namespace App\Http\Controllers;

use App\Models\Color;
use App\Models\Department;
use App\Models\Format;
use App\Models\Leave;
use App\Models\Project;
use App\Models\User;
use App\Models\UserTask;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use App\Services\ReportService;

class ReportsController extends Controller
{
    use ReportService;
    
    public function __construct(){
        if($this->currentCompany()->firstDay=='Sun')
            Carbon::setWeekStartsAt(Carbon::SUNDAY);
    }

}
