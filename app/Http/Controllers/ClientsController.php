<?php

namespace App\Http\Controllers;

use App\Models\Key;
use App\Models\Project;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Client;

use App\Repositories\Eloquent\CanCreateResponseCode;
use App\Repositories\Eloquent\CannotAcceptWhiteSpace;
use Illuminate\Support\Facades\Auth;

class ClientsController extends Controller
{
    use CanCreateResponseCode, CannotAcceptWhiteSpace;

    public function __construct(Client $clients){
        $this->model = $clients;
    }

    /*
    * GET RECORD IF EXISTS
    */

    public function get(){

        $client = $this->model->where('id',$_GET['id'])->first();

        if(!$client)
            return response()->json($this->generateResponse('client-not-exists'));

        $data['projectsCount'] = 0;
        $data['tasksCount'] = 0;

        $projects = Project::fromCurrentCompany()->where('client_id',$client->id)->get();

        foreach ($projects as $project){
            foreach ($project->tasks as $task){
                    $data['tasksCount'] += count($task->utasks);
            }
        }

        $data['projectsCount'] = count($projects);
        $data['client'] = $client;

        return response()->json($this->generateResponseWithData('create-success',$data));
    }

    /*
    * CHECK IF RECORD EXISTS AND STORE
    */

    public function store(Request $request){
        $input = $request->input();
        $company_id = $this->currentCompanyId();

        if(isset($input['company_id']))
            $company_id = $input['company_id'];

        if(!$this->checkWhiteSpaces($input['name']))
            return response()->json($this->generateResponse('white-spaces'));

        if($this->model->fromCompany($company_id)->where('name',$input['name'])->count())
            return response()->json($this->generateResponse('client-exists'));

        if($input['name']){
            $input['name'] = strip_tags(trim($input['name']));
            $this->model->create(array(
                'name'=>$input['name'],
                'color'=>$input['color'],
                'company_id'=> $company_id
            ));
        }

        $departments = $this->getAll($company_id);

        return response()->json($this->generateResponseWithData('create-client-success',$departments));
    }

    /*
    * UPDATE EXISTING AND RECORD
    */

    public function update(Request $request){
        $input = $request->input();
        $company_id = $this->currentCompanyId();

        if(isset($input['company_id']))
            $company_id = $input['company_id'];

        $stage = $this->model->where('id',$input['id'])->first();

        if(!$stage)
            return response()->json($this->generateResponse('client-not-exist'));

        if(!$this->checkWhiteSpaces($input['name']))
            return response()->json($this->generateResponse('white-spaces'));

        if($input['name']){
            if(strtolower($input['name'])==strtolower($stage->name))
                $stage->update(array('color'=>$input['color'],'name'=>$input['name']));

            elseif($this->model->fromCompany($company_id)->where('name',$input['name'])->first())
                return response()->json($this->generateResponse('client-exists'));

            else {
                $input['name'] = strip_tags(trim($input['name']));
                $stage->update(array(
                    'name'=>$input['name'],
                    'color'=>$input['color']
                ));
            }
        }

        $stages = $this->getAll($company_id);

        return response()->json($this->generateResponseWithData('update-client-success',$stages));
    }

    /*
    * DELETE A RECORD THROUGH ID
    */
    public function delete(){

        $data['clients'] = $this->model->where('id',strip_tags($_GET['client_id']))->first();
        $data['deleted'] = $_GET['client_id'];

        if($data){

            if($data['clients']->company_id != Auth::user()->company->id)
                return response()->json($this->generateResponse('record-not-exists'));

            if($data['clients']->delete()) {
                Project::where('client_id',$_GET['client_id'])->delete();
                $data['clients'] = $this->getAll();
                return response()->json($this->generateResponseWithData('delete-client-success',$data));
            }
        }

        return response()->json($this->generateResponse('record-not-exists'));
    }

    /*
    * GET ALL RECORDS AND RETURN AS ARRAY
    */

    private function getAll($company_id = null){

        if(!$company_id)
            return $this->model->fromCurrentCompany()->get()->toArray();

        return $this->model->fromCompany($company_id)->get()->toArray();
    }

    public function getAvailableClientsJson(){
        $clientsAll = Client::where('company_id',$this->currentCompanyId())->orderBy('name','ASC')->get();
        $clients = [];

        foreach($clientsAll as $index=>$client){
            $clients[$index]['label'] = $client->name;
            $clients[$index]['value'] = $client->color;
        }

        return json_encode($clients);
    }

}
