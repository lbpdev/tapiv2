<?php

namespace App\Http\Controllers;

use App\Models\Key;
use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Department;

use App\Repositories\Eloquent\CanCreateResponseCode;
use App\Repositories\Eloquent\CannotAcceptWhiteSpace;
use Illuminate\Support\Facades\Auth;

class DepartmentsController extends Controller
{
    use CanCreateResponseCode, CannotAcceptWhiteSpace;

    public function __construct(Department $department){
        $this->model = $department;
    }


    /*
    * GET RECORD IF EXISTS
    */

    public function get(){

        $department = $this->model->where('id',$_GET['id'])->first();

        if(!$department)
            return response()->json($this->generateResponse('department-not-exists'));

        $data['usersCount'] = 0;
        $data['typeCounts'] = 0;

        $users = User::whereHas('department',function($query) use($department){
            $query->where('department_id',$department->id);
        })->get();

        $data['usersCount'] = count($users);

        $types = Key::where('department_id',$department->id)->get();
        $data['typeCounts'] = count($types);

        $data['department'] = $department;

        return response()->json($this->generateResponseWithData('create-department-success',$data));
    }

    /*
    * CHECK IF RECORD EXISTS AND STORE
    */

    public function store(Request $request){
        $input = $request->input();
        $company_id = $this->currentCompanyId();

        if(isset($input['company_id']))
            $company_id = $input['company_id'];

        if(!$this->checkWhiteSpaces($input['name']))
            return response()->json($this->generateResponse('white-spaces'));

        if($this->model->fromCompany($company_id)->where('name',$input['name'])->count())
            return response()->json($this->generateResponse('department-exists'));

        if($input['name']){
            $new = $this->model->create(array(
                'name'=>strip_tags(trim($input['name'])),
                'color'=>$input['color'],
                'work_hours'=>Auth::user()->company->workHours,
                'company_id'=>$company_id
            ));

            $data['new_department'] = $new;
        }

        $data['departments'] = $this->getAll($company_id);

        return response()->json($this->generateResponseWithData('create-department-success',$data));
    }

    /*
    * UPDATE EXISTING AND RECORD
    */

    public function update(Request $request){
        $input = $request->input();
        $company_id = $this->currentCompanyId();

        if(isset($input['company_id']))
            $company_id = $input['company_id'];

        $department = $this->model->where('id',$input['id'])->first();

        if(!$department)
            return response()->json($this->generateResponse('department-not-exist'));

        if(!$this->checkWhiteSpaces($input['name']))
            return response()->json($this->generateResponse('white-spaces'));

        if($input['name']){

            if(strtolower($input['name'])==strtolower($department->name)){
                $department->update(array('color'=>$input['color'],'work_hours'=>$input['work_hours']));
            }

            elseif($this->model->fromCompany($company_id)->where('name',$input['name'])->first())
                return response()->json($this->generateResponse('department-exists'));

            else {
                $department->update(array(
                    'name'=>strip_tags(trim($input['name'])),
                    'work_hours'=>$input['work_hours'],
                    'color'=>$input['color']
                ));

            }
            $data['new_department'] = $department;
        }

        $data['departments'] = $this->getAll($company_id);

        return response()->json($this->generateResponseWithData('update-department-success',$data));
    }

    /*
    * DELETE A RECORD THROUGH ID
    */
    public function delete(){

        $data['departments'] = $this->model->where('id',strip_tags($_GET['department_id']))->first();
        $data['deleted'] = $_GET['department_id'];


        if($data)
            if($data['departments']) {

                $company_id = $data['departments']->company_id;

                $hasAdmins = $data['departments']->users()->whereHas('role',function($query){
                    $query->where('role_id',1);
                })->count();

                if($hasAdmins)
                    return response()->json($this->generateResponse('department-has-admins'));

                if($data['departments']->delete()) {

                    Key::where('department_id',$_GET['department_id'])->delete();

                    $users = User::whereHas('department', function ($query) {
                        $query->where('department_id', $_GET['department_id']);
                    })->whereHas('role', function ($query) {
                        $query->where('role_id', 3);
                    })->get();

                    foreach($users as $user){
                        $user->is_active = 0;
                        $user->save();
                    }

                    $data['departments'] = $this->getAll($company_id);

                    $departments = Department::fromCurrentCompany()->with('users.role','users.logs')->orderBy('name','ASC')->get();
                    $data['empData'][0] = $departments->toArray();

                    foreach($departments as $depIndex=>$department)
                        foreach ($department->users as $userIndex=>$user)
                            $data['empData'][0][$depIndex]['users'][$userIndex]['last_login'] = $user->lastLogin ? $user->lastLogin->created_at->format('M d Y') : '';

                    $data['empData'][1][0]['users'] = User::with('role')->whereDoesntHave('department')->where('company_id',Auth::user()->company->id)->get();

                    return response()->json($this->generateResponseWithData('delete-department-success',$data));
                }
            }

        return response()->json($this->generateResponse('record-not-exists'));
    }

    /*
    * GET ALL KEYS AND RETURN AS ARRAY
    */

    public function getKeys($company_id = null){

        if(!$company_id)
            $company_id = $this->currentCompanyId();

        if(strip_tags($_GET['id'])=='general')
            $departments = Key::where('department_id',null)->fromCompany($company_id)->get();
        else
            $departments = Key::where('department_id',strip_tags($_GET['id']))->fromCompany($company_id)->get();

        return response()->json($departments);
    }

    /*
    * GET ALL RECORDS AND RETURN AS ARRAY
    */

    private function getAll($company_id = null){

        if(!$company_id)
            return $this->model->with('users.role')->fromCurrentCompany()->orderBy('name','ASC')->get()->toArray();

        return $this->model->with('users.role')->fromCompany($company_id)->orderBy('name','ASC')->get()->toArray();

    }

}
