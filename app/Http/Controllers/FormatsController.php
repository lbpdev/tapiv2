<?php

namespace App\Http\Controllers;

use App\Models\Format;
use App\Models\Project;
use App\Repositories\Eloquent\CanCreateSlug;
use App\Repositories\Eloquent\CanCreateResponseCode;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Repositories\Eloquent\CannotAcceptWhiteSpace;
use Illuminate\Support\Facades\Auth;

class FormatsController extends Controller
{
    use CanCreateResponseCode,CanCreateSlug,CannotAcceptWhiteSpace;

    public function __construct(Format $format){
        $this->model = $format;
    }


    /*
    * GET RECORD IF EXISTS
    */

    public function get(){

        $format = $this->model->where('id',$_GET['id'])->first();

        if(!$format)
            return response()->json($this->generateResponse('format-not-exists'));

        $data['projectsCount'] = 0;
        $data['tasksCount'] = 0;

        $projects = Project::fromCurrentCompany()->where('format_id',$format->id)->get();

        foreach ($projects as $project){
            foreach ($project->tasks as $task){
                $data['tasksCount'] += count($task->utasks);
            }
        }

        $data['projectsCount'] = count($projects);
        $data['format'] = $format;

        return response()->json($this->generateResponseWithData('create-success',$data));
    }

    /*
    * GET RECORD IF EXISTS
    */

    public function getForSelect(){

        $format = $this->model->where('company_id',$this->currentCompanyId())->get();

        return response()->json($format);
    }

    /*
    * CHECK IF RECORD EXISTS AND STORE
    */

    public function store(Request $request){
        $input = $request->input();
        $company_id = $this->currentCompanyId();

        if(isset($input['company_id']))
            $company_id = $input['company_id'];

        if(!$this->checkWhiteSpaces($input['name']))
            return response()->json($this->generateResponse('white-spaces'));

        if($this->model->fromCompany($company_id)->where('name',$input['name'])->count())
            return response()->json($this->generateResponse('format-exists'));

        if($input['name']){

            $input['name'] = strip_tags(trim($input['name']));

            $this->model->create(array(
                'name'=>$input['name'],
                'slug'=>$this->generateSlug($input['name']),
                'company_id'=> $company_id
            ));
        }

        $departments = $this->getAll($company_id);

        return response()->json($this->generateResponseWithData('create-format-success',$departments));
    }

    /*
    * UPDATE EXISTING AND RECORD
    */

    public function update(Request $request){
        $input = $request->input();
        $company_id = $this->currentCompanyId();

        if(isset($input['company_id']))
            $company_id = $input['company_id'];

        $format = $this->model->where('id',$input['id'])->first();

        if(!$format)
            return response()->json($this->generateResponse('format-not-exist'));

        if(!$this->checkWhiteSpaces($input['name']))
            return response()->json($this->generateResponse('white-spaces'));
        
        if($input['name']){

            if(( strtolower($format->name) != strtolower($input['name']) ) && $this->model->fromCompany($company_id)->where('name',$input['name'])->first())
                return response()->json($this->generateResponse('format-exists'));

            else {
                $input['name'] = strip_tags(trim($input['name']));
                $format->update(array(
                    'name'=>$input['name'],
                    'slug'=>$this->generateSlug($input['name'])
                ));
            }
        }

        $formats = $this->getAll($company_id);

        return response()->json($this->generateResponseWithData('update-format-success',$formats));
    }


    /*
    * DELETE A RECORD THROUGH ID
    */
    public function delete(Request $request){

        $data = Format::where('id',strip_tags($request->input('id')))->first();

        if($data){
            if($data->company_id != Auth::user()->company->id)
                return response()->json($this->generateResponse('record-not-exists'));

            $company_id = $data->company_id;
            if($data->delete()) {
                Project::where('format_id',$request->input('id'))->delete();
                $data = $this->getAll($company_id);
                return response()->json($this->generateResponseWithData('delete-format-success',$data));
            }
        }

        return response()->json($this->generateResponse('record-not-exists'));
    }

    /*
    * GET ALL RECORDS AND RETURN AS ARRAY
    */

    private function getAll($company_id = null){

        if(!$company_id)
            return $this->model->fromCurrentCompany()->get()->toArray();

        return $this->model->fromCompany($company_id)->get()->toArray();
    }

}
